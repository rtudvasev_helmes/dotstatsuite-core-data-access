﻿using System;

namespace DotStat.Domain
{
    public class TransactionLog
    {
        public int Id { get; set; }
        public string UserEmail { get; set; }
        public DateTime? Date { get; set; }
        public string Level { get; set; }
        public string Server { get; set; }
        public string Logger { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
    }
}
