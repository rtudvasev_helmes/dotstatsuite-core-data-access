﻿using System.Collections.Generic;
using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace DotStat.Domain
{
    public class Constraint
    {
        public readonly bool Include;
        public readonly HashSet<string> Codes;
        public readonly ITimeRange TimeRange;

        public Constraint(bool include, IList<string> codes, ITimeRange timeRange)
        {
            Include = include;
            Codes = new HashSet<string>(codes ?? Enumerable.Empty<string>());
            TimeRange = timeRange;
        }

        public bool IsCodeListBased => Codes.Any();
    }
}
