﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace DotStat.Domain
{
    public static class Extensions
    {
        public static Dictionary<string, string> ToDict(this IList<ITextTypeWrapper> names)
        {
            return names.ToDictionary(x => x.Locale, x => x.Value);
        }

        #region String

        public static Guid ToGuid(this string str)
        {
            return Guid.Parse(str);
        }

        public static DateTime? ToDt(this string str)
        {
            DateTime temp;
            return !string.IsNullOrEmpty(str) && str.Trim().Length > 0 && DateTime.TryParse(str, out temp)
                        ? (DateTime?)temp
                        : null;
        }

        public static bool? ToBool(this string str)
        {
            bool temp;
            return !string.IsNullOrEmpty(str) && str.Trim().Length > 0 && bool.TryParse(str, out temp)
                        ? (bool?)temp
                        : null;
        }

        public static int? ToInt(this string str)
        {
            int temp;
            return !string.IsNullOrEmpty(str) && str.Trim().Length > 0 && int.TryParse(str, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out temp)
                        ? (int?)temp
                        : null;
        }

        public static decimal? ToDec(this string str)
        {
            decimal temp;
            return !string.IsNullOrEmpty(str) && str.Trim().Length > 0 && decimal.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out temp)
                        ? (decimal?)temp
                        : null;
        }

        public static double? ToDouble(this string str)
        {
            double temp;
            return !string.IsNullOrEmpty(str) && str.Trim().Length > 0 && double.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out temp)
                        ? (double?)temp
                        : null;
        }

        public static string Limit(this string str, int size)
        {
            if (string.IsNullOrEmpty(str) || str.Length <= size)
                return str;

            return str.Substring(0, size);
        }

        #endregion

        #region Constraint

        /// <summary>
        /// Is dimension member not constrained
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="dimMemberCode"></param>
        /// <returns></returns>
        public static bool IsAllowed(this Dimension dimension, string dimMemberCode)
        {
            return dimension.Constraint.IsAllowed(dimMemberCode);
        }

        /// <summary>
        /// Is attribute member not constrained
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public static bool IsAllowed(this Attribute attribute, string code)
        {
            return attribute.Constraint.IsAllowed(code);
        }

        /// <summary>
        /// Is dimension member not constrained
        /// </summary>
        /// <param name="constraint"></param>
        /// <param name="dimMemberCode"></param>
        /// <returns></returns>
        public static bool IsAllowed(this Constraint constraint, string dimMemberCode)
        {
            return constraint == null 
                   || !constraint.Codes.Any() 
                   || !(constraint.Include ^ constraint.Codes.Contains(dimMemberCode));
        }
        #endregion
    }
}
