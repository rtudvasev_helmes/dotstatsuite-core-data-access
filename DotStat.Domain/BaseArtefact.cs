using System;
using System.Collections.Generic;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace DotStat.Domain
{
    public interface IDotStatIdentifiable
    {
        string DbType { get; }
        int DbId { get; set; }
        string Code { get; }
        string FullId { get; }
    }

    public abstract class IdentifiableDotStatObject<T> : IDotStatIdentifiable where T : IIdentifiableObject 
    {
        public string DbType { get; protected set; }
        public int DbId { get; set; }
        public string Code => Base.Id;
        public T Base { get; }

        public virtual string FullId => $"{Base.MaintainableParent.AgencyId}:{Base.MaintainableParent.Id}({Base.MaintainableParent.Version}).{Base.Id}";

        internal IdentifiableDotStatObject(T @base)
        {
            Base = @base;
        }
    }

    public abstract class NameableDotStatObject<T> : IdentifiableDotStatObject<T> where T : INameableObject
    {
        public readonly Dictionary<string, string> Names;

        internal NameableDotStatObject(T @base) : base(@base)
        {
            Names = @base.Names.ToDict();
        }
    }

    public interface IDotStatMaintainable : IDotStatIdentifiable
    {
        SdmxVersion Version { get; }
        string AgencyId { get; }
    }

    public abstract class MaintainableDotStatObject<T> : NameableDotStatObject<T>, IDotStatMaintainable where T: IMaintainableObject
    {
        public SdmxVersion Version { get; protected set; }

        public string AgencyId => @Base.AgencyId;

        public override string FullId => $"{Base.AgencyId}:{Base.Id}({Base.Version})";

        internal MaintainableDotStatObject(T @base) : base(@base)
        {
            Version = new SdmxVersion(@base.Version);
        }
    }

    public interface IDotStatCodeListBasedIdentifiable : IDotStatIdentifiable
    {
        Codelist Codelist { get; }

        Code FindMember(string key, bool withLabels = true);

        bool HasCode(string key);

        Localization.ResourceId ResourceIdOfCodelistCodeRemoved { get; }
        Localization.ResourceId ResourceIdOfCodelistCodeAdded { get; }
        Localization.ResourceId ResourceIdOfCodelistChanged { get; }
    }

    public interface IConstraintable : IDotStatCodeListBasedIdentifiable
    {
    }

    public abstract class CodeListBasedIdentifiableDotStatObject<T> : IdentifiableDotStatObject<T>, IDotStatCodeListBasedIdentifiable where T : IIdentifiableObject
    {
        public Dsd Dsd { get; set; }

        public abstract Localization.ResourceId ResourceIdOfCodelistCodeRemoved { get; }
        public abstract Localization.ResourceId ResourceIdOfCodelistCodeAdded { get; }
        public abstract Localization.ResourceId ResourceIdOfCodelistChanged { get; }

        public static string GetFullCode(string code)
        {
            return typeof(T).Name + "::" + code;
        }

        public readonly string FullCode;

        public Codelist Codelist { get; protected set; }

        private HashSet<string> _languages;
        private HashSet<string> _codes;
        private Dictionary<string, Code> _labelsCache;

        internal CodeListBasedIdentifiableDotStatObject(T @base, Codelist codelist = null) : base(@base)
        {
            Codelist        = codelist;
            FullCode        = GetFullCode(Code);

            BuildLabelCache(codelist);
        }

        private string GetCacheKey(string language, string label)
        {
            return language + "::" + label;
        }

        private void BuildLabelCache(Codelist codelist)
        {
            if (codelist == null)
                return;

            _codes = new HashSet<string>();
            _languages = new HashSet<string>();
            _labelsCache = new Dictionary<string, Code>(StringComparer.OrdinalIgnoreCase);

            foreach (var member in codelist.Codes)
            {
                _codes.Add(member.Code);
                _labelsCache[member.Code] = member;

                foreach (var pair in member.Names)
                {
                    var key = GetCacheKey(pair.Key, pair.Value);

                    _labelsCache[key] = member;
                    _languages.Add(pair.Key);
                }
            }
        }

        /// <summary>
        /// Tries to find Code by it's code / label in any language
        /// </summary>
        public Code FindMember(string key, bool withLabels = true)
        {
            if(Codelist == null)
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CodelistNotDefined),
                    key));

            Code code = null;

            if (!_labelsCache.TryGetValue(key, out code) && withLabels)
                foreach (var lang in _languages)
                    if(_labelsCache.TryGetValue(GetCacheKey(lang, key), out code))
                        break;

            return code;
        }

        public bool HasCode(string key)
        {
            return !string.IsNullOrEmpty(key) &&_codes.Contains(key);
        }
    }
}
