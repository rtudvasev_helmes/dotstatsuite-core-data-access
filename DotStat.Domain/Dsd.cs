﻿using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Enums;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

namespace DotStat.Domain
{
    public class Dsd : MaintainableDotStatObject<IDataStructureObject>
    {
        public char? LiveVersion { get; set; }
        public char? PITVersion { get; set; }
        public System.DateTime? PITReleaseDate { get; set; }
        public System.DateTime? PITRestorationDate { get; set; }
        public string ObservationValueDataType { get; set; } = "string";
        public int? MaxTextAttributeLength { get; set; }
        public Dimension TimeDimension => Dimensions.FirstOrDefault(dim => dim.Base.TimeDimension);
        public IReadOnlyList<Dimension> Dimensions { get; private set; }
        public IReadOnlyList<Attribute> Attributes { get; private set; }
        public PrimaryMeasure PrimaryMeasure { get; private set; }
        public Msd Msd { get; set; }
        public int? MsdDbId { get; set; }

        public Dsd(IDataStructureObject @base) : base(@base)
        {
            DbType = DbTypes.GetDbType(SDMXArtefactType.Dsd);
            Dimensions = new Dimension[0];
            Attributes = new Attribute[0];
        }

        public void SetDimensions(IEnumerable<Dimension> dimensions)
        {
            Dimensions = dimensions.ToArray();
            foreach (var dim in Dimensions)
            {
                dim.Dsd = this;
            }
        }

        public void SetAttributes(IEnumerable<Attribute> attributes)
        {
            Attributes = attributes.ToArray();
            foreach (var attr in Attributes)
            {
                attr.Dsd = this;
            }

        }

        public void SetPrimaryMeasure(PrimaryMeasure primaryMeasure)
        {
            PrimaryMeasure = primaryMeasure;

            primaryMeasure.Dsd = this;
        }

        public void CopyDataManagementPropertiesFrom(Dsd dsdFrom)
        {
            DbId = dsdFrom.DbId;

            LiveVersion = dsdFrom.LiveVersion;
            PITVersion = dsdFrom.PITVersion;
            PITReleaseDate = dsdFrom.PITReleaseDate;
            PITRestorationDate = dsdFrom.PITRestorationDate;

            ObservationValueDataType = dsdFrom.ObservationValueDataType;

            MaxTextAttributeLength = dsdFrom.MaxTextAttributeLength;

            Msd = dsdFrom.Msd;
            MsdDbId = dsdFrom.MsdDbId;
        }
    }
}
