﻿using System.Collections.Generic;
using System.Linq;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Domain
{
    public class TransferContent
    {
        /// <summary>
        /// Observation values, observation level attributes and optionally 
        /// non-observation attributes reported at observation level
        /// </summary>
        public IAsyncEnumerable<ObservationRow> DataObservations { get; set; }

        /// <summary>
        /// Metadata attributes reported in the IObservation
        /// </summary>
        public IAsyncEnumerable<ObservationRow> MetadataObservations { get; set; }

        /// <summary>
        /// Dataset level attributes
        /// </summary>
        public IList<DataSetAttributeRow> DatasetAttributes { get; set; }

        /// <summary>
        /// Reported components in the data source file
        /// </summary>
        public ReportedComponents ReportedComponents { get; set; }
    }

    public class ReportedComponents
    {
        public List<Dimension> Dimensions = new();
        public Dimension TimeDimension;
        public List<Attribute> DatasetAttributes = new();
        public List<Attribute> SeriesAttributes = new();
        /// <summary>
        /// Observation level attributes including series level attributes referencing time dimension
        /// </summary>
        public List<Attribute> ObservationAttributes = new();
        public List<MetadataAttribute> MetadataAttributes = new();
        public bool IsPrimaryMeasureReported = false;
        public bool IsMetadataReported()=> MetadataAttributes.Any();
        public bool IsDataReported() => IsPrimaryMeasureReported || DatasetAttributes.Any() || SeriesAttributes.Any() || ObservationAttributes.Any();
    }

    public class ObservationRow
    {
        public readonly int DataSetNumber;
        public readonly IObservation Observation;
        public readonly StagingRowActionEnum Action;
        
        public ObservationRow(int dataSetNumber, StagingRowActionEnum action, IObservation observation)
        {
            DataSetNumber = dataSetNumber;
            Action = action;
            Observation = observation;
        }
    }

    public class DataSetAttributeRow
    {
        public readonly int DataSetNumber;
        public IList<IKeyValue> Attributes = new List<IKeyValue>();
        public StagingRowActionEnum Action;

        public DataSetAttributeRow(int dataSetNumber, StagingRowActionEnum action)
        {
            DataSetNumber = dataSetNumber;
            Action = action;
        }
    }

    public enum StagingRowActionEnum
    {
        Skip = 0,
        Merge = 1,
        Delete = 2,
        DeleteAll = 3
    }
}