﻿using System.Collections.Generic;

namespace DotStat.Domain.Cache
{
    public class MemoryCache : ICache
    {
        private readonly Dictionary<string, object> _cache;

        public MemoryCache()
        {
            _cache = new Dictionary<string, object>();
        }

        public bool TryGetValue<T>(string key, out T value)
        {
            if (_cache.TryGetValue(key, out var cachedValue))
            {
                value = (T)cachedValue;
                return true;
            }

            value = default;
            return false;
        }

        public void Set<T>(string key, object obj)
        {
            if (_cache.ContainsKey(key))
            {
                _cache[key] = obj;
            }
            else
            {
                _cache.Add(key, obj);
            }
        }
    }
}
