﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotStat.Domain
{
    public static class SpecialValues
    {
        public static readonly Dictionary<string, KeyValuePair<StagingRowActionEnum, object>> SpecialValueMap =
            new Dictionary<string, KeyValuePair<StagingRowActionEnum, object>>
            {
                { "#N/A", new KeyValuePair<StagingRowActionEnum, object>(StagingRowActionEnum.Delete, null) },
                { "*", new KeyValuePair<StagingRowActionEnum, object>(StagingRowActionEnum.Delete, null) },
                { "0", new KeyValuePair<StagingRowActionEnum, object>(StagingRowActionEnum.Delete, null) }
            };

        public static readonly Dictionary<StagingRowActionEnum, List<string>> Present =
            new Dictionary<StagingRowActionEnum, List<string>>
            {
                { StagingRowActionEnum.Merge, new List<string> { } },
                { StagingRowActionEnum.Delete, new List<string> { } }
            };

        public static readonly Dictionary<StagingRowActionEnum, List<string>> Obmitted =
            new Dictionary<StagingRowActionEnum, List<string>>
            {
                { StagingRowActionEnum.Merge, new List<string> { } },
                { StagingRowActionEnum.Delete, new List<string> { } }
            };

    }
}
