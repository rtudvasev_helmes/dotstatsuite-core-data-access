﻿using System.Collections.Generic;
using DotStat.Common.Enums;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;

namespace DotStat.Domain
{
    public class Msd : MaintainableDotStatObject<IMetadataStructureDefinitionObject>
    {
        public List<MetadataAttribute> MetadataAttributes { get; } = new List<MetadataAttribute>();

        public Msd(IMetadataStructureDefinitionObject @base) : base(@base)
        {
            DbType = DbTypes.GetDbType(SDMXArtefactType.Msd);
            GetAllMetadataAttributes(@base);
        }

        private void GetAllMetadataAttributes(IMetadataStructureDefinitionObject @base)
        {
            foreach (var reportStructure in @base.ReportStructures)
            {
                foreach (var metadataAttributeBase in reportStructure.MetadataAttributes)
                {
                    SetMetadataAttributes(new MetadataAttribute(metadataAttributeBase) { HierarchicalId = metadataAttributeBase.Id});
                }
            }
        }

        private void SetMetadataAttributes(MetadataAttribute metadataAttribute)
        {
            if (!metadataAttribute.Base.Presentational.IsTrue)
            {
                MetadataAttributes.Add( metadataAttribute);
                return;
            }

            foreach (var metadataAttributeBase in metadataAttribute.Base.MetadataAttributes)
            {
                var metaattr = new MetadataAttribute(metadataAttributeBase)
                {
                    HierarchicalId = metadataAttributeBase.IdentifiableParent.Id + "." + metadataAttributeBase.Id
                };
                SetMetadataAttributes(metaattr);
            }
        }
    }
}
