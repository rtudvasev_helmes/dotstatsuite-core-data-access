# HISTORY

## v18.0.3 (DataDB v7.0.0, CommonDB v3.8)
### Description

### Issues
- [#440](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/440) Fixed error of false duplicates reported in advance validation


## v18.0.2 (DataDB v7.0.0, CommonDB v3.8)
### Description

### Issues
- [#286](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/286) TIME_PRE_FORMATED table cleanup fix
- [#289](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/289) References updated to match with NSI WS v8.12.2
- [#435](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/435) Fixed errors with missing referenced dimensions and dataflow attribute management
- [#437](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/437) Fix of index creation applied on staging table in case of large file imports


## v18.0.1 (DataDB v7.0.0, CommonDB v3.8)
### Description

### Issues
- [#430](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/430) Revert the isolation level to READ COMMITTED in connections with no transaction
- [#431](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/431) Fix performance issue with the merge statement from staging to fact table


## v18.0.0 (DataDB v7.0.0, CommonDB v3.8)
### Description

### Issues
- [#91](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/91) Fix multiple contradictory values error at dataset attributes
- [#127](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/127) Introduced data DELETE operation
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/251) References updated to match with NSI WS v8.12.0
- [#281](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/281) References updated to match with NSI WS v8.12.1
- [#285](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/285) Allow views to return higher level attribute values when there are no observation level component rows
- [#397](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/397) Implemented MERGE action for CSV v2 data imports
- [#402](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/402) Update test file for the improved detection of data or metadata content of a CSV v2 file
- [#409](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/409) Fixed transaction id in time-out log message
- [#410](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/410) No exception is thrown at init/allMappingsets method in case of no DSDs or no dataflows in data space
- [#416](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/416) Fix bug in case there are no dataset attribute values to modify
- [#424](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/424) Fix issue when deleting specific years
- [#425](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/425) Fixed import of first dataset level metadata attribute
- [#426](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/426) Updated authentication to support ADFS


## v17.0.4 (DataDB v6.2.1, CommonDB v3.8)
### Description

### Issues
- [#387](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/387) Add optional SMTP HFrom header configuration
- [#90](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/90) Grant view server state command is executed with sysadmin role only, dbup tool version printed to console


## v17.0.3 (DataDB v6.2.1, CommonDB v3.8)
### Description

### Issues
- [#403](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/403) Fix creation of actual-CC


## v17.0.2 (DataDB v6.2.1, CommonDB v3.8)
### Description

### Issues
- [#379](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/379) Add MailSent
- [#382](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/382) Cleanup of orphaned codelists
- [#383](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/383) Fix "already added" in MappingStoreDataAccess, Add missing application name in structdbconnetionString
- [#386](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/386) Avoid re-accessing the same column


## v17.0.1 (DataDB v6.2.1, CommonDB v3.8)
### Description

### Issues
- Disabled resource auto detection of Google logger in DbUp


## v17.0.0 (DataDB v6.2.1, CommonDB v3.8)
### Description

### Issues
- [#83](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/83) Upgraded from .NET Core 3.1 to .NET 6
- [#86](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/86) The method cleanup/dsd deletes the mapping sets and actual content constraint of the dataflows
- [#87](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/87) Added connection close fix and isolation level fix
- [#245](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/245) Mappingset cleanup transaction is executed in dataflow scope
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/251) ESTAT references updated to v8.11.0
- [#267](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/267) Added log4net google appender
- [#277](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/277) Added batchNumber to logs for staging data and metadata
- [#285](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/285) Always set the Actual CC validFrom date
- [#291](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/291) Introduced cancellation tokens and readonly db connections
- [#317](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/317) Fix for performance degradation of dataflow view when adding a value for a dataset-level attribute
- [#353](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/353) Modify TryNewTransaction to allow requeueing requests
- [#361](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/361) Initialize dsd object of dataflows with data management properties
- [#362](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/362) Updated msd cleanup
- [#363](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/363) Fix tryNewTransaction when first request for a dsd is initdataflow
- [#369](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/369) Fix of dataset attribute import
- [#373](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/373) Set QUEUED_DATETIME value to EXECUTION_START for transaction rows existed in db before the addition of the field QUEUED_DATETIME


## v16.1.1 (DataDB v6.1.1, CommonDB v3.8)
### Description

### Issues
- [#269](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/269) Correction of some transfer messages
- [#350](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/350) Introduce ICache param to mappingstoredataaccess


## v16.1.0 (DataDB v6.1.1, CommonDB v3.8)
### Description

### Issues
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/278) Mark transactions as aborted or closed
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/319) Import from URL made async 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/320) Bug fix formatting of the email summary 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/83) Upgrade from netstandard2.1/netcoreapp3.1 to net6.0 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/114) Support for NaN as observation 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/304) Manage unhandled exceptions
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/147) add servicebus, added classes for messaging service (part 1)
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/182) Fix of dataset column mapping issue 


## v16.0.1 (DataDB v6.1.0, CommonDB v3.8)
### Description

### Issues
- [#321](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/321) Fix for issue of data transactions failing with dataflows supporting ref.metadata


## v16.0.0 (DataDB v6.1.0, CommonDB v3.8)
### Description
- [#310](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/310) block all transactions when cleanup process
- [#316](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/316) Add HttpClientTimeOut 
- [#82](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/82) Add validation of time dimension for referential metadata, fix identification Dsd level metadata rows 
- [#306](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/306) Fix ACC validity update at metadata imports 
- [#234](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/234) References updated to NSI v8.9.2 
- [#130](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/130) Improvements to firstNObservations and lastNObservations queries 
- [#302](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/302) Fix of object ref not set error at simple transaction creation 
- [#305](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/305) fix Hierarchical Referential Metadata Attributes are not Imported 
- [#603](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer/-/issues/603) Dataset Attribute parameter passed as NVarchar 
- [#224](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/224) References updated to NSI v8.9.1 h
- [#296](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/296) Correct the transfer log messages for Referential Metadata transactions
- [#293](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/293) Add a proper error description when uploading metadata file to a structure without annotation link to MSD
- [#82](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/82) Fix MetadataDataFlow view period_start and period_end add datetime support 
- [#176](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/176) Add source dataspace and data source fields to transaction logs 
- [#295](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/295) Remove ORDER BY for metadata Mappingsets 
- [#81](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/81) added mappingsets for metadata queries when DSD has MSD reference
- [#80](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/80) Metadata storage implementation 


## v15.0.0 (DataDB v5.6.3, CommonDB v3.8)
### Description

### Issues
- [#184](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/184) Improved management of time preformatted entries and deletion of mapping set relates objects from database 
- [#230](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/230) SDMX-CSV 2.0.0 (meta)data upload 
- [#228](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/228) One of the multiple attributes values at same series key is taken at basic validation  

## v14.0.0 (DataDB v5.6.3, CommonDB v3.8)
### Description

### Issues
- [#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/174) References updated to match Eurostat NSI v8.7.1
- [#189](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/189) Manage Time in group attributes - part 2


## v13.0.1 (DataDB v5.6.3, CommonDB v3.8)
### Description

### Issues
- [#76](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/76) Errors found in the AllowMissingComponents SQL migration script for the recreation of the DSD/DF views
- [#77](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/77) Errors found in the ChangeFactTableIndexes SQL migration script for the recreation of the Fact table indexes


## v13.0.0 (DataDB v5.6, CommonDB v3.8)
### Description

### Issues
- [#33](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/33) Added TokenUrl param to auth config
- [#75](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/75) Change primary key from PERIOD_SDMX to PERIOD_START and PERIOD_END, fix of semantic error displayed when time period is invalid
- [#113](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/113) Support REPORTING_YEAR_START_DAY attribute, Time range format, date + time format, fixed NumberStyles used at float and double, usage of SUPPORT_DATETIME annotation added
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/161) References updated to match Eurostat NSI v8.5.0
- [#178](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/178) Added new localisation keys for file management
- [#210](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/210) New date format applied on PIT release and restoration dates
- [#212](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/212) Added missing Primary measure representation warning
- [#214](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/214) Fix of codelist item ids not in consecutive order issue
- [#215](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/215) Allow non reported components
- [#223](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/223) Changed localization text for bulk copy notifications and Improve merge performance with full details
- [#226](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/226) Added number of obs to pre-generated actual content constraints, fix of NumberStyle for float and double during observation validation
- [#231](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/231) Fill order_by information for mappingsets, recreate DSD and DataFlow views include SID column
- [#237](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/237) Updated mapping set creation function to use new TIME_PRE_FORMATED mapping
- [#242](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/242) Data db connection info not saved to MSDB depending on ApplyEmptyDataDbConnectionStringInMSDB configuration parameter
- [#245](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/245) Fix of finding reported attributes in DataflowMetadataBuilder
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/251) Corrected the creation of staging table, use fact table info to create time columns, when the fact table exists; fix missing attributes in destination during transfer


## v12.4.0 (DataDB v5.3, CommonDB v3.8)
### Description

### Issues
[#72](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/72) Creation of read-only user added to DbUp
[#116](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/116) Added support for Azure SQL Database and Azure SQL Managed Instance to DbUp


## v12.3.0
### Description

### Issues
[#70](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/70) Password change added to DbUp 
[#71](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/71) Replaced sql CONCAT_WS function with CONCAT function
[#20](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-kube-core-rp/-/issues/20) Added Devops DB config tool 
[#123](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/123) Add basic and full validations 
[#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/174) Create transaction item for init/allMappingsets method  
[#123](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/123) Remove primary key in staging table  
[#4](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/4) Update Transfer service error messages 
[#198](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/198) Initialize empty mappingsets and actual content constraints for the first data upload, even if the transaction fails 
[#205](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/205) Fix bug, actual content constraint not updated after data transfer 
[#202](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/202) Fix of wrong content constraint start date update issue


## v12.2.0
### Description

### Issues
[#16](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/16) Bugfix, 4.0 Time dimension without codelist. 


## v12.1.0
### Description

### Issues
[#69](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/69) Bugfix for issue, dbup changes target db to single user mode at start and sets back to multiuser on finishs. 


## v12.0.0
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

This version is a major update with breaking changes to the both code and DataDB. 

### DB changes: 

DataDB v5.0
 
- SQL data type of ENUM_ID column in management.ENUMERATIONS table changed to bigint
- SQL data type of ENUM_ID column in management.COMPONENT table changed to bigint
- MIN_LENGTH column added to management.COMPONENT table
- MAX_LENGTH column added to management.COMPONENT table
- PATTERN column added to management.COMPONENT table
- MIN_VALUE column added to management.COMPONENT table
- MAX_VALUE column added to management.COMPONENT table
- Primary measure component inserted into management.COMPONENT table for all existing DSDs with ENUM_ID = 7 (float)
- DSD_OBS_VALUE_DATA_TYPE column deleted from management.ARTEFACT table
- SQL data type of data.FACT_{dsd ID}_{table version}.VALUE column in FACT table depends on primary measure's SDMX representation in the related DSD (see documentation for further info)
- data.ATTR_{dsd Id}_{table version}_DIMGROUP table deleted
- data.ATTR_{dsd Id}_{table version} table added
- data.FILT_{dsd ID}.ROW_ID column deleted if DSD contains less than 33 non-time dimensions
- data.FACT_{dsd ID}_{table version}.DIM_TIME column deleted from FACT table
- data.FACT_{dsd ID}_{table version}.PERIOD_SDMX column added to FACT table
- data.FACT_{dsd ID}_{table version}.PERIOD_START column added to FACT table
- data.FACT_{dsd ID}_{table version}.PERIOD_END column added to FACT table

CommonDB v3.7
- Grant SELECT permission on table dbo.DB_VERSION

### Issues
[#96](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/96) Cleanup mappingsets updated. 
<BR>[#168](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/168) Fix codelist mapping identification of DSD components. 
<BR>[#25](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/25) Bugfix, add script for granting select permission.
<BR>[#60](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/60) Improve error messages for mistakes in the csv layout. 
<BR>[#68](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/68) Bugfix time format. 
<BR>[#124](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/124) Non-numeric measure type implementation. 
<BR>[#69](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/69) dbup changes target db to single user mode at start and sets back to multiuser on finish. 
<BR>[#159](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/159) Fix string.Format exceptions and time dim check. 
<BR>[#67](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/67) Add ExecutionTimeout in DbUp. 
<BR>[#]() DB documention update
<BR>[#159](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/159) Manage Time in group attribute. 
<BR>[#84](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/84) Support for DSD without Time dimension. 
<BR>[#57](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/57) Add localization key sdmx-ml groups before series. 
<BR>[#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/167) Validation of data database version. 

## v11.0.7 (MSDB v6.12, DataDB v3.5) 
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

This version is a major update with breaking changes to the both code and DataDB. 

### Issues
[#66](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/66) Fix of bug in SqlManagementRepository.CleanUpDsd. 
<BR>[#65](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/65) Add dbup functionality to clear orphan DSD/DF records in Artefact table, for which there is not FILTER/FACT tables

## v11.0.0 (MSDB v6.12, DataDB v3.5) 
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

This version is a major update with breaking changes to the both code and DataDB. 

### Issues
- [#63](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/63) Fix of version issue at querying mapping sets and log feature fixes.
- [#103](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/103) References updated to match Eurostat NSI v8.1.2
- [#110](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/110) Add allowed content constraint check for attributes.
- [#160](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/160) Refactor validation and exception handling.
- [#165](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/165) Bug fix for fix allDataflows fails with a Timed Out Error.
- [#163](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/163) Fix of attribute related issues in DbUp script updating dataflow views.
- [#63](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/63) Fix Error executing generated SQL and populating SDMX model.
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/161) Fix dataflow views with non mandatory dsd level attributes.
- [#120](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/120) Feature to consult the status of the data imports/transactions.
- [#49](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/49) Automatically create mapping sets.
- [#157](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/157) Validate when mappingset valid to date is datemax, set it to datemax - 1 second.
- [#20](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/20) Update of AdminRole permissions added.
- [#108](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/108) Use Estat PermissionType.
- [#143](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/143) Detailed import summary added.
- [#48](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/48) Automatically create data database sql views for dsd and dataflows.
- [#53](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/53) Grant permision to create view to dotstatwriter role.
 


## v10.2.0 (MSDB v6.10, DataDB v2.1) 
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/53

## v10.1.0 (MSDB v6.9, DataDB v2.1) 
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/102
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/101
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/107
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/100
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/47

## v10.0.5 202006-08 (MSDB v6.9,DataDB v2.1)
- Bugfix related to https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/79

## v10.0.0 202006-08 (MSDB v6.9,DataDB v2.1)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/111
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/46
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/101
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/1
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/96
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/94
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/78
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/29
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/95
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/79
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/83
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/93

## v8.0.3 2020-04-18  (MSDB v6.8, DataDB v2.1)

- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/47
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/84
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/62
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/103

##  v7.3.1 2020-03-30 (MSDB v6.8, DataDB v2.1)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/88 + bugfix

##  v7.2.1 2020-03-30 (MSDB v6.8, DataDB v2.1)
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/88

 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/74
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/76
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/86
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/issues/104
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/85
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/42

##  v7.1.1 2020-03-20 (MSDB v6.8, DataDB v2.1)
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/issues/43
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/74
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/76
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/86
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/issues/104
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/85
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/42

##  v7.0.1 2020-03-20 (MSDB v6.8, DataDB v2.1)
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/issues/37
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/39
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/issues/34

##  v6.2.1 2020-02-13 (MSDB v6.8, DataDB v2.1)
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/36

##  v6.1.1 2020-01-31 (MSDB v6.7, DataDB v2.1)

- sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/issues/35

##  v6.0.1 2020-01-22 (MSDB v6.7, DataDB v2.1)

- sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/66

##  v5.2.6 2020-01-22 (MSDB v6.7, DataDB v2.1)

- sis-cc/.stat-suite/dotstatsuite-core-common/issues/102