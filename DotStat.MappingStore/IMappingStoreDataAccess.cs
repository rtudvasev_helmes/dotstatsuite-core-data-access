﻿using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using System;
using System.Collections.Generic;

namespace DotStat.MappingStore
{
    /// <summary>
    /// This interface is not thread safe and should be used in a Scoped|Transient context
    /// </summary>
    public interface IMappingStoreDataAccess
    {
        Dataflow GetDataflow(string dataSpaceId, string agencyId, string dataflowId, string version, bool throwErrorIfNotFound = true, ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies, bool useCache = true);
        IList<IDataflowObject> GetDataflows(string dataSpaceId);
        ISdmxObjects GetDsd(string dataSpaceId, string agencyId, string dsdId, string version);
        IList<IDataStructureObject> GetDataStructures(string dataSpaceId);
        ISdmxObjectRetrievalManager GetRetrievalManager(string dataSpaceId);
        void DeleteActualContentConstraint(string dataSpaceId, string agencyId, string id, string version, char targetTable, TargetVersion targetVersion);
        public void DeleteAllActualContentConstraints(string dataSpaceId, Dataflow dataflow);
        void SaveContentConstraint(string dataSpaceId, Dataflow dataflow, IList<IKeyValuesMutable> cubeRegion, bool? includedCubeRegion, char targetTable, TargetVersion targetVersion, DateTime liveAccStartDate, int? obsCount = null);
        public void UpdateValidityOfContentConstraints(string dataSpaceId, Dataflow dataflow, char targetTable, TargetVersion targetVersion);
        bool HasMappingSet(string dataSpaceId, Dataflow dataFlow);
        bool HasUserCreatedMappingSets(string dataSpaceId, Dataflow dataFlow);
        bool HasNonMigratedDataSets(string dataSpaceId, Dataflow dataFlow);
        bool DeleteDataSetsAndMappingSets(string dataSpaceId, Dataflow dataFlow, bool deleteMsdObjectsOnly); 
        bool CreateOrUpdateMappingSet(string dataSpaceId, Dataflow dataFlow, char targetVersion, DateTime? validFrom, DateTime? validTo, string query, string orderByTimeAsc, string orderByTimeDesc, bool isMetadata=false);
    }
}