﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.MappingStore.Exception;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Org.Sdmxsource.Util.Extensions;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.MappingStore
{
    public static class SdmxParser
    {
        public static Dataflow ParseStructure(ISdmxObjects sdmxObjects, ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies)
        {
            if (!sdmxObjects.Dataflows.Any())
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowNotFoundBySdmxParser));
            }

            var dataflow = sdmxObjects.Dataflows.First();

            if (dataflow.IsExternalReference?.IsTrue ?? false)
            {
                throw new ExternalDataflowException(
                    string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExternalDataflow),
                        $"{dataflow.AgencyId}:{dataflow.Id}({dataflow.Version})"));
            }

            if (!sdmxObjects.DataStructures.Any())
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DSDNotFoundBySdmxParser));
            }

            var sdmxDsd = sdmxObjects.DataStructures.First();
            var dsd = new Dsd(sdmxDsd);

            var msd = GetCurrentMsd(sdmxDsd, sdmxObjects.MetadataStructures);

            if (msd != null)
            {
                dsd.Msd = new Msd(msd);
            }

            // Return dataflow along with its dsd only.
            if (resolveCrossReferences == ResolveCrossReferences.DoNotResolve)
            {
                return new Dataflow(dataflow, dsd);
            }

            var sdmxCodelists = sdmxObjects.Codelists;
            var constraintResolver = new ConstraintResolver(sdmxObjects.ContentConstraintObjects.FirstOrDefault(c => !c.IsDefiningActualDataPresent));

            dsd.SetDimensions(BuildDimensions(sdmxDsd, sdmxCodelists, constraintResolver));
            dsd.SetAttributes(BuildAttributes(sdmxDsd, sdmxCodelists, constraintResolver));
            dsd.SetPrimaryMeasure(BuildPrimaryMeasure(sdmxDsd, sdmxCodelists));

            return new Dataflow(dataflow, dsd);
        }

        private static IEnumerable<Dimension> BuildDimensions(
            IDataStructureObject sdmxDsd, 
            ISet<ICodelistObject> sdmxCodelists,
            ConstraintResolver constraintResolver
        )
        {
            var index = 0;

            foreach (var dim in sdmxDsd.DimensionList.Dimensions)
            {
                Codelist codelist = null;

                if (!dim.TimeDimension)
                {
                    if (!dim.HasCodedRepresentation())
                        throw new DotStatException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoCodelistRepresentation),
                            dim.Id)
                        );

                    var agencyId = dim.Representation?.Representation?.AgencyId;
                    var codelistId = dim.Representation?.Representation?.MaintainableId;
                    var version = dim.Representation?.Representation?.Version;

                    codelist = !string.IsNullOrEmpty(codelistId)
                        ? BuildCodelist(sdmxCodelists.First(cl =>
                            cl.Id.Equals(codelistId, StringComparison.InvariantCultureIgnoreCase) &&
                            cl.AgencyId.Equals(agencyId, StringComparison.InvariantCultureIgnoreCase) &&
                            cl.Version.Equals(version, StringComparison.InvariantCultureIgnoreCase)))
                        : null;

                }

                yield return new Dimension(index++, dim, codelist, constraintResolver[dim.Id]);
            }
        }

        private static PrimaryMeasure BuildPrimaryMeasure(
            IDataStructureObject sdmxDsd,
            ISet<ICodelistObject> sdmxCodelists
        )
        {
            if (sdmxDsd.PrimaryMeasure == null)
            {
                return null;
            }

            var agencyId = sdmxDsd.PrimaryMeasure.Representation?.Representation?.AgencyId;
            var codelistId = sdmxDsd.PrimaryMeasure.Representation?.Representation?.MaintainableId;
            var version = sdmxDsd.PrimaryMeasure.Representation?.Representation?.Version;

            var codelist = !string.IsNullOrEmpty(codelistId)
                ? BuildCodelist(sdmxCodelists.First(cl =>
                    cl.Id.Equals(codelistId, StringComparison.InvariantCultureIgnoreCase) &&
                    cl.AgencyId.Equals(agencyId, StringComparison.InvariantCultureIgnoreCase) &&
                    cl.Version.Equals(version, StringComparison.InvariantCultureIgnoreCase)))
                : null;

            return new PrimaryMeasure(sdmxDsd.PrimaryMeasure, codelist);
        }

        private static IEnumerable<Attribute> BuildAttributes(
            IDataStructureObject sdmxDsd, 
            ISet<ICodelistObject> sdmxCodelists, 
            ConstraintResolver constraintResolver
        )
        {
            foreach (var attr in sdmxDsd.Attributes)
            {

                var agencyId = attr.Representation?.Representation?.AgencyId;
                var codelistId = attr.Representation?.Representation?.MaintainableId;
                var version = attr.Representation?.Representation?.Version;

                var codelist = !string.IsNullOrEmpty(codelistId)
                    ? BuildCodelist(sdmxCodelists.First(cl =>
                        cl.Id.Equals(codelistId, StringComparison.InvariantCultureIgnoreCase) &&
                        cl.AgencyId.Equals(agencyId, StringComparison.InvariantCultureIgnoreCase) &&
                        cl.Version.Equals(version, StringComparison.InvariantCultureIgnoreCase)))
                    : null;

                yield return new Attribute(attr, codelist, constraintResolver[attr.Id]);
            }
        }

        private static Codelist BuildCodelist(ICodelistObject sdmxCodelist)
        {
            var codelist = new Codelist(sdmxCodelist);

            var hash = sdmxCodelist.Items.ToDictionary(c => c.Id, c => new Code(codelist, c));

            foreach (var code in hash.Values.Where(c => c.Base.ParentCode != null))
            {
                if (hash.ContainsKey(code.Base.ParentCode))
                {
                    hash[code.Base.ParentCode].AddChildCode(code);
                }
            }

            codelist.SetCodes(hash.Values);

            return codelist;
        }

        private class ConstraintResolver
        {
            private readonly Dictionary<string, Constraint> _dict = new Dictionary<string, Constraint>();

            public ConstraintResolver(IContentConstraintObject c)
            {
                if (c?.IncludedCubeRegion == null)
                    return;

                _dict
                    .AddAll(GetConstraints(c.IncludedCubeRegion.KeyValues, true));

                _dict
                    .AddAll(GetConstraints(c.IncludedCubeRegion.AttributeValues, true));

                // todo do we need check excluded constraints ?
            }

            private IEnumerable<KeyValuePair<string, Constraint>> GetConstraints(IList<IKeyValues> values, bool include)
            {
                return values.Select(kv => new KeyValuePair<string, Constraint>(kv.Id, new Constraint(true, kv.Values, kv.TimeRange)));
            }

            public Constraint this[string code] => _dict.ContainsKey(code) ? _dict[code] : null;

        }

        private static IMetadataStructureDefinitionObject GetCurrentMsd(IDataStructureObject dsd, ISet<IMetadataStructureDefinitionObject> msdSet)
        {
            if (!dsd.Annotations.Any() || msdSet == null || !msdSet.Any())
            {
                return null;
            }

            var msdAnnotation = dsd.Annotations.FirstOrDefault(x => x.Type.Equals("Metadata", StringComparison.OrdinalIgnoreCase));

            if (msdAnnotation == null || string.IsNullOrEmpty(msdAnnotation.Title))
            {
                return null;
            }

            return msdSet.FirstOrDefault(x => x.Urn.ToString().Equals(msdAnnotation.Title, StringComparison.OrdinalIgnoreCase));
        }
    }
}