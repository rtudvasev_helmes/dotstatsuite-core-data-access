# Purpose of the tool

The **Database Configuration Tool** can be used for the configuration of MS SQL server databases in situations where those databases cannot be accessed directly, e.g. in containerised environments like the SIS-CC DevOps environments.

The behaviour of the tool is similar to DbUp that is used for database upgrade,
but includes only sql scripts that are specific to fine-tuning of MS SQL installation configuration.

**Note:** Unlike DbUp, the tool should be **run once per MS SQL instance** and not per database.

The list of currently supported tasks:

- Set database recovery mode to `SIMPLE` (always executed)
- Create daily index optimization job with the corresponding stored procedures (always executed)
- Adjust maximum size of Data & Log files (optional)

# Usage of the tool

The tool requires a connection with sysadmin user role.

```
DotStat.Devops.Db.Config.exe upgrade --connectionString "Server=localhost;Database=master;Trusted_connection=true"
```

# Configuration of the tool

In addition to a mandatory `connectionString` parameter it is possible to configure individual database parametes (currently max Data & Log file sizes).

The tool is using standard .Net Core configuration model and input can come in these 3 forms

- JSON file located in a `config` directory next to executable (name of a file not important)

```json
{
  "Databases": {
    "DB_NAME1": {
      "DataMaxSize": 2000,
      "LogMaxSize": 1000
    },
    "DB_NAME2": {
      "DataMaxSize": 5000,
      "LogMaxSize": 2500
    }
  }
}
```

- environment variables with [.net core configuration naming conventions](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-5.0#environment-variables) 

```
Databases__DB_NAME1__DataMaxSize=2000
Databases__DB_NAME1__LogMaxSize=1000
Databases__DB_NAME2__DataMaxSize=5000
Databases__DB_NAME2__LogMaxSize=3000
```

- command line arguments with [.net core configuration naming conventions](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-5.0#command-line-arguments)  

```
DotStat.Devops.Db.Config.exe upgrade \
--connectionString "Server=localhost;Database=master;Trusted_connection=true" \
Databases:DB_NAME1:DataMaxSize=2000 \
Databases:DB_NAME1:LogMaxSize=1000 \
Databases:DB_NAME2:DataMaxSize=5000 \
Databases:DB_NAME2:LogMaxSize=3000
```

Databases collection contains a list of objects, where the keys (DB_NAME1 & DB_NAME2 in examples above) are the names of deployed databases in a target location.  

## Database Configuration parameters:

Key|Type|Description
---|---|---
|DataMaxSize|Int|Max size of a data file in MB
|LogMaxSize|Int|Max size of a log file in MB
