﻿SET NOCOUNT ON;
SET CONCAT_NULL_YIELDS_NULL ON;
GO
  
--CLEAN TEMP TABLES
IF OBJECT_ID(N'tempdb..#Temp_DFs') IS NOT NULL
BEGIN
DROP TABLE #Temp_DF_Components
END

IF OBJECT_ID(N'tempdb..#Temp_DSD_Components') IS NOT NULL
BEGIN
	DROP TABLE #Temp_DSD_Components
END


SELECT ART_ID, ID, DF_DSD_ID
INTO #Temp_DFs
FROM [management].[ARTEFACT] 
WHERE [type] = 'DF' 

DECLARE @DF_ID int,
    @DSD_ID int,
	@DF NVARCHAR(MAX),
	@msg NVARCHAR(MAX),
	@Transaction_ID int,
    @sql NVARCHAR(MAX),
    @viewA NVARCHAR(MAX),
    @viewB NVARCHAR(MAX),
	@SelectPart NVARCHAR(MAX),
	@FromPart NVARCHAR(MAX),
	@SubQuerySelectPart NVARCHAR(MAX),
	@SubQueryJoinPart NVARCHAR(MAX),
	@HasTimeDim as bit,
    @NewLineChar AS CHAR(2) =  CHAR(10);


WHILE (Select Count(*) FROM #Temp_DFs) > 0
BEGIN
	SELECT @SubQuerySelectPart= '', @SubQueryJoinPart=''
    SELECT TOP 1 @DF_ID = ART_ID, @DSD_ID = DF_DSD_ID FROM #Temp_DFs

    SELECT c.DSD_ID, c.ID, c.CL_ID, c.COMP_ID,c.[TYPE], c.ATT_ASS_LEVEL, c.ATT_STATUS
    INTO #Temp_DSD_Components
    FROM [management].[ARTEFACT] a 
    INNER JOIN [management].[COMPONENT] c 
    ON a.ART_ID = c.DSD_ID
    WHERE A.ART_ID=@DSD_ID

	SELECT @HasTimeDim = 0
	IF EXISTS(SELECT 1 FROM sys.columns 
		WHERE Name = N'DIM_TIME'
		AND Object_ID = Object_ID(N'[data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_A]'))
	BEGIN
		SELECT @HasTimeDim=1
	END
	
	--SELECT
	--dimensions			
	SELECT @SelectPart='SELECT'+ STUFF((  
		SELECT ', ['+[C].[ID]+']'
		FROM #Temp_DSD_Components C 
		WHERE c.[TYPE]='Dimension'
		FOR XML PATH('')
	), 1, 1, '')		

	--check if dsd has time dimension
	IF (@HasTimeDim =1)
	BEGIN
		SELECT @SelectPart+= ', [TIME_PERIOD]'
	END
	--measure
	SELECT @SelectPart+=', [OBS_VALUE]'
	
	--attributes
	SELECT @SelectPart+=', '+ STUFF((  
		SELECT ', ['+[C].[ID]+']'
		FROM #Temp_DSD_Components C 
		WHERE c.[TYPE]='Attribute'
		FOR XML PATH('')
	), 1, 1, '') +@NewLineChar
	
	--Has dataset level attributes
	IF((Select Count(*) FROM #Temp_DSD_Components WHERE ATT_ASS_LEVEL ='DataSet') > 0)
	BEGIN
		--SUB QUERY
		SELECT @SubQuerySelectPart='SELECT'+ STUFF((  
			SELECT ', '+
			  CASE 
			  --coded attributes at dimgroup level
				WHEN C.[CL_ID] IS NOT NULL
					THEN '[CL_'+[C].[ID]+'].[ID] AS ['+[C].[ID]+']'
			  --Non-coded attributes at dimgroup level
				ELSE 
					'[ADF].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+']'
			  END
			FROM #Temp_DSD_Components C 
			WHERE ATT_ASS_LEVEL ='DataSet'
		 FOR XML PATH('')
		), 1, 1, '') +@NewLineChar
	
		SELECT @SubQuerySelectPart+='FROM [data].[ATTR_' +CAST(@DSD_ID AS VARCHAR)+'_{V}_DF] ADF'+@NewLineChar
		--PRINT @SubQuerySelectPart

		--JOIN
		SELECT @SubQueryJoinPart= STUFF(( 
			SELECT 
				CASE
				WHEN CL_ID IS NOT NULL AND c.ATT_STATUS ='Mandatory'
					THEN 'INNER JOIN [management].[CL_'+CAST(CL_ID AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON ADF.COMP_'+CAST(COMP_ID AS VARCHAR)+' = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar --CODED
				
				WHEN CL_ID IS NOT NULL AND c.ATT_STATUS ='Conditional'
					THEN 'LEFT JOIN [management].[CL_'+CAST(CL_ID AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON ADF.COMP_'+CAST(COMP_ID AS VARCHAR)+' = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar --CODED
				ELSE '' END
			FROM #Temp_DSD_Components C
			WHERE ATT_ASS_LEVEL ='DataSet' FOR XML PATH('')
		), 1, 0, '') 
		SELECT @SubQueryJoinPart+= 'WHERE DF_ID = ' + CAST(@DF_ID AS VARCHAR)+@NewLineChar+') AS ATTR'
		--PRINT @SubQueryJoinPart

		--FROM
		SELECT @FromPart= 'FROM [data].[VI_CurrentDataDsd_'+CAST(@DSD_ID AS VARCHAR)+'_{V}], ('+ @NewLineChar 
		--PRINT @FromPart
		
	END
	ELSE
	BEGIN
		SELECT @FromPart='FROM [data].[VI_CurrentDataDsd_'+CAST(@DSD_ID AS VARCHAR)+'_{V}]'+ @NewLineChar
	END
	
	SELECT @SelectPart+= @FromPart + @SubQuerySelectPart + @SubQueryJoinPart
	
	--DELETE VIEW A IF EXISTS
	SET @viewA ='[data].[VI_CurrentDataDataFlow_'+ CAST( @DF_ID AS VARCHAR)+'_A]'
	IF OBJECT_ID(@viewA, 'V') IS NOT NULL
	BEGIN
		EXEC  ('DROP VIEW '+@viewA)
	END
	
	--DELETE VIEW B IF EXISTS
	SET @viewB ='[data].[VI_CurrentDataDataFlow_'+ CAST( @DF_ID AS VARCHAR)+'_B]'
	IF OBJECT_ID(@viewB, 'V') IS NOT NULL
	BEGIN
		EXEC  ('DROP VIEW '+@viewB)
	END
	
	BEGIN TRY  
		--CREATE THE VIEWS
		SELECT @sql = 'CREATE VIEW '+@viewA+' AS '+@NewLineChar+ REPLACE(@SelectPart,'{V}','A')
		--PRINT @sql
		EXEC sp_executesql @sql

		SELECT @sql = 'CREATE VIEW '+@viewB+' AS '+@NewLineChar+ REPLACE(@SelectPart,'{V}','B')
		--PRINT @sql
		EXEC sp_executesql @sql
	END TRY  
	BEGIN CATCH  
		
		SELECT @DF  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3]IS NULL THEN '0' ELSE CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DF_ID AND [TYPE] = 'DF'

		--Create new transaction for DSD
		SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		INSERT INTO [management].[DSD_TRANSACTION] 
		([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[USER_EMAIL])
		VALUES (@Transaction_ID, @DSD_ID, 'X',GETDATE(),GETDATE(),0,NULL)
		
		SELECT @msg= 'The following error was found while trying to create the DataFlow Views for the Dataflow ['+ @DF +'] (The creation of the view will be skiped):'+ ' ErrorNumber:'+CAST(ERROR_NUMBER() AS VARCHAR)+' ErrorMessage:'+ERROR_MESSAGE()+@NewLineChar 
	
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2020-08-04-02.AutomaticallyCreateDataFlowViews.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
		
		--update transaction info
		UPDATE  [management].[DSD_TRANSACTION] 
		SET [EXECUTION_END]= GETDATE(), [SUCCESSFUL] = 1
		WHERE [TRANSACTION_ID]=@Transaction_ID
	END CATCH

	DROP TABLE #Temp_DSD_Components
    DELETE #Temp_DFs WHERE ART_ID = @DF_ID

END

DROP TABLE #Temp_DFs