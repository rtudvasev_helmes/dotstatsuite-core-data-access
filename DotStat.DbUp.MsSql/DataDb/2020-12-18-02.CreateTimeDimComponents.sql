SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO

DECLARE
	@DSD_ID int,
	@NewLineChar AS CHAR(2) =  CHAR(10);

DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY);

INSERT into @DSD_IDS 
	SELECT ART_ID FROM [management].[ARTEFACT] WHERE [type] = 'DSD' ORDER BY ART_ID

WHILE (SELECT Count(*) FROM @DSD_IDS) > 0
BEGIN

	SELECT TOP 1 @DSD_ID = ART_ID FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID

	BEGIN TRY
		IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'PERIOD_SDMX' AND Object_ID = Object_ID(N'[data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_A]'))
		BEGIN
			CONTINUE
		END

		IF EXISTS(SELECT 1 FROM [management].[COMPONENT] WHERE [ID] = N'TIME_PERIOD' AND [DSD_ID] = @DSD_ID)
		BEGIN
			CONTINUE
		END

		print 'Inserting component TIME_PERIOD for DSD: ' + CAST(@DSD_ID AS VARCHAR)
		INSERT INTO [management].[COMPONENT] ([ID],[TYPE],[DSD_ID]) VALUES (N'TIME_PERIOD', N'TimeDimension', @DSD_ID)
	END TRY
	BEGIN CATCH
		
		DECLARE 
			@DSD nvarchar(100),
			@msg nvarchar(1000),
			@Transaction_ID int

		SELECT @DSD  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3]IS NULL THEN '0' ELSE CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'

		--Create new transaction for DSD
		SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];

		INSERT INTO [management].[DSD_TRANSACTION] 
		([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[ARTEFACT_FULL_ID],[USER_EMAIL])
		VALUES (@Transaction_ID, @DSD_ID, 'X',GETDATE(),GETDATE(),0, @DSD,null)

		SELECT @msg= 'The following error was found while trying to insert component TIME_PERIOD for DSD ['+ @DSD +'] (The insetion will be skiped):'+ ' ErrorNumber:'+CAST(ERROR_NUMBER() AS VARCHAR)+' ErrorMessage:'+ERROR_MESSAGE()+@NewLineChar 

		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2020-12-18-02.CreateTimeDimComponents.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
		
		--update transaction info
		UPDATE  [management].[DSD_TRANSACTION] 
		SET [EXECUTION_END]= GETDATE(), [SUCCESSFUL] = 1,TIMED_OUT=0
		WHERE [TRANSACTION_ID]=@Transaction_ID
	END CATCH
END
