SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
  
DECLARE 	
	@DSD_ID int,
	@DF_ID int,

	@sql_migration NVARCHAR(MAX),
	
	@sql_view NVARCHAR(MAX),
	@sql_view_name varchar(100),

	@table_version char(1),	
    @NewLineChar AS CHAR(2) =  CHAR(10);

IF NOT EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[CL_TIME]') and OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
	RETURN;
END 

DECLARE 
	@VERSIONS TABLE ([version] char(1));
	
DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY);

DECLARE
	@DF_IDS TABLE(ART_ID int PRIMARY KEY);

INSERT into @DSD_IDS 
	SELECT ART_ID FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' ORDER BY ART_ID
	
WHILE (Select Count(*) FROM @DSD_IDS) > 0
BEGIN

    SELECT TOP 1 @DSD_ID = ART_ID FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID
	
	print 'Migrating DSD: ' + CAST(@DSD_ID AS VARCHAR)

--------------------------------------------------------
	DELETE @VERSIONS
	INSERT into @VERSIONS SELECT 'A' union all SELECT 'B'
--------------------------------------------------------

	BEGIN TRY
		BEGIN TRAN
		WHILE (Select Count(*) FROM @VERSIONS) > 0
		BEGIN
			-- select next version
			SELECT top 1 @table_version = [version] from @VERSIONS
			DELETE FROM @VERSIONS where [version]=@table_version

			IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE Name = N'DIM_TIME' AND Object_ID = Object_ID(N'[data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_' + @table_version + ']'))
			BEGIN
				CONTINUE
			END

			-- select dataflows belonging to current DSD
			INSERT into @DF_IDS 
			select ART_ID from management.ARTEFACT where [TYPE]='DF' and DF_DSD_ID=@DSD_ID

			-- add new columns
			set @sql_migration = 'ALTER table data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version +
' add PERIOD_SDMX varchar(10) not null default '''',
PERIOD_START date not null default(getdate()),
PERIOD_END date not null default(getdate())';
		
			exec sp_executesql @sql_migration;

			-- migrate from codelist
			set @sql_migration = 'update data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version +
' set PERIOD_SDMX = time.ID,
PERIOD_START = time.START_PERIOD,
PERIOD_END = time.END_PERIOD
FROM management.[CL_TIME] time
WHERE DIM_TIME = time.ITEM_ID'

			exec sp_executesql @sql_migration;

			-- handle dsd view
			set @sql_view_name = 'VI_CurrentDataDsd_'+ CAST( @DSD_ID AS VARCHAR) + '_' + @table_version
			select @sql_view = v.VIEW_DEFINITION from INFORMATION_SCHEMA.VIEWS v where v.TABLE_NAME = @sql_view_name
			SELECT @sql_view = REPLACE(@sql_view, 'CREATE VIEW', 'CREATE OR ALTER VIEW');
			SELECT @sql_view = REPLACE(@sql_view, 'INNER JOIN [management].[CL_TIME] T ON FA.[DIM_TIME] = T.[ITEM_ID]', '');
			select @sql_view = REPLACE(@sql_view, 'INNER JOIN [management].[CL_TIME] AS T ON T.[ITEM_ID]=fact.[DIM_TIME]','');
			SELECT @sql_view = REPLACE(@sql_view, '[T].[ID] AS [TIME_PERIOD]', 'PERIOD_SDMX AS [TIME_PERIOD],[PERIOD_START],[PERIOD_END]');		
		
			exec sp_executesql @sql_view;

			-- handle dataflow views

			WHILE (Select Count(*) FROM @DF_IDS) > 0
			BEGIN
				SELECT TOP 1 @DF_ID = ART_ID FROM @DF_IDS
				DELETE @DF_IDS WHERE ART_ID = @DF_ID

				print 'Migrating DF: ' + CAST(@DF_ID AS VARCHAR) + '_' + @table_version;

				set @sql_view_name = 'VI_CurrentDataDataFlow_'+ CAST( @DF_ID AS VARCHAR) + '_' + @table_version
				select @sql_view = v.VIEW_DEFINITION from INFORMATION_SCHEMA.VIEWS v where v.TABLE_NAME = @sql_view_name
				SELECT @sql_view = REPLACE(@sql_view, 'CREATE VIEW', 'CREATE OR ALTER VIEW');
				SELECT @sql_view = REPLACE(@sql_view, '[TIME_PERIOD]', '[TIME_PERIOD],[PERIOD_START],[PERIOD_END]');
				exec sp_executesql @sql_view;
			END

			-- drop pk
			set @sql_migration = 'ALTER TABLE data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + ' DROP CONSTRAINT PK_DSD_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;
			exec sp_executesql @sql_migration;
		
			-- drop index on DIM_TIME	
			set @sql_migration = 'DROP INDEX NCCI_TIME_DIM_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + ' ON data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;
			exec sp_executesql @sql_migration;

			-- drop DIM_TIME column	
			set @sql_migration = 'ALTER TABLE data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + ' DROP COLUMN DIM_TIME'
			exec sp_executesql @sql_migration;

			-- add pk
			set @sql_migration = 'ALTER TABLE data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + ' ADD CONSTRAINT PK_DSD_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + ' PRIMARY KEY (SID,PERIOD_SDMX)';
			exec sp_executesql @sql_migration;

			-- add time index
			set @sql_migration = 'CREATE INDEX I_TIME_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + ' ON data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + '(PERIOD_START,PERIOD_END)';
			exec sp_executesql @sql_migration;
				
			/*  _DELETED table   -------------------------------------- */

			-- add new columns
			set @sql_migration = 'ALTER table data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + '_DELETED' +
' add PERIOD_SDMX varchar(10) not null default '''',
PERIOD_START date not null default(getdate()),
PERIOD_END date not null default(getdate())';
		
			exec sp_executesql @sql_migration;

			-- migrate from codelist
			set @sql_migration = 'update data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + '_DELETED' +
' set PERIOD_SDMX = time.ID,
PERIOD_START = time.START_PERIOD,
PERIOD_END = time.END_PERIOD
FROM management.[CL_TIME] time
WHERE DIM_TIME = time.ITEM_ID'

			exec sp_executesql @sql_migration;

			-- drop DIM_TIME column	
			set @sql_migration = 'ALTER TABLE data.FACT_' + CAST(@DSD_ID AS VARCHAR) + '_' + @table_version + '_DELETED DROP COLUMN DIM_TIME'
			exec sp_executesql @sql_migration;
		
		END
		COMMIT TRAN
	END TRY  
	BEGIN CATCH  		
		
		ROLLBACK TRAN

		DECLARE 
			@DSD nvarchar(100),
			@msg nvarchar(1000),
			@Transaction_ID int

		SELECT @DSD  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3]IS NULL THEN '0' ELSE CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'
	
	
		--Create new transaction for DSD
		SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		
		
		INSERT INTO [management].[DSD_TRANSACTION] 
		([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[ARTEFACT_FULL_ID],[USER_EMAIL])
		VALUES (@Transaction_ID, @DSD_ID, 'X',GETDATE(),GETDATE(),0, @DSD,null)
		
		SELECT @msg= 'The following error was found while trying to create the DSD Views for the DSD ['+ @DSD +'] (The creation of the view will be skiped):'+ ' ErrorNumber:'+CAST(ERROR_NUMBER() AS VARCHAR)+' ErrorMessage:'+ERROR_MESSAGE()+@NewLineChar 
		
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2020-12-18-01.MigrateTimeDim.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
		
		--update transaction info
		UPDATE  [management].[DSD_TRANSACTION] 
		SET [EXECUTION_END]= GETDATE(), [SUCCESSFUL] = 1,TIMED_OUT=0
		WHERE [TRANSACTION_ID]=@Transaction_ID
		
	END CATCH
END

IF (Select Count(*) FROM @DSD_IDS) = 0
BEGIN
	print 'DELETE management.CL_TIME'
	exec sp_executesql N'DROP TABLE management.CL_TIME';
END