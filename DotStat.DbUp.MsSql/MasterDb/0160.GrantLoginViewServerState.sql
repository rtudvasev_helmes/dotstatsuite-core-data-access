﻿IF IS_SRVROLEMEMBER('sysadmin') = 1
BEGIN
   grant view server state to [$loginName$];
END