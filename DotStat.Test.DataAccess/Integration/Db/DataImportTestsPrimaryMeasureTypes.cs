﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Test.DataAccess.Integration.Db
{
    [TestFixture]
    public class DataImportTestsPrimaryMeasureTypes : BaseDbIntegrationTests
    {

        public DataImportTestsPrimaryMeasureTypes() : base("sdmx/OBS_TYPE_TEST.xml", DbToInit.Data)
        {
        }

        [TestCase(TextEnumType.String, null, null, null, StagingRowActionEnum.Merge, true)]
        [TestCase(TextEnumType.String, null, null, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.String, "#N/A", "#N/A", null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.String, "#N/A", "1", null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.String, "NaN", "#N/A", null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.String, "NaN", "1", null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.String, "normal value", "normal value", "normal value", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.String, "normal value", "1", null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Decimal, "#N/A", "#N/A", null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Decimal, "#N/A", "1", null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Decimal, "NaN", "#N/A", null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Decimal, "NaN", "1", null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Decimal, "5.5", "5.5", "5.5", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Decimal, "5.5", "1", null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Integer, "#N/A", -2_147_483_648, null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Integer, "#N/A", 1, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Integer, "NaN", -2_147_483_648, null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Integer, "NaN", 1, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Integer, "5", 5, "5", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Integer, "5", 1, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Long, "#N/A", -9_223_372_036_854_775_808, null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Long, "#N/A", 1, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Long, "NaN", -9_223_372_036_854_775_808, null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Long, "NaN", 1, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Long, "5", 5, "5", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Long, "5", 1, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Short, "#N/A", -32_768, null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Short, "#N/A", 1, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Short, "NaN", -32_768, null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Short, "NaN", 1, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Short, "5", 5, "5", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Short, "5", 1, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Boolean, "#N/A", -32_768, null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Boolean, "#N/A", 1, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Boolean, "NaN", -32_768, null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Boolean, "NaN", 1, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Boolean, "false", 0, "0", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Boolean, "false", 1, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Float, "#N/A", -3.4E38F, null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Float, "#N/A", 1.0, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Float, "NaN", -3.4E38F, null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Float, "NaN", 1.0, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Float, "5.5", 5.5, "5.5", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Float, "5.5", 1.0, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Double, "#N/A", -1.7976931348623157E308, null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Double, "#N/A", 1.0, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Double, "NaN", -1.7976931348623157E308, null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Double, "NaN", 1.0, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Double, "5.5", 5.5, "5.5", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Double, "5.5", 1.0, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.GregorianYear, "#N/A", "#N/A", null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.GregorianYear, "#N/A", "1", null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.GregorianYear, "NaN", "#N/A", null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.GregorianYear, "NaN", "1", null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.GregorianYear, "2020", "2020", "2020", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.GregorianYear, "2020", "1", null, StagingRowActionEnum.Delete)]

        public async Task CheckImportedMeasureTypes(TextEnumType textType, string observationValueStr, object expectedValueInStaging, object expectedValueInFact, StagingRowActionEnum action, bool skipped =false)
        {
            var dataFlow = MsAccess.GetDataflow();
            dataFlow.Dsd.SetPrimaryMeasure(GetDsd(textType).PrimaryMeasure);

            //Test case setup
            var seriesKey = new List<IKeyValue> { new KeyValueImpl("A", "FREQ") };
            var key = new KeyableImpl(dataFlow.Base, dataFlow.Dsd.Base, seriesKey, null);
            var observation = new ObservationImpl(key, "2020", observationValueStr, new List<IKeyValue>(), crossSectionValue: null);
            var observationRows = new List<ObservationRow> { new (1, action, observation) }.ToAsyncEnumerable();

            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";

            var reportedComponents = GetReportedComponents(dataFlow);

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, dataFlow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, CancellationToken);
            // create dataflow DB objects 
            await DotStatDbService.TryNewTransaction(transaction, dataFlow, null, MsAccess, CancellationToken);

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(dataFlow.Dsd, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);
            
            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(dataFlow, CancellationToken);

            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observationRows, reportedComponents, codeTranslator, dataFlow,
                fullValidation, isTimeAtTimeDimensionSupported, CancellationToken);

            //CHECK VALUES IN STAGING TABLE
            var table = dataFlow.Dsd.SqlStagingTable();
            var sql = $@"SELECT [VALUE] FROM [{DotStatDb.DataSchema}].{table};";
            var valueInDb = await DotStatDb.ExecuteScalarSqlAsync(sql, CancellationToken);
            Assert.IsNotNull(valueInDb);
            if (valueInDb == DBNull.Value)
                valueInDb = null;
            Assert.AreEqual(expectedValueInStaging, valueInDb);
            
            // merge from staging
            await DotStatDbService.MergeStagingTable(dataFlow, reportedComponents,
                bulkImportResult.DataSetLevelAttributeRows, codeTranslator, tableVersion, true, bulkImportResult, CancellationToken);

            // close transaction
            dataFlow.Dsd.LiveVersion = (char?)tableVersion;
            await DotStatDbService.CloseTransaction(transaction, dataFlow, false, false, MsAccess, codeTranslator, CancellationToken);


            //CHECK VALUES IN VIEW 
            var view = dataFlow.SqlDataDataFlowViewName((char)tableVersion);

            if (action is StagingRowActionEnum.Merge)
            {
                valueInDb = await DotStatDb.ExecuteScalarSqlAsync(
                    $"SELECT [OBS_VALUE] FROM [{DotStatDb.DataSchema}].{view}", CancellationToken);
                if (valueInDb == DBNull.Value)
                    valueInDb = null;
                
                Assert.AreEqual(expectedValueInFact, valueInDb);
            }

            var expectedObsCount = action is StagingRowActionEnum.Delete || skipped ? 0 : 1;
            var obsCountInDb = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT(*) FROM [{DotStatDb.DataSchema}].{view}", CancellationToken);
            Assert.AreEqual(expectedObsCount, obsCountInDb);

            //Empty fact table
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dataFlow.Dsd.SqlFactTable((char)tableVersion), CancellationToken);
        }

        private Dsd GetDsd(TextEnumType textType)
        {
            var sdmxObjects = MsAccess.GetSdmxObjects("sdmx/OBS_TYPE_TEST.xml");
            
            var dsdId = "OBS_TYPE_DSD_" + (textType == TextEnumType.TimesRange ? "TIMERANGE" : textType.ToString().ToUpper());
            var dsDataStructureObject = sdmxObjects.DataStructures.FirstOrDefault(d => d.Id.Equals(dsdId, StringComparison.InvariantCultureIgnoreCase));
            
            var dsd = new Dsd(dsDataStructureObject);
            dsd.SetPrimaryMeasure(new PrimaryMeasure(dsDataStructureObject.PrimaryMeasure));
            
            return dsd;
        }
        
    }
}
