﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Test.DataAccess.Integration.Db
{
    [TestFixture("sdmx/CsvV2.xml", "........./?startPeriod=1900&endPeriod=2079-01")]
    public class MetadataValidationTests : BaseDbIntegrationTests
    {
        private readonly Dataflow _dataflow;
        private readonly string _dataQuery;

        public MetadataValidationTests(string structure, string dataQuery) : base(structure, DbToInit.Data)
        {
            
            _dataflow = this.GetDataflow();
            _dataQuery = dataQuery;
        }
        
        [Test, Order(1)]
        public async Task ImportBasicValidation()
        {
            const DbTableVersion tableVersion = DbTableVersion.A;
            const TargetVersion targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";
            
            var reportedComponents = new ReportedComponents
            {
                Dimensions = _dataflow.Dsd.Dimensions.ToList(),
                MetadataAttributes = _dataflow.Dsd.Msd.MetadataAttributes.ToList()
            };

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, CancellationToken);

            // create dataflow DB objects 
            var success = await DotStatDbService.TryNewTransaction(transaction, _dataflow, null, MsAccess, CancellationToken);
            Assert.IsTrue(success);

            // generate observations
            const int obsCount = 1000;
            var observations = ObservationGenerator.Generate(
                _dataflow,
                false,
                1980,
                2020,
                obsCount, null,
                true,
                action: StagingRowActionEnum.Merge
            );

            Assert.AreEqual(obsCount, await observations.CountAsync());

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.MetadataStoreRepository.RecreateMetadataStagingTables(_dataflow, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);
            var bulkImportResult = await UnitOfWork.MetadataStoreRepository.BulkInsertMetadata(observations, reportedComponents, codeTranslator, _dataflow, fullValidation, isTimeAtTimeDimensionSupported, CancellationToken);
            Assert.AreEqual(0, bulkImportResult.Errors.Count, "No of errors during bulk insert.");
            Assert.AreEqual(obsCount, bulkImportResult.RowsCopied);

            // merge from staging
            var mergeResult= await UnitOfWork.MetadataStoreRepository.MergeMetadataStagingToMetadataTable(_dataflow, reportedComponents, tableVersion, fullValidation, bulkImportResult.RowsCopied, CancellationToken);
            Assert.AreEqual(0, mergeResult.Errors.Count, "No of errors during merging to META.");
            // --------------------------------------------------------------------------

            // check view is created & if metadata rows are returned
            var view = _dataflow.SqlMetadataDataFlowViewName((char)tableVersion);
            Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));

            var metadataCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.AreEqual(obsCount, metadataCount);

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
            success = await DotStatDbService.CloseTransaction(transaction, _dataflow, false, false, MsAccess, codeTranslator, CancellationToken);
            Assert.IsTrue(success);

            // ---------------------------------

            //var sqlRepo = new SqlObservationRepository(new SqlServerDbManager(Configuration, Configuration));

            //var dataQuery = new DataQueryImpl(
            //     new RESTDataQueryCore($"data/{_dataflow.AgencyId},{_dataflow.Code},{_dataflow.Version}/{_dataQuery}"),
            //     new InMemoryRetrievalManager(new SdmxObjectsImpl(_dataflow.Dsd.Base, _dataflow.Base)));

            //var dbObservations = sqlRepo.GetObservations(
            //        dataQuery,
            //        _dataflow,
            //        codeTranslator,
            //        _dataspace.Id,
            //        targetVersion
            //    )
            //    .ToArray();

            //Assert.AreEqual(dbObsCount, dbObservations.Length);
            //Assert.AreEqual(_dataflow.Dimensions.Count(x => !x.Base.TimeDimension), observations[0].SeriesKey.Key.Count);
        }
        
        [Test, Order(2)]
        public async Task ImportFullValidation()
        {
            const DbTableVersion tableVersion = DbTableVersion.A;
            const TargetVersion targetVersion = TargetVersion.Live;
            const bool fullValidation = true;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";
            
            var reportedComponents = new ReportedComponents
            {
                Dimensions = _dataflow.Dsd.Dimensions.ToList(),
                MetadataAttributes = _dataflow.Dsd.Msd.MetadataAttributes.ToList()
            };

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(
                transactionId, _dataflow.FullId, null, 
                null, dataSource,
                TransactionType.Import, targetVersion, CancellationToken);

            // create dataflow DB objects 
            var success = await DotStatDbService.TryNewTransaction(transaction, _dataflow, null, MsAccess, CancellationToken);
            Assert.IsTrue(success);

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.MetadataStoreRepository.RecreateMetadataStagingTables(_dataflow, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);
            var bulkImportResult = await UnitOfWork.MetadataStoreRepository.BulkInsertMetadata(AsyncEnumerable.Empty<ObservationRow>(), reportedComponents, codeTranslator, _dataflow, fullValidation, isTimeAtTimeDimensionSupported, CancellationToken);
           
            //---------------------
            Assert.GreaterOrEqual(bulkImportResult.Errors.Count, 0);
            //Assert.GreaterOrEqual(errorList.Count(e => e.Type == ValidationErrorType.MandatoryAttributeWithNullValueInDatabase), 1);
            
            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
            success = await DotStatDbService.CloseTransaction(transaction, _dataflow, false, true, MsAccess, codeTranslator, CancellationToken);
            Assert.IsTrue(success);
        }
    }
}
