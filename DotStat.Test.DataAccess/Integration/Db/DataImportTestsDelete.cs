﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;

namespace DotStat.Test.DataAccess.Integration.Db
{
    [TestFixture("sdmx/OECD-DF_TEST_DELETE-1.0-all_structures.xml", "........./?startPeriod=1900&endPeriod=2079-01")]
    public class DataImportTestsDelete : BaseDbIntegrationTests
    {
        private readonly Dataflow _dataflow;
        private readonly string _dataQuery;

        public DataImportTestsDelete(string structure, string dataQuery) : base(structure, DbToInit.Data)
        {
            _dataflow = this.GetDataflow();
            _dataQuery = dataQuery;
        }

        [Test]
        [TestCase(new[] { "", "", "", "", "", "", "" }, 0, "OBS_VALUE", 0, null, 0, null, 0, TestName = "case_1__delete_whole_content_of_the_dataflow")]
        [TestCase(new[] { null, null, null, "*", "*", null, null, null }, 0, "OBS_VALUE", 0, null, 9, null, 1, TestName = "case_2__delete_all_observations_of_the_dataflow_and_the_observation-level_attributes")]
        [TestCase(new[] { null, null, null, "*", null, null, null, null }, 27, "OBS_VALUE", 0, null, 9, null, 1, TestName = "case_3__delete_all_observation_values_of_the_dataflow")]
        [TestCase(new[] { null, null, null, null, "*", "*", "*", "*"}, 27, "OBS_ATTR", 0, null, 0, null, 0, TestName = "case_4__delete_all_attributes_of_the_dataflow")]
        [TestCase(new[] { null, null, null, null, null, null, null, "*"}, 27, "DF_ATTR", 0, null, 9, null, 0, TestName = "case_5__delete_attributes_attached_at_dataflow_level")]
        [TestCase(new[] { null, "B", null, null, null, null, null, null }, 18, "OBS_VALUE", 18, null, 6, null, 1, TestName = "case_6-1__delete_everything_related_to_DIM_2_B_way1")]
        [TestCase(new[] { null, "B", null, "*", "*", "*", "*", null }, 18, "OBS_VALUE", 18, null, 6, null, 1, TestName = "case_6-2__delete_everything_related_to_DIM_2_B_way2")]
        [TestCase(new[] { null, "B", null, null, null, null, "*", null }, 27, "GR_ATTR", 18, "COMP_7", 6, null, 1, TestName = "case_7__delete_everything_attached_to_DIM_2_B")]
        [TestCase(new[] { null, "B", null, "*", "*", null, null, null }, 18, "OBS_VALUE", 18, null, 9, null, 1, TestName = "case_8__delete_all_observations__and_its_observation-level_attributes__related_to_DIM_2_B")]
        [TestCase(new[] { null, null, null, null, null, "*", null, null }, 27, "TS_ATTR", 0, "COMP_6", 0, null, 1, TestName = "case_9__delete_all_time_series_attributes")]
        [TestCase(new[] { "A", "B", null, null, null, null, null, null }, 24, "OBS_VALUE", 24, null, 8, null, 1, TestName = "case_10__delete_whole_time_series_DIM_1_A_DIM_2_B")]
        [TestCase(new[] { "A", "B", null, null, null, "*", null, null }, 27, "TS_ATTR", 24, "COMP_6", 8, null, 1, TestName = "case_11__delete_the_attributes_attached_to_time_series_DIM_1_A_DIM_2_B")]
        [TestCase(new[] { "A", "B", null, "*", "*", null, null, null }, 24, "OBS_VALUE", 24, null, 9, null, 1, TestName = "case_12__delete_observation_values___observation-level_attributes_for_time_series_DIM_1_A_DIM_2_B")]
        [TestCase(new[] { "A", "B", "2020", null, null, null, null, null }, 26, "OBS_VALUE", 26, null, 9, null, 1, TestName = "case_13__delete_observation__and_its_observation-level_attributes__attached_to_key_DIM_1_A_DIM_2_B_TIME_PERIOD_2021")]
        [TestCase(new[] { "A", "B", "2020", "*", null, null, null, null }, 27, "OBS_VALUE", 26, null, 9, null, 1, TestName = "case_14__delete_observation_value_attached_to_key_DIM_1_A_DIM_2_B_TIME_PERIOD_2021")]
        public async Task DeleteTests(string[] components, int remainingViewRows, string? obsColumn, int remainingObsCount, string attrColumn, int remainingAttrCount, string dfAttrColumn, int remainingDfAttrCount)
        {
            //Initialize all data
            await InitializeAllData();

            //Test case setup
            var observationRows = new List<ObservationRow>().ToAsyncEnumerable();

            //Special use delete all
            if (components.ToList().All(string.IsNullOrEmpty))
            {
                observationRows = new List<ObservationRow>
                {
                    new(1, StagingRowActionEnum.DeleteAll, null)
                }.ToAsyncEnumerable();
            }
            else
            {
                var i = 0;
                //Dimensions
                var seriesKey = new List<IKeyValue>();
                foreach (var dimId in _dataflow.Dimensions.Where(d => !d.Base.TimeDimension).Select(d => d.Base.Id))
                {
                    seriesKey.Add(new KeyValueImpl(components[i++], dimId));
                }
                var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);
                var obsTime = components[i++];
                //Measure
                var obsVal = components[i++];
                //Attributes
                var attributeValues = new List<IKeyValue>();
                foreach (var dimId in _dataflow.Dsd.Attributes.Select(d => d.Base.Id))
                {
                    attributeValues.Add(new KeyValueImpl(components[i++], dimId));
                }

                var observation = new ObservationImpl(key, obsTime, obsVal, attributeValues, crossSectionValue: null);
                observationRows = new List<ObservationRow>
                {
                    new (1, StagingRowActionEnum.Delete, observation)
                }.ToAsyncEnumerable();
            }

            //Delete
            await ImportObservations(observationRows);
            
            const DbTableVersion tableVersion = DbTableVersion.A;

            //Check view deletions
            var view = _dataflow.SqlDataDataFlowViewName((char)tableVersion);
            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT (*) FROM [data].{view} WHERE [LAST_STATUS]='A'", CancellationToken);
            Assert.AreEqual(remainingViewRows, dbObsCount);

            //Check obs level deletions
            var table = _dataflow.SqlDataDataFlowViewName((char)tableVersion);
            var filterNulledColumn = string.IsNullOrEmpty(obsColumn) ? "": $"WHERE [LAST_STATUS]='A' AND [{obsColumn}] IS NOT NULL;";
            dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT (*) FROM [data].{table} {filterNulledColumn}", CancellationToken);
            Assert.AreEqual(remainingObsCount, dbObsCount);

            //Check attr level deletions
            table = _dataflow.Dsd.SqlAttributeTable((char)tableVersion);
            filterNulledColumn = string.IsNullOrEmpty(attrColumn) ? "" : $"WHERE [{attrColumn}] IS NOT NULL;";
            dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT (*) FROM [data].{table} {filterNulledColumn}", CancellationToken);
            Assert.AreEqual(remainingAttrCount, dbObsCount);

            //Check df attr level deletions
            table = _dataflow.Dsd.SqlDsdAttrTable((char)tableVersion);
            filterNulledColumn = string.IsNullOrEmpty(dfAttrColumn) ? "" : $"WHERE [{dfAttrColumn}] IS NOT NULL;";
            dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"SELECT COUNT (*) FROM [data].{table} {filterNulledColumn}", CancellationToken);
            Assert.AreEqual(remainingDfAttrCount, dbObsCount);
        }
        
        private async Task InitializeAllData()
        {
            // generate observations
            const int obsCount = 27;
            var observations = ObservationGenerator.Generate(
                _dataflow,
                true,
                2020,
                2022,
                obsCount,
                action: StagingRowActionEnum.Merge
            );

            //Populate all data
            await ImportObservations(observations, true);
        }

        private async Task ImportObservations(IAsyncEnumerable<ObservationRow> observations, bool initialiseData = false)
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";

            var reportedComponents = GetReportedComponents(_dataflow);
            
            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, CancellationToken);
            // create dataflow DB objects 
            await DotStatDbService.TryNewTransaction(transaction, _dataflow, null, MsAccess, CancellationToken);
                
            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(_dataflow.Dsd, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);

            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observations, reportedComponents, codeTranslator, _dataflow,
                fullValidation, isTimeAtTimeDimensionSupported, CancellationToken);
                
            // merge from staging

            var dfAttributes =initialiseData? new List<DataSetAttributeRow>
            {
                new (1, StagingRowActionEnum.Merge)
                {
                    Attributes = new List<IKeyValue>{ new KeyValueImpl("DF", "DF_ATTR") }
                }
            } : bulkImportResult.DataSetLevelAttributeRows;
            
            await DotStatDbService.MergeStagingTable(_dataflow, reportedComponents,
                dfAttributes, codeTranslator, tableVersion, true, bulkImportResult, CancellationToken);
                
            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
             await DotStatDbService.CloseTransaction(transaction, _dataflow, false, false, MsAccess, codeTranslator, CancellationToken);
        }

        //private void GetDeleteTestCases()
        //{
        //    var deleteTestCases = new List<List<KeyValueImpl>>
        //    {
        //        //DIM_1,DIM_2,TIME_PERIOD,OBS_VALUE,OBS_ATTR,TS_ATTR,GR_ATTR,DF_ATTR
        //        //case_1__delete_whole_content_of_the_dataflow.csv
        //        new (),
        //        //case_2__delete_all_observations_of_the_dataflow_and_the_observation-level_attributes.csv
        //        new (){new KeyValueImpl("OBS_VALUE","*"), new KeyValueImpl("OBS_ATTR","*")},
        //        //case_3__delete_all_observation_values_of_the_dataflow.csv
        //        new (){new KeyValueImpl("OBS_ATTR","*")},
        //        //case_4__delete_all_attributes_of_the_dataflow.csv
        //        new (){new KeyValueImpl("OBS_ATTR","#N/A"),new KeyValueImpl("TS_ATTR","*"), new KeyValueImpl("GR_ATTR","-1"), new KeyValueImpl("DF_ATTR","A")},
        //        //case_5__delete_attributes_attached_at_dataflow_level.csv
        //        new (){new KeyValueImpl("DF_ATTR","*")},
        //        //case_6-1__delete_everything_related_to_DIM_2_B_way1.csv
        //        new (){new KeyValueImpl("DIM_2","B")},
        //        //case_6-2__delete_everything_related_to_DIM_2_B_way2.csv
        //        new ()
        //        {
        //            new KeyValueImpl("DIM_2","B"),
        //            new KeyValueImpl("OBS_VALUE","*"), new KeyValueImpl("OBS_ATTR","#N/A"), new KeyValueImpl("TS_ATTR","*"), new KeyValueImpl("GR_ATTR","-1")
        //        },
        //        //case_7__delete_everything_attached_to_DIM_2_B.csv
        //        new ()
        //        {
        //            new KeyValueImpl("DIM_2","B"),
        //            new KeyValueImpl("GR_ATTR","-1")
        //        },
        //        //case_8__delete_all_observations__and_its_observation-level_attributes__related_to_DIM_2_B.csv
        //        new ()
        //        {
        //            new KeyValueImpl("DIM_2","B"),
        //            new KeyValueImpl("OBS_VALUE","*"), new KeyValueImpl("OBS_ATTR","#N/A")
        //        },
        //        //case_9__delete_all_time_series_attributes.csv
        //        new (){new KeyValueImpl("TS_ATTR","*")},
        //        //case_10__delete_whole_time_series_DIM_1_A_DIM_2_B.csv
        //        new ()
        //        {
        //            new KeyValueImpl("DIM_1","A"), new KeyValueImpl("DIM_2","B")
        //        },
        //        //case_11__delete_the_attributes_attached_to_time_series_DIM_1_A_DIM_2_B.csv
        //        new ()
        //        {
        //            new KeyValueImpl("DIM_1","A"), new KeyValueImpl("DIM_2","B"),
        //            new KeyValueImpl("TS_ATTR","*")
        //        },
        //        //case_12__delete_observation_values___observation-level_attributes_for_time_series_DIM_1_A_DIM_2_B.csv
        //        new ()
        //        {
        //            new KeyValueImpl("DIM_1","A"), new KeyValueImpl("DIM_2","B"),
        //            new KeyValueImpl("OBS_VALUE","*"), new KeyValueImpl("OBS_ATTR","#N/A")
        //        },
        //        //case_13__delete_observation__and_its_observation-level_attributes__attached_to_key_DIM_1_A_DIM_2_B_TIME_PERIOD_2021.csv
        //        new ()
        //        {
        //            new KeyValueImpl("DIM_1","A"), new KeyValueImpl("DIM_2","B"), new KeyValueImpl("TIME_PERIOD","2014-01"),
        //        },
        //        //case_14__delete_observation_value_attached_to_key_DIM_1_A_DIM_2_B_TIME_PERIOD_2021.csv
        //        new ()
        //        {
        //            new KeyValueImpl("DIM_1","A"), new KeyValueImpl("DIM_2","B"), new KeyValueImpl("TIME_PERIOD","2014-01"),
        //            new KeyValueImpl("OBS_VALUE","*")
        //        },
        //    };
        //}

    }
}
