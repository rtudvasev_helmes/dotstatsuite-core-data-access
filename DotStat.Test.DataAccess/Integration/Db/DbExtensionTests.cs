﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Dto;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Engine.SqlServer;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test.Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

namespace DotStat.Test.DataAccess.Integration.Db
{
    [TestFixture]
    public class DbExtensionTests : BaseDbIntegrationTests
    {
        public DbExtensionTests() : base("sdmx/OBS_TYPE_TEST.xml", BaseDbIntegrationTests.DbToInit.Data)
        {
        }

        public void InitDsdIds(Dsd dsd)
        {
            var i = 100;

            foreach (var dim in dsd.Dimensions)
                dim.DbId = i++;

            foreach (var attr in dsd.Attributes)
                attr.DbId = i++;

            dsd.PrimaryMeasure.DbId = i;
        }

        [TestCase(TextEnumType.String, "#N/A")]
        [TestCase(TextEnumType.Alpha, "#N/A")]
        [TestCase(TextEnumType.Alphanumeric, "#N/A")]
        [TestCase(TextEnumType.Uri, "#N/A")]
        [TestCase(TextEnumType.Numeric, "#N/A")]
        [TestCase(TextEnumType.BigInteger, "#N/A")]
        [TestCase(TextEnumType.Decimal, "#N/A")]
        [TestCase(TextEnumType.Integer, -2_147_483_648)]
        [TestCase(TextEnumType.Count, -2_147_483_648)]
        [TestCase(TextEnumType.Long, -9_223_372_036_854_775_808)]
        [TestCase(TextEnumType.Short, -32_768)]
        [TestCase(TextEnumType.Boolean, -32_768)]
        [TestCase(TextEnumType.Float, -3.4E38F)]
        [TestCase(TextEnumType.Double, -1.7976931348623157E308)]
        [TestCase(TextEnumType.ObservationalTimePeriod, "#N/A")]
        [TestCase(TextEnumType.StandardTimePeriod, "#N/A")]
        [TestCase(TextEnumType.BasicTimePeriod, "#N/A")]
        [TestCase(TextEnumType.GregorianTimePeriod, "#N/A")]
        [TestCase(TextEnumType.GregorianYear, "#N/A")]
        [TestCase(TextEnumType.GregorianYearMonth, "#N/A")]
        [TestCase(TextEnumType.GregorianDay, "#N/A")]
        [TestCase(TextEnumType.ReportingTimePeriod, "#N/A")]
        [TestCase(TextEnumType.ReportingYear, "#N/A")]
        [TestCase(TextEnumType.ReportingSemester, "#N/A")]
        [TestCase(TextEnumType.ReportingTrimester, "#N/A")]
        [TestCase(TextEnumType.ReportingQuarter, "#N/A")]
        [TestCase(TextEnumType.ReportingMonth, "#N/A")]
        [TestCase(TextEnumType.ReportingWeek, "#N/A")]
        [TestCase(TextEnumType.ReportingDay, "#N/A")]
        [TestCase(TextEnumType.DateTime, "#N/A")]
        [TestCase(TextEnumType.TimesRange, "#N/A")]
        [TestCase(TextEnumType.Month, "#N/A")]
        [TestCase(TextEnumType.MonthDay, "#N/A")]
        [TestCase(TextEnumType.Day, "#N/A")]
        [TestCase(TextEnumType.Time, "#N/A")]
        [TestCase(TextEnumType.Duration, "#N/A")]

        public async Task Check_Dsd_Staging_Table_Min_Values(TextEnumType textType, object expectedValue)
        {
            var baseDsd = GetDsd(textType);
            var dsd = new Dsd(baseDsd);
            dsd.SetPrimaryMeasure(new PrimaryMeasure(baseDsd.PrimaryMeasure));
            InitDsdIds(dsd);

            // Create Staging table
            const bool isTimeAtTimeDimensionSupported = false;

            var reportedComponents = GetReportedComponents(dsd);

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(dsd, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            var table = dsd.SqlStagingTable();
            Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");

            //Insert row with minimum value
            var value = dsd.PrimaryMeasure.GetSqlMinValue(true);
            var sql = $@"INSERT INTO [{DotStatDb.DataSchema}].{table}([VALUE],[REQUESTED_ACTION],[REAL_ACTION_FACT],[REAL_ACTION_ATTR]) 
            VALUES ({value},1,1,1);";

            await DotStatDb.ExecuteNonQuerySqlAsync(sql, CancellationToken);

            //Get minimum value
            sql = $@"SELECT [VALUE] FROM [{DotStatDb.DataSchema}].{table};";
            var valueInDb = await DotStatDb.ExecuteScalarSqlAsync(sql, CancellationToken);
            Assert.AreEqual(expectedValue, valueInDb);

        }
       
        private IDataStructureObject GetDsd(TextEnumType textType)
        {
            var sdmxObjects = MsAccess.GetSdmxObjects("sdmx/OBS_TYPE_TEST.xml");

            var dsdId = "OBS_TYPE_DSD_" + (textType == TextEnumType.TimesRange ? "TIMERANGE" : textType.ToString().ToUpper());
            var dsd = sdmxObjects.DataStructures.FirstOrDefault(d => d.Id.Equals(dsdId, StringComparison.InvariantCultureIgnoreCase));

            Assert.IsNotNull(dsd, $"DSD {dsdId} not found.");
            return dsd;
        }
    }
}
