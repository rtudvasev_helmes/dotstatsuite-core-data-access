﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Dto;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Engine.SqlServer;
using DotStat.Db.Repository.SqlServer;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.Db.Engine
{
    [TestFixture]
    public class SqlAttributeEngineTests : BaseDbIntegrationTests
    {
        private DataspaceInternal _dataspace;
        private SqlAttributeEngine _engine;
        private SqlDotStatDb _db;
        private Domain.Dataflow _dataflow;

        public SqlAttributeEngineTests() : base("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml", BaseDbIntegrationTests.DbToInit.Data)
        {
        }

        [OneTimeSetUp]
        public void Init()
        {
            _dataspace = Configuration.SpacesInternal.First();
            _db = DotStatDb as SqlDotStatDb;
            _engine = new SqlAttributeEngine(Configuration);
            _dataflow = this.GetDataflow();

            InitDataForTests();
        }

        private void InitDataForTests()
        {
            _dataflow.Dsd.DbId = 5;

            var i = 100;

            foreach (var dim in _dataflow.Dsd.Dimensions)
                dim.DbId = i++;

            foreach (var attr in _dataflow.Dsd.Attributes)
                attr.DbId = i++;

            _dataflow.Dsd.PrimaryMeasure.DbId = i;
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            foreach(var attr in _dataflow.Dsd.Attributes)
                Assert.AreEqual(-1, await _engine.GetDbId(attr.Code, _dataflow.Dsd.DbId, _db, CancellationToken));
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            foreach (var attr in _dataflow.Dsd.Attributes)
                Assert.IsTrue((attr.DbId = await _engine.InsertToComponentTable(attr, _db, CancellationToken)) > 0);
        }

        [Test, Order(3)]
        public async Task Check_Tables_Created()
        {
            var dsd = _dataflow.Dsd;

            // Create Attribute tables ...

            await _engine.CreateDynamicDbObjects(dsd, _db, CancellationToken);

            var expectedTables = new[]
            {
                dsd.SqlDsdAttrTable('A'),
                dsd.SqlDsdAttrTable('B'),
                dsd.SqlDimGroupAttrTable('A'),
                dsd.SqlDimGroupAttrTable('B')
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await _db.TableExists(table, CancellationToken), $"Table {table} have not been found");
        }

        [Test, Order(4)]
        public async Task CleanUp()
        {
            foreach (var attr in _dataflow.Dsd.Attributes)
            {
                Assert.IsTrue(await _engine.GetDbId(attr.Code, _dataflow.Dsd.DbId, _db, CancellationToken) > 0);

                await _engine.CleanUp(attr, _db, CancellationToken);

                Assert.AreEqual(-1, await _engine.GetDbId(attr.Code, _dataflow.Dsd.DbId, _db, CancellationToken));
            }
        }

    }
}
