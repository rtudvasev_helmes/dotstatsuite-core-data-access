﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Dto;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Engine.SqlServer;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.Db.Engine
{
    [TestFixture]
    public class SqlMsdEngineTests : BaseDbIntegrationTests
    {
        private SqlMsdEngine _engine;
        private Domain.Dataflow _dataflow;

        public SqlMsdEngineTests() : base("sdmx/CsvV2.xml", DbToInit.Data)
        {
        }

        [OneTimeSetUp]
        public void Init()
        {
            _engine = new SqlMsdEngine(Configuration);
            _dataflow = this.GetDataflow();

            InitMetadataForTests();
        }

        private void InitMetadataForTests()
        {
            var i = 100;

            foreach (var dim in _dataflow.Dsd.Dimensions)
                dim.DbId = i++;

            foreach (var attr in _dataflow.Dsd.Msd.MetadataAttributes)
                attr.DbId = i++;
            
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            var id = await _engine.GetDbId(_dataflow.Dsd.Msd.Code, _dataflow.Dsd.Msd.AgencyId, _dataflow.Dsd.Msd.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(-1, id);
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            var id = await _engine.InsertToArtefactTable(_dataflow.Dsd.Msd, DotStatDb, CancellationToken);

            Assert.IsTrue(id > 0);

            _dataflow.Dsd.Msd.DbId = await _engine.GetDbId(_dataflow.Dsd.Msd.Code, _dataflow.Dsd.Msd.AgencyId, _dataflow.Dsd.Msd.Version, DotStatDb, CancellationToken);
            _dataflow.Dsd.MsdDbId = _dataflow.Dsd.Msd.DbId;

            Assert.AreEqual(_dataflow.Dsd.Msd.DbId, id);
        }

        [Test, Order(3)]
        public async Task Check_Msd_Meta_Tables_Created()
        {
            var msd = _dataflow.Dsd.Msd;

            // Create Meta tables ...

            await _engine.CreateDynamicDbObjects(_dataflow.Dsd, DotStatDb, CancellationToken);

            var expectedTables = new string[]
            {
                $"{_dataflow.Dsd.SqlMetadataDsdTable('A')}",
                $"{_dataflow.Dsd.SqlMetadataDsdTable('B')}"
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
        }

    }
}
