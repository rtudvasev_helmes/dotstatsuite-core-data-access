﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Dto;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Engine.SqlServer;
using DotStat.Db.Repository.SqlServer;
using DotStat.MappingStore;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.Db.Engine
{
    [TestFixture]
    public class DbExtensionTests : BaseDbIntegrationTests
    {
        private SqlDsdEngine _engine;
        private Domain.Dataflow _dataflow;

        public DbExtensionTests() : base("sdmx/264D_264_SALDI+2.1.xml", BaseDbIntegrationTests.DbToInit.Data)
        {
        }

        [OneTimeSetUp]
        public void Init()
        {
            _engine = new SqlDsdEngine(Configuration);
            _dataflow = this.GetDataflow();

            InitDataForTests();
        }

        private void InitDataForTests()
        {
            var i = 100;

            foreach (var dim in _dataflow.Dsd.Dimensions)
                dim.DbId = i++;

            foreach (var attr in _dataflow.Dsd.Attributes)
                attr.DbId = i++;

            _dataflow.Dsd.PrimaryMeasure.DbId = i;
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            var id = await _engine.GetDbId(_dataflow.Dsd.Code, _dataflow.Dsd.AgencyId, _dataflow.Dsd.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(-1, id);
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            var id = await _engine.InsertToArtefactTable(_dataflow.Dsd, DotStatDb, CancellationToken);

            Assert.IsTrue(id > 0);

            _dataflow.Dsd.DbId = await _engine.GetDbId(_dataflow.Dsd.Code, _dataflow.Dsd.AgencyId, _dataflow.Dsd.Version, DotStatDb, CancellationToken);

            Assert.AreEqual(_dataflow.Dsd.DbId, id);
        }

        [Test, Order(3)]
        public async Task Check_Dsd_Fact_Tables_Created()
        {
            var dsd = _dataflow.Dsd;

            // Create Fact tables ...

            await _engine.CreateDynamicDbObjects(dsd, DotStatDb, CancellationToken);

            var expectedTables = new string[]
            {
                dsd.SqlFactTable('A'),
                dsd.SqlFactTable('B'),
                dsd.SqlFilterTable(),
            };

            foreach (var table in expectedTables)
                Assert.IsTrue(await DotStatDb.TableExists(table, CancellationToken), $"Table {table} have not been found");
        }

        [TestCase(false, false, "CONVERT([binary](3), [DIM_1])+CONVERT([binary](3), [DIM_2])")]
        [TestCase(false, true, "CONVERT([binary](3), [DIM_1])+CONVERT([binary](3), [DIM_2])+CONVERT([binary](3), [DIM_TIME])")]
        [TestCase(true, false, "CONVERT([binary](3), ISNULL([DIM_1], 0))+CONVERT([binary](3), ISNULL([DIM_2], 0))")]
        [TestCase(true, true, "CONVERT([binary](3), ISNULL([DIM_1], 0))+CONVERT([binary](3), ISNULL([DIM_2], 0))+CONVERT([binary](3), ISNULL([DIM_TIME], 0))")]
        public void BuildRowIdFormula(bool convertNull, bool includeTimeDim, string expectedSql)
        {
            var dsd = new Domain.Dsd(_dataflow.Dsd.Base);
            dsd.SetDimensions(_dataflow.Dsd.Dimensions.Where((d,index)=> index < 2 || d.Base.TimeDimension));

            var i = 1;

            foreach (var dim in dsd.Dimensions)
                dim.DbId = i++;

            var sql = dsd.SqlBuildRowIdFormula(convertNull, includeTimeDim);
            
            Assert.AreEqual(expectedSql, sql);
        }
    }
}
