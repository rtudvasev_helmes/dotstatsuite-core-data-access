﻿using System.Threading.Tasks;
using DotStat.Db.Exception;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.Db.Repository.DsdChangeTests
{
    [TestFixture]
    public class ObservationAttributeChangeTests : BaseDsdChangeTests
    {
        private Dataflow _originalDataFlow;
        private Dataflow _originalDataFlow4NewCodeTest;

        [OneTimeSetUp]
        public async Task SdmxObjectsGenerator()
        {
            _originalDataFlow = await InitOriginalDataflow("ATTR_TYPE_DIFF_TEST.xml");

            _originalDataFlow4NewCodeTest = await InitOriginalDataflow("ATTR_TYPE_DIFF_TEST_OBS_2.xml");
        }

        [Test]
        public void ObservationAttributeAddedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("ATTR_TYPE_DIFF_TEST_OBS_ATTR_ADDED.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
            
            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_NEW", ex.Message);
        }

        [Test]
        public void ObservationAttributeRemovedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("ATTR_TYPE_DIFF_TEST_OBS_ATTR_REMOVED.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
            

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1)", ex.Message);
            StringAssert.Contains("ATTR_OBS_STRING", ex.Message);
        }

        [Test]
        public void ObservationAttributeCodedChangedToNonCodedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_OBS_ENUMERATED_TO_STRING.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
            

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_ENUMERATED", ex.Message);
        }

        [Test]
        public void ObservationAttributeNonCodedChangedToCodedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_OBS_STRING_TO_ENUMERATED.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
            

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_STRING", ex.Message);
        }

        [Test]
        public void ObservationAttributeMandatoryToConditional()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_OBS_MANDATORY_TO_CONDITIONAL.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
            

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_ENUMERATED", ex.Message);
            StringAssert.Contains("from Mandatory to Conditional", ex.Message);
        }

        [Test]
        public void ObservationAttributeConditionalToMandatory()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_OBS_CONDITIONAL_TO_MANDATORY.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
            

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_STRING", ex.Message);
            StringAssert.Contains("Conditional to Mandatory", ex.Message);
        }

        [Test]
        public void ObservationAttributeChangedToDatasetLevelTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_OBS_TO_DATASET.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
            

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_STRING", ex.Message);
            StringAssert.Contains("from Observation to DataSet", ex.Message);
        }

        [Test]
        public void ObservationAttributeChangedToDimensionLevelTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_OBS_TO_DIMENSION.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
            

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_STRING", ex.Message);
            StringAssert.Contains("from Observation to DimensionGroup", ex.Message);
        }

        [Test]
        public void ObservationAttributeChangedToGroupLevelTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_OBS_TO_GROUP.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
            

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_STRING", ex.Message);
            StringAssert.Contains("from Observation to Group", ex.Message);
        }


        [Test]
        public void ObservationAttributeCodelistChangeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("ATTR_TYPE_DIFF_TEST_OBS_NEW_CODELIST.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
            

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_ENUMERATED", ex.Message);
            StringAssert.Contains("OECD:CL_FREQ(11.0)", ex.Message); //New code list
        }

        [Test]
        public void ObservationAttributeCodelistHasNewCodeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_OBS_2_NEW_CODE_IN_CODELIST.xml");
            dataflow.Dsd.DbId = _originalDataFlow4NewCodeTest.Dsd.DbId;

            Assert.DoesNotThrowAsync(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
            
        }

        [Test]
        public void ObservationAttributeCodelistHasRemovedCodeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_OBS_CODE_REMOVED_FROM_CODELIST.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
            

            StringAssert.Contains("T2, T3", ex.Message); // codes removed
            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_OBS_ENUMERATED", ex.Message); //affected component
            StringAssert.Contains("OECD:CL_TEST_OBS(11.0)", ex.Message); //affected codelist
        }
    }
}

