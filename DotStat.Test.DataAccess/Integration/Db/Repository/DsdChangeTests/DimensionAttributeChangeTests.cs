﻿using System.Threading.Tasks;
using DotStat.Db.Exception;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.Db.Repository.DsdChangeTests
{
    [TestFixture]
    public class DimensionAttributeChangeTests : BaseDsdChangeTests
    {
        private Dataflow _originalDataFlow;
        private Dataflow _originalDataFlow4NewCodeTest;

        [OneTimeSetUp]
        public async Task SdmxObjectsGenerator()
        {
            _originalDataFlow = await InitOriginalDataflow("ATTR_TYPE_DIFF_TEST.xml");

            _originalDataFlow4NewCodeTest = await InitOriginalDataflow("ATTR_TYPE_DIFF_TEST_DIM_2.xml");
        }

        [Test]
        public void DimensionAttributeAddedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_ATTR_ADDED.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_NEW", ex.Message);
        }

        [Test]
        public void DimensionAttributeRemovedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_ATTR_REMOVED.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1)", ex.Message);
            StringAssert.Contains("ATTR_DIM_STRING", ex.Message);
        }

        [Test]
        public void DimensionAttributeCodedChangedToNonCodedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DIM_ENUMERATED_TO_STRING.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_ENUMERATED", ex.Message);
        }

        [Test]
        public void DimensionAttributeNonCodedChangedToCodedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DIM_STRING_TO_ENUMERATED.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_STRING", ex.Message);
        }

        [Test]
        public void DimensionAttributeMandatoryToConditional()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DIM_MANDATORY_TO_CONDITIONAL.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_ENUMERATED", ex.Message);
            StringAssert.Contains("from Mandatory to Conditional", ex.Message);
        }

        [Test]
        public void DimensionAttributeConditionalToMandatory()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DIM_CONDITIONAL_TO_MANDATORY.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_STRING", ex.Message);
            StringAssert.Contains("Conditional to Mandatory", ex.Message);
        }

        [Test]
        public void DimensionAttributeChangedToDatasetLevelTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DIM_TO_DATASET.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_STRING", ex.Message);
            StringAssert.Contains("from DimensionGroup to DataSet", ex.Message);
        }

        [Test]
        public void DimensionAttributeChangedToObservationLevelTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DIM_TO_OBSERVATION.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_STRING", ex.Message);
            StringAssert.Contains("from DimensionGroup to Observation", ex.Message);
        }

        [Test]
        public void DimensionAttributeChangedToGroupLevelTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DIM_TO_GROUP.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_STRING", ex.Message);
            StringAssert.Contains("from DimensionGroup to Group", ex.Message);
        }

        [Test]
        public void DimensionAttributeCodelistChangeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_NEW_CODELIST.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_ENUMERATED", ex.Message);
            StringAssert.Contains("OECD:CL_FREQ(11.0)", ex.Message); //New code list
        }

        [Test]
        public void DimensionAttributeCodelistHasNewCodeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DIM_2_NEW_CODE_IN_CODELIST.xml");
            dataflow.Dsd.DbId = _originalDataFlow4NewCodeTest.Dsd.DbId;

            Assert.DoesNotThrowAsync(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
        }

        [Test]
        public void DimensionAttributeCodelistHasRemovedCodeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_DIM_CODE_REMOVED_FROM_CODELIST.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("T2, T3", ex.Message); // codes removed
            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_ENUMERATED", ex.Message); //affected component
            StringAssert.Contains("OECD:CL_TEST_DIM(11.0)", ex.Message); //affected codelist
        }
    }
}

