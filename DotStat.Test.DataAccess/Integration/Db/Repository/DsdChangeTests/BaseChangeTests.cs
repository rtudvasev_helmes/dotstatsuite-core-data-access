﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Common.Configuration;
using DotStat.Db.Engine.SqlServer;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test.Moq;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;

namespace DotStat.Test.DataAccess.Integration.Db.Repository.DsdChangeTests
{
    [SetUpFixture]
    public abstract class BaseDsdChangeTests : BaseDbIntegrationTests
    {
        protected TestMappingStoreDataAccess MappingStoreDataAccess;
        protected ISdmxObjects SdmxObjects;

        protected int TransactionId;

        protected BaseDsdChangeTests() : base(null, DbToInit.Data)
        {
        }
        
        [OneTimeSetUp]
        public void GetBaseSdmxObjects()
        {
            MappingStoreDataAccess = new TestMappingStoreDataAccess();

            var configuration = GetConfiguration().Get<BaseConfiguration>();
            configuration.MaxTextAttributeLength = 10;
            configuration.MaxTransferErrorAmount = 100;

            SdmxObjects = GetSdmxObjects("DIFF_TEST_BASE.xml");
        }

        protected void AppendBaseStructures(ISdmxObjects sdmxObjects)
        {
            foreach (var sdmxObjectsConceptScheme in SdmxObjects.ConceptSchemes)
            {
                sdmxObjects.AddConceptScheme(sdmxObjectsConceptScheme);
            }

            var existingCodelists = sdmxObjects.Codelists.ToArray();
            foreach (var sdmxObjectsCodelist in SdmxObjects.Codelists)
            {
                if (!existingCodelists.Any(cl => cl.Urn.Equals(sdmxObjectsCodelist.Urn)))
                {
                    sdmxObjects.AddCodelist(sdmxObjectsCodelist);
                }
            }
        }

        protected Dataflow GetDataflowWithBaseStructures(string fileName, string folderName = "sdmx/dsd-change-test")
        {
            var sdmxObjects =
                MappingStoreDataAccess.GetSdmxObjects($"{folderName}/{fileName}");

            AppendBaseStructures(sdmxObjects);

            var dataflow = SdmxParser.ParseStructure(sdmxObjects);

            return dataflow;
        }

        protected async Task<Dataflow> InitOriginalDataflow(string fileName, string folderName = "sdmx/dsd-change-test")
        {
            var dataflow = GetDataflowWithBaseStructures(fileName, folderName);
            var engine = new SqlDsdEngine(Configuration);
            var dsdDbId = await engine.InsertToArtefactTable(dataflow.Dsd, DotStatDb, CancellationToken);
            dataflow.Dsd.DbId = dsdDbId;
            await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken);
            return dataflow;
        }

        protected ISdmxObjects GetSdmxObjects(string fileName, string folderName = "sdmx/dsd-change-test")
        {
            var sdmxObjects = MappingStoreDataAccess.GetSdmxObjects($"{folderName}/{fileName}");

            return sdmxObjects;
        }
    }
}

