﻿using System;
using System.Threading.Tasks;
using DotStat.Common.Exceptions;
using DotStat.Db.Exception;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace DotStat.Test.DataAccess.Integration.Db.Repository.DsdChangeTests
{
    [TestFixture]
    public class PrimaryMeasureChangeTests : BaseDsdChangeTests
    {
        private Dataflow _originalStringDataFlow;
        private Dataflow _originalEnumeratedDataFlow;
        private Dataflow _originalEnumerated2DataFlow;

        [OneTimeSetUp]
        public async Task SdmxObjectsGenerator()
        {
            _originalStringDataFlow = await InitOriginalDataflow("OBS_TYPE_DIFF_TEST_STRING_ALL.xml");

            _originalEnumeratedDataFlow =
                await InitOriginalDataflow("OBS_TYPE_DIFF_TEST_ENUMERATED.xml");

            _originalEnumerated2DataFlow =
                await InitOriginalDataflow("OBS_TYPE_DIFF_TEST_ENUMERATED2.xml");
        }

        [Test]
        public void TextFormatMaxLengthChangeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_MAX_LENGTH.xml");
            dataflow.Dsd.DbId = _originalStringDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("maxLength", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]5[^0-9].*$", ex.Message); //Original value
            StringAssert.IsMatch("^.*[^0-9]89[^0-9].*$", ex.Message); //New value

            dataflow = GetDataflowWithBaseStructures(
                "OBS_TYPE_DIFF_TEST_STRING_NO_MAX_LENGTH.xml");
            dataflow.Dsd.DbId = _originalStringDataFlow.Dsd.DbId;

            ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("maxLength", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]5[^0-9].*$", ex.Message); //Original value
        }

        [Test]
        public void TextFormatMinLengthChangeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_MIN_LENGTH.xml");
            dataflow.Dsd.DbId = _originalStringDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("minLength", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]2[^0-9].*$", ex.Message); //Original value
            StringAssert.IsMatch("^.*[^0-9]1[^0-9].*$", ex.Message); //New value

            dataflow = GetDataflowWithBaseStructures(
                "OBS_TYPE_DIFF_TEST_STRING_NO_MIN_LENGTH.xml");
            dataflow.Dsd.DbId = _originalStringDataFlow.Dsd.DbId;
            
            ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("minLength", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]2[^0-9].*$", ex.Message); //Original value
        }

        [Test]
        public void TextFormatPatternChangeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_PATTERN.xml");
            dataflow.Dsd.DbId = _originalStringDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("pattern", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.Contains("^[A-Z][a-zA-Z0-0]*$", ex.Message); //Original value
            StringAssert.Contains("^[0-9][a-zA-Z0-0]*$", ex.Message); //New value

            dataflow = GetDataflowWithBaseStructures(
                "OBS_TYPE_DIFF_TEST_STRING_NO_PATTERN.xml");
            dataflow.Dsd.DbId = _originalStringDataFlow.Dsd.DbId;

            ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("pattern", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.Contains("^[A-Z][a-zA-Z0-0]*$", ex.Message); //Original value
        }

        [Test]
        public void TextFormatMaxValueChangeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_MAX_VALUE.xml");
            dataflow.Dsd.DbId = _originalStringDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("maxValue", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]10000[^0-9].*$", ex.Message); //Original value
            StringAssert.IsMatch("^.*[^0-9]9000[^0-9].*$", ex.Message); //New value

            dataflow = GetDataflowWithBaseStructures(
                "OBS_TYPE_DIFF_TEST_STRING_NO_MAX_VALUE.xml");
            dataflow.Dsd.DbId = _originalStringDataFlow.Dsd.DbId;

            ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("maxValue", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]10000[^0-9].*$", ex.Message); //Original value
        }

        [Test]
        public void TextFormatMinValueChangeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_MIN_VALUE.xml");
            dataflow.Dsd.DbId = _originalStringDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("minValue", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]10[^0-9].*$", ex.Message); //Original value
            StringAssert.IsMatch("^.*[^0-9]100[^0-9].*$", ex.Message); //New value

            dataflow = GetDataflowWithBaseStructures(
                "OBS_TYPE_DIFF_TEST_STRING_NO_MIN_VALUE.xml");
            dataflow.Dsd.DbId = _originalStringDataFlow.Dsd.DbId;

            ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("minValue", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.IsMatch("^.*[^0-9]10[^0-9].*$", ex.Message); //Original value
        }

        [Test]
        public void TextFormatRepresentationChangeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_ALPHA.xml");
            dataflow.Dsd.DbId = _originalStringDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("textformat representation", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.Contains("String", ex.Message); //Original value
            StringAssert.Contains("Alpha", ex.Message); //New value
        }

        [Test]
        public void TextFormatNonCodedToCodedChangeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ALL_ENUMERATED.xml");
            dataflow.Dsd.DbId = _originalStringDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("representation", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_MINMAX_STRING(11.0).OBS_VALUE", ex.Message);
            StringAssert.Contains("String", ex.Message); //Original value
            StringAssert.Contains("OECD:CL_FREQ(11.0)", ex.Message); //New value
        }

        [Test]
        public async Task DefaultTextFormatWhenNotProvidedTest()
        {
            var origDataflow =
                await InitOriginalDataflow("OBS_TYPE_DIFF_TEST_STRING_NO_REPRESENTATION.xml");

            var dataflow =
                GetDataflowWithBaseStructures("OBS_TYPE_DIFF_TEST_STRING_ONLY_STRING.xml");
            dataflow.Dsd.DbId = origDataflow.Dsd.DbId;

            Assert.DoesNotThrowAsync(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
        }

        [Test]
        public void TextFormatCodelistChangeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("OBS_TYPE_DIFF_TEST_ENUMERATED_NEW_CODELIST.xml");
            dataflow.Dsd.DbId = _originalEnumeratedDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("new code list", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_ENUMERATED(11.0).OBS_VALUE", ex.Message);
            StringAssert.Contains("OECD:CL_FREQ(11.0)", ex.Message); //New code list
        }

        [Test]
        public void TextFormatCodelistHasNewCodeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "OBS_TYPE_DIFF_TEST_ENUMERATED2_NEW_CODE_IN_CODELIST.xml");
            dataflow.Dsd.DbId = _originalEnumerated2DataFlow.Dsd.DbId;

            Assert.DoesNotThrowAsync(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
        }

        [Test]
        public void TextFormatCodelistHasRemovedCodeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "OBS_TYPE_DIFF_TEST_ENUMERATED_CODE_REMOVED_FROM_CODELIST.xml");
            dataflow.Dsd.DbId = _originalEnumeratedDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("27, 28", ex.Message); // codes removed
            StringAssert.Contains("OECD:OBS_TYPE_DSD_ENUMERATED(11.0).OBS_VALUE", ex.Message); //affected component
            StringAssert.Contains("OECD:CL_MEASURE(11.0)", ex.Message); //affected codelist
        }

        [Test]
        public void TextFormatCodedToNonCodedChangeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("OBS_TYPE_DIFF_TEST_ENUMERATED_STRING.xml");
            dataflow.Dsd.DbId = _originalEnumeratedDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("representation", ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_ENUMERATED(11.0).OBS_VALUE", ex.Message);
            StringAssert.Contains("OECD:CL_MEASURE(11.0)", ex.Message); //Original value
            StringAssert.Contains("String", ex.Message); //New value
        }

        [Test]
        public void TextFormatIncrementalNotSupported()
        {
            var ex = Assert.ThrowsAsync(typeof(DotStatException),
                () => InitOriginalDataflow("OBS_TYPE_DIFF_TEST_INCREMENTAL.xml"));

            StringAssert.Contains(TextEnumType.Incremental.ToString(), ex.Message);
            StringAssert.Contains("OECD:OBS_TYPE_DSD_INCREMENTAL(11.0).OBS_VALUE", ex.Message);
        }
    }
}

