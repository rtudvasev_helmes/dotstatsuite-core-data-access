﻿using System.Threading.Tasks;
using DotStat.Db.Exception;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.Db.Repository.DsdChangeTests
{
    [TestFixture]
    public class GroupAttributeChangeTests : BaseDsdChangeTests
    {
        private Dataflow _originalDataFlow;
        private Dataflow _originalDataFlow4NewCodeTest;

        [OneTimeSetUp]
        public async Task SdmxObjectsGenerator()
        {
            _originalDataFlow = await InitOriginalDataflow("ATTR_TYPE_DIFF_TEST.xml");

            _originalDataFlow4NewCodeTest = await InitOriginalDataflow("ATTR_TYPE_DIFF_TEST_GROUP_2.xml");
        }

        [Test]
        public void GroupAttributeAddedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_ATTR_ADDED.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_NEW", ex.Message);
        }

        [Test]
        public void GroupAttributeRemovedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_ATTR_REMOVED.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1)", ex.Message);
            StringAssert.Contains("ATTR_GROUP_STRING", ex.Message);
        }

        [Test]
        public void GroupAttributeCodedChangedToNonCodedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_GROUP_ENUMERATED_TO_STRING.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_ENUMERATED", ex.Message);
        }

        [Test]
        public void GroupAttributeNonCodedChangedToCodedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_GROUP_STRING_TO_ENUMERATED.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_STRING", ex.Message);
        }

        [Test]
        public void GroupAttributeMandatoryToConditional()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_GROUP_MANDATORY_TO_CONDITIONAL.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_ENUMERATED", ex.Message);
            StringAssert.Contains("from Mandatory to Conditional", ex.Message);
        }

        [Test]
        public void GroupAttributeConditionalToMandatory()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_GROUP_CONDITIONAL_TO_MANDATORY.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_STRING", ex.Message);
            StringAssert.Contains("Conditional to Mandatory", ex.Message);
        }

        [Test]
        public void GroupAttributeGroupChanegeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_GROUP_NEW_GROUP.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_STRING", ex.Message);
            StringAssert.Contains("from TestGroup to NewGroup", ex.Message);
        }

        [Test]
        public void GroupAttributeChangedToDatasetLevelTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_GROUP_TO_DATASET.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_STRING", ex.Message);
            StringAssert.Contains("from Group to DataSet", ex.Message);
        }

        [Test]
        public void GroupAttributeChangedToObservationLevelTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_GROUP_TO_OBSERVATION.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_STRING", ex.Message);
            StringAssert.Contains("from Group to Observation", ex.Message);
        }

        [Test]
        public void GroupAttributeChangedToDimensionLevelTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_GROUP_TO_DIMENSION.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_STRING", ex.Message);
            StringAssert.Contains("from Group to DimensionGroup", ex.Message);
        }

        [Test]
        public void GroupAttributeCodelistChangeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("ATTR_TYPE_DIFF_TEST_GROUP_NEW_CODELIST.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_ENUMERATED", ex.Message);
            StringAssert.Contains("OECD:CL_FREQ(11.0)", ex.Message); //New code list
        }

        [Test]
        public void GroupAttributeCodelistHasNewCodeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_GROUP_2_NEW_CODE_IN_CODELIST.xml");
            dataflow.Dsd.DbId = _originalDataFlow4NewCodeTest.Dsd.DbId;

            Assert.DoesNotThrowAsync(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
        }

        [Test]
        public void GroupAttributeCodelistHasRemovedCodeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "ATTR_TYPE_DIFF_TEST_GROUP_CODE_REMOVED_FROM_CODELIST.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("T2, T3", ex.Message); // codes removed
            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_GROUP_ENUMERATED", ex.Message); //affected component
            StringAssert.Contains("OECD:CL_TEST_GROUP(11.0)", ex.Message); //affected codelist
        }
    }
}

