﻿using System.Threading.Tasks;
using DotStat.Db.Exception;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.Db.Repository.DsdChangeTests
{
    [TestFixture]
    public class DimensionChangeTests : BaseDsdChangeTests
    {
        private Dataflow _originalDataFlow;
        private Dataflow _originalDataFlowNoTimeDim;
        private Dataflow _originalDataFlow4NewCodeTest;

        [OneTimeSetUp]
        public async Task SdmxObjectsGenerator()
        {
            _originalDataFlow = await InitOriginalDataflow("DIM_TYPE_DIFF_TEST.xml");

            _originalDataFlowNoTimeDim = await InitOriginalDataflow("DIM_TYPE_DIFF_TEST_NO_TIME_DIM.xml");

            _originalDataFlow4NewCodeTest = await InitOriginalDataflow("DIM_TYPE_DIFF_TEST2.xml");
        }

        [Test]
        public void DimensionAddedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("DIM_TYPE_DIFF_TEST_DIM_ADDED.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:DIM_TYPE_DSD(11.1).NEW_DIM", ex.Message);
        }

        [Test]
        public void DimensionRemovedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("DIM_TYPE_DIFF_TEST_DIM_REMOVED.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:DIM_TYPE_DSD(11.1)", ex.Message);
            StringAssert.Contains("MEAS", ex.Message);
        }
        
        [Test]
        public void TimeDimensionAddedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("DIM_TYPE_DIFF_TEST_NO_TIME_DIM_ADDED_TIME_DIM.xml");
            dataflow.Dsd.DbId = _originalDataFlowNoTimeDim.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
            
            StringAssert.Contains("OECD:DIM_TYPE_DSD_NO_TIME_DIM(11.1)", ex.Message);
            StringAssert.Contains("time dimension was added", ex.Message);
        }

        [Test]
        public void TimeDimensionRemovedTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("DIM_TYPE_DIFF_TEST_TIME_DIM_REMOVED.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:DIM_TYPE_DSD(11.1)", ex.Message);
            StringAssert.Contains("time dimension was removed", ex.Message);
        }

        [Test]
        public void DimensionCodelistChangeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures("DIM_TYPE_DIFF_TEST_DIM_NEW_CODELIST.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("OECD:DIM_TYPE_DSD(11.1).MEAS", ex.Message);
            StringAssert.Contains("OECD:CL_TEST(11.0)", ex.Message); //New code list
        }

        [Test]
        public void DimensionCodelistHasNewCodeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "DIM_TYPE_DIFF_TEST2_DIM_NEW_CODE_IN_CODELIST.xml");
            dataflow.Dsd.DbId = _originalDataFlow4NewCodeTest.Dsd.DbId;

            Assert.DoesNotThrowAsync(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));
        }

        [Test]
        public void DimensionCodelistHasRemovedCodeTest()
        {
            var dataflow =
                GetDataflowWithBaseStructures(
                    "DIM_TYPE_DIFF_TEST_DIM_CODE_REMOVED_FROM_CODELIST.xml");
            dataflow.Dsd.DbId = _originalDataFlow.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(dataflow, CancellationToken));

            StringAssert.Contains("Q, W", ex.Message); // codes removed
            StringAssert.Contains("OECD:DIM_TYPE_DSD(11.1).FREQ", ex.Message); //affected component
            StringAssert.Contains("OECD:CL_FREQ(11.0)", ex.Message); //affected codelist
        }
    }
}

