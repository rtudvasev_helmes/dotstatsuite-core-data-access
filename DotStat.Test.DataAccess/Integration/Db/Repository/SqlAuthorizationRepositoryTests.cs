﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Enums;
using DotStat.Common.Model;
using DotStat.Db.Repository.SqlServer;
using Estat.Sdmxsource.Extension.Constant;
using FluentAssertions;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.Db.Repository
{
    [TestFixture]
    public class SqlAuthorizationRepositoryCrud : BaseDbIntegrationTests
    {
        private IEnumerable<string> _groups;
        private DotStatPrincipal _principal;
        private SqlAuthorizationRepository _repository;

        private string _email;
        private UserAuthorization _rule;

        public SqlAuthorizationRepositoryCrud() : base(databasesToInit:DbToInit.Common)
        {
            _email = "test@gmail.com";
            _groups = new[] { "GROUP1", "GROUP2", "GROUP3" };

            var claims = new List<Claim>()
            {
                new Claim("claim/email", _email),
            };

            foreach (var g in _groups)
                claims.Add(new Claim("claim/groups", g));

            var claimsMap = new Dictionary<string, string>()
            {
                {"email","claim/email"},
                {"groups","claim/groups"}
            };

            _principal = new DotStatPrincipal(new ClaimsPrincipal(new ClaimsIdentity(claims)), claimsMap);

            // -------------------------------------

            _repository = new SqlAuthorizationRepository(GetConfiguration().Get<BaseConfiguration>());
        }

        [Test, Order(1)]
        public void Insert()
        {
            _rule = new UserAuthorization()
            {
                UserMask = _email,
                IsGroup = false,
                DataSpace = "*",
                ArtefactType = SDMXArtefactType.Any,
                ArtefactAgencyId = "OECD",
                ArtefactId = "DF",
                ArtefactVersion = "*",
                Permission = PermissionType.AdminRole,
                EditedBy = "TESTER",
                EditDate = DateTime.Now
            };

            var id = _repository.Insert(_rule);

            Assert.IsTrue(id > 0);
        }

        [Test, Order(2)]
        public void GetId()
        {
            var dbRule = _repository.GetById(_rule.Id);

            Assert.IsNotNull(dbRule);
            _rule.Should().BeEquivalentTo(dbRule);
        }

        [Test, Order(3)]
        public void GetByUser()
        {
            var rules = _repository.GetByUser(_principal);

            Assert.IsNotNull(rules);
            Assert.AreEqual(1, rules.Count());

            _rule.Should().BeEquivalentTo(rules.First());
        }

        [Test, Order(4)]
        public void Update()
        {
            _rule.ArtefactAgencyId = "UN";
            _repository.Update(_rule);

            var dbRule = _repository.GetById(_rule.Id);

            Assert.IsNotNull(dbRule);
            _rule.Should().BeEquivalentTo(dbRule);
        }

        [Test, Order(5)]
        public void Delete()
        {
            _repository.Delete(_rule.Id);
            var dbRule = _repository.GetById(_rule.Id);

            Assert.IsNull(dbRule);
        }
    }
}