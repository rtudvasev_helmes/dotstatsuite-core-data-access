﻿using System;
using System.Configuration;
using System.Data.SqlTypes;
using System.Linq;
using DotStat.Common.Configuration.Dto;
using DotStat.DB;
using DotStat.Db.DB;
using DotStat.Db.Repository;
using DotStat.Db.Repository.SqlServer;
using DotStat.Domain;
using DotStat.Domain.Cache;
using DotStat.MappingStore;
using DryIoc;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Utils;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Factory;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.Plugin.SqlServer.Engine;
using Estat.Sri.Plugin.SqlServer.Factory;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;

namespace DotStat.Test.DataAccess.Integration.Db.Repository
{
    // todo make a real integration test

    [TestFixture, Explicit]
    public class MappingStoreDataAccessTests : UnitTestBase
    {
        private readonly DataspaceInternal dataSpace;
        private MappingStoreDataAccess _mappingStoreDataAccess;
        private IDataStoreRepository _dataStoreRepository;
        private readonly Dataflow Df;
        private readonly Dataflow DfWithMeta;

        public MappingStoreDataAccessTests()
        {
            dataSpace = Configuration.SpacesInternal.First();

            // MappingStore configuration----------------------

            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var name = $"DotStatSuiteCoreStructDb_{dataSpace.Id}";
            config.ConnectionStrings.ConnectionStrings.Remove(name);
            config.ConnectionStrings.ConnectionStrings.Add(new ConnectionStringSettings()
            {
                Name = name,
                ConnectionString = dataSpace.DotStatSuiteCoreStructDbConnectionString,
                ProviderName = "System.Data.SqlClient"
            });

            // Save the connection string settings to the configuration file.
            config.Save(ConfigurationSaveMode.Modified);

            // -------------------------------------------------

            var assemblies = new[]
            {
               // typeof(IDatabaseProviderManager).Assembly,
                typeof(DatabaseManager).Assembly,
                typeof(IEntityRetrieverManager).Assembly
            };

            MappingStoreIoc.Container.Register<IRetrievalEngineContainerFactory, RetrievalEngineContainerFactory>(Reuse.Singleton);

            MappingStoreIoc.Container.RegisterMany(assemblies,
                type =>
                    !typeof(IEntity).IsAssignableFrom(type) && !typeof(Exception).IsAssignableFrom(type) 
                       && type != typeof(SingleRequestScope), 
                reuse: Reuse.Singleton, 
                made: FactoryMethod.ConstructorWithResolvableArguments
            );

            MappingStoreIoc.Container.Register<IDatabaseProviderEngine, SqlServerDatabaseProviderEngine>();
            MappingStoreIoc.Container.Register<IDatabaseProviderFactory, SqlServerDatabaseProviderFactory>();
            MappingStoreIoc.Container.Register<IStructureParsingManager, StructureParsingManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep, made: FactoryMethod.ConstructorWithResolvableArguments);

            _mappingStoreDataAccess = new MappingStoreDataAccess(
                    Configuration,
                    MappingStoreIoc.Container.Resolve<IEntityPersistenceManager>(),
                    MappingStoreIoc.Container.Resolve<IEntityRetrieverManager>(),
                    MappingStoreIoc.Container.Resolve<IDatabaseProviderManager>(),
                    new MemoryCache()
                );

            var db = new SqlDotStatDb(dataSpace, SupportedDatabaseVersion.DataDbVersion);

            _dataStoreRepository = new SqlDataStoreRepository(db, Configuration);

            Df = _mappingStoreDataAccess.GetDataflow(
                dataSpace.Id,
                "FCSA",
                "DF_CROPALL",
                "1.4.0",
                true,
                ResolveCrossReferences.ResolveExcludeAgencies
            );

            Df.DbId = 239;

            DfWithMeta = _mappingStoreDataAccess.GetDataflow(
                dataSpace.Id,
                "UNSD",
                "DF_JENS_DAILY",
                "1.0",
                true,
                ResolveCrossReferences.ResolveExcludeAgencies
            );

        }

        [Test]
        public void GetDataflow()
        {
            Assert.IsNotNull(Df);
            Assert.IsNull(Df.Dsd.Msd);
        }

        [Test]
        public void TestConnStrings()
        {
            var configurationStoreManager = new ConfigurationStoreManager(new AppConfigStoreFactory());
            var connectionStringSettings = configurationStoreManager.GetSettings<ConnectionStringSettings>();

            foreach (var conn in connectionStringSettings)
            {
                Console.WriteLine($"Name:{conn.Name}, String:{conn.ConnectionString}, Provider: {conn.ProviderName}");
            }
        }

        [Test]
        public void CheckMappingSet()
        {
            var hasMappingSet = _mappingStoreDataAccess.HasMappingSet(dataSpace.Id, Df);
            Console.WriteLine(hasMappingSet);
        }

        [Test]
        public void CreateMappingSet()
        {
            var tableVersion = DbTableVersion.A;
            var query = _dataStoreRepository.GetDataViewQuery(Df, (char) tableVersion);

            var result = _mappingStoreDataAccess.CreateOrUpdateMappingSet(
                dataSpace.Id,
                Df,
                (char)tableVersion,
                SqlDateTime.MinValue.Value,
                SqlDateTime.MaxValue.Value,
                query,
                "[SID], [PERIOD_START], [PERIOD_END] DESC",
                "[SID], [PERIOD_START] DESC, [PERIOD_END]"
            );

            Assert.IsTrue(result);
        }

        [Test]
        public void DeleteMappingSet()
        {
            var result = _mappingStoreDataAccess.DeleteDataSetsAndMappingSets(dataSpace.Id, Df, false);
            Assert.IsTrue(result);
        }

        [Test]
        public void GetDataflowWithMsd()
        {
            Assert.IsNotNull(DfWithMeta);
            Assert.IsNotNull(DfWithMeta.Dsd.Msd);
        }

        [Test]
        public void CreateMappingSetMetadata()
        {
            var tableVersion = DbTableVersion.A;
            var query = @"some_query";

            var result = _mappingStoreDataAccess.CreateOrUpdateMappingSet(
                dataSpace.Id,
                DfWithMeta,
                (char)tableVersion,
                null,
                null,
                query,
                "[SID], [PERIOD_START], [PERIOD_END] DESC",
                "[SID], [PERIOD_START] DESC, [PERIOD_END]",
                true
            );

            Assert.IsTrue(result);
        }

        [Test]
        public void DeleteMappingSetMeta()
        {
            var result = _mappingStoreDataAccess.DeleteDataSetsAndMappingSets(dataSpace.Id, DfWithMeta, false);
            Assert.IsTrue(result);
        }


    }
}