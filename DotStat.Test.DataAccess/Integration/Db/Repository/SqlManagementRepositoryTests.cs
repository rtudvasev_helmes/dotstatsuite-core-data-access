﻿using DotStat.Common.Configuration.Dto;
using DotStat.Db.Engine.SqlServer;
using DotStat.Domain;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace DotStat.Test.DataAccess.Integration.Db.Repository
{
    [TestFixture]
    public class SqlManagementRepositoryTests : BaseDbIntegrationTests
    {
        private DataspaceInternal _dataspace;
        private Dataflow _dataflow;
        
        public SqlManagementRepositoryTests() : base("sdmx/CsvV2.xml", DbToInit.Data)
        {
            _dataspace = Configuration.SpacesInternal.First();
            _dataflow = this.GetDataflow();
        }
        
        [Test]
        public void GetDb()
        {
            Assert.IsNotNull(DotStatDb);
            Assert.AreEqual(_dataspace.Id.ToUpper(), DotStatDb.Id);
            Assert.AreEqual(_dataspace.DatabaseCommandTimeoutInSec, DotStatDb.DatabaseCommandTimeout);
            Assert.AreEqual("management", DotStatDb.ManagementSchema);
            Assert.AreEqual("data", DotStatDb.DataSchema);
        }

        [Test]
        public async Task GetNextTransactionId()
        {
            var id1 = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var id2 = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);

            Assert.IsTrue(id2 > id1);
        }

        [Test]
        public async Task CleanUpNonExistentDsd()
        {
            var dataflowArtefacts = await UnitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(-1, CancellationToken);
            var allDsdComponents = await UnitOfWork.ComponentRepository.GetAllComponents(CancellationToken);
            var result = await UnitOfWork.ArtefactRepository.CleanUpDsd(-1, dataflowArtefacts, allDsdComponents.ToList(),
                CancellationToken);
            Assert.IsFalse(result);
        }
        [Test]
        public async Task CleanUpNonExistentMsd()
        {
            var dataflowArtefacts = await UnitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(100, CancellationToken);
            var allDsdComponents = await UnitOfWork.ComponentRepository.GetAllComponents(CancellationToken);
            var allMsdComponents = await UnitOfWork.MetaMetadataComponentRepository.GetAllMetadataAttributeComponents(CancellationToken);
            Assert.IsFalse(await UnitOfWork.ArtefactRepository.CleanUpMsdOfDsd(100, dataflowArtefacts, allDsdComponents.ToList(), allMsdComponents.ToList(), CancellationToken));
        }

        [Test]
        public async Task CleanUpExistingDsd()
        {
            _dataflow.Dsd.DbId = await new SqlDsdEngine(Configuration).InsertToArtefactTable(_dataflow.Dsd, DotStatDb, CancellationToken);

            await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(_dataflow, CancellationToken, false);

            var dataflowArtefacts = await UnitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(_dataflow.Dsd.DbId, CancellationToken);
            var allDsdComponents = await UnitOfWork.ComponentRepository.GetAllComponents(CancellationToken);
            Assert.IsTrue(await UnitOfWork.ArtefactRepository.CleanUpDsd(_dataflow.Dsd.DbId, dataflowArtefacts, allDsdComponents.ToList(), CancellationToken));
        }

        [Test]
        public async Task CleanUpExistingMsd()
        {
            _dataflow.Dsd.DbId = await new SqlDsdEngine(Configuration).InsertToArtefactTable(_dataflow.Dsd, DotStatDb, CancellationToken);
            await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(_dataflow, CancellationToken, false);

            var dataflowArtefacts = await UnitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(_dataflow.Dsd.DbId, CancellationToken);
            var allDsdComponents = await UnitOfWork.ComponentRepository.GetAllComponents(CancellationToken);
            var allMsdComponents = await UnitOfWork.MetaMetadataComponentRepository.GetAllMetadataAttributeComponents(CancellationToken);

            Assert.IsTrue(
                await UnitOfWork.ArtefactRepository.CleanUpMsdOfDsd(_dataflow.Dsd.DbId, dataflowArtefacts, allDsdComponents.ToList(), allMsdComponents.ToList(), CancellationToken));
        }

    }
}
