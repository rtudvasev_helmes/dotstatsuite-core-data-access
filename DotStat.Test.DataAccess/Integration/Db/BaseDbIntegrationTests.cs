﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DbUp;
using DbUp.Engine;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.DB;
using DotStat.DB.Repository;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Service;
using FluentAssertions;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.Db
{
    [SetUpFixture]
    public abstract class BaseDbIntegrationTests : SdmxUnitTestBase
    {
        public enum DbToInit
        {
            Common,
            Data
        }
        
        private readonly DbToInit[] _dbs;
        private readonly Dictionary<DbToInit, KeyValuePair<string, string>[]> _initConfigMap;
        public SqlDotStatDb DotStatDb;
        public IDotStatDbService  DotStatDbService;
        public IUnitOfWork  UnitOfWork;
        private readonly DataspaceInternal _dataSpace;
        protected BaseDbIntegrationTests(string dfPath = null, params DbToInit[] databasesToInit) : base(dfPath)
        {
            _dbs = databasesToInit ?? throw new ArgumentException(nameof(databasesToInit));

            _initConfigMap = new Dictionary<DbToInit, KeyValuePair<string, string>[]>
            {
                {DbToInit.Common, new[] {
                        new KeyValuePair<string, string>(Configuration.DotStatSuiteCoreCommonDbConnectionString, "CommonDb")
                    }
                },
                {DbToInit.Data, Configuration
                    .SpacesInternal
                    .Select(x=>new KeyValuePair<string,string>(x.DotStatSuiteCoreDataDbConnectionString, "DataDb"))
                    .ToArray()
                },
            };

            AssertionOptions.AssertEquivalencyUsing(options =>
            {
                options.Using<DateTime>(ctx => ctx.Subject.Should().BeCloseTo(ctx.Expectation, TimeSpan.FromMilliseconds(100))).WhenTypeIs<DateTime>();
                options.Using<DateTimeOffset>(ctx => ctx.Subject.Should().BeCloseTo(ctx.Expectation, TimeSpan.FromMilliseconds(100))).WhenTypeIs<DateTimeOffset>();
                return options;
            });

            _dataSpace = Configuration.SpacesInternal.FirstOrDefault();
            
        }

        private void CreateDb(string connectionString, string scriptFolder)
        {
            var dbVersion = DbUp.DatabaseVersion.DataDbVersion;

            EnsureDatabase.For.SqlDatabase(connectionString);

            var dbUpEngine = DeployChanges.To
                .SqlDatabase(connectionString)
                .WithScriptsEmbeddedInAssembly(typeof(DbUp.MsSql.Resources).Assembly, s => s.Contains(scriptFolder, StringComparison.OrdinalIgnoreCase))
                .WithScript("version.sql", $"UPDATE [dbo].[DB_VERSION] SET VERSION = '{dbVersion}'", new SqlScriptOptions(){RunGroupOrder = 1000})
                .WithVariables(new Dictionary<string, string>()
                {
                    { "dbName", new SqlConnectionStringBuilder(connectionString).InitialCatalog },
                    { "loginName", "dotStatTestUser" },
                    { "loginPwd", "j48j0Lh_rkd$jjRnGr_ejLj0Jr" }
                })
                .WithExecutionTimeout(TimeSpan.FromSeconds(180))
                .LogToConsole()
                .Build();

            var result = dbUpEngine.PerformUpgrade();

            if (!result.Successful)
            {
                throw result.Error;
            }
            InitObjects();
        }

        private void InitObjects()
        {
            IDotStatDb dotStatDb = new SqlDotStatDb(
                _dataSpace,
                DbUp.DatabaseVersion.DataDbVersion
                );

            DotStatDb = dotStatDb as SqlDotStatDb;

            UnitOfWork = new UnitOfWork(
                dotStatDb,
                new SqlArtefactRepository(DotStatDb, Configuration),
                new SqlAttributeRepository(DotStatDb, Configuration),
                new SqlDataStoreRepository(DotStatDb, Configuration),
                new SqlMetadataStoreRepository(DotStatDb, Configuration),
                new SqlMetadataComponentRepository(DotStatDb, Configuration),
                new SqlObservationRepository(DotStatDb, Configuration),
                new SqlTransactionRepository(DotStatDb, Configuration),
                new SqlComponentRepository(DotStatDb, Configuration),
                new SqlCodelistRepository(DotStatDb, Configuration)
            );

            DotStatDbService = new DotStatDbService(UnitOfWork, Configuration);
        }
        private void DeleteDb(string connectionString)
        {
            DropDatabase.For.SqlDatabase(connectionString);

            using (var conn = new SqlConnection(connectionString))
                SqlConnection.ClearPool(conn);
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            foreach (var db in _dbs)
                foreach (var dbConfig in _initConfigMap[db])
                    CreateDb(dbConfig.Key, dbConfig.Value);

            System.Threading.Thread.Sleep(15000);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            foreach (var db in _dbs)
                foreach (var dbConfig in _initConfigMap[db])
                    DeleteDb(dbConfig.Key);
        }
    }
}
