﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.Db
{
    [TestFixture("sdmx/CsvV2.xml", "........./?startPeriod=1900&endPeriod=2079-01")]
    public class MetadataImportTests : BaseDbIntegrationTests
    {
        private readonly Dataflow _dataflow;
        private readonly string _dataQuery;

        public MetadataImportTests(string structure, string dataQuery) : base(structure, DbToInit.Data)
        {
            _dataflow = this.GetDataflow();
            _dataQuery = dataQuery;
        }

        [Test, Order(1)]
        public async Task Import()
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";
            
            var reportedComponents = new ReportedComponents
            {
                Dimensions = _dataflow.Dimensions.ToList(),
                MetadataAttributes = _dataflow.Dsd.Msd.MetadataAttributes.ToList(),
                IsPrimaryMeasureReported = false
            };
            
            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction= await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, CancellationToken);

            // create dataflow DB objects 
            var success =
                await DotStatDbService.TryNewTransaction(transaction, _dataflow, null, MsAccess, CancellationToken);
            Assert.IsTrue(success);

            // generate observations
            var obsCount = 4;
            var observations = ObservationGenerator.Generate(
                    _dataflow,
                    false,
                    1980,
                    2020,
                    obsCount,
                    action: StagingRowActionEnum.Merge
                );

            Assert.AreEqual(obsCount, await observations.CountAsync());

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.MetadataStoreRepository.RecreateMetadataStagingTables(_dataflow, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);

            var bulkImportResult = await UnitOfWork.MetadataStoreRepository.BulkInsertMetadata(observations, reportedComponents, codeTranslator, _dataflow,
                fullValidation, isTimeAtTimeDimensionSupported, CancellationToken);
            Assert.AreEqual(0, bulkImportResult.Errors.Count, "No of errors during bulk insert.");

            // merge from staging
            var mergeResult = await UnitOfWork.MetadataStoreRepository.MergeMetadataStagingToMetadataTable(_dataflow, reportedComponents,
                tableVersion, true, bulkImportResult.RowsCopied, CancellationToken);
            Assert.AreEqual(0, mergeResult.Errors.Count, "No of errors during merging to fact data.");
            Assert.AreEqual(obsCount, mergeResult.TotalCount);
            // --------------------------------------------------------------------------

            // check view is created & if observations are returned
            var view = _dataflow.Dsd.SqlMetaDataDsdViewName((char) tableVersion);
            Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));


            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.AreEqual(obsCount, dbObsCount);

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?) tableVersion;
            success = await DotStatDbService.CloseTransaction(transaction, _dataflow,false, obsCount>0, MsAccess, codeTranslator, CancellationToken);
            Assert.IsTrue(success);

            // ---------------------------------

            //var sqlRepo = new SqlObservationRepository(new SqlServerDbManager(Configuration, Configuration));

            //var dataQuery = new DataQueryImpl(
            //    new RESTDataQueryCore($"data/{_dataflow.AgencyId},{_dataflow.Code},{_dataflow.Version}/{_dataQuery}"),
            //    new InMemoryRetrievalManager(new SdmxObjectsImpl(_dataflow.Dsd.Base, _dataflow.Base)));

            //var dbObservations = sqlRepo.GetObservations(
            //        dataQuery,
            //        _dataflow,
            //        codeTranslator,
            //        _dataspace.Id,
            //        targetVersion
            //    )
            //    .ToArray();

            //Assert.AreEqual(dbObsCount, dbObservations.Length);
            //Assert.AreEqual(_dataflow.Dimensions.Count(x => !x.Base.TimeDimension),
            //    observations[0].SeriesKey.Key.Count);
        }

        /// <summary>
        ///  runs only after Import Test
        /// </summary>
        [Test, Order(2)]
        public async Task CopyToNewVersion()
        {
            var sourceVersion = DbTableVersion.A;
            var destinationVersion = DbTableVersion.B;
            var targetVersion = TargetVersion.PointInTime;
            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, "file.csv", TransactionType.Import, targetVersion, CancellationToken);

            await UnitOfWork.MetadataStoreRepository.DeleteMetadata(_dataflow.Dsd, destinationVersion, CancellationToken);
            await UnitOfWork.MetadataStoreRepository.CopyMetadataToNewVersion(_dataflow, sourceVersion, destinationVersion, CancellationToken);

            var view = _dataflow.Dsd.SqlMetaDataDsdViewName((char) destinationVersion);

            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.IsTrue(dbObsCount > 0);
        }
    }
}
