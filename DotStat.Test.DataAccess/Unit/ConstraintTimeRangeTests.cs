﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Unit
{
    /// <summary>
    /// DF_SDG_GLB has following TimeRange constraints:
    /// <common:StartPeriod isInclusive="true">2013-01-01T00:00:00</common:StartPeriod>
    /// <common:EndPeriod isInclusive="true">2017-01-01T00:00:00</common:EndPeriod>
    /// </summary>
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public class ConstraintTimeRangeTests : SdmxUnitTestBase
    {
        private readonly CodeTranslator _codeTranslator;
        private readonly Dataflow _dataflow;

        /// <summary>
        /// Tests Predicate construction for Dataflow with Time range Constraints
        /// </summary>
        public ConstraintTimeRangeTests() : base("sdmx/DF_SDG_GLB (with time range constraints).xml")
        {
            _dataflow = GetDataflow();
            var codeListRepository = new TestCodeListRepository(_dataflow);
            _codeTranslator = new CodeTranslator(codeListRepository);
        }

        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all", "([DIM_101]=1) AND ([DIM_102]=1) AND ([DIM_104]=439) AND ([DIM_110]=1) AND ([DIM_111]=1) AND ([DIM_112]=1) AND ([DIM_113]=1) AND ([DIM_114]=1) AND ([DIM_115]=1) AND PERIOD_END>='2013-01-01' AND PERIOD_START<'2017-01-01'")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?startPeriod=2014&endPeriod=2015", "([DIM_101]=1) AND ([DIM_102]=1) AND ([DIM_104]=439) AND ([DIM_110]=1) AND ([DIM_111]=1) AND ([DIM_112]=1) AND ([DIM_113]=1) AND ([DIM_114]=1) AND ([DIM_115]=1) AND PERIOD_END>='2014-01-01' AND PERIOD_START<'2016-01-01'")]
        public async Task ValidQueries(string restQuery, string expectedSql)
        {
            var query = GetQuery(restQuery);
            var sql = await Predicate.BuildWhereForDataObservationLevel(_codeTranslator, query, _dataflow, CancellationToken);

            Assert.AreEqual(expectedSql, sql);
        }

        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all", "PERIOD_END>='2013-01-01' AND PERIOD_START<'2017-01-01'")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/............../all", "PERIOD_END>='2013-01-01' AND PERIOD_START<'2017-01-01'")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?startPeriod=2012", "PERIOD_END>='2013-01-01' AND PERIOD_START<'2017-01-01'")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?endPeriod=2018", "PERIOD_END>='2013-01-01' AND PERIOD_START<'2017-01-01'")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?startPeriod=2014", "PERIOD_END>='2014-01-01' AND PERIOD_START<'2017-01-01'")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?endPeriod=2015", "PERIOD_END>='2013-01-01' AND PERIOD_START<'2016-01-01'")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?endPeriod=2015-02", "PERIOD_END>='2013-01-01' AND PERIOD_START<'2015-03-01'")]
        [TestCase("data/UNSD,DF_SDG_GLB,1.0/all?startPeriod=2014&endPeriod=2015-Q2", "PERIOD_END>='2014-01-01' AND PERIOD_START<'2015-07-01'")]
        public void TimeRange(string restQuery, string expectedSql)
        {
            var query = GetQuery(restQuery);
            var where = new List<string>();

            Predicate.AddTimePredicate(
                where,
                _dataflow,
                _codeTranslator,
                query?.SelectionGroups?.FirstOrDefault(),
                NullTreatment.ExcludeNulls
            );

            Assert.AreEqual(expectedSql, string.Join(" AND ", where));
        }

        [Test]
        public void DimensionWithoutConstraintTest()
        {
            var dim = _dataflow.Dimensions.FirstOrDefault(d => d.Constraint == null);

            Assert.IsNotNull(dim);
            Assert.IsTrue(dim.IsAllowed("test"));
        }


        [TestCase(true)]
        [TestCase(false)]
        public void ConstraintObjTest(bool include)
        {
            var c = new Constraint(include, new[] { "A", "B", "C" }, null);

            Assert.AreEqual(include, c.IsAllowed("A"));
            Assert.AreEqual(!include, c.IsAllowed("X"));
        }

        [Test]
        public void TimeRangeMapping()
        {
            var timeDim = _dataflow.Dimensions.FirstOrDefault(x => x.Base.TimeDimension);

            Assert.IsNotNull(timeDim);
            Assert.IsNotNull(timeDim.Constraint);
            Assert.IsNotNull(timeDim.Constraint.TimeRange);
        }
    }
}
