﻿using DotStat.Common.Configuration;
using DotStat.Common.Localization;
using DotStat.Db;
using DotStat.Db.Validation;
using DotStat.Domain;
using DotStat.Test.Moq;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using System.Collections.Generic;
using System.Linq;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public class MetadataValidationTests : UnitTestBase
    {
        private readonly Dataflow _dataflow;
        private readonly BaseConfiguration _configuration;
        private readonly ReportedComponents _reportedComponents;

        public MetadataValidationTests()
        {
            _dataflow = new TestMappingStoreDataAccess("sdmx/CsvV2.xml").GetDataflow();
            _configuration = GetConfiguration().Get<BaseConfiguration>();
            _reportedComponents = new ReportedComponents()
            {
                Dimensions = _dataflow.Dimensions.ToList(),
                MetadataAttributes = _dataflow.Dsd.Msd.MetadataAttributes
            };
        }

        [Test]
        public void DublicationTest()
        {
            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("D", "FREQ"),
                new KeyValueImpl(null, "REPORTING_TYPE"),
                new KeyValueImpl("SI_POV_NAHC", "SERIES"),
                new KeyValueImpl("ET", "REF_AREA"),
                new KeyValueImpl(null, "SEX")
            };

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);

            var observations = new List<IObservation>
            {
                new ObservationImpl(key, "2010", string.Empty, new List<IKeyValue>(new []{new KeyValueImpl("A", "STRING_TYPE")})),
                new ObservationImpl(key, "2010", string.Empty, new List<IKeyValue>(new []{new KeyValueImpl("B", "STRING_TYPE") })),
                new ObservationImpl(key, "2011", string.Empty, new List<IKeyValue>(new []{new KeyValueImpl("D", "STRING_TYPE") })),
                new ObservationImpl(key, "2010", string.Empty, new List<IKeyValue>(new []{new KeyValueImpl("E", "STRING_TYPE") })),
            };
            
            var isValid = true;
            var validator = new MetadataValidator(_dataflow, _reportedComponents, _configuration, true, false);

            for (var i = 1; i <= observations.Count; i++)
            {
                isValid &= validator.ValidateObservation(observations[i - 1], i);
            }

            Assert.IsFalse(isValid);

            var errors = validator.GetErrors();

            Assert.AreEqual(2, errors.Count);

            Assert.AreEqual(ValidationErrorType.DuplicatedCoordinate, errors[0].Type);
            Assert.AreEqual(2, errors[0].Row);
            var expectedError = string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DuplicatedCoordinate),
                2, observations[1].GetFullKey(true, observations[1]?.SeriesKey.DataStructure.TimeDimension?.Id), null, 3);
            Assert.True(errors[0].ToString().Contains(expectedError));

            Assert.AreEqual(ValidationErrorType.DuplicatedCoordinate, errors[1].Type);
            Assert.AreEqual(4, errors[1].Row);
        }

        [Test]
        public void UnknownCodeMemberTest()
        {
            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("D", "FREQ"),
                new KeyValueImpl(null, "REPORTING_TYPE"),
                new KeyValueImpl("WRONG_MEMBER", "SERIES"),
                new KeyValueImpl("ET", "REF_AREA"),
                new KeyValueImpl(null, "SEX")
            };

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);

            var observation = new ObservationImpl(key, "2010", string.Empty,
                    new List<IKeyValue>(new[] {new KeyValueImpl("A", "STRING_TYPE")}));

            var validator = new MetadataValidator(_dataflow, _reportedComponents, _configuration, true, false);

            Assert.IsFalse(validator.ValidateObservation(observation));

            var errors = validator.GetErrors();

            Assert.AreEqual(1, errors.Count);
            Assert.AreEqual(ValidationErrorType.UnknownCodeMember, errors[0].Type);
        }
    }
}
