﻿using DotStat.Common.Configuration.Dto;
using DotStat.Db.DB;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotStat.Test.DataAccess.Unit.Service
{
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public class DotStatDbServiceTests : UnitTestBase
    {
        private readonly DataspaceInternal _dataSpace;
        //private Mock<SqlDotStatDb> _dotStatDbMock;
        //TODO complete all unitests 
        public DotStatDbServiceTests()
        {
            _dataSpace = Configuration.SpacesInternal.First();

            var dotStatDbMock = new Mock<SqlDotStatDb>(_dataSpace, DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            //_dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            //_dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");
        }
    }
}
