﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Exception;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public class ConstraintTests : SdmxUnitTestBase
    {
        private readonly CodeTranslator _codeTranslator;
        private readonly Dataflow _dataflow;

        /// <summary>
        /// Tests Predicate construction for Dataflow with Constraints
        /// </summary>
        public ConstraintTests() : base("sdmx/ECB_AME_(with constraints).xml")
        {
            _dataflow = GetDataflow();
            var codeListRepository = new TestCodeListRepository(_dataflow);
            _codeTranslator = new CodeTranslator(codeListRepository);
        }

        [TestCase("data/ECB,AME,4.0/all", "([DIM_101]=8) AND ([DIM_102]=34 OR [DIM_102]=82 OR [DIM_102]=94 OR [DIM_102]=101) AND ([DIM_103]=2) AND ([DIM_104]=5) AND ([DIM_105]=1 OR [DIM_105]=6) AND ([DIM_106]=1) AND ([DIM_107]=396 OR [DIM_107]=222 OR [DIM_107]=349 OR [DIM_107]=878)")]
        [TestCase("data/ECB,AME,4.0/....../all", "([DIM_101]=8) AND ([DIM_102]=34 OR [DIM_102]=82 OR [DIM_102]=94 OR [DIM_102]=101) AND ([DIM_103]=2) AND ([DIM_104]=5) AND ([DIM_105]=1 OR [DIM_105]=6) AND ([DIM_106]=1) AND ([DIM_107]=396 OR [DIM_107]=222 OR [DIM_107]=349 OR [DIM_107]=878)")]
        [TestCase("data/ECB,AME,4.0/.IRL...../all", "([DIM_102]=82) AND ([DIM_101]=8) AND ([DIM_103]=2) AND ([DIM_104]=5) AND ([DIM_105]=1 OR [DIM_105]=6) AND ([DIM_106]=1) AND ([DIM_107]=396 OR [DIM_107]=222 OR [DIM_107]=349 OR [DIM_107]=878)")]
        [TestCase("data/ECB,AME,4.0/Q.IRL.3.4.319.0.ZUTN/all", "([DIM_101]=8) AND ([DIM_102]=82) AND ([DIM_103]=2) AND ([DIM_104]=5) AND ([DIM_105]=6) AND ([DIM_106]=1) AND ([DIM_107]=878)")]
        [TestCase("data/ECB,AME,4.0/A+Q.IRL+XXX...../all", "([DIM_101]=8) AND ([DIM_102]=82) AND ([DIM_103]=2) AND ([DIM_104]=5) AND ([DIM_105]=1 OR [DIM_105]=6) AND ([DIM_106]=1) AND ([DIM_107]=396 OR [DIM_107]=222 OR [DIM_107]=349 OR [DIM_107]=878)")]
        public async Task ValidQueries(string restQuery, string expectedSql)
        {
            var query = GetQuery(restQuery);
            var sql = await Predicate.BuildWhereForDataObservationLevel(_codeTranslator, query, _dataflow, CancellationToken);

            Assert.AreEqual(expectedSql, sql);
        }

        [Test]
        public void NonExistingCode()
        {
            var query = GetQuery("data/ECB,AME,4.0/XXX....../all");

            Assert.ThrowsAsync<SdmxNoResultsException>(
                async () => await Predicate.BuildWhereForDataObservationLevel(_codeTranslator, query, _dataflow, CancellationToken)
            );
        }

        [Test]
        public void ConstrainedCode()
        {
            var query = GetQuery("data/ECB,AME,4.0/A.EST...../all");

            Assert.ThrowsAsync<SdmxNoResultsException>(
                async () => await Predicate.BuildWhereForDataObservationLevel(_codeTranslator, query, _dataflow, CancellationToken)
            );
        }

    }
}
