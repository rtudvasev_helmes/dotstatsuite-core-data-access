﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Db.Reader;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Explicit]
    public sealed class SdmxObservationReaderPerfomanceTests : UnitTestBase
    {
        private readonly TestMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly CodeTranslator _codeTranslator;
        private readonly Dataflow _dataflow;
        private readonly ReportedComponents _reportedComponents;

        public SdmxObservationReaderPerfomanceTests()
        {
            _mappingStoreDataAccess = new TestMappingStoreDataAccess("sdmx/DF_DROPOUT_RT.xml");
            _dataflow = _mappingStoreDataAccess.GetDataflow();
            var codeListRepository = new TestCodeListRepository(_dataflow);
            _codeTranslator = new CodeTranslator(codeListRepository);
            _reportedComponents = GetReportedComponents(_dataflow);
        }

        [OneTimeSetUp]
        public async Task Init()
        {
            await _codeTranslator.FillDict(_dataflow, CancellationToken);
        }

        [Test]
        public async Task Measure_Observation_Construction_Time()
        {
            var sw = new Stopwatch();
            sw.Start();
            var cnt = 0;
            var observations = ObservationGenerator.Generate(_dataflow, true, 2010, 2010, 1000000);
            foreach (var obs in await observations.ToListAsync())
            {
                cnt++;
            }

            sw.Stop();
            Console.WriteLine(cnt);
            Console.WriteLine($"{(decimal.Divide(sw.ElapsedMilliseconds, 1000)):0.00000}");
        }

        [Test]
        public void Measure_Read_Time()
        {
            var observations = ObservationGenerator.Generate(_dataflow, true, 2010, 2019, 1000000);

            var reader = new SdmxObservationReader(observations, _reportedComponents, _dataflow, _codeTranslator, Configuration, true, false); // 10.49

            var cnt = 0;
            var sw = new Stopwatch();
            sw.Start();

            while (reader.Read())
                cnt++;

            foreach (var e in reader.GetErrors())
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine($"Reads: {cnt}, Processed observations: {reader.CurrentIndex}, Errors: {reader.GetErrors().Count}");
            Console.WriteLine($"{(decimal.Divide(sw.ElapsedMilliseconds, 1000)):0.00000}");
        }
    }
}
