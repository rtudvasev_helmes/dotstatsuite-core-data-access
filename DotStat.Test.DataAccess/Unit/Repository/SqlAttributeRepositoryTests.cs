﻿using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.Test.Moq;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Attribute = DotStat.Domain.Attribute;
using Dataflow = DotStat.Domain.Dataflow;
using DotStat.Db.Dto;
using System.Data;
using DotStat.Db.Exception;
using DotStat.Common.Enums;
using System;
using DotStat.Db.Repository;
using DotStat.Common.Exceptions;

namespace DotStat.Test.DataAccess.Unit.Repository
{
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public class SqlAttributeRepositoryTests : SdmxUnitTestBase
    {
        private readonly SqlAttributeRepository _sqlAttributeRepository;
        private readonly Mock<SqlDotStatDb> _dotStatDbMock;
        public SqlAttributeRepositoryTests()
        {
            _dotStatDbMock = new Mock<SqlDotStatDb>(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            _dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            _dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");

            _sqlAttributeRepository = new SqlAttributeRepository(_dotStatDbMock.Object, Configuration);
        }

        [TestCase("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml")]
        [TestCase("sdmx/NO_ATTRIBUTE_TEST_1.xml")]
        public async Task CreateAttribute(string file)
        {
            var dataflow = GetDataflow(file);
            var attributes = dataflow.Dsd.Attributes;
            var i = 1;
            var dimansionsInDb = dataflow.Dsd.Dimensions.Select(d => new ComponentItem() { 
                    Id = d.Code, DbId = i++, 
                    Type = d.Base.TimeDimension ? DbTypes.GetDbType(SDMXArtefactType.TimeDimension) : DbTypes.GetDbType(SDMXArtefactType.Dimension) }).ToList();

            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            var j = 1;
            foreach (var attribute in attributes)
            {
                _dotStatDbMock.Setup(m =>
                    m.ExecuteScalarSqlWithParamsAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<DbParameter[]>()))
                    .Returns(() => Task.FromResult((object)j));

                await _sqlAttributeRepository.CreateAttribute(attribute, dataflow.Dsd, dimansionsInDb, CancellationToken);
                Assert.AreEqual(j, attribute.DbId);
                
                j++;
            }
        }


        [TestCase]
        public void CreateAttributeDimensionsNotInDb()
        {
            var dataflow = GetDataflow();
            var attributes = dataflow.Dsd.Attributes;
            var dimansionsInDb = new List<ComponentItem>();

            var i = 1;
            foreach (var attribute in attributes)
            {
                _dotStatDbMock.Setup(m =>
                    m.ExecuteScalarSqlWithParamsAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<DbParameter[]>()))
                    .Returns(() => Task.FromResult((object)i));

                if (attribute.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup
                    || attribute.Base.AttachmentLevel == AttributeAttachmentLevel.Group)
                {
                    Assert.ThrowsAsync<DimensionNotFoundByTranslatorException>(
                        async () => await _sqlAttributeRepository.CreateAttribute(attribute, dataflow.Dsd, dimansionsInDb, CancellationToken));
                }
                else
                {
                    Assert.DoesNotThrowAsync(async () => await _sqlAttributeRepository.CreateAttribute(attribute, dataflow.Dsd, dimansionsInDb, CancellationToken));
                    Assert.AreEqual(i, attribute.DbId);
                }
                i++;
            }

        }

        [TestCase]
        public void CreateAttributeNoReferencedDimensions()
        {
            var dataflow = GetDataflow("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml");
            var wrongDataflow = GetDataflow("sdmx/KH_NIS,DF_AGRI_NO_TIME,2.0.xml");
            var attributes = dataflow.Dsd.Attributes;
            var i = 1;

            var dimansionsInDb = dataflow.Dsd.Dimensions.Select(d => new ComponentItem()
            {
                Id = d.Code,
                DbId = i++,
                Type = DbTypes.GetDbType(SDMXArtefactType.Dimension)
            }).ToList();

            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            var j = 1;
            foreach (var attribute in attributes)
            {
                _dotStatDbMock.Setup(m =>
                    m.ExecuteScalarSqlWithParamsAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<DbParameter[]>()))
                    .Returns(() => Task.FromResult((object)j));

                if (attribute.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup
                    || attribute.Base.AttachmentLevel == AttributeAttachmentLevel.Group)
                {
                    var hasDimensionReferences = false;
                    if (attribute.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                    {
                        hasDimensionReferences = attribute.Base.DimensionReferences.Count>0;
                    }
                    else if (attribute.Base.AttachmentLevel == AttributeAttachmentLevel.Group)
                    {
                        hasDimensionReferences = wrongDataflow.Dsd.Base.Groups.FirstOrDefault(g =>
                            g.Id.Equals(attribute.Base.AttachmentGroup, StringComparison.InvariantCultureIgnoreCase))?.DimensionRefs.Count > 0;
                    }

                    //TEST attribute referencing a dimension which is not part of the DSD
                    if(hasDimensionReferences)
                        Assert.ThrowsAsync<DotStatException>(
                            async () => await _sqlAttributeRepository.CreateAttribute(attribute, wrongDataflow.Dsd, dimansionsInDb, CancellationToken));
                }
                else
                {
                    Assert.DoesNotThrowAsync(async () => await _sqlAttributeRepository.CreateAttribute(attribute, dataflow.Dsd, dimansionsInDb, CancellationToken));
                    Assert.AreEqual(j, attribute.DbId);
                }
                j++;
            }

        }


        //TODO: complete unit tests
        //[TestCase("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml")]
        //[TestCase("sdmx/NO_ATTRIBUTE_TEST_1.xml")]
        //public async Task GetDatasetAttributes(string file)
        //{
        //    var dataflow = GetDataflow(file);
        //    var dsdAttributes = dataflow.Dsd.Attributes.Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet);

        //    //Setup mocks
        //    SetDotStatDbMock(dataflow, dsdAttributes.ToList());

        //    var codelistProjection = new Mock<ICodelistProjection>(new string[] { });
        //    codelistProjection.Setup(c => c[It.IsAny<int>()]).Returns("code");
        //    var codeListRepository = new Mock<ICodelistRepository>();
        //    var codeTranslator = new Mock<ICodeTranslator>();

        //    codeListRepository.Setup(c =>
        //        c.GetDimensionCodesFromDb(It.IsAny<int>(), It.IsAny<CancellationToken>()))
        //        .Returns(Task.FromResult(new string[] { "1", "2"}));

        //    codeTranslator.Setup(c =>
        //        c[It.IsAny<Attribute>(), It.IsAny<CancellationToken>()])
        //        .Returns(new ValueTask<ICodelistProjection>(codelistProjection.Object));
            
        //    //TODO: mock datareader access by column name
        //    //var datasetAttributesEnumerable = _sqlAttributeRepository.GetDatasetAttributes(dataflow, codeTranslator.Object, DbTableVersion.A, CancellationToken);
           
        //    //var datasetAttributesInDb = await datasetAttributesEnumerable.ToListAsync();
        //    //if (dsdAttributes.Any())
        //    //{
        //    //    Assert.Equals(dsdAttributes.Count(), datasetAttributesInDb.Count);
        //    //    foreach(var dsdAttr in dsdAttributes)
        //    //    {
        //    //        var dbAttr = datasetAttributesInDb.FirstOrDefault(dbAttr => dbAttr.Code == dsdAttr.Code);
        //    //        Assert.IsNotNull(dbAttr);
        //    //        Assert.Equals(dsdAttr.Base.AttachmentLevel, AttributeAttachmentLevel.DataSet);
        //    //        Assert.Equals(dsdAttr.Base.Id, dbAttr.Concept);
        //    //        Assert.Equals(dsdAttr.DbId, 1);
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    Assert.Equals(datasetAttributesInDb.Count(), 0);
        //    //}
        //}

        private void SetDotStatDbMock(Dataflow dataflow, List<Attribute> attributes, bool resetMock = true)
        {
            if(resetMock) _dotStatDbMock.Reset();
            var mockAttributes = new List<string[]>();
            var dbArray = new[] { $"{dataflow.FullId}" };
            if (attributes.Count > 0) { 
                for (var i = 1; i < attributes.Count; i++)
                {
                    dbArray.Append($"{i}");
                }
                mockAttributes.Add(dbArray);
            }

            DbDataReader dbDataReader = new TestDbDataReader(mockAttributes);

            _dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(dbDataReader));
        }


    }
}