﻿using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Db.Repository.SqlServer;
using DotStat.Domain;
using DotStat.Test.Moq;
using Moq;
using NUnit.Framework;
using System;
using System.Data;
using DotStat.Common.Exceptions;

namespace DotStat.Test.DataAccess.Unit.Repository
{
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public class SqlCodelistRepositoryTests : SdmxUnitTestBase
    {
        private readonly SqlCodelistRepository _sqlCodelistRepository;
        private readonly Mock<SqlDotStatDb> _dotStatDbMock;
        private readonly Dataflow _dataflow;
        public SqlCodelistRepositoryTests()
        {
            _dataflow = GetDataflow();

            _dotStatDbMock = new Mock<SqlDotStatDb>(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            _dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            _dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");

            _sqlCodelistRepository = new SqlCodelistRepository(_dotStatDbMock.Object, Configuration);
        }

        [TestCase]
        public async Task GetDimensionCodesFromDb()
        {
            var dimensions = _dataflow.Dsd.Dimensions;
            for(var i = 0; i< dimensions.Count; i++)
            {
                SetDotStatDbMock(dimensions[i]?.Codelist?.Codes);
                if (dimensions[i].Base.TimeDimension || !dimensions[i].Base.HasCodedRepresentation())
                {
                    Assert.ThrowsAsync<DotStatException>(() => _sqlCodelistRepository.GetDimensionCodesFromDb(i, CancellationToken));
                }
                else
                {
                    var dimensionCodes = await _sqlCodelistRepository.GetDimensionCodesFromDb(i, CancellationToken);
                    Assert.AreEqual(dimensions[i].Codelist?.Codes?.Count, dimensionCodes.Count());
                    for(var j = 0; j < dimensions[i].Codelist?.Codes?.Count; j++)
                    {
                        Assert.AreEqual(dimensions[i].Codelist?.Codes[j].Code, dimensionCodes[j]);
                    }
                }
            }
        }

        private void SetDotStatDbMock(IReadOnlyList<Code> codes)
        {
            _dotStatDbMock.Reset();
            var codeMaps = new List<string[]>();
            if (codes != null) { 
                for (var i = codes.Count - 1; i > -1; i--)
                {
                    codeMaps.Add(new[] { $"{i}", codes[i].Base.Id });
                } 
            }

            DbDataReader dbDataReader = new TestDbDataReader(codeMaps);

            _dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>()))
                .Returns(() => Task.FromResult(dbDataReader));
        }
    }
}