﻿using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Db.Repository.SqlServer;
using DotStat.Domain;
using DotStat.Test.Moq;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Dataflow = DotStat.Domain.Dataflow;
using System;
using System.Data;
using DotStat.Db.Validation;
using System.Data.SqlClient;
using DotStat.Common.Configuration.Dto;
using DotStat.Db;
using DotStat.DB.Repository;
using DotStat.Db.Util;
using DotStat.DB.Util;

namespace DotStat.Test.DataAccess.Unit.Repository
{
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public class SqlDataStoreRepositoryTests : SdmxUnitTestBase
    {
        private readonly SqlDataStoreRepository _repository;
        private readonly Mock<SqlDotStatDb> _dotStatDbMock;
        private readonly Dataflow _dataflow;
        private IUnitOfWork _unitOfWork;
        private readonly DataspaceInternal _dataSpace;
        public SqlDataStoreRepositoryTests()
        {
            _dataflow = GetDataflow();

            _dotStatDbMock = new Mock<SqlDotStatDb>(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            _dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            _dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");

            _repository = new SqlDataStoreRepository(_dotStatDbMock.Object, Configuration);
            _dataSpace = Configuration.SpacesInternal.FirstOrDefault();
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            InitObjects();
        }

        private void InitObjects()
        {
            SqlDotStatDb dotStatDb = new SqlDotStatDb(
                _dataSpace,
                DbUp.DatabaseVersion.DataDbVersion
            );
            
            _unitOfWork = new UnitOfWork(
                dotStatDb,
                new SqlArtefactRepository(dotStatDb, Configuration),
                new SqlAttributeRepository(dotStatDb, Configuration),
                new SqlDataStoreRepository(dotStatDb, Configuration),
                new SqlMetadataStoreRepository(dotStatDb, Configuration),
                new SqlMetadataComponentRepository(dotStatDb, Configuration),
                new SqlObservationRepository(dotStatDb, Configuration),
                new SqlTransactionRepository(dotStatDb, Configuration),
                new SqlComponentRepository(dotStatDb, Configuration),
                new SqlCodelistRepository(dotStatDb, Configuration)
            );
        }
        //TODO: complete unit tests
        //[TestCase]
        //public async Task BulkInsertData()
        //{
        //    // TODO: SqlBulkCopy not testable as is
        //    // As an option, create an Interface 'IBulkCopy' and pass it as parameter of BulkInsertData
        //}

        [TestCase]
        public void DropStagingTables()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async ()=> 
                await _repository.DropStagingTables(_dataflow.Dsd, CancellationToken));
        }

        [TestCase]
        public void RecreateStagingTables()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            var reportedComponents = GetReportedComponents(_dataflow);

            Assert.DoesNotThrowAsync(async () =>
                await _repository.RecreateStagingTables(
                    _dataflow.Dsd, reportedComponents, _dataflow.Dsd.TimeDimension != null, CancellationToken));

            // Primary measure not reported
            reportedComponents.IsPrimaryMeasureReported = false;

            Assert.DoesNotThrowAsync(async () =>
                await _repository.RecreateStagingTables(
                    _dataflow.Dsd, reportedComponents, _dataflow.Dsd.TimeDimension != null, CancellationToken));

            //Not all attributes reported
            reportedComponents.ObservationAttributes = new List<Domain.Attribute> { reportedComponents.ObservationAttributes.FirstOrDefault() };

            Assert.DoesNotThrowAsync(async () =>
                await _repository.RecreateStagingTables(
                    _dataflow.Dsd, reportedComponents, _dataflow.Dsd.TimeDimension != null, CancellationToken));

            //Not all Dimensions reported
            reportedComponents.Dimensions = new List<Dimension> { _dataflow.Dimensions.FirstOrDefault()};
            reportedComponents.TimeDimension = null;

            Assert.DoesNotThrowAsync(async () =>
                await _repository.RecreateStagingTables(
                    _dataflow.Dsd, reportedComponents, _dataflow.Dsd.TimeDimension != null, CancellationToken));
        }

        [TestCase]
        public void AddIndexStagingTable()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(1));

            //Attributes
            var reportedComponents = new ReportedComponents
            {
                DatasetAttributes = _dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList(),
                SeriesAttributes = _dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                    .ToList(),
                ObservationAttributes = _dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation).ToList()
            };

            Assert.DoesNotThrowAsync(async () =>
                await _repository.AddIndexStagingTable(_dataflow.Dsd, reportedComponents, CancellationToken));

            // Primary measure
            reportedComponents = new ReportedComponents
            {
                IsPrimaryMeasureReported = true
            };

            Assert.DoesNotThrowAsync(async () =>
                await _repository.AddIndexStagingTable(_dataflow.Dsd, reportedComponents, CancellationToken));

            // Dimensions
            reportedComponents = new ReportedComponents
            {
                Dimensions = _dataflow.Dsd.Dimensions.ToList()
            };

            Assert.DoesNotThrowAsync(async () =>
                await _repository.AddIndexStagingTable(_dataflow.Dsd, reportedComponents, CancellationToken));

        }

        [TestCase]
        public void DeleteData()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.DeleteData(_dataflow.Dsd, DbTableVersion.A, CancellationToken));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.DeleteData(_dataflow.Dsd, DbTableVersion.B, CancellationToken));
        }

        [TestCase]
        public void CopyDataToNewVersion()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.CopyDataToNewVersion(_dataflow.Dsd, DbTableVersion.A, DbTableVersion.B, CancellationToken));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.CopyDataToNewVersion(_dataflow.Dsd, DbTableVersion.B, DbTableVersion.A, CancellationToken));

            Assert.ThrowsAsync<ArgumentException>(async () =>
                await _repository.CopyDataToNewVersion(_dataflow.Dsd, DbTableVersion.A, DbTableVersion.A, CancellationToken));
            
            Assert.ThrowsAsync<ArgumentException>(async () =>
                await _repository.CopyDataToNewVersion(_dataflow.Dsd, DbTableVersion.B, DbTableVersion.B, CancellationToken));
        }

        [TestCase]
        public async Task SetBatchNumberToStagingTableRows()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                    m.ExecuteScalarSqlAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult((object)1));

            var reportedComponents = GetReportedComponents(_dataflow);
            var totalNumberObBatches = await _repository.SetBatchNumberToStagingTableRows(_dataflow.Dsd, reportedComponents, CancellationToken);
            Assert.AreEqual(1, totalNumberObBatches);
        }

        [TestCase]
        public void MergeStagingToFilterTable()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(1));
            
            var reportedComponents = GetReportedComponents(_dataflow);
            
            Assert.DoesNotThrowAsync(async () =>
                await _repository.MergeStagingToFilterTable(_dataflow.Dsd, reportedComponents, CancellationToken));
        }

        [TestCase]
        public async Task MergeStagingToFactTable()
        {
            _dotStatDbMock.Reset();

            var reportedComponents = GetReportedComponents(_dataflow);
            var bulkImportResult = new BulkImportResult
            {
                RowsCopied = 100,
                NumberOfRowsToMerge = 25,
                NumberOfRowsToDelete = 50
            };

            DbDataReader dbDataReader = new TestDbDataReader(
                    new List<object[]> { new object[]
                    {
                        bulkImportResult.RowsCopied - bulkImportResult.NumberOfRowsToMerge - bulkImportResult.NumberOfRowsToDelete,//Inserted
                        bulkImportResult.NumberOfRowsToMerge,//Updated
                        bulkImportResult.NumberOfRowsToDelete,//Deleted
                        bulkImportResult.RowsCopied//total
                    } });

            //Basic validation
            _dotStatDbMock.Setup(m =>
                    m.ExecuteNonQuerySqlWithParamsAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));
            
            var mergeResult = await _repository.MergeStagingToFactTable(_dataflow.Dsd, reportedComponents, DbTableVersion.A, false, 0, bulkImportResult, CancellationToken);
            Assert.AreEqual(0, mergeResult.TotalCount);
            Assert.AreEqual(0, mergeResult.InsertCount);
            Assert.AreEqual(0, mergeResult.UpdateCount);
            Assert.AreEqual(0, mergeResult.DeleteCount);
            Assert.AreEqual(0, mergeResult.Errors.Count);

            //Advance validation
            _dotStatDbMock.Setup(m =>
                    m.ExecuteReaderSqlWithParamsAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<CommandBehavior>(),
                        It.IsAny<bool>(),
                        It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(dbDataReader));
            var mergeResultWithSummary = await _repository.MergeStagingToFactTable(_dataflow.Dsd, reportedComponents, DbTableVersion.A, true, 0, bulkImportResult, CancellationToken);
            Assert.AreEqual(bulkImportResult.RowsCopied, mergeResultWithSummary.TotalCount);
            Assert.AreEqual(bulkImportResult.RowsCopied - bulkImportResult.NumberOfRowsToMerge - bulkImportResult.NumberOfRowsToDelete, mergeResultWithSummary.InsertCount);
            Assert.AreEqual(bulkImportResult.NumberOfRowsToMerge, mergeResultWithSummary.UpdateCount);
            Assert.AreEqual(bulkImportResult.NumberOfRowsToDelete, mergeResultWithSummary.DeleteCount);
            Assert.AreEqual(0, mergeResult.Errors.Count);

            //Duplicate rows key violation
            _dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>(),
                    It.IsAny<DbParameter[]>()))
                .Throws(SqlExceptionHelper.NewSqlException(2627, "duplicated rows"));
            
            mergeResult = await _repository.MergeStagingToFactTable(_dataflow.Dsd, reportedComponents, DbTableVersion.A, true, 0, bulkImportResult, CancellationToken);
            Assert.AreEqual(1, mergeResult.Errors.Count);
            Assert.AreEqual(ValidationErrorType.DuplicatedRowsInStagingTable, mergeResult.Errors[0].Type);

            //Duplicate rows multiple rows modifying the same destination rows
            _dotStatDbMock.Setup(m =>
                    m.ExecuteReaderSqlWithParamsAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<CommandBehavior>(),
                        It.IsAny<bool>(),
                        It.IsAny<DbParameter[]>()))
                .Throws(SqlExceptionHelper.NewSqlException(8672, "multiple rows modifying same region"));

            _dotStatDbMock.Setup(m =>
                    m.ExecuteScalarSqlAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult((object)1));

            mergeResult = await _repository.MergeStagingToFactTable(_dataflow.Dsd, reportedComponents, DbTableVersion.A, true, 0, bulkImportResult, CancellationToken);
            Assert.AreEqual(1, mergeResult.Errors.Count);
            Assert.AreEqual(ValidationErrorType.DuplicatedRowsInStagingTable, mergeResult.Errors[0].Type);

            //Multiple Rows Modifying The Same Region
            _dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>(),
                    It.IsAny<DbParameter[]>()))
                .Throws(SqlExceptionHelper.NewSqlException(8672, "multiple rows modifying same region"));

            _dotStatDbMock.Setup(m =>
                    m.ExecuteScalarSqlAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult((object)null));

            mergeResult = await _repository.MergeStagingToFactTable(_dataflow.Dsd, reportedComponents, DbTableVersion.A, true, 0, bulkImportResult, CancellationToken);
            Assert.AreEqual(1, mergeResult.Errors.Count);
            Assert.AreEqual(ValidationErrorType.MultipleRowsModifyingTheSameRegionInStagingTable, mergeResult.Errors[0].Type);

            //Other SQL error 
            _dotStatDbMock.Setup(m =>
                    m.ExecuteReaderSqlWithParamsAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<CommandBehavior>(),
                        It.IsAny<bool>(),
                        It.IsAny<DbParameter[]>()))
                .Throws(SqlExceptionHelper.NewSqlException(1, "other error"));
            
            Assert.ThrowsAsync<SqlException>(async () =>
                await _repository.MergeStagingToFactTable(_dataflow.Dsd, reportedComponents, DbTableVersion.A, true, 0, bulkImportResult, CancellationToken));
        }

        [TestCase]
        public async Task MergeStagingToAttrTable()
        {
            _dotStatDbMock.Reset();

            var dataFlow = GetDataflow("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml");
            var reportedComponents = GetReportedComponents(dataFlow);
            var bulkImportResult = new BulkImportResult
            {
                RowsCopied = 100,
                NumberOfRowsToMerge = 25,
                NumberOfRowsToDelete = 50
            };

            DbDataReader dbDataReader = new TestDbDataReader(
                    new List<object[]> { new object[]
                    {
                        bulkImportResult.RowsCopied - bulkImportResult.NumberOfRowsToMerge - bulkImportResult.NumberOfRowsToDelete,//Inserted
                        bulkImportResult.NumberOfRowsToMerge,//Updated
                        bulkImportResult.NumberOfRowsToDelete,//Deleted
                        bulkImportResult.RowsCopied//total
                    } });

            //Basic validation
            _dotStatDbMock.Setup(m =>
                    m.ExecuteNonQuerySqlAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(1));
            
            var mergeResult = await _repository.MergeStagingToAttrTable(dataFlow.Dsd, reportedComponents, DbTableVersion.A, false, 0, bulkImportResult, CancellationToken);
            Assert.AreEqual(0, mergeResult.TotalCount);
            Assert.AreEqual(0, mergeResult.InsertCount);
            Assert.AreEqual(0, mergeResult.UpdateCount);
            Assert.AreEqual(0, mergeResult.DeleteCount);
            Assert.AreEqual(0, mergeResult.Errors.Count);

            //Advance validation
            _dotStatDbMock.Setup(m =>
                    m.ExecuteReaderSqlAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<CommandBehavior>(),
                        It.IsAny<bool>()))
                .Returns(() => Task.FromResult(dbDataReader));

            var mergeResultWithSummary = await _repository.MergeStagingToAttrTable(dataFlow.Dsd, reportedComponents, DbTableVersion.A, true, 0, bulkImportResult, CancellationToken);
            Assert.AreEqual(bulkImportResult.RowsCopied, mergeResultWithSummary.TotalCount);
            Assert.AreEqual(bulkImportResult.RowsCopied - bulkImportResult.NumberOfRowsToMerge - bulkImportResult.NumberOfRowsToDelete, mergeResultWithSummary.InsertCount);
            Assert.AreEqual(bulkImportResult.NumberOfRowsToMerge, mergeResultWithSummary.UpdateCount);
            Assert.AreEqual(bulkImportResult.NumberOfRowsToDelete, mergeResultWithSummary.DeleteCount);
            Assert.AreEqual(0, mergeResult.Errors.Count);

            //Duplicate rows Key violation
            _dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>()))
                .Throws(SqlExceptionHelper.NewSqlException(2627, "duplicated rows"));

            mergeResult = await _repository.MergeStagingToAttrTable(dataFlow.Dsd, reportedComponents, DbTableVersion.A, true, 0, bulkImportResult, CancellationToken);
            Assert.AreEqual(1, mergeResult.Errors.Count);
            Assert.AreEqual(ValidationErrorType.DuplicatedRowsInStagingTable, mergeResult.Errors[0].Type);

            //Duplicate rows multiple rows modifying the same destination rows
            _dotStatDbMock.Setup(m =>
                    m.ExecuteReaderSqlAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<CommandBehavior>(),
                        It.IsAny<bool>()))
                .Throws(SqlExceptionHelper.NewSqlException(8672, "multiple rows modifying same region"));

            _dotStatDbMock.Setup(m =>
                    m.ExecuteScalarSqlAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult((object)1));

            mergeResult = await _repository.MergeStagingToAttrTable(dataFlow.Dsd, reportedComponents, DbTableVersion.A, true, 0, bulkImportResult, CancellationToken);
            Assert.AreEqual(1, mergeResult.Errors.Count);
            Assert.AreEqual(ValidationErrorType.DuplicatedRowsInStagingTable, mergeResult.Errors[0].Type);


            //Multiple Rows Modifying The Same Region
            _dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>()))
                .Throws(SqlExceptionHelper.NewSqlException(8672, "multiple rows modifying same region"));

            _dotStatDbMock.Setup(m =>
                    m.ExecuteScalarSqlAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult((object)null));

            mergeResult = await _repository.MergeStagingToAttrTable(dataFlow.Dsd, reportedComponents, DbTableVersion.A, true, 0, bulkImportResult, CancellationToken);
            Assert.AreEqual(1, mergeResult.Errors.Count);
            Assert.AreEqual(ValidationErrorType.MultipleRowsModifyingTheSameRegionInStagingTable, mergeResult.Errors[0].Type);

            //Other SQL error 
            _dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<CommandBehavior>(),
                        It.IsAny<bool>()))
                .Throws(SqlExceptionHelper.NewSqlException(1, "other error"));

            Assert.ThrowsAsync<SqlException>(async () =>
                await _repository.MergeStagingToAttrTable(dataFlow.Dsd, reportedComponents, DbTableVersion.A, true, 0, bulkImportResult, CancellationToken));
        }

        //TODO: mock codeTranslator
        //[TestCase]
        //public async Task MergeStagingToDatasetTable()
        //{
        //    _dotStatDbMock.Reset();
           
        //    var bulkImportResult = new BulkImportResult
        //    {
        //        RowsCopied = 5,
        //        NumberOfRowsToMerge = 3,
        //        NumberOfRowsToDelete = 2
        //    };

        //    var codeTranslator = new CodeTranslator(_unitOfWork.CodeListRepository);
        //    await codeTranslator.FillDict(_dataflow, CancellationToken);
            
        //    _dotStatDbMock.Setup(m =>
        //        m.ExecuteNonQuerySqlWithParamsAsync(
        //            It.IsAny<string>(),
        //            It.IsAny<CancellationToken>()))
        //        .Returns(() => Task.FromResult(1));

        //    //Basic validation
        //    var mergeResult = await _repository.MergeStagingToDatasetAttributes(_dataflow, new List<DataSetAttributeRow>(),
        //        codeTranslator, DbTableVersion.A, false, CancellationToken);
        //    Assert.AreEqual(bulkImportResult.RowsCopied, mergeResult.TotalCount);
        //    Assert.AreEqual(0, mergeResult.InsertCount);
        //    Assert.AreEqual(0, mergeResult.UpdateCount);
        //    Assert.AreEqual(0, mergeResult.DeleteCount);

        //    DbDataReader dbDataReader = new TestDbDataReader(
        //        new List<object[]> { new object[]
        //        {
        //            0,
        //            bulkImportResult.NumberOfRowsToMerge
        //        } });

        //    _dotStatDbMock.Setup(m =>
        //            m.ExecuteReaderSqlWithParamsAsync(
        //                It.IsAny<string>(),
        //                It.IsAny<CancellationToken>(),
        //                It.IsAny<CommandBehavior>(),
        //                It.IsAny<bool>()))
        //        .Returns(() => Task.FromResult(dbDataReader));

            
        //    //Advance validation
        //    var mergeResultWithSummary = await _repository.MergeStagingToDatasetAttributes(_dataflow, new List<DataSetAttributeRow>(),
        //        codeTranslator, DbTableVersion.A, false, CancellationToken);
        //    Assert.AreEqual(bulkImportResult.RowsCopied, mergeResultWithSummary.TotalCount);
        //    Assert.AreEqual(bulkImportResult.RowsCopied - bulkImportResult.NumberOfRowsToMerge - bulkImportResult.NumberOfRowsToDelete, mergeResultWithSummary.InsertCount);
        //    Assert.AreEqual(bulkImportResult.NumberOfRowsToMerge, mergeResultWithSummary.UpdateCount);
        //    Assert.AreEqual(bulkImportResult.NumberOfRowsToDelete, mergeResultWithSummary.DeleteCount);
            
        //    //Other SQL error 
        //    _dotStatDbMock.Setup(m =>
        //        m.ExecuteScalarSqlWithParamsAsync(
        //            It.IsAny<string>(),
        //            It.IsAny<CancellationToken>(),
        //            It.IsAny<DbParameter[]>()))
        //        .Throws(SqlExceptionHelper.NewSqlException(1, "other error"));

        //    // Assign merge batch number
        //    Assert.ThrowsAsync<SqlException>(async () =>
        //        await _repository.MergeStagingToDatasetAttributes(_dataflow, new List<DataSetAttributeRow>(),
        //            codeTranslator, DbTableVersion.A, false, CancellationToken));
        //}

        [TestCase]
        public void UpdateStatisticsOfFactTable()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.UpdateStatisticsOfFactTable(_dataflow.Dsd, DbTableVersion.A, CancellationToken));
            Assert.DoesNotThrowAsync(async () =>
                await _repository.UpdateStatisticsOfFactTable(_dataflow.Dsd, DbTableVersion.B, CancellationToken));

        }

        [TestCase]
        public void UpdateStatisticsOfFilterTable()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.UpdateStatisticsOfFilterTable(_dataflow.Dsd, CancellationToken));

        }
        
        [TestCase]
        public void CopyAttributesToNewVersion()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.CopyAttributesToNewVersion(_dataflow.Dsd, DbTableVersion.A, DbTableVersion.B, CancellationToken));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.CopyAttributesToNewVersion(_dataflow.Dsd, DbTableVersion.B, DbTableVersion.A, CancellationToken));

            Assert.ThrowsAsync<ArgumentException>(async () =>
                await _repository.CopyAttributesToNewVersion(_dataflow.Dsd, DbTableVersion.A, DbTableVersion.A, CancellationToken));
            
            Assert.ThrowsAsync<ArgumentException>(async () =>
                    await _repository.CopyAttributesToNewVersion(_dataflow.Dsd, DbTableVersion.B, DbTableVersion.B, CancellationToken));
        }

        //TODO: complete unit tests
        //[TestCase]
        //public async Task MergeStagingToDatasetAttributes()
        //{
        //   // _dotStatDbMock.Reset();
        //   // _dotStatDbMock.Setup(m =>
        //   //     m.ExecuteNonQuerySqlWithParamsAsync(
        //   //         It.IsAny<string>(),
        //   //         It.IsAny<CancellationToken>(),
        //   //         It.IsAny<DbParameter[]>()))
        //   //     .Returns(() => Task.FromResult(1));

        //   // Assert.DoesNotThrowAsync(async () =>
        //   //     await _repository.MergeStagingToDatasetAttributes(_dataflow.Dsd, DbTableVersion.A, DbTableVersion.B, CancellationToken));

        //   //// DimensionNotFoundByTranslatorException
        //   ////CodeNotFoundByTranslatorException
        //   // Assert.ThrowsAsync<ArgumentException>(async () =>
        //   //     await _repository.MergeStagingToDatasetAttributes(_dataflow.Dsd, DbTableVersion.A, DbTableVersion.A, CancellationToken));

        //}

        [TestCase]
        public void DropDsdTables()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.DropDsdTables(_dataflow.Dsd, CancellationToken));
        }

        [TestCase]
        public void DropAttributeTables()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.DropAttributeTables(_dataflow.Dsd, CancellationToken));
        }

        [TestCase]
        public void TruncateDsdTables()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.TruncateDsdTables(_dataflow.Dsd, DbTableVersion.A, CancellationToken));
            Assert.DoesNotThrowAsync(async () =>
                await _repository.TruncateDsdTables(_dataflow.Dsd, DbTableVersion.B, CancellationToken));
        }
        
        [TestCase]
        public void TruncateAttributeTables()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.TruncateAttributeTables(_dataflow.Dsd, DbTableVersion.A, CancellationToken));
            Assert.DoesNotThrowAsync(async () =>
                await _repository.TruncateAttributeTables(_dataflow.Dsd, DbTableVersion.B, CancellationToken));
        }

        [TestCase]
        public void GetDataViewQuery()
        {
            var tableVersion = (char)DbTableVersion.A;
            _dataflow.DbId = 1;
            var viewQueryTemplate = $"SELECT [SID], [FREQ],[REF_AREA],[IND_TYPE],[ADJUSTMENT],[MEASURE],[ETA],[SESSO],[GRADO_ISTRUZ],[CATEG_PROF],[TIME_PERIOD] AS [TIME_PERIOD],[PERIOD_START],[PERIOD_END],[OBS_VALUE],[COMMENT],[CONF_STATUS],[OBS_PRE_BREAK],[OBS_STATUS],[DECIMALS],[NOTA_IT],[NOTA_EN],[BREAK],[COLLECTION],[TIME_FORMAT],[TITLE],[METADATA_EN],[METADATA_IT],[LAST_UPDATED],[LAST_STATUS] FROM [data].[VI_CurrentDataDataFlow_{_dataflow.DbId}_{tableVersion}]";
            var viewQuery = _repository.GetDataViewQuery(_dataflow, tableVersion);
            Assert.AreEqual(viewQueryTemplate, viewQuery);

            tableVersion = (char)DbTableVersion.B;
            _dataflow.DbId = 2; 
            viewQueryTemplate = $"SELECT [SID], [FREQ],[REF_AREA],[IND_TYPE],[ADJUSTMENT],[MEASURE],[ETA],[SESSO],[GRADO_ISTRUZ],[CATEG_PROF],[TIME_PERIOD] AS [TIME_PERIOD],[PERIOD_START],[PERIOD_END],[OBS_VALUE],[COMMENT],[CONF_STATUS],[OBS_PRE_BREAK],[OBS_STATUS],[DECIMALS],[NOTA_IT],[NOTA_EN],[BREAK],[COLLECTION],[TIME_FORMAT],[TITLE],[METADATA_EN],[METADATA_IT],[LAST_UPDATED],[LAST_STATUS] FROM [data].[VI_CurrentDataDataFlow_{_dataflow.DbId}_{tableVersion}]";

            viewQuery = _repository.GetDataViewQuery(_dataflow, tableVersion);
            Assert.AreEqual(viewQueryTemplate, viewQuery);
        }

        [TestCase]
        public void GetEmptyDataViewQuery()
        {
            var emptyQuery = $"SELECT TOP 0 NULL AS [SID], NULL AS [FREQ],NULL AS [REF_AREA],NULL AS [IND_TYPE],NULL AS [ADJUSTMENT],NULL AS [MEASURE],NULL AS [ETA],NULL AS [SESSO],NULL AS [GRADO_ISTRUZ],NULL AS [CATEG_PROF],NULL AS [TIME_PERIOD],NULL AS [PERIOD_START],NULL AS [PERIOD_END],NULL AS [OBS_VALUE],NULL AS [COMMENT],NULL AS [CONF_STATUS],NULL AS [OBS_PRE_BREAK],NULL AS [OBS_STATUS],NULL AS [DECIMALS],NULL AS [NOTA_IT],NULL AS [NOTA_EN],NULL AS [BREAK],NULL AS [COLLECTION],NULL AS [TIME_FORMAT],NULL AS [TITLE],NULL AS [METADATA_EN],NULL AS [METADATA_IT],NULL as [LAST_UPDATED],NULL as [LAST_STATUS]";
            var viewQuery = _repository.GetEmptyDataViewQuery(_dataflow);
            Assert.AreEqual(emptyQuery, viewQuery);
        }

        [TestCase("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml")]
        [TestCase("sdmx/KH_NIS,DF_AGRI_NO_TIME,2.0.xml")]
        public void GetOrderByClause(string file)
        {
            var dataflow = GetDataflow(file);
            var hasTimeDim = dataflow.Dsd.Base.TimeDimension != null;

            if (hasTimeDim)
            {
                var orderBy = "[SID] ASC,[PERIOD_START] ASC,[PERIOD_END] DESC";
                var orderByAsc = _repository.GetOrderByClause(dataflow);
                Assert.AreEqual(orderBy, orderByAsc);


                orderBy = "[SID] ASC,[PERIOD_START] DESC,[PERIOD_END] ASC";
                var orderByDesc = _repository.GetOrderByClause(dataflow, false);
                Assert.AreEqual(orderBy, orderByDesc);
            }
            else
            {
                var orderBy = "[SID] ASC";
                var orderByNoTime = _repository.GetOrderByClause(dataflow, false);
                Assert.AreEqual(orderBy, orderByNoTime);
                
            }
        }

        [TestCase]
        public async Task DataTablesExist()
        {
            _dotStatDbMock.Reset();
            var tableName = _dataflow.Dsd.SqlFactTable((char)DbTableVersion.A);
            _dotStatDbMock.Setup(m =>
                m.TableExists(
                    tableName,
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(true));
            
            tableName = _dataflow.Dsd.SqlFactTable((char)DbTableVersion.B);
            _dotStatDbMock.Setup(m =>
                m.TableExists(
                    tableName,
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(false));

            Assert.AreEqual(true, await _repository.DataTablesExist(_dataflow.Dsd, CancellationToken));

            tableName = _dataflow.Dsd.SqlFactTable((char)DbTableVersion.A);
            _dotStatDbMock.Setup(m =>
                m.TableExists(
                    tableName,
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(false));

            tableName = _dataflow.Dsd.SqlFactTable((char)DbTableVersion.B);
            _dotStatDbMock.Setup(m =>
                m.TableExists(
                    tableName,
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(false));

            Assert.AreEqual(false, await _repository.DataTablesExist(_dataflow.Dsd, CancellationToken));
        }

    }
}