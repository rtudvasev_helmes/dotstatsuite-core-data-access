﻿using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Db.Repository.SqlServer;
using DotStat.Domain;
using DotStat.Test.Moq;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Dataflow = DotStat.Domain.Dataflow;
using System;
using System.Collections.Generic;
using DotStat.Db.Validation;
using System.Data.SqlClient;
using DotStat.Db;
using System.Data;
using DotStat.DB.Util;

namespace DotStat.Test.DataAccess.Unit.Repository
{
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public class SqlMetadataStoreRepositoryTests : SdmxUnitTestBase
    {
        private readonly SqlMetadataStoreRepository _repository;
        private readonly Mock<SqlDotStatDb> _dotStatDbMock;
        private readonly Dataflow _dataflow;

        public SqlMetadataStoreRepositoryTests()
        {
            _dataflow = GetDataflow();

            _dotStatDbMock = new Mock<SqlDotStatDb>(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            _dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            _dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");

            _repository = new SqlMetadataStoreRepository(_dotStatDbMock.Object, Configuration);
        }

        //TODO: complete unit tests
        //[TestCase]
        //public async Task BulkInsertMetadata()
        //{
        //    // TODO: SqlBulkCopy not testable as is
        //    // As an option, create an Interface 'IBulkCopy' and pass it as parameter of BulkInsertMetadata
        //}

        [TestCase]
        public void DropMetadataStagingTables()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async ()=> 
                await _repository.DropMetadataStagingTables(_dataflow.Dsd, CancellationToken));
        }

        [TestCase]
        public void DropMetadataStagingTablesWithDbId()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.DropMetadataStagingTables(_dataflow.Dsd.DbId, CancellationToken));
        }

        [TestCase]
        public void RecreateMetadataStagingTables()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            //Attributes
            var reportedComponents = new ReportedComponents
            {
                DatasetAttributes = _dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList(),
                SeriesAttributes = _dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                    .ToList(),
                ObservationAttributes = _dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation).ToList()
            };            

            Assert.DoesNotThrowAsync(async () =>
                await _repository.RecreateMetadataStagingTables(
                    _dataflow, reportedComponents, _dataflow.Dsd.TimeDimension != null, CancellationToken));
            
            // Primary measure
            reportedComponents = new ReportedComponents
            {
                IsPrimaryMeasureReported = true
            };

            Assert.DoesNotThrowAsync(async () =>
                await _repository.RecreateMetadataStagingTables(
                    _dataflow, reportedComponents, _dataflow.Dsd.TimeDimension != null, CancellationToken));

            // Dimensions 
            reportedComponents = new ReportedComponents
            {
                Dimensions = _dataflow.Dsd.Dimensions.ToList()
            };

            Assert.DoesNotThrowAsync(async () =>
                await _repository.RecreateMetadataStagingTables(
                    _dataflow, reportedComponents, _dataflow.Dsd.TimeDimension != null, CancellationToken));
        }

        [TestCase("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml")]
        [TestCase("sdmx/CsvV2.xml")]
        public void AddIndexMetadataStagingTable(string file)
        {
            var dataflow = GetDataflow(file);
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(1));

            //Attributes
            var reportedComponents = new ReportedComponents
            {
                DatasetAttributes = _dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList(),
                SeriesAttributes = _dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                    .ToList(),
                ObservationAttributes = _dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation).ToList()
            };

            Assert.DoesNotThrowAsync(async () =>
                await _repository.AddIndexMetadataStagingTable(dataflow, reportedComponents, CancellationToken));

            // Primary measure
            reportedComponents = new ReportedComponents
            {
                IsPrimaryMeasureReported = true
            };

            Assert.DoesNotThrowAsync(async () =>
                await _repository.AddIndexMetadataStagingTable(dataflow, reportedComponents, CancellationToken));

            // Dimensions
            reportedComponents = new ReportedComponents
            {
                Dimensions = _dataflow.Dsd.Dimensions.ToList()
            };

            Assert.DoesNotThrowAsync(async () =>
                await _repository.AddIndexMetadataStagingTable(dataflow, reportedComponents, CancellationToken));

            if (dataflow.Dsd.Msd != null)
            {
                //Referential Metadata Attributes
                reportedComponents = new ReportedComponents
                {
                    MetadataAttributes = dataflow.Dsd.Msd.MetadataAttributes.ToList()
                };

                Assert.DoesNotThrowAsync(async () =>
                    await _repository.AddIndexMetadataStagingTable(dataflow, reportedComponents, CancellationToken));
            }
        }

        [TestCase]
        public void DeleteMetadata()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.DeleteMetadata(_dataflow.Dsd, DbTableVersion.A, CancellationToken));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.DeleteMetadata(_dataflow.Dsd, DbTableVersion.B, CancellationToken));
        }

        [TestCase]
        public void CopyMetadataToNewVersion()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.CopyMetadataToNewVersion(_dataflow, DbTableVersion.A, DbTableVersion.B, CancellationToken));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.CopyMetadataToNewVersion(_dataflow, DbTableVersion.B, DbTableVersion.A, CancellationToken));

            Assert.ThrowsAsync<ArgumentException>(async () =>
                await _repository.CopyMetadataToNewVersion(_dataflow, DbTableVersion.A, DbTableVersion.A, CancellationToken));
            
            Assert.ThrowsAsync<ArgumentException>(async () =>
                await _repository.CopyMetadataToNewVersion(_dataflow, DbTableVersion.B, DbTableVersion.B, CancellationToken));
        }

        [TestCase("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml")]
        [TestCase("sdmx/CsvV2.xml")]
        public async Task MergeMetadataStagingToMetadataTable(string file)
        {
            var dataflow = GetDataflow(file);
            var reportedComponents = new ReportedComponents();
            var hasMsd = dataflow.Dsd.Msd != null;
            if (hasMsd)
            {
                reportedComponents.MetadataAttributes = dataflow.Dsd.Msd.MetadataAttributes;
            }

            var updatedRows = hasMsd ? 50 : 0;

            //Basic validation
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                    m.ExecuteNonQuerySqlWithParamsAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(updatedRows));


            var rowsCopied = hasMsd ? 100 : 0;
            var mergeResult = await _repository.MergeMetadataStagingToMetadataTable(dataflow, reportedComponents, DbTableVersion.A, false, rowsCopied, CancellationToken);
            Assert.AreEqual(0, mergeResult.TotalCount);
            Assert.AreEqual(0, mergeResult.InsertCount);
            Assert.AreEqual(0, mergeResult.UpdateCount);
            Assert.AreEqual(0, mergeResult.DeleteCount);
            Assert.AreEqual(0, mergeResult.Errors.Count);

            var bulkImportResult = new BulkImportResult
            {
                RowsCopied = 100,
                NumberOfRowsToMerge = 25,
                NumberOfRowsToDelete = 50
            };

            DbDataReader dbDataReader = new TestDbDataReader(
                new List<object[]> { new object[]
                {
                    bulkImportResult.RowsCopied - bulkImportResult.NumberOfRowsToMerge - bulkImportResult.NumberOfRowsToDelete,//Inserted
                    bulkImportResult.NumberOfRowsToMerge,//Updated
                    bulkImportResult.NumberOfRowsToDelete,//Deleted
                    bulkImportResult.RowsCopied//total
                } });

            //Advance validation
            _dotStatDbMock.Setup(m =>
                    m.ExecuteReaderSqlWithParamsAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<CommandBehavior>(),
                        It.IsAny<bool>(),
                        It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(dbDataReader));

            //Advance validation
            var mergeResultWithSummary = await _repository.MergeMetadataStagingToMetadataTable(dataflow, reportedComponents, DbTableVersion.A, true, rowsCopied, CancellationToken);
            Assert.AreEqual(rowsCopied, mergeResultWithSummary.TotalCount);
            Assert.AreEqual(hasMsd ? 25 : 0, mergeResultWithSummary.InsertCount);
            Assert.AreEqual(hasMsd ? 25 : 0, mergeResultWithSummary.UpdateCount);

            if (hasMsd)
            {
                //Duplicate rows
                _dotStatDbMock.Setup(m =>
                        m.ExecuteReaderSqlWithParamsAsync(
                            It.IsAny<string>(),
                            It.IsAny<CancellationToken>(),
                            It.IsAny<CommandBehavior>(),
                            It.IsAny<bool>(),
                            It.IsAny<DbParameter[]>()))
                    .Throws(SqlExceptionHelper.NewSqlException(2627, "duplicated rows"));

                mergeResult = await _repository.MergeMetadataStagingToMetadataTable(dataflow, reportedComponents, DbTableVersion.A, true, rowsCopied, CancellationToken);
                Assert.AreEqual(1, mergeResult.Errors.Count);
                Assert.AreEqual(ValidationErrorType.DuplicatedRowsInMetadataStagingTable, mergeResult.Errors[0].Type);

                _dotStatDbMock.Setup(m =>
                        m.ExecuteReaderSqlWithParamsAsync(
                            It.IsAny<string>(),
                            It.IsAny<CancellationToken>(),
                            It.IsAny<CommandBehavior>(),
                            It.IsAny<bool>(),
                            It.IsAny<DbParameter[]>()))
                    .Throws(SqlExceptionHelper.NewSqlException(8672, "duplicated rows"));

                mergeResult = await _repository.MergeMetadataStagingToMetadataTable(dataflow, reportedComponents, DbTableVersion.A, true, rowsCopied, CancellationToken);
                Assert.AreEqual(1, mergeResult.Errors.Count);
                Assert.AreEqual(ValidationErrorType.MultipleRowsModifyingTheSameRegionInStagingTable, mergeResult.Errors[0].Type);

                //Other SQL error 
                _dotStatDbMock.Setup(m =>
                        m.ExecuteReaderSqlWithParamsAsync(
                            It.IsAny<string>(),
                            It.IsAny<CancellationToken>(),
                            It.IsAny<CommandBehavior>(),
                            It.IsAny<bool>(),
                            It.IsAny<DbParameter[]>()))
                    .Throws(SqlExceptionHelper.NewSqlException(1, "other error"));

                Assert.ThrowsAsync<SqlException>(async () =>
                    await _repository.MergeMetadataStagingToMetadataTable(dataflow, reportedComponents, DbTableVersion.A, true, rowsCopied, CancellationToken));
            }
        }

        [TestCase]
        public void UpdateStatisticsOfMetadataTable()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.UpdateStatisticsOfMetadataTable(_dataflow.Dsd, DbTableVersion.A, CancellationToken));
            Assert.DoesNotThrowAsync(async () =>
                await _repository.UpdateStatisticsOfMetadataTable(_dataflow.Dsd, DbTableVersion.B, CancellationToken));

        }

        [TestCase]
        public void MergeMetadataStagingToDatasetMetadataTable()
        {
            //_dotStatDbMock.Reset();
            //_dotStatDbMock.Setup(m =>
            //    m.ExecuteNonQuerySqlAsync(
            //        It.IsAny<string>(),
            //        It.IsAny<CancellationToken>()))
            //    .Returns(() => Task.FromResult(1));

            //Assert.DoesNotThrowAsync(async () =>
            //    await _repository.MergeMetadataStagingToDatasetMetadataTable(_dataflow, CancellationToken));

        }

        [TestCase]
        public void DropMetadataTables()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.DropMetadataTables(_dataflow.Dsd, CancellationToken));
        }

        [TestCase]
        public void DropMetadataTablesByTableVersion()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.DropMetadataTables(_dataflow.Dsd, DbTableVersion.A, CancellationToken));
        }

        [TestCase]
        public void TruncateMetadataTables()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.DoesNotThrowAsync(async () =>
                await _repository.TruncateMetadataTables(_dataflow.Dsd, DbTableVersion.A, CancellationToken));
            Assert.DoesNotThrowAsync(async () =>
                await _repository.TruncateMetadataTables(_dataflow.Dsd, DbTableVersion.B, CancellationToken));
        }
        
        [TestCase("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml")]
        [TestCase("sdmx/CsvV2.xml")]
        public void GetMetadataViewQuery(string file)
        {
            var dataflow = GetDataflow(file);
            var tableVersion = (char)DbTableVersion.A;
            dataflow.Dsd.DbId = 1;
            var viewQueryTemplate = $"SELECT [FREQ],[REPORTING_TYPE],[SERIES],[REF_AREA],[SEX],[TIME_PERIOD] AS [TIME_PERIOD],[PERIOD_START],[PERIOD_END],[STRING_TYPE],[STRING_MULTILANG_TYPE],[ALPHANUMERIC_TYPE],[ALPHANUMERIC_MULTILANG_TYPE],[BOOLEAN_TYPE],[XHTML_TYPE],[XHTML_MULTILANG_TYPE],[INTEGER_TYPE],[INTEGER_MULTILANG_TYPE],[DECIMAL_TYPE],[DECIMAL_MULTILANG_TYPE],[DATETIME_TYPE],[TIME_TYPE],[GREGORIANDAY_TYPE],[GREGORIAN_YEAR_TYPE],[GREGORIAN_YEARMONTH_TYPE],[CONTACT.CONTACT_NAME],[CONTACT.CONTACT_EMAIL],[CONTACT.CONTACT_PHONE],[CONTACT.CONTACT_ORGANISATION] FROM [data].[VI_MetadataDataFlow_{_dataflow.Dsd.DbId}_{tableVersion}]";

            if (dataflow.Dsd.Msd != null)
            {
                var viewQuery = _repository.GetMetadataViewQuery(dataflow, tableVersion);
                Assert.AreEqual(viewQueryTemplate, viewQuery);
            }
            else
            {
                Assert.Throws<ArgumentException>(
                    () => _repository.GetMetadataViewQuery(dataflow, tableVersion));
            }

            tableVersion = (char)DbTableVersion.B;
            dataflow.Dsd.DbId = 2;
            viewQueryTemplate = $"SELECT [FREQ],[REPORTING_TYPE],[SERIES],[REF_AREA],[SEX],[TIME_PERIOD] AS [TIME_PERIOD],[PERIOD_START],[PERIOD_END],[STRING_TYPE],[STRING_MULTILANG_TYPE],[ALPHANUMERIC_TYPE],[ALPHANUMERIC_MULTILANG_TYPE],[BOOLEAN_TYPE],[XHTML_TYPE],[XHTML_MULTILANG_TYPE],[INTEGER_TYPE],[INTEGER_MULTILANG_TYPE],[DECIMAL_TYPE],[DECIMAL_MULTILANG_TYPE],[DATETIME_TYPE],[TIME_TYPE],[GREGORIANDAY_TYPE],[GREGORIAN_YEAR_TYPE],[GREGORIAN_YEARMONTH_TYPE],[CONTACT.CONTACT_NAME],[CONTACT.CONTACT_EMAIL],[CONTACT.CONTACT_PHONE],[CONTACT.CONTACT_ORGANISATION] FROM [data].[VI_MetadataDataFlow_{_dataflow.Dsd.DbId}_{tableVersion}]";

            if (dataflow.Dsd.Msd != null)
            {
                var viewQuery = _repository.GetMetadataViewQuery(dataflow, tableVersion);
                Assert.AreEqual(viewQueryTemplate, viewQuery);
            }
            else
            {
                Assert.Throws<ArgumentException>(
                    () => _repository.GetMetadataViewQuery(dataflow, tableVersion));
            }
        }

        [TestCase("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml")]
        [TestCase("sdmx/CsvV2.xml")]
        public void GetEmptyMetadataViewQuery(string file)
        {
            var dataflow = GetDataflow(file);
            var emptyQuery = $"SELECT TOP 0 NULL AS [FREQ],NULL AS [REPORTING_TYPE],NULL AS [SERIES],NULL AS [REF_AREA],NULL AS [SEX],NULL AS [TIME_PERIOD],NULL AS [PERIOD_START],NULL AS [PERIOD_END],NULL AS [STRING_TYPE],NULL AS [STRING_MULTILANG_TYPE],NULL AS [ALPHANUMERIC_TYPE],NULL AS [ALPHANUMERIC_MULTILANG_TYPE],NULL AS [BOOLEAN_TYPE],NULL AS [XHTML_TYPE],NULL AS [XHTML_MULTILANG_TYPE],NULL AS [INTEGER_TYPE],NULL AS [INTEGER_MULTILANG_TYPE],NULL AS [DECIMAL_TYPE],NULL AS [DECIMAL_MULTILANG_TYPE],NULL AS [DATETIME_TYPE],NULL AS [TIME_TYPE],NULL AS [GREGORIANDAY_TYPE],NULL AS [GREGORIAN_YEAR_TYPE],NULL AS [GREGORIAN_YEARMONTH_TYPE],NULL AS [CONTACT.CONTACT_NAME],NULL AS [CONTACT.CONTACT_EMAIL],NULL AS [CONTACT.CONTACT_PHONE],NULL AS [CONTACT.CONTACT_ORGANISATION]";

            if (dataflow.Dsd.Msd != null)
            {
                var viewQuery = _repository.GetEmptyMetadataViewQuery(dataflow);
                Assert.AreEqual(viewQuery, viewQuery);
            }
            else
            {
                Assert.Throws<ArgumentException>(
                    () => _repository.GetEmptyMetadataViewQuery(dataflow));
            }
        }

        [TestCase]
        public async Task MetadataTablesExist()
        {
            _dotStatDbMock.Reset();
            var tableName = _dataflow.Dsd.SqlMetadataTable((char)DbTableVersion.A);
            _dotStatDbMock.Setup(m =>
                m.TableExists(
                    tableName,
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(true));
            
            tableName = _dataflow.Dsd.SqlMetadataTable((char)DbTableVersion.B);
            _dotStatDbMock.Setup(m =>
                m.TableExists(
                    tableName,
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(false));

            Assert.AreEqual(true, await _repository.MetadataTablesExist(_dataflow.Dsd, CancellationToken));

            tableName = _dataflow.Dsd.SqlMetadataTable((char)DbTableVersion.A);
            _dotStatDbMock.Setup(m =>
                m.TableExists(
                    tableName,
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(false));

            tableName = _dataflow.Dsd.SqlMetadataTable((char)DbTableVersion.B);
            _dotStatDbMock.Setup(m =>
                m.TableExists(
                    tableName,
                    It.IsAny<CancellationToken>()))
                .Returns(() => Task.FromResult(false));

            Assert.AreEqual(false, await _repository.MetadataTablesExist(_dataflow.Dsd, CancellationToken));
        }

    }
}