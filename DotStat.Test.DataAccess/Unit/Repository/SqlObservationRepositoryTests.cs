﻿using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db;
using DotStat.Db.DB;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.Test.Moq;
using DotStat.MappingStore;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Attribute = DotStat.Domain.Attribute;
using Dataflow = DotStat.Domain.Dataflow;
using DotStat.Common.Configuration.Dto;
using System;
using System.Data;

namespace DotStat.Test.DataAccess.Unit.Repository
{
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public class SqlObservationRepositoryTests : SdmxUnitTestBase
    {
        private readonly TestMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly CodeTranslator         _codeTranslator;
        private readonly Dataflow _dataflow;
        private readonly IGeneralConfiguration _testConfiguration;
        public SqlObservationRepositoryTests()
        {
            _mappingStoreDataAccess     = new TestMappingStoreDataAccess();
            _dataflow = _mappingStoreDataAccess.GetDataflow();

            var codeListRepository = new TestCodeListRepository(_dataflow);
            _codeTranslator = new CodeTranslator(codeListRepository);

            _testConfiguration = new BaseConfiguration()
            {
                MaxTextAttributeLength = 200
            };
        }

        [Test]
        public void DimensionColumnList()
        {
            Assert.AreEqual("PERIOD_SDMX,[DIM_101],[DIM_102],[DIM_103],[DIM_104],[DIM_105],[DIM_106],[DIM_107],[DIM_108],[DIM_109]", _dataflow.Dimensions.ToColumnList());
        }

        
        [TestCase("[COMP_201],[COMP_202],[COMP_203],[COMP_204],[COMP_205],[COMP_206],[COMP_207],[COMP_208],[COMP_209],[COMP_210],[COMP_211],[COMP_212],[COMP_213]", null, false)]
        [TestCase("[COMP_201],[COMP_202],[COMP_203],[COMP_204],[COMP_205],[COMP_206],[COMP_207],[COMP_208]", new [] {AttributeAttachmentLevel.Observation}, false)]
        [TestCase("[COMP_209],[COMP_210],[COMP_211],[COMP_212],[COMP_213]", new [] {AttributeAttachmentLevel.DataSet}, false)]
        [TestCase("[COMP_201],[COMP_202],[COMP_203],[COMP_204],[COMP_205],[COMP_206],[COMP_207],[COMP_208],[COMP_209],[COMP_210],[COMP_211],[COMP_212],[COMP_213]", new [] { AttributeAttachmentLevel.Observation, AttributeAttachmentLevel.DataSet}, false)]
        [TestCase("[COMP_209] [int],[COMP_210] [int],[COMP_211] [nvarchar](200) NOT NULL,[COMP_212] [nvarchar](200),[COMP_213] [nvarchar](200)", new[] { AttributeAttachmentLevel.DataSet }, true)]
        [TestCase("", new[] { AttributeAttachmentLevel.Group }, false)]
        public void AttributeColumnList(string expectedString, IList<AttributeAttachmentLevel> levels, bool withType)
        {
            Assert.AreEqual(expectedString, _dataflow.Dsd.Attributes.ToColumnList(_testConfiguration.MaxTextAttributeLength, levels, withType));
        }

        [TestCase("[COMP_204] [nvarchar](300),[COMP_205] [int]", new[] { AttributeAttachmentLevel.DataSet }, null)]
        [TestCase("[COMP_206] [nvarchar](300) NOT NULL,[COMP_207] [nvarchar](300),[COMP_208] [nvarchar](300),[COMP_209] [int] NOT NULL,[COMP_210] [int] NOT NULL", new[] { AttributeAttachmentLevel.DimensionGroup, AttributeAttachmentLevel.Group }, null)]
        [TestCase("[COMP_201] [nvarchar](300),[COMP_202] [int],[COMP_203] [nvarchar](300)", new[] { AttributeAttachmentLevel.Observation }, null)]
        [TestCase("[COMP_204] [nvarchar](400),[COMP_205] [int]", new[] { AttributeAttachmentLevel.DataSet }, 400)]
        [TestCase("[COMP_206] [nvarchar](400) NOT NULL,[COMP_207] [nvarchar](400),[COMP_208] [nvarchar](400),[COMP_209] [int] NOT NULL,[COMP_210] [int] NOT NULL", new[] { AttributeAttachmentLevel.DimensionGroup, AttributeAttachmentLevel.Group }, 400)]
        [TestCase("[COMP_201] [nvarchar](400),[COMP_202] [int],[COMP_203] [nvarchar](400)", new[] { AttributeAttachmentLevel.Observation }, 400)]
        [TestCase("[COMP_204] [nvarchar](MAX),[COMP_205] [int]", new[] { AttributeAttachmentLevel.DataSet }, 0)]
        [TestCase("[COMP_206] [nvarchar](MAX) NOT NULL,[COMP_207] [nvarchar](MAX),[COMP_208] [nvarchar](MAX),[COMP_209] [int] NOT NULL,[COMP_210] [int] NOT NULL", new[] { AttributeAttachmentLevel.DimensionGroup, AttributeAttachmentLevel.Group }, 0)]
        [TestCase("[COMP_201] [nvarchar](MAX),[COMP_202] [int],[COMP_203] [nvarchar](MAX)", new[] { AttributeAttachmentLevel.Observation }, 0)]
        [TestCase("[COMP_204] [nvarchar](MAX),[COMP_205] [int]", new[] { AttributeAttachmentLevel.DataSet }, 5000)]
        [TestCase("[COMP_206] [nvarchar](MAX) NOT NULL,[COMP_207] [nvarchar](MAX),[COMP_208] [nvarchar](MAX),[COMP_209] [int] NOT NULL,[COMP_210] [int] NOT NULL", new[] { AttributeAttachmentLevel.DimensionGroup, AttributeAttachmentLevel.Group }, 5000)]
        [TestCase("[COMP_201] [nvarchar](MAX),[COMP_202] [int],[COMP_203] [nvarchar](MAX)", new[] { AttributeAttachmentLevel.Observation }, 5000)]
        public void AttributeColumnListWithDsdAnnotation(string expectedString, IList<AttributeAttachmentLevel> levels, int? dsdMaxTextAttributeLength)
        {
            var sdmxObjects = _mappingStoreDataAccess.GetSdmxObjects("sdmx/MANDATORY_ATTRIBUTE_TEST.Dsd.With.Annotation.xml");

            var dataflow = SdmxParser.ParseStructure(sdmxObjects);

            dataflow.Dsd.MaxTextAttributeLength = dsdMaxTextAttributeLength;

            new TestManagementRepository(dataflow).FillIdsFromDisseminationDb(dataflow);

            var attributes = dataflow.Dsd.Attributes;

            Assert.AreEqual(
                expectedString,
                attributes.ToColumnList(_testConfiguration.MaxTextAttributeLength, levels, true));
        }

        [TestCase("data/264D_264_SALDI/all", null)]
        [TestCase("data/264D_264_SALDI/.IT+ITC1.1_S+COF_ECO...TOTAL...","([DIM_102]=2 OR [DIM_102]=13) AND ([DIM_103]=6 OR [DIM_103]=3) AND ([DIM_106]=221)")]
        [TestCase("data/264D_264_SALDI/......../?startPeriod=2010-01-01&endPeriod=2010-12-31", "PERIOD_END>='2010-01-01' AND PERIOD_START<'2011-01-01'")]
        [TestCase("data/264D_264_SALDI/all/?startPeriod=2010-02&endPeriod=2015", "PERIOD_END>='2010-02-01' AND PERIOD_START<'2016-01-01'")]
        [TestCase("data/264D_264_SALDI/.IT.1_S...TOTAL.../?startPeriod=2009-Q1&endPeriod=2015-12", "([DIM_102]=2) AND ([DIM_103]=6) AND ([DIM_106]=221) AND PERIOD_END>='2009-01-01' AND PERIOD_START<'2016-01-01'")]
        [TestCase("data/264D_264_SALDI/A........2D1+1AB/?startPeriod=2010-01-01", "([DIM_101]=1) AND ([DIM_109]=13 OR [DIM_109]=4) AND PERIOD_END>='2010-01-01'")]
        public async Task PredicateBuildTest(string restQuery, string expectedSql)
        {
            //TODO: Add referential metadata test cases
            var query   = GetQuery(restQuery);
            var sql     = await Predicate.BuildWhereForDataObservationLevel(_codeTranslator, query, _dataflow, CancellationToken);

            Assert.AreEqual(expectedSql, sql);
        }

        [TestCase(false, "data/264D_264_SALDI/all", null, null, "SELECT PERIOD_SDMX,[DIM_101],[DIM_102],[DIM_103],[DIM_104],[DIM_105],[DIM_106],[DIM_107],[DIM_108],[DIM_109],[value] FROM [data].FILT_7 FI INNER JOIN [data].FACT_7_A FA ON FI.SID = FA.SID")]
        [TestCase(false, "data/264D_264_SALDI/all", 1, 100, "SELECT TOP(@top) PERIOD_SDMX,[DIM_101],[DIM_102],[DIM_103],[DIM_104],[DIM_105],[DIM_106],[DIM_107],[DIM_108],[DIM_109],[value] FROM [data].FILT_7 FI INNER JOIN [data].FACT_7_A FA ON FI.SID = FA.SID")]
        [TestCase(true, "data/264D_264_SALDI/all", null, null, "SELECT PERIOD_SDMX,[DIM_101],[DIM_102],[DIM_103],[DIM_104],[DIM_105],[DIM_106],[DIM_107],[DIM_108],[DIM_109],[value],[COMP_201],[COMP_202],[COMP_203],[COMP_204],[COMP_205],[COMP_206],[COMP_207],[COMP_208] FROM [data].FILT_7 FI INNER JOIN [data].FACT_7_A FA ON FI.SID = FA.SID")]
        [TestCase(true, "data/264D_264_SALDI/.IT+ITC1.1_S+COF_ECO...TOTAL...", null, null, "SELECT PERIOD_SDMX,[DIM_101],[DIM_102],[DIM_103],[DIM_104],[DIM_105],[DIM_106],[DIM_107],[DIM_108],[DIM_109],[value],[COMP_201],[COMP_202],[COMP_203],[COMP_204],[COMP_205],[COMP_206],[COMP_207],[COMP_208] FROM [data].FILT_7 FI INNER JOIN [data].FACT_7_A FA ON FI.SID = FA.SID WHERE ([DIM_102]=2 OR [DIM_102]=13) AND ([DIM_103]=6 OR [DIM_103]=3) AND ([DIM_106]=221)")]
        [TestCase(true, "data/264D_264_SALDI/A..COF......", null, null, "SELECT PERIOD_SDMX,[DIM_101],[DIM_102],[DIM_103],[DIM_104],[DIM_105],[DIM_106],[DIM_107],[DIM_108],[DIM_109],[value],[COMP_201],[COMP_202],[COMP_203],[COMP_204],[COMP_205],[COMP_206],[COMP_207],[COMP_208] FROM [data].FILT_7 FI INNER JOIN [data].FACT_7_A FA ON FI.SID = FA.SID WHERE ([DIM_101]=1) AND ([DIM_103]=1)")]
        [TestCase(true, "data/264D_264_SALDI/.ITF1.......", null, null, "SELECT PERIOD_SDMX,[DIM_101],[DIM_102],[DIM_103],[DIM_104],[DIM_105],[DIM_106],[DIM_107],[DIM_108],[DIM_109],[value],[COMP_201],[COMP_202],[COMP_203],[COMP_204],[COMP_205],[COMP_206],[COMP_207],[COMP_208] FROM [data].FILT_7 FI INNER JOIN [data].FACT_7_A FA ON FI.SID = FA.SID WHERE ([DIM_102]=6101)")]
        [TestCase(true, "data/264D_264_SALDI/.ITF1.......", 1, 50, "SELECT TOP(@top) PERIOD_SDMX,[DIM_101],[DIM_102],[DIM_103],[DIM_104],[DIM_105],[DIM_106],[DIM_107],[DIM_108],[DIM_109],[value],[COMP_201],[COMP_202],[COMP_203],[COMP_204],[COMP_205],[COMP_206],[COMP_207],[COMP_208] FROM [data].FILT_7 FI INNER JOIN [data].FACT_7_A FA ON FI.SID = FA.SID WHERE ([DIM_102]=6101)")]
        public async Task QueryBuildTest(bool withAttributes, string restQuery,  int? startRow, int? endRow, string expectedSql)
        {
            //TODO: Add referential metadata test cases
            var sdmxQuery       = GetQuery(restQuery);
            var dotStatDb = new SqlDotStatDb(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion);
            var repository      = new SqlObservationRepository(dotStatDb, _testConfiguration);

            var sqlQuery = await repository.BuildDataSqlQuery(
                sdmxQuery, 
                _codeTranslator, 
                _dataflow, 
                _dataflow.Dimensions.ToArray(),
                withAttributes ? _dataflow.ObsAttributes.ToArray() : new Attribute[0], 
                new List<DbParameter>(),
                DbTableVersion.A,
                startRow,
                endRow,
                CancellationToken
            );

            Assert.AreEqual(expectedSql, sqlQuery);
        }
        
        [Test]
        public async Task GetObservations()
        {
            var sdmxQuery       = GetQuery();
            DbDataReader dbDataReader = new TestDbDataReader(new List<string[]>()
                    {
                        new [] { "1900-Q1", "7","6103","1","1","1","2","1","1","1","1996.15416211418",null,null,null,null,null,null,null,null},
                        new [] { "2012", "7", "6101", "1","1","1","2","1","1","1","1996.15416211418",null,null,null,null,null,null,null,null},
                        new [] { "2012", "7", "6102", "1", "1", "1", "2", "1", "1", "1", "1996.15416211418", null, null, null, null, null, null, null, null }
                    });

            var dotStatDbMock = new Mock<SqlDotStatDb>(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(dbDataReader));

            dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");

            var repository = new SqlObservationRepository(dotStatDbMock.Object, _testConfiguration);
          
            var observations = repository.GetDataObservations(
                sdmxQuery,
                _dataflow,
                _codeTranslator,
                DbTableVersion.A,
                CancellationToken
            );

            Assert.IsNotNull(observations);

            var listOfObservations = await observations.ToListAsync();
            var obs = listOfObservations.FirstOrDefault().Observation;

            Assert.AreEqual("1900-Q1", obs.ObsTime);
            Assert.AreEqual(9, obs.SeriesKey.Key.Count);

            Assert.AreEqual("FREQ", obs.SeriesKey.Key[0].Concept);
            Assert.AreEqual("Q", obs.SeriesKey.Key[0].Code);

            Assert.AreEqual("REF_AREA", obs.SeriesKey.Key[1].Concept);
            Assert.AreEqual("066001", obs.SeriesKey.Key[1].Code);

            Assert.AreEqual(3, listOfObservations.Count);
        }

        //TODO: complete unit tests
        //[Test]
        //public async Task GetObservationCount()
        //{
        //}

        //[Test]
        //public async Task GetMetadataObservations()
        //{
        //}
    }
}