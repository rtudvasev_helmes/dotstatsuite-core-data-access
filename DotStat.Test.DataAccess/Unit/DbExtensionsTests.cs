﻿using System;
using System.Linq;
using DotStat.Db;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using PrimaryMeasure = DotStat.Domain.PrimaryMeasure;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public class DbExtensionsTests : UnitTestBase
    {
        private readonly TestMappingStoreDataAccess _mappingStoreDataAccess;
        
        public DbExtensionsTests()
        {
            _mappingStoreDataAccess = new TestMappingStoreDataAccess("sdmx/OBS_TYPE_TEST.xml");
        }

        [TestCase(TextEnumType.String, null, null, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.String, null, null, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.String, "#N/A", "#N/A", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.String, "#N/A", "1", StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.String, "NaN", "#N/A", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.String, "NaN", "1", StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.String, "normal value", "normal value", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.String, "normal value", "1", StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Decimal, "#N/A", "#N/A", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Decimal, "#N/A", "1", StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Decimal, "NaN", "#N/A", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Decimal, "NaN", "1", StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Decimal, "normal value", "normal value", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Decimal, "normal value", "1", StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Integer, "#N/A", -2_147_483_648, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Integer, "#N/A", 1, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Integer, "NaN", -2_147_483_648, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Integer, "NaN", 1, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Integer, "5", 5, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Integer, "5", 1, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Long, "#N/A", -9_223_372_036_854_775_808, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Long, "#N/A", 1, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Long, "NaN", -9_223_372_036_854_775_808, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Long, "NaN", 1, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Long, "5", 5, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Long, "5", 1, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Short, "#N/A", -32_768, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Short, "#N/A", 1, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Short, "NaN", -32_768, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Short, "NaN", 1, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Short, "5", 5, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Short, "5", 1, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Boolean, "#N/A", -32_768, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Boolean, "#N/A", 1, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Boolean, "NaN", -32_768, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Boolean, "NaN", 1, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Boolean, "false", 0, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Boolean, "false", 1, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Float, "#N/A", -3.4E38F, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Float, "#N/A", 1.0, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Float, "NaN", -3.4E38F, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Float, "NaN", 1.0, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Float, "5.0", 5.0, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Float, "5.0", 1.0, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Double, "#N/A", -1.7976931348623157E308, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Double, "#N/A", 1.0, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Double, "NaN", -1.7976931348623157E308, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Double, "NaN", 1.0, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.Double, "5.0", 5.0, StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.Double, "5.0", 1.0, StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.ObservationalTimePeriod, "#N/A", "#N/A", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.ObservationalTimePeriod, "#N/A", "1", StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.ObservationalTimePeriod, "NaN", "#N/A", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.ObservationalTimePeriod, "NaN", "1", StagingRowActionEnum.Delete)]
        [TestCase(TextEnumType.ObservationalTimePeriod, "normal value", "normal value", StagingRowActionEnum.Merge)]
        [TestCase(TextEnumType.ObservationalTimePeriod, "normal value", "1", StagingRowActionEnum.Delete)]
        public void GetObjectFromStringMeasure(TextEnumType textType, string observationValueStr, object expectedValue, StagingRowActionEnum action)
        {
            var dsd = GetDsd(_mappingStoreDataAccess, textType);
            var primaryMeasure = new PrimaryMeasure(dsd.PrimaryMeasure);
            var actualValue = primaryMeasure.GetObjectFromString(observationValueStr, action);
            Assert.AreEqual(expectedValue, actualValue);
        }
        
        [TestCase(TextEnumType.String, "#N/A")]
        [TestCase(TextEnumType.Alpha, "#N/A")]
        [TestCase(TextEnumType.Alphanumeric, "#N/A")]
        [TestCase(TextEnumType.Uri, "#N/A")]
        [TestCase(TextEnumType.Numeric, "#N/A")]
        [TestCase(TextEnumType.BigInteger, "#N/A")]
        [TestCase(TextEnumType.Decimal, "#N/A")]
        [TestCase(TextEnumType.Integer, -2_147_483_648)]
        [TestCase(TextEnumType.Count, -2_147_483_648)]
        [TestCase(TextEnumType.Long, -9_223_372_036_854_775_808)]
        [TestCase(TextEnumType.Short, -32_768)]
        [TestCase(TextEnumType.Boolean, -32_768)]
        [TestCase(TextEnumType.Float, -3.4E38F)]
        [TestCase(TextEnumType.Double, -1.7976931348623157E308)]
        [TestCase(TextEnumType.ObservationalTimePeriod, "#N/A")]
        [TestCase(TextEnumType.StandardTimePeriod, "#N/A")]
        [TestCase(TextEnumType.BasicTimePeriod, "#N/A")]
        [TestCase(TextEnumType.GregorianTimePeriod, "#N/A")]
        [TestCase(TextEnumType.GregorianYear, "#N/A")]
        [TestCase(TextEnumType.GregorianYearMonth, "#N/A")]
        [TestCase(TextEnumType.GregorianDay, "#N/A")]
        [TestCase(TextEnumType.ReportingTimePeriod, "#N/A")]
        [TestCase(TextEnumType.ReportingYear, "#N/A")]
        [TestCase(TextEnumType.ReportingSemester, "#N/A")]
        [TestCase(TextEnumType.ReportingTrimester, "#N/A")]
        [TestCase(TextEnumType.ReportingQuarter, "#N/A")]
        [TestCase(TextEnumType.ReportingMonth, "#N/A")]
        [TestCase(TextEnumType.ReportingWeek, "#N/A")]
        [TestCase(TextEnumType.ReportingDay, "#N/A")]
        [TestCase(TextEnumType.DateTime, "#N/A")]
        [TestCase(TextEnumType.TimesRange, "#N/A")]
        [TestCase(TextEnumType.Month, "#N/A")]
        [TestCase(TextEnumType.MonthDay, "#N/A")]
        [TestCase(TextEnumType.Day, "#N/A")]
        [TestCase(TextEnumType.Time, "#N/A")]
        [TestCase(TextEnumType.Duration, "#N/A")]
        public void GetSqlMinValueMeasure(TextEnumType textType, object expectedValue)
        {
            var dsd = GetDsd(_mappingStoreDataAccess, textType);
            var primaryMeasure = new PrimaryMeasure(dsd.PrimaryMeasure);
            
            Assert.AreEqual(expectedValue, primaryMeasure.GetSqlMinValue(asSqlString: false));
            
            expectedValue = expectedValue.ToString().Equals("#N/A") ? $"'{expectedValue}'": $"{expectedValue}";
            if (textType == TextEnumType.Float)
                expectedValue = "CAST('-3.4e38' AS real)";
            
            Assert.AreEqual(expectedValue, primaryMeasure.GetSqlMinValue(asSqlString: true));
        }

        //Textual
        [TestCase(TextEnumType.String, true)]
        [TestCase(TextEnumType.Alpha, true)]
        [TestCase(TextEnumType.Alphanumeric, true)]
        [TestCase(TextEnumType.Uri, true)]
        [TestCase(TextEnumType.Numeric, true)]
        [TestCase(TextEnumType.ObservationalTimePeriod, true)]
        [TestCase(TextEnumType.StandardTimePeriod, true)]
        [TestCase(TextEnumType.BasicTimePeriod, true)]
        [TestCase(TextEnumType.GregorianTimePeriod, true)]
        [TestCase(TextEnumType.GregorianYear, true)]
        [TestCase(TextEnumType.GregorianYearMonth, true)]
        [TestCase(TextEnumType.GregorianDay, true)]
        [TestCase(TextEnumType.ReportingTimePeriod, true)]
        [TestCase(TextEnumType.ReportingYear, true)]
        [TestCase(TextEnumType.ReportingSemester, true)]
        [TestCase(TextEnumType.ReportingTrimester, true)]
        [TestCase(TextEnumType.ReportingQuarter, true)]
        [TestCase(TextEnumType.ReportingMonth, true)]
        [TestCase(TextEnumType.ReportingWeek, true)]
        [TestCase(TextEnumType.ReportingDay, true)]
        [TestCase(TextEnumType.DateTime, true)]
        [TestCase(TextEnumType.TimesRange, true)]
        [TestCase(TextEnumType.Month, true)]
        [TestCase(TextEnumType.MonthDay, true)]
        [TestCase(TextEnumType.Day, true)]
        [TestCase(TextEnumType.Time, true)]
        [TestCase(TextEnumType.Duration, true)]
        //non-textual
        [TestCase(TextEnumType.BigInteger, false)]
        [TestCase(TextEnumType.Integer, false)]
        [TestCase(TextEnumType.Long, false)]
        [TestCase(TextEnumType.Short, false)]
        [TestCase(TextEnumType.Decimal, false)]
        [TestCase(TextEnumType.Float, false)]
        [TestCase(TextEnumType.Double, false)]
        //To be revised, Boolean looks like it is not considered as numeric https://www.w3.org/TR/xmlschema11-2/#boolean thus doesn't support NaN
        [TestCase(TextEnumType.Boolean, false)]
        [TestCase(TextEnumType.Count, false)]
        public void HasTextualRepresentation(TextEnumType textType, bool hasTextualRepresentation)
        {
            var dsd = GetDsd(_mappingStoreDataAccess, textType);
            var primaryMeasure = new PrimaryMeasure(dsd.PrimaryMeasure);
            var actualValue = primaryMeasure.HasTextualRepresentation();
            Assert.AreEqual(hasTextualRepresentation, actualValue);
        }

        [Test]
        public void HasTextualRepresentation()
        {
            var dataflow = _mappingStoreDataAccess.GetDataflow("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml");
            foreach(var attr in dataflow.Dsd.Attributes)
            {
                var actualValue = attr.HasTextualRepresentation();
                Assert.AreEqual(true, actualValue);
            }
        }

        private IDataStructureObject GetDsd(TestMappingStoreDataAccess mappingStoreDataAccess, TextEnumType textType)
        {
            var sdmxObjects = mappingStoreDataAccess.GetSdmxObjects("sdmx/OBS_TYPE_TEST.xml");

            var dsdId = "OBS_TYPE_DSD_" + (textType == TextEnumType.TimesRange ? "TIMERANGE" : textType.ToString().ToUpper());
            var dsd = sdmxObjects.DataStructures.FirstOrDefault(d => d.Id.Equals(dsdId, StringComparison.InvariantCultureIgnoreCase));

            Assert.IsNotNull(dsd, $"DSD {dsdId} not found.");
            return dsd;
        }

    }
}
