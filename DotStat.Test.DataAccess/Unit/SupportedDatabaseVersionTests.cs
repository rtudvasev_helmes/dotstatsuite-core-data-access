﻿using System;
using System.Collections.Generic;
using DotStat.DB;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public class SupportedDatabaseVersionTests
    {
        [TestCase("2.0", "3.1", false)]
        [TestCase("2.1", "3.1", false)]
        [TestCase("2.1.2", "3.1", false)]
        [TestCase("3.0", "3.1", false)]
        [TestCase("3.0.3", "3.1", false)]
        [TestCase("3.1", "3.1", true)]
        [TestCase("3.1", "3.1.2", true)]
        [TestCase("3.1.1", "3.1.2", true)]
        [TestCase("3.1.2", "3.1.2", true)]
        [TestCase("3.1.3", "3.1.2", true)]
        [TestCase("3.1.3", "3.1", true)]
        [TestCase("3.2", "3.1", true)]
        [TestCase("3.2.3", "3.1", true)]
        [TestCase("4.0", "3.1", false)]
        [TestCase("4.1", "3.1", false)]
        [TestCase("4.1.3", "3.1", false)]
        [TestCase(null, "3.1", false)]
        [TestCase("3.1", null, false)]
        [TestCase(null, null, false)]
        public void CompatibleSupportedDatabaseVersionTest(string databaseVersionStr, string supportedVersionStr, bool isCompatible)
        {
            var databaseVersion = string.IsNullOrEmpty(databaseVersionStr) ? null : new Version(databaseVersionStr);
            var supportedVersion = string.IsNullOrEmpty(supportedVersionStr) ? null : new Version(supportedVersionStr);

            Assert.AreEqual(isCompatible,
                SupportedDatabaseVersion.IsDataDbVersionSupported(databaseVersion, supportedVersion), $"Data database version:{databaseVersionStr}, supported version:{supportedVersionStr}");

            Assert.AreEqual(isCompatible,
                SupportedDatabaseVersion.IsCommonDbVersionSupported(databaseVersion, supportedVersion), $"Common database version:{databaseVersionStr}, supported version:{supportedVersionStr}");
        }

        [TestCase("2.0", "3.1", false)]
        [TestCase("2.1", "3.1", false)]
        [TestCase("2.1.2", "3.1", false)]
        [TestCase("3.0", "3.1", false)]
        [TestCase("3.0.3", "3.1", false)]
        [TestCase("3.1", "3.1", true)]
        [TestCase("3.1", "3.1.2", true)]
        [TestCase("3.1.1", "3.1.2", true)]
        [TestCase("3.1.2", "3.1.2", true)]
        [TestCase("3.1.3", "3.1.2", true)]
        [TestCase("3.1.3", "3.1", true)]
        [TestCase("3.2", "3.1", true)]
        [TestCase("3.2.3", "3.1", true)]
        [TestCase("4.0", "3.1", false)]
        [TestCase("4.1", "3.1", false)]
        [TestCase("4.1.3", "3.1", false)]
        [TestCase(null, "3.1", false)]
        [TestCase("3.1", null, false)]
        [TestCase(null, null, false)]
        [TestCase("", null, false)]
        [TestCase(null, "", false)]
        public void CompatibleSupportedDatabaseVersionStringTest(string databaseVersionStr, string supportedVersionStr, bool isCompatible)
        {
            Assert.AreEqual(isCompatible,
                SupportedDatabaseVersion.IsDataDbVersionSupported(databaseVersionStr, supportedVersionStr), $"Data database version:{databaseVersionStr}, supported version:{supportedVersionStr}");

            Assert.AreEqual(isCompatible,
                SupportedDatabaseVersion.IsCommonDbVersionSupported(databaseVersionStr, supportedVersionStr), $"Common database version:{databaseVersionStr}, supported version:{supportedVersionStr}");
        }

        [Test]
        public void CompatibleSupportedDefaultDatabaseVersionTest()
        {
            foreach (var testData in GetTestDbVersions(SupportedDatabaseVersion.DataDbVersion))
            {
                Assert.AreEqual(testData.ExpectedResult, SupportedDatabaseVersion.IsDataDbVersionSupported(testData.DatabaseVersion), $"Object - Data DbVersion: {testData.DatabaseVersion}, supported Data db version: {SupportedDatabaseVersion.DataDbVersion}");
                Assert.AreEqual(testData.ExpectedResult, SupportedDatabaseVersion.IsDataDbVersionSupported(testData.DatabaseVersion.ToString()), $"String - Data DbVersion: {testData.DatabaseVersion}, supported Data db version: {SupportedDatabaseVersion.DataDbVersion}");
            }

            foreach (var testData in GetTestDbVersions(SupportedDatabaseVersion.CommonDbVersion))
            {
                Assert.AreEqual(testData.ExpectedResult, SupportedDatabaseVersion.IsCommonDbVersionSupported(testData.DatabaseVersion), $"Object - Common DbVersion: {testData.DatabaseVersion}, supported Common db version: {SupportedDatabaseVersion.CommonDbVersion}");
                Assert.AreEqual(testData.ExpectedResult, SupportedDatabaseVersion.IsCommonDbVersionSupported(testData.DatabaseVersion.ToString()), $"String - Common DbVersion: {testData.DatabaseVersion}, supported Common db version: {SupportedDatabaseVersion.CommonDbVersion}");
            }
        }

        private IList<(Version DatabaseVersion,bool ExpectedResult)> GetTestDbVersions(Version defaultVersion)
        {
            var testData = new List<(Version, bool)>();

            testData.Add(((defaultVersion, true)));

            var majorVersion = defaultVersion.Major;
            var minorVersion = defaultVersion.Minor;
            var patchVersion = defaultVersion.Build;

            // If major and minor are the same, any patch version should be OK
            testData.Add((new Version(majorVersion, minorVersion), true));
            testData.Add((new Version(majorVersion, minorVersion, patchVersion + 1), true));
            if (patchVersion > 0)
            {
                testData.Add((new Version(majorVersion, minorVersion, patchVersion - 1), true));
            }

            // If major version is the same, but DB's minor version is below DA minor version, then not compatible, DB minor version can be higher than DA minor version
            testData.Add((new Version(majorVersion, minorVersion + 1), true));

            testData.Add((new Version(majorVersion, minorVersion + 1, patchVersion > -1 ? patchVersion : 0), true));

            if (minorVersion > 0)
            {
                testData.Add((new Version(majorVersion, minorVersion - 1), false));
                testData.Add((new Version(majorVersion, minorVersion - 1, patchVersion > -1 ? patchVersion : 0), false));
            }

            // If major version is different then not compatible
            testData.Add((new Version(majorVersion + 1, minorVersion), false));
            testData.Add((new Version(majorVersion + 1, minorVersion, patchVersion > -1 ? patchVersion : 0), false));

            testData.Add((new Version(majorVersion - 1, minorVersion), false));
            testData.Add((new Version(majorVersion - 1, minorVersion, patchVersion > -1 ? patchVersion : 0), false));

            return testData;
        }

        [TestCase("2.0", "3.1", false)]
        [TestCase("2.1", "3.1", false)]
        [TestCase("2.1.2", "3.1", false)]
        [TestCase("3.0", "3.1", false)]
        [TestCase("3.0.3", "3.1", false)]
        [TestCase("3.1", "3.1", true)]
        [TestCase("3.1", "3.1.2", false)]
        [TestCase("3.1.1", "3.1.2", false)]
        [TestCase("3.1.2", "3.1.2", true)]
        [TestCase("3.1.3", "3.1.2", false)]
        [TestCase("3.1.3", "3.1", false)]
        [TestCase("3.2", "3.1", false)]
        [TestCase("3.2.3", "3.1", false)]
        [TestCase("4.0", "3.1", false)]
        [TestCase("4.1", "3.1", false)]
        [TestCase("4.1.3", "3.1", false)]
        public void EqualSupportedDatabaseVersionTest(string databaseVersionStr, string supportedVersionStr, bool isEqual)
        {
            var databaseVersion = new Version(databaseVersionStr);
            var supportedVersion = new Version(supportedVersionStr);

            Assert.AreEqual(isEqual,
                SupportedDatabaseVersion.IsDataDbVersionSupported(databaseVersion, supportedVersion, true), $"Data database version:{databaseVersionStr}, supported version:{supportedVersionStr}");

            Assert.AreEqual(isEqual,
                SupportedDatabaseVersion.IsCommonDbVersionSupported(databaseVersion, supportedVersion, true), $"Common database version:{databaseVersionStr}, supported version:{supportedVersionStr}");
        }

        [TestCase("2.0", "3.1", false)]
        [TestCase("2.1", "3.1", false)]
        [TestCase("2.1.2", "3.1", false)]
        [TestCase("3.0", "3.1", false)]
        [TestCase("3.0.3", "3.1", false)]
        [TestCase("3.1", "3.1", true)]
        [TestCase("3.1", "3.1.2", false)]
        [TestCase("3.1.1", "3.1.2", false)]
        [TestCase("3.1.2", "3.1.2", true)]
        [TestCase("3.1.3", "3.1.2", false)]
        [TestCase("3.1.3", "3.1", false)]
        [TestCase("3.2", "3.1", false)]
        [TestCase("3.2.3", "3.1", false)]
        [TestCase("4.0", "3.1", false)]
        [TestCase("4.1", "3.1", false)]
        [TestCase("4.1.3", "3.1", false)]
        public void EqualSupportedDatabaseVersionStringTest(string databaseVersionStr, string supportedVersionStr, bool isEqual)
        {
            Assert.AreEqual(isEqual,
                SupportedDatabaseVersion.IsDataDbVersionSupported(databaseVersionStr, supportedVersionStr, true), $"Data database version:{databaseVersionStr}, supported version:{supportedVersionStr}");

            Assert.AreEqual(isEqual,
                SupportedDatabaseVersion.IsCommonDbVersionSupported(databaseVersionStr, supportedVersionStr, true), $"Common database version:{databaseVersionStr}, supported version:{supportedVersionStr}");
        }
    }
}
