﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;


namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public class DataflowConstraintsTests: SdmxUnitTestBase
    {
        private TestMappingStoreDataAccess _mappingStoreDataAccess;
        private Dataflow _dataflow;
        private CodeTranslator _codeTranslator;

        public DataflowConstraintsTests()
        {
            _mappingStoreDataAccess = new TestMappingStoreDataAccess("sdmx/ECB_AME_(with constraints).xml");
            _dataflow = _mappingStoreDataAccess.GetDataflow();
            var codeListRepository = new TestCodeListRepository(_dataflow);
            _codeTranslator = new CodeTranslator(codeListRepository);
        }

        [TestCase("AME_REF_AREA", "IRL", false, true)]
        [TestCase("AME_REF_AREA", "Denmark", false, false)]
        [TestCase("AME_REF_AREA", "Denmark", true, true)]
        [TestCase("AME_REF_AREA", "some dummy text", true, false)]
        public void FindByLabel(string dimCode, string key, bool withLabel, bool expectedResult)
        {
            var dimension = _dataflow.Dimensions.First(dim => dim.Code == dimCode);

            Assert.IsNotNull(dimension);

            var code = dimension.FindMember(key, withLabel);

            Assert.AreEqual(expectedResult, code != null);
        }


        [Test]
        public void TestDataflowAttributesWithConstraints()
        {
            var dsdDim = _dataflow.Dsd.Dimensions.First(dim => dim.Code == "AME_ITEM");

            Assert.IsNotNull(dsdDim.Constraint);

            Assert.AreEqual(882, dsdDim.Codelist.Codes.Count);
            Assert.AreEqual(4, dsdDim.Constraint.Codes.Count);

            // Dimension member not present in constraint, but present in DSD (in full codelist)
            var code = dsdDim.FindMember("CVGD3", false);
            Assert.IsNotNull(code);
            Assert.IsFalse(dsdDim.IsAllowed(code.Code));

            // Dimension member present in constraint, and in DSD (in full codelist)
            code = dsdDim.FindMember("UDGGL", false);
            Assert.IsNotNull(code);
            Assert.IsTrue(dsdDim.IsAllowed(code.Code));

            // Check of constrainted attribute
            Assert.AreEqual(_dataflow.Dsd.Attributes.Count(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation), _dataflow.ObsAttributes.Count);

            var dsdAttribute = _dataflow.Dsd.Attributes.First(a => a.Code.Equals("OBS_STATUS"));

            Assert.IsNotNull(dsdAttribute);
            Assert.IsNotNull(dsdAttribute.Constraint);
            Assert.AreEqual(17, dsdAttribute.Codelist.Codes.Count);
            Assert.AreEqual(6, dsdAttribute.Constraint.Codes.Count);

            // Attribute code member not present in constraint, but present in DSD (in full codelist)
            code = dsdAttribute.FindMember("V", false);
            Assert.IsNotNull(code);
            Assert.IsFalse(dsdAttribute.IsAllowed(code.Code));

            // Attribute code member present in constraint, and in DSD (in full codelist)
            code = dsdAttribute.FindMember("A", false);
            Assert.IsNotNull(code);
            Assert.IsTrue(dsdAttribute.IsAllowed(code.Code));

            // ----------------------------------------

            dsdAttribute = _dataflow.Dsd.Attributes.First(a => a.Code.Equals("OBS_CONF"));

            Assert.IsNotNull(dsdAttribute);
            Assert.IsNotNull(dsdAttribute.Constraint);
            Assert.AreEqual(9, dsdAttribute.Codelist.Codes.Count);
            Assert.AreEqual(5, dsdAttribute.Constraint.Codes.Count);

            // Attribute code member not present in constraint, but present in DSD (in full codelist)
            code = dsdAttribute.FindMember("T", false);
            Assert.IsNotNull(code);
            Assert.IsFalse(dsdAttribute.IsAllowed(code.Code));

            // Attribute code member present in constraint, and in DSD (in full codelist)
            code = dsdAttribute.FindMember("A", false);
            Assert.IsNotNull(code);
            Assert.IsTrue(dsdAttribute.IsAllowed(code.Code));
        }

        [Test]
        public async Task ConstraintAtObservationAndDimGroupAttributeSqlTest()
        {
            var where = await Predicate.BuildWhereForDataObservationLevel(_codeTranslator, null, _dataflow, CancellationToken);

            Assert.AreEqual(
                "([DIM_101]=8) AND ([DIM_102]=34 OR [DIM_102]=82 OR [DIM_102]=94 OR [DIM_102]=101) AND ([DIM_103]=2) AND ([DIM_104]=5) AND ([DIM_105]=1 OR [DIM_105]=6) AND ([DIM_106]=1) AND ([DIM_107]=396 OR [DIM_107]=222 OR [DIM_107]=349 OR [DIM_107]=878)",
                where);
        }
    }
}