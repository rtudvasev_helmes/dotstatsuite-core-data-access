﻿using System;
using System.Linq;
using DotStat.Common.Exceptions;
using DotStat.Test.Moq;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public class SdmxParserTests
    {
        [Test]
        public void GetSdmxObjects()
        {
            Assert.Catch(
                typeof(DotStatException), 
                delegate ()
                {
                    var msAccess = new TestMappingStoreDataAccess("sdmx/Culture_DF.xml");
                    var dataflow = msAccess.GetDataflow();

                }, 
                "Dimension [CULT_Unit] has no codelist representation");
        }

        [Test]
        public void GetSdmxObjectsCodelist()
        {
            var msAccess = new TestMappingStoreDataAccess("sdmx/DF_CORE_VS_LOCAL_REPRESENTATION.xml");
            var dataflow = msAccess.GetDataflow();

            StringAssert.AreEqualIgnoringCase("TEST:DF_CORE_VS_LOCAL_REPRESENTATION(1.0)", dataflow.FullId);

            var dim = dataflow.Dimensions.FirstOrDefault(d => d.Code.Equals("DIM1"));

            Assert.IsNotNull(dim);
            StringAssert.AreEqualIgnoringCase("TEST:DSD_CORE_VS_LOCAL_REPRESENTATION(1.0).DIM1", dim.FullId);
            StringAssert.AreEqualIgnoringCase("TEST:CL_DIM(2.1)", dim.Codelist.FullId);

            var attr = dataflow.ObsAttributes.FirstOrDefault(d => d.Code.Equals("ATTR1"));

            Assert.IsNotNull(attr);
            StringAssert.AreEqualIgnoringCase("TEST:DSD_CORE_VS_LOCAL_REPRESENTATION(1.0).ATTR1", attr.FullId);
            StringAssert.AreEqualIgnoringCase("TEST:CL_ATTR(2.1)", attr.Codelist.FullId);

            var meas = dataflow.Dsd.PrimaryMeasure;

            Assert.IsNotNull(meas);
            StringAssert.AreEqualIgnoringCase("TEST:DSD_CORE_VS_LOCAL_REPRESENTATION(1.0).OBS_VALUE", meas.FullId);
            StringAssert.AreEqualIgnoringCase("TEST:CL_MEAS(2.1)", meas.Codelist.FullId);
        }

    }
}
