﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration;
using DotStat.Db.DB;
using DotStat.Db.Reader;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Util;
using DotStat.Db.Validation;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public class SdmxObservationReaderTests : SdmxUnitTestBase
    {
        private readonly TestMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly CodeTranslator _codeTranslator;
        private readonly Dataflow _dataflow;
        private readonly ReportedComponents _reportedComponents;

        public SdmxObservationReaderTests()
        {
            _mappingStoreDataAccess = new TestMappingStoreDataAccess("sdmx/ECB_AME_(with constraints).xml");
            _dataflow = _mappingStoreDataAccess.GetDataflow();
            var codeListRepository = new TestCodeListRepository(_dataflow);
            _codeTranslator = new CodeTranslator(codeListRepository);
            _reportedComponents = GetReportedComponents(_dataflow);
        }

        [OneTimeSetUp]
        public async Task Init()
        {
            await _codeTranslator.FillDict(_dataflow, CancellationToken);
        }

        [Test]
        public async Task Read_Valid_Observations()
        {
            var cnt = 0;
            var observations = GetValidObservations().ToAsyncEnumerable();
            var observationsList = await observations.ToListAsync(CancellationToken);

            var reader = new SdmxObservationReader(observations, _reportedComponents, _dataflow, _codeTranslator, Configuration, true, false);

            while (reader.Read())
                cnt++;

            Assert.AreEqual(observationsList.Count, cnt);
            Assert.IsTrue(!reader.GetErrors().Any());
        }

        [Test]
        public void Insure_Order()
        {
            // TIME,FREQ,AME_REF_AREA,AME_TRANSFORMATION,AME_AGG_METHOD,AME_UNIT,AME_REFERENCE,AME_ITEM,VALUE
            // TIME_FORMAT,OBS_STATUS,OBS_CONF,OBS_PRE_BREAK,PUBL_ECB,PUBL_MU,PUBL_PUBLIC,COMPILATION,EXT_TITLE,EXT_UNIT,TITLE_COMPL
            // STATUS
            var expected = new[]
            {
                new object[] { "2010",8,34,2,5,6,1,396,7111f,"TIME_FORMAT - text",1,4,"OBS_PRE_BREAK - text",null,null,"PUBL_ECB - text","PUBL_MU - text","PUBL_PUBLIC - text","COMPILATION - text","EXT_TITLE - text","EXT_UNIT - text","TITLE_COMPL - text", 1, 1, 1, null, new DateTime(2010,1,1), new DateTime(2010,12,31,23,59,59)},
                new object[] { "2011", 8,34,2,5,6,1,396,7222f, null, 2, null, null, null, null, null, null, null, null, null, null, null, 1, 1, 0, null, new DateTime(2011,1,1), new DateTime(2011, 12, 31, 23, 59, 59) },
                new object[] { "2012", 8,34,2,5,6,1,396,7333f, null, 3, null, null, null, null, null, null, null, null, null, null, null, 1, 1, 0, null, new DateTime(2012,1,1), new DateTime(2012, 12, 31, 23, 59, 59) },
                new object[] { "2015", 8,34,2,5,6,1,396,7444f,"TIME_FORMAT - text",1,4,"OBS_PRE_BREAK - text",null,null,"PUBL_ECB - text","PUBL_MU - text","PUBL_PUBLIC - text","COMPILATION - text","EXT_TITLE - text","EXT_UNIT - text","TITLE_COMPL - text", 1, 1, 1, null, new DateTime(2015,1,1), new DateTime(2015, 12, 31, 23, 59, 59) },
                new object[] { "2016", 8,34,2,5,6,1,396,7555f, null, 2, null, null, null, null, null, null, null, null, null, null, null, 1, 1, 0, null, new DateTime(2016,1,1), new DateTime(2016, 12, 31, 23, 59, 59) },
                new object[] { "2017", 8,34,2,5,6,1,396,7666f, null, 3, null, null, null, null, null, null, null, null, null, null, null, 1, 1, 0, null, new DateTime(2017,1,1), new DateTime(2017, 12, 31, 23, 59, 59) }
            };

            var observations = GetValidObservations().ToAsyncEnumerable();

            var reportedComponents = GetReportedComponents(_dataflow);

            var reader = new SdmxObservationReader(observations, reportedComponents, _dataflow, _codeTranslator, Configuration, true, false);
            var row = 0;

            while (reader.Read())
            {
                for(var i=0;i<reader.FieldCount;i++)
                    Assert.AreEqual(expected[row][i], reader.GetValue(i), $"Error on row: {row+1}, column: {i+1}");

                row++;
            }
        }

        [TestCase(100, 10, 2, 5, 5)]  // error in the 12th observation
        [TestCase(100, 40, 10, 5, 5)]  // error in the 50th observation
        [TestCase(100, 98, 2, 5, 1)] // error in the last observation
        [TestCase(100, 0, 1, 5, 5)]  // error in the first observation
        [TestCase(100, 0, 0, 5, 0)]  // no error
        [TestCase(100, 800, 10, 5, 0)]  // no error, error offset bigger then total observations 
        [TestCase(100, 10, 100, 5, 0)]  // no error, error repeat bigger then total observations
        public void Insure_Reader_Stops_On_Error(
            int observationCount, 
            int errorOffset, 
            int errorRepeat, 
            int maxTransferErrorAmount, 
            int expectedErrorCount
        )
        {
            var testConfig = new BaseConfiguration()
            {
                MaxTransferErrorAmount = maxTransferErrorAmount,
            };

            var observations = ObservationGenerator.Generate(_dataflow,true, 1980, 2019, observationCount, new GenerationSettings()
            {
                InvalidObservationOffset = errorOffset,
                InvalidObservationRepeat = errorRepeat,
                MixDimensionOrder = true
            }, action: StagingRowActionEnum.Merge);

            var readCount = 0;
            var reader = new SdmxObservationReader(observations, _reportedComponents, _dataflow, _codeTranslator, testConfig, true, false);

            while (reader.Read())
                readCount++;

            Console.WriteLine($"Reads: {readCount}, Processed observations: {reader.CurrentIndex}, Errors: {reader.GetErrors().Count}");

            // Observed errors count
            Assert.AreEqual(expectedErrorCount, reader.GetErrors().Count);

            // Processed observations until max Error count met
            Assert.AreEqual(errorRepeat > 0 ? Math.Min(observationCount, errorOffset + errorRepeat * Math.Max(1, expectedErrorCount)) : observationCount, reader.CurrentIndex);

            // Successful reads before first error
            Assert.AreEqual(errorRepeat > 0 ? Math.Min(observationCount, errorOffset + errorRepeat - 1) : observationCount, readCount);
        }

        [Test]
        public async Task Read_Invalid_Observations()
        {
            var configuration = new BaseConfiguration()
            {
                MaxTransferErrorAmount = 2,
                MaxTextAttributeLength = 2000
            };

            var cnt = 0;
            var numRowsRead = 0;

            var observations = GetInvalidObservations().ToAsyncEnumerable();
            var observationsLIst = await observations.ToListAsync(CancellationToken);
            var reader = new SdmxObservationReader(observations, _reportedComponents, _dataflow, _codeTranslator, configuration, true, false);

            Assert.IsFalse(reader.IsClosed);

            while (reader.Read())
            {
                cnt++;
            }

            Assert.IsTrue(reader.IsClosed);
            Assert.IsTrue(observationsLIst.Count > configuration.MaxTransferErrorAmount);
            Assert.AreEqual(numRowsRead, cnt);
            Assert.AreEqual(configuration.MaxTransferErrorAmount, reader.GetErrors().Count);
        }

        [Test]
        public void Read_Invalid_Source()
        {
            var observations = new List<ObservationRow>() { null }.ToAsyncEnumerable();
            
            var reader = new SdmxObservationReader(observations, _reportedComponents, _dataflow, _codeTranslator, Configuration, true, false);

            Assert.IsFalse(reader.Read());
            Assert.IsTrue(reader.IsClosed);
            Assert.IsFalse(reader.Read());
            Assert.IsTrue(reader.IsClosed);
            Assert.AreEqual(reader.GetErrors().Count, 1);
            Assert.AreEqual(reader.GetErrors()[0].Type, ValidationErrorType.Undefined);
        }


        private IList<ObservationRow> GetValidObservations()
        {
            var orderedKey = new KeyableImpl(
                _dataflow.Base, 
                _dataflow.Dsd.Base,
                new List<IKeyValue>
                {
                    new KeyValueImpl("Q", "FREQ"),
                    new KeyValueImpl("DNK", "AME_REF_AREA"),
                    new KeyValueImpl("3", "AME_TRANSFORMATION"),
                    new KeyValueImpl("4", "AME_AGG_METHOD"),
                    new KeyValueImpl("319", "AME_UNIT"),
                    new KeyValueImpl("0", "AME_REFERENCE"),
                    new KeyValueImpl("UDGGL", "AME_ITEM"),
                }, 
                null
            );

            var unorderedKey = new KeyableImpl(
                _dataflow.Base,
                _dataflow.Dsd.Base,
                new List<IKeyValue>
                {
                    new KeyValueImpl("UDGGL", "AME_ITEM"),
                    new KeyValueImpl("3", "AME_TRANSFORMATION"),
                    new KeyValueImpl("DNK", "AME_REF_AREA"),
                    new KeyValueImpl("Q", "FREQ"),
                    new KeyValueImpl("319", "AME_UNIT"),
                    new KeyValueImpl("0", "AME_REFERENCE"),
                    new KeyValueImpl("4", "AME_AGG_METHOD"),
                },
                null
            );

            var observations= new List<ObservationRow>
            {
                // same order of attributes as in DSD ------------------------------------------------------------
                new (0, StagingRowActionEnum.Merge, new ObservationImpl(orderedKey, "2010", "7111", new []
                {
                    new KeyValueImpl("TIME_FORMAT - text", "TIME_FORMAT"),          // group lvl
                    new KeyValueImpl("A", "OBS_STATUS"),                            // obsevation lvl
                    new KeyValueImpl("F", "OBS_CONF"),                              // obsevation lvl
                    new KeyValueImpl("OBS_PRE_BREAK - text", "OBS_PRE_BREAK"),      // obsevation lvl
                    new KeyValueImpl("PUBL_ECB - text", "PUBL_ECB"),                // group lvl
                    new KeyValueImpl("PUBL_MU - text", "PUBL_MU"),                  // group lvl
                    new KeyValueImpl("PUBL_PUBLIC - text", "PUBL_PUBLIC"),          // group lvl
                    new KeyValueImpl("COMPILATION - text", "COMPILATION"),          // group lvl
                    new KeyValueImpl("EXT_TITLE - text", "EXT_TITLE"),              // group lvl
                    new KeyValueImpl("EXT_UNIT - text", "EXT_UNIT"),                // group lvl
                    new KeyValueImpl("TITLE_COMPL - text", "TITLE_COMPL")           // group lvl
                })),
                new (0, StagingRowActionEnum.Merge, new ObservationImpl(orderedKey, "2011", "7222", new []
                {
                    new KeyValueImpl("B", "OBS_STATUS")
                })),
                new (0, StagingRowActionEnum.Merge, new ObservationImpl(orderedKey, "2012", "7333", new []
                {
                    new KeyValueImpl("D", "OBS_STATUS")
                })),
                // order different from DSD -----------------------------------------------------------------------
                new (0, StagingRowActionEnum.Merge, new ObservationImpl(unorderedKey, "2015", "7444", new []
                {
                    new KeyValueImpl("TITLE_COMPL - text", "TITLE_COMPL"),
                    new KeyValueImpl("TIME_FORMAT - text", "TIME_FORMAT"),
                    new KeyValueImpl("EXT_UNIT - text", "EXT_UNIT"),
                    new KeyValueImpl("F", "OBS_CONF"),
                    new KeyValueImpl("OBS_PRE_BREAK - text", "OBS_PRE_BREAK"),
                    new KeyValueImpl("PUBL_ECB - text", "PUBL_ECB"),
                    new KeyValueImpl("A", "OBS_STATUS"),
                    new KeyValueImpl("COMPILATION - text", "COMPILATION"),
                    new KeyValueImpl("EXT_TITLE - text", "EXT_TITLE"),
                    new KeyValueImpl("PUBL_MU - text", "PUBL_MU"),
                    new KeyValueImpl("PUBL_PUBLIC - text", "PUBL_PUBLIC")

                })),
                new (0, StagingRowActionEnum.Merge, new ObservationImpl(unorderedKey, "2016", "7555", new []
                {
                    new KeyValueImpl("B", "OBS_STATUS")
                })),
                new (0, StagingRowActionEnum.Merge, new ObservationImpl(unorderedKey, "2017", "7666", new []
                {
                    new KeyValueImpl("D", "OBS_STATUS")
                })),
            };

            return observations;
        }

        private IList<ObservationRow> GetInvalidObservations()
        {
            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("Q", "FREQ"),
                new KeyValueImpl("DNK", "AME_REF_AREA"),
                new KeyValueImpl("3", "AME_TRANSFORMATION"),
                new KeyValueImpl("4", "AME_AGG_METHOD"),
                new KeyValueImpl("319", "AME_UNIT"),
                new KeyValueImpl("0", "AME_REFERENCE"),
                new KeyValueImpl("UDGGL", "AME_ITEM"),
            };

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);

            var observations=  new List<ObservationRow>
            {
                new (0, StagingRowActionEnum.Merge, new ObservationImpl(key, "2010", "1", null)),
                new (0, StagingRowActionEnum.Merge, new ObservationImpl(key, "2011", "2", new List<IKeyValue>(new []
                {
                    new KeyValueImpl("M", "OBS_STATUS")
                }))),
                new (0, StagingRowActionEnum.Merge, new ObservationImpl(key, "2011", "2", new List<IKeyValue>(new []
                {
                    new KeyValueImpl("N", "OBS_STATUS")
                }))),
                new (0, StagingRowActionEnum.Merge, new ObservationImpl(key, "2012", "3", new List<IKeyValue>(new []
                {
                    new KeyValueImpl("A", "OBS_STATUS")
                }))),
                new (0, StagingRowActionEnum.Merge, new ObservationImpl(key, "2013", "4", new List<IKeyValue>(new []
                {
                    new KeyValueImpl("B", "OBS_STATUS")
                }))),
                new (0, StagingRowActionEnum.Merge, new ObservationImpl(key, "2015", "string", null)), 
                new (0, StagingRowActionEnum.Merge, new ObservationImpl(key, "2016", "6", new List<IKeyValue>(new []
                {
                    new KeyValueImpl("", "OBS_STATUS")
                }))),
            };
            
            return observations;
        }

    }
}
