﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Domain;
using DotStat.MappingStore;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Api.Manager.Retrieval;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Util;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using Org.Sdmxsource.Util.Io;

namespace DotStat.Test.Moq
{
    public class TestMappingStoreDataAccess : IMappingStoreDataAccess
    {
        private Dataflow _dataflow;
        private ISdmxObjectRetrievalManager _objectRetrieval;
        private readonly string _dataflowPath;

        public TestMappingStoreDataAccess(string dataflowPath = "sdmx/264D_264_SALDI+2.1.xml")
        {
            _dataflowPath = dataflowPath;
        }

        public ISdmxObjects GetSdmxObjects(string filename)
        {
            ISdmxObjects sdmxObjects = new SdmxObjectsImpl();
            IStructureParsingManager structureParsingManager = new StructureParsingManager();
            using (IReadableDataLocation dataLocation = new FileReadableDataLocation(filename))
            {
                sdmxObjects.Merge(structureParsingManager.ParseStructures(dataLocation).GetStructureObjects(false));
            }
            return sdmxObjects;
        }

        public Dataflow GetDataflow(string dataSpace = null, string agencyId = null, string dataflowId = null, string version = null, bool throwErrorIfNotFound = true, ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies, bool useCache = true)
        {
            var sdmxObjects = this.GetSdmxObjects(_dataflowPath);
            _dataflow = _dataflow ?? SdmxParser.ParseStructure(sdmxObjects);

            if(_dataflow.Dsd.Msd==null)
                SetDsdMsd(dataSpace, _dataflow.Dsd);

            return _dataflow;
        }
        
        public ISdmxObjectRetrievalManager GetRetrievalManager(string dataSpace)
        {
            return _objectRetrieval = _objectRetrieval ?? new InMemoryRetrievalManager(this.GetSdmxObjects(_dataflowPath));
        }

        public void DeleteAllActualContentConstraints(string dataSpace, Dataflow dataflow)
        {}

        public void DeleteActualContentConstraint(string dataSpace, string agencyId, string id, string version, char targetTable, TargetVersion targetVersion)
        {}

        public void SaveContentConstraint(string dataSpace, Dataflow dataflow, IList<IKeyValuesMutable> cubeRegion, bool? includedCubeRegion, char targetTable, TargetVersion targetVersion, DateTime liveAccStartDate, int? obsCount = null)
        {}

        public void UpdateValidityOfContentConstraints(string dataSpace, Dataflow dataflow, char targetTable,
            TargetVersion targetVersion)
        {}

        public bool HasMappingSet(string dataSpace, Dataflow dataFlow)
        {
            //TODO: Create database mock
            return true;
        }

        public ISdmxObjects GetDsd(string dataSpace, string agencyId, string dsdId, string version)
        {
            var reference = new StructureReferenceImpl(agencyId, dsdId, version, SdmxStructureEnumType.Dsd);
            return this.GetRetrievalManager(dataSpace)
                .GetSdmxObjects(reference, ResolveCrossReferences.ResolveExcludeAgencies);
        }

        private void SetDsdMsd(string dataSpace, Dsd dsd)
        {
            if (!dsd.Base.Annotations.Any())
            {
                return;
            }

            var msdAnnotation = dsd.Base.Annotations.FirstOrDefault(x => x.Type.Equals("Metadata", StringComparison.OrdinalIgnoreCase));
            if (msdAnnotation == null || string.IsNullOrEmpty(msdAnnotation.Title))
            {
                return;
            }
            
            var reference = new StructureReferenceImpl(msdAnnotation.Title);
            var obj=this.GetRetrievalManager(dataSpace)
                .GetSdmxObjects(reference, ResolveCrossReferences.ResolveExcludeAgencies);

            dsd.Msd = new Msd(obj.MetadataStructures.FirstOrDefault());
        }

        public bool HasUserCreatedMappingSets(string dataSpace, Dataflow dataFlow)
        {
            return false;
        }
        public bool HasNonMigratedDataSets(string dataSpace, Dataflow dataFlow)
        {
            return false;
        }

        public bool DeleteDataSetsAndMappingSets(string dataSpace, Dataflow dataFlow, bool deleteMsdObjectsOnly)
        {
            return true;
        }

        public bool CreateOrUpdateMappingSet(
            string dataSpace, 
            Dataflow dataFlow,
            char targetVersion,
            DateTime? validFrom,
            DateTime? validTo,
            string query,
            string orderByTimeAsc,
            string orderByTimeDesc,
            bool isMetadata
         )
        {
            return true;
        }

        public IList<IDataStructureObject> GetDataStructures(string dataSpace)
        {
            var reference = new StructureReferenceImpl(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dsd));

            var ret = this.GetRetrievalManager(dataSpace)
                .GetSdmxObjects(reference, ResolveCrossReferences.DoNotResolve);

            if (ret.HasDataStructures)
                return ret.DataStructures.ToList();

            return new List<IDataStructureObject>();
        }

        public IList<IDataflowObject> GetDataflows(string dataSpace)
        {
            var reference = new StructureReferenceImpl(SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow));

            var ret = this.GetRetrievalManager(dataSpace)
                .GetSdmxObjects(reference, ResolveCrossReferences.DoNotResolve);

            if (ret.HasDataflows)
                return ret.Dataflows.ToList();

            return new List<IDataflowObject>();
        }
    }
}