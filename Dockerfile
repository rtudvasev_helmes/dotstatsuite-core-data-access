FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY DotStat.DbUp/*.csproj ./DotStat.DbUp/
COPY DotStat.DbUp.MsSql/*.csproj ./DotStat.DbUp.MsSql/
RUN dotnet restore DotStat.DbUp

# Copy everything else and build
COPY DotStat.DbUp ./DotStat.DbUp/
COPY DotStat.DbUp.MsSql ./DotStat.DbUp.MsSql/
WORKDIR /app/DotStat.DbUp
RUN dotnet publish -c Release -o out -r linux-x64

# Build runtime image
FROM mcr.microsoft.com/dotnet/runtime:6.0 AS runtime-env
WORKDIR /app
COPY --from=build-env /app/DotStat.DbUp/out/ .

# Single parameter
ENV EXECUTION_PARAMETERS=help

ENTRYPOINT dotnet DotStat.DbUp.dll $EXECUTION_PARAMETERS