using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Attribute = DotStat.Domain.Attribute;
using MetadataAttribute = DotStat.Domain.MetadataAttribute;
using PrimaryMeasure = DotStat.Domain.PrimaryMeasure;
using Dsd = DotStat.Domain.Dsd;
using DotStat.Db.Util;

namespace DotStat.Db
{
    public static class DbExtensions
    {
        public const string TimeDimColumn = "PERIOD_SDMX";
        public const string LAST_STATUS_COLUMN = "LAST_STATUS";
        public const string LAST_UPDATED_COLUMN = "LAST_UPDATED";
        public const string ArtefactTable = "ARTEFACT";
        private const string _valueToIndicateColumnPresent = "1";
        private static List<string> _wildCardValues = new() {"*", "#N/A", "N/A" };
        private static List<string> _valuesToIndicateIntentionallyMissing = new() { "#N/A", "NaN" };

        #region Naming conventions

        public static string SqlColumn(this Dimension dimension)
        {
            return dimension.Base.TimeDimension ? TimeDimColumn : $"[DIM_{dimension.DbId}]";
        }

        public static string SqlArtefactTableName(string alias = null)
        {
            return $"{ArtefactTable} {alias}".Trim();
        }

        public static string SqlPeriodStart(bool withType = false, bool withHourSupport = false, bool allowNull = false)
        {
            return SqlPeriod("PERIOD_START", withType, withHourSupport, allowNull);
        }

        public static string SqlPeriodEnd(bool withType = false, bool withHourSupport = false, bool allowNull = false)
        {
            return SqlPeriod("PERIOD_END", withType, withHourSupport, allowNull);
        }

        private static string SqlPeriod(string column, bool withType, bool withHourSupport, bool allowNull = false)
        {
            return column + (withType ? $" {(withHourSupport ? "datetime2(0)" : "date")} {(allowNull?"NULL":"NOT NULL")}" : "");
        }

        public static string SqlColumn(this Attribute attribute)
        {
            return $"[COMP_{attribute.DbId}]";
        }

        public static string SqlMinValue(this Attribute attribute)
        {
            return $"[COMP_{attribute.DbId}]";
        }

        public static string SqlColumn(this MetadataAttribute metaAttribute)
        {
            return $"[COMP_{metaAttribute.DbId}]";
        }

        public static string SqlStagingTable(this Dsd dsd, string alias = null)
        {
            return SqlStagingTable(dsd.DbId, alias);
        }

        public static string SqlStagingTable(int dsdDbId, string alias = null)
        {
            return $"DSD_{dsdDbId}_STAGING {alias}".Trim();
        }
        
        public static string SqlFactTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return $"FACT_{dsd.DbId}_{tableVersion} {alias}".Trim();
        }


        public static string SqlFilterTable(this Dsd dsd, string alias = null)
        {
            return $"FILT_{dsd.DbId} {alias}".Trim();
        }

        public static string SqlAttributeTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return $"ATTR_{dsd.DbId}_{tableVersion} {alias}".Trim();
        }

        public static string SqlDimGroupAttrTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return $"ATTR_{dsd.DbId}_{tableVersion} {alias}".Trim();
        }

        public static string SqlDsdAttrTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return $"ATTR_{dsd.DbId}_{tableVersion}_DF {alias}".Trim();
        }

        public static string SqlMetadataStagingTable(this Dsd dsd, string alias = null)
        {
            return SqlMetadataStagingTable(dsd.DbId, alias);
        }

        public static string SqlMetadataStagingTable(int dbId, string alias = null)
        {
            return $"MSD_{dbId}_STAGING {alias}".Trim();
        }

        public static string SqlMetadataTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return $"META_{dsd.DbId}_{tableVersion} {alias}".Trim();
        }

        public static string SqlMetadataDsdTable(this Dsd dsd, char tableVersion, string alias = null)
        {
            return $"META_{dsd.DbId}_{tableVersion}_DF {alias}".Trim();
        }

        public static string SqlMetadataAttributeTable(string alias = null)
        {
            return $"METADATA_ATTRIBUTE {alias}".Trim();
        }

        public static string SqlDataDsdViewName(this Dsd dsd, char viewVersion)
        {
            return SqlDataDsdViewName(dsd.DbId, viewVersion);
        }

        public static string SqlDataDsdViewName(int dsdDbId, char viewVersion)
        {
            return $"VI_CurrentDataDsd_{dsdDbId}_{viewVersion}";
        }

        public static string SqlDataDataFlowViewName(this Dataflow dataflow, char viewVersion)
        {
            return SqlDataDataFlowViewName(dataflow.DbId, viewVersion);
        }

        public static string SqlDataDataFlowViewName(int dataFlowDsdId, char viewVersion)
        {
            return $"VI_CurrentDataDataFlow_{dataFlowDsdId}_{viewVersion}";
        }

        public static string SqlMetaDataDsdViewName(this Dsd dsd, char viewVersion)
        {
            return SqlMetaDataDsdViewName(dsd.DbId, viewVersion);
        }

        public static string SqlMetaDataDsdViewName(int dsdDbId, char viewVersion)
        {
            return $"VI_MetadataDsd_{dsdDbId}_{viewVersion}";
        }

        public static string SqlMetadataDataFlowViewName(this Dataflow dataFlow, char viewVersion)
        {
            return SqlMetadataDataFlowViewName(dataFlow.DbId, viewVersion);
        }

        public static string SqlMetadataDataFlowViewName(int dataFlowDsdId, char viewVersion)
        {
            return $"VI_MetadataDataFlow_{dataFlowDsdId}_{viewVersion}";
        }
        
        public static string GetCodelistTableName(int codelistId)
        {
            return $"CL_{(codelistId == 0 ? "TIME" : codelistId.ToString())}";
        }
        

        #endregion

        public static string SqlBuildRowIdFormula(this Dsd dsd, bool convertNull = false, bool includeTimeDim = true)
        {
            var str = new StringBuilder();

            foreach (var dim in dsd.Dimensions)
            {
                if (!dim.Base.TimeDimension)
                {
                    str.Append("CONVERT([binary](3), ");
                    str.Append(convertNull ? $"ISNULL([DIM_{dim.DbId}], 0)" : $"[DIM_{dim.DbId}]");
                    str.Append(")+");
                }
            }

            if (dsd.TimeDimension != null && includeTimeDim)
            {
                str.Append("CONVERT([binary](3), ");
                str.Append(convertNull ? "ISNULL([DIM_TIME], 0)" : "[DIM_TIME]");
                str.Append(")");
            }

            return str.ToString().TrimEnd('+');
        }
        
        public static IEnumerable<Dimension> OrderTimeFirst(this IEnumerable<Dimension> dims)
        {
            return dims.OrderBy(x => x.Base.TimeDimension ? 0 : 1);
        }

        public static string ToColumnList(this IEnumerable<Dimension> dimensions, bool withType = false, string defaultDataType = null, bool allowNull=false)
        {
            return string.Join(",", dimensions.OrderTimeFirst().Select(dim => dim.SqlColumn() + (withType ? defaultDataType ?? $" {(dim.Base.TimeDimension ? "varchar(30)" : "int")} {(allowNull ? "NULL" : "NOT NULL")}" : "")));
        }

        public static string ToColumnList(this IEnumerable<Attribute> attributes, 
            int? maxTextAttributeLength, 
            IList<AttributeAttachmentLevel> levelFilter = null,
            bool withType = false, bool applyNotNull = true)
        {
            return string.Join(",", attributes
                .Where(attr => levelFilter == null || levelFilter.Contains(attr.Base.AttachmentLevel))
                .Select(attr => attr.SqlColumn() + attr.GetSqlType(withType, applyNotNull, maxTextAttributeLength))
            );
        }

        public static string ToColumnList(this IEnumerable<MetadataAttribute> MetadataAttributes,
            bool withType = false)
        {
            return string.Join(",", MetadataAttributes
                .Select(attr => attr.SqlColumn() + attr.GetSqlType(withType))
            );
        }

        private static string GetSqlType(this Attribute attr, bool withType, bool applyNotNull, int? maxTextAttributeLength)
        {
            if (!withType)
            {
                return null;
            }
            
            var maxAttributeLength =
                attr.Dsd.MaxTextAttributeLength ?? attr.Dsd.GetMaxLengthOfTextAttributeFromConfig(maxTextAttributeLength);

            var sqlType =
                $"[nvarchar]({(maxAttributeLength <= 0 || maxAttributeLength > 4000 ? "MAX" : maxAttributeLength.ToString())})";

            if (attr.Base.HasCodedRepresentation())
            {
                sqlType = "[int]";
            }

            return " " + sqlType + ((applyNotNull && attr.Base.Mandatory) ? " NOT NULL" : "");
        }

        private static string GetSqlType(this MetadataAttribute attr, bool withType)
        {
            if (!withType)
            {
                return null;
            }

            if (attr.Base.HasCodedRepresentation())
            {
                //0 when no value
                return "[int] NOT NULL";
            }

            return "[nvarchar](4000) ";

        }

        public static string GetSqlValue(this Attribute attr, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return "NULL";
            }

            return attr.Base.HasCodedRepresentation() ? value : $"{value.Replace("'", "''")}";
        }

        public static object GetSqlMinValue(this Attribute attr, bool asSqlString)
        {
            return attr.Base.HasCodedRepresentation() ? (asSqlString? "-1":-1) : (asSqlString ? "'#N/A'" : "#N/A");
        }

        public static string GetSqlValue(this MetadataAttribute attr, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return "NULL";
            }

            return attr.Base.HasCodedRepresentation() ? value : $"{value.Replace("'", "''")}";
        }
        
        public static object GetObjectFromString(this Attribute attribute, string attrStrVal, ICodeTranslator codeTranslator, StagingRowActionEnum action)
        {
            if (attrStrVal is null)
                return null;

            //Indicate that the value of this component is present in the delete operation
            if (action == StagingRowActionEnum.Delete)
            {
                if (attribute.Base.HasCodedRepresentation())
                    return int.TryParse(_valueToIndicateColumnPresent, out var integerValue) ? integerValue : _valueToIndicateColumnPresent;
                
                return _valueToIndicateColumnPresent;
            }
            
            if (string.IsNullOrEmpty(attrStrVal))
                return null;

            //Intentionally missing value
            if (_valuesToIndicateIntentionallyMissing.Any(w => w.Equals(attrStrVal, StringComparison.InvariantCultureIgnoreCase)))
                return attribute.GetSqlMinValue(false);
            

            return attribute.Base.HasCodedRepresentation()
                ? codeTranslator.TranslateCodeToId(attribute.Code, attrStrVal)
                : attrStrVal;
            
        }

        public static bool HasSupportDateTimeAnnotation(this IDataStructureObject dsd)
        {
            var hasAnnotation = dsd.Annotations.Any(a =>
                a.Type.Equals("SUPPORT_DATETIME", StringComparison.InvariantCultureIgnoreCase));

            return hasAnnotation;
        }

        public static string GetMaxLengthOfTextAttributeAnnotationValue(this IDataStructureObject dsd)
        {
            var annotation = dsd?.Annotations.FirstOrDefault(a =>
                a.Type.Equals("MaxTextAttributeLength", StringComparison.InvariantCultureIgnoreCase) &&
                !string.IsNullOrWhiteSpace(a.Title));

            return annotation?.Title;
        }

        public static int GetMaxLengthOfTextAttributeFromConfig(this Dsd dsd, int? maxTextAttributeLength)
        {
            var annotation = dsd.Base.GetMaxLengthOfTextAttributeAnnotationValue();

            if (!string.IsNullOrEmpty(annotation))
            {
                if (int.TryParse(annotation, out int maxLengthOfAttribute))
                {
                    return maxLengthOfAttribute;
                }

                Log.Warn(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .MaximumTextAttributeLengthInvalidDSDAnnotationValue), dsd.FullId, annotation));
            }

            return maxTextAttributeLength ?? 150;
        }

        public static string GetSqlType(this PrimaryMeasure primaryMeasure, IGeneralConfiguration generalConfiguration)
        {
            if (generalConfiguration == null)
            {
                throw new ArgumentNullException(nameof(generalConfiguration));
            }

            if (primaryMeasure.Codelist != null)
            {
                // Primary measure is coded, the code value is stored
                return "[varchar](150)"; // Identical to the data type of ID column in code list tables
            }

            // Measure is not coded, text format of representation defines the sql data type
            if (primaryMeasure.TextFormat == null)
            {
                // No representation is defined in DSD so the default string textFormat is applied.
                return "[nvarchar](max)";
            }

            switch (primaryMeasure.TextFormat.TextType.EnumType)
            {
                case TextEnumType.String:
                case TextEnumType.Alpha:
                case TextEnumType.Alphanumeric:
                case TextEnumType.Uri:
                {
                    var size = primaryMeasure.TextFormat.MaxLength.HasValue && primaryMeasure.TextFormat.MaxLength > 0 && primaryMeasure.TextFormat.MaxLength <= 4000
                        ? primaryMeasure.TextFormat.MaxLength.ToString()
                        : "MAX";

                    return $"[nvarchar]({size})";
                    }
                case TextEnumType.Numeric:
                case TextEnumType.BigInteger:
                case TextEnumType.Decimal:
                {
                    var size = primaryMeasure.TextFormat.MaxLength.HasValue && primaryMeasure.TextFormat.MaxLength > 0 && primaryMeasure.TextFormat.MaxLength <= 8000
                        ? primaryMeasure.TextFormat.MaxLength.ToString()
                        : "MAX";

                    return $"[varchar]({size})";
                }
                case TextEnumType.Integer:
                case TextEnumType.Count:
                    return "[int]";
                case TextEnumType.Long:
                    return "[bigint]";
                case TextEnumType.Short:
                case TextEnumType.Boolean:
                    return "[smallint]";
                case TextEnumType.Float:
                    return "[float](24)";
                case TextEnumType.Double:
                    return "[float](53)";
                case TextEnumType.ObservationalTimePeriod:
                case TextEnumType.StandardTimePeriod:
                case TextEnumType.BasicTimePeriod:
                case TextEnumType.GregorianTimePeriod:
                case TextEnumType.GregorianYear:
                case TextEnumType.GregorianYearMonth:
                case TextEnumType.GregorianDay:
                case TextEnumType.ReportingTimePeriod:
                case TextEnumType.ReportingYear:
                case TextEnumType.ReportingSemester:
                case TextEnumType.ReportingTrimester:
                case TextEnumType.ReportingQuarter:
                case TextEnumType.ReportingMonth:
                case TextEnumType.ReportingWeek:
                case TextEnumType.ReportingDay:
                case TextEnumType.DateTime:
                case TextEnumType.TimesRange:
                case TextEnumType.Month:
                case TextEnumType.MonthDay:
                case TextEnumType.Day:
                case TextEnumType.Time:
                case TextEnumType.Duration:
                    {
                        return "[varchar](100)";
                    }
                default:
                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.InvalidValueFormatNotSupported),
                        primaryMeasure.TextFormat.TextType.EnumType,
                        primaryMeasure.FullId));
            }
        }

        public static object GetSqlMinValue(this PrimaryMeasure primaryMeasure, bool asSqlString)
        {
            if (primaryMeasure.Codelist != null)
            {
                return asSqlString ? "-1": -1; 
            }

            // Measure is not coded, text format of representation defines the sql data type
            if (primaryMeasure.TextFormat == null)
            {
                // No representation is defined in DSD so the default string textFormat is applied.
                return asSqlString ? "'#N/A'" : "#N/A";
            }

            switch (primaryMeasure.TextFormat.TextType.EnumType)
            {
                case TextEnumType.String:
                case TextEnumType.Alpha:
                case TextEnumType.Alphanumeric:
                case TextEnumType.Uri:
                case TextEnumType.Numeric:
                case TextEnumType.BigInteger:
                case TextEnumType.Decimal:
                    return asSqlString ? "'#N/A'" : "#N/A";
                case TextEnumType.Integer:
                case TextEnumType.Count:
                    return asSqlString ? $"{SqlInt32.MinValue.Value}" : SqlInt32.MinValue.Value;//int
                case TextEnumType.Long:
                    return asSqlString ? $"{SqlInt64.MinValue.Value}" : SqlInt64.MinValue.Value;//bigint
                case TextEnumType.Short:
                case TextEnumType.Boolean:
                    return asSqlString ? $"{SqlInt16.MinValue.Value}" : SqlInt16.MinValue.Value;//smallint
                case TextEnumType.Float:
                    return asSqlString ? "CAST('-3.4e38' AS real)" : -3.4E38F; //"[float](24)";
                case TextEnumType.Double:
                    return asSqlString ? $"{SqlDouble.MinValue.Value}" : SqlDouble.MinValue.Value;//"[float](53)";
                case TextEnumType.ObservationalTimePeriod:
                case TextEnumType.StandardTimePeriod:
                case TextEnumType.BasicTimePeriod:
                case TextEnumType.GregorianTimePeriod:
                case TextEnumType.GregorianYear:
                case TextEnumType.GregorianYearMonth:
                case TextEnumType.GregorianDay:
                case TextEnumType.ReportingTimePeriod:
                case TextEnumType.ReportingYear:
                case TextEnumType.ReportingSemester:
                case TextEnumType.ReportingTrimester:
                case TextEnumType.ReportingQuarter:
                case TextEnumType.ReportingMonth:
                case TextEnumType.ReportingWeek:
                case TextEnumType.ReportingDay:
                case TextEnumType.DateTime:
                case TextEnumType.TimesRange:
                case TextEnumType.Month:
                case TextEnumType.MonthDay:
                case TextEnumType.Day:
                case TextEnumType.Time:
                case TextEnumType.Duration:
                    return asSqlString ? "'#N/A'" : "#N/A";
                default:
                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.InvalidValueFormatNotSupported),
                        primaryMeasure.TextFormat.TextType.EnumType,
                        primaryMeasure.FullId));
            }
        }
        
        public static object GetObjectFromString(this PrimaryMeasure primaryMeasure, string observationValueStr, StagingRowActionEnum action)
        {
            if (observationValueStr is null)
                return null;

            //Indicate that the value of this component should be set to NULL
            if (action == StagingRowActionEnum.Delete)
                observationValueStr = _valueToIndicateColumnPresent;

            if (string.IsNullOrEmpty(observationValueStr))
                return null;
            
            //Intentionally missing value
            if (_valuesToIndicateIntentionallyMissing.Any(w => w.Equals(observationValueStr, StringComparison.InvariantCultureIgnoreCase)))
                return primaryMeasure.GetSqlMinValue(false);
            
            if (primaryMeasure.Codelist != null || primaryMeasure.TextFormat == null)
            {
                // Primary measure is coded or Measure is not coded and no representation is defined in DSD so the default string textformat is applied.
                return observationValueStr;
            }

            switch (primaryMeasure.TextFormat.TextType.EnumType)
            {
                case TextEnumType.Integer:
                case TextEnumType.Count:
                    return int.TryParse(observationValueStr, out var integerValue) ? integerValue : (object) observationValueStr;
                case TextEnumType.Long:
                    return long.TryParse(observationValueStr, out var longValue) ? longValue : (object)observationValueStr;
                case TextEnumType.Short:
                    return short.TryParse(observationValueStr, out var shortValue) ? shortValue : (object)observationValueStr;
                case TextEnumType.Float:
                    return float.TryParse(observationValueStr, NumberStyles.Float, CultureInfo.InvariantCulture, out var floatValue) ? floatValue : (object)observationValueStr;
                case TextEnumType.Double:
                    return double.TryParse(observationValueStr, NumberStyles.Float, CultureInfo.InvariantCulture, out var doubleValue) ? doubleValue : (object)observationValueStr;
                case TextEnumType.Boolean:
                {
                    if (observationValueStr.Equals("1"))
                    {
                        return 1;
                    }

                    if (observationValueStr.Equals("0"))
                    {
                        return 0;
                    }

                    return bool.TryParse(observationValueStr, out var boolValue) ? (short)(boolValue ? 1 : 0) : (object)observationValueStr;
                }
                default:
                {
                    return observationValueStr;
                }
            }
        }

        public static string HarmonizeSpecialCaseValues(this string strVal)
        {
            if (string.IsNullOrEmpty(strVal))
                return null;
            if (_wildCardValues.Any(w => w.Equals(strVal, StringComparison.InvariantCultureIgnoreCase)))
                return null;
            if (_valuesToIndicateIntentionallyMissing.Any(w => w.Equals(strVal, StringComparison.InvariantCultureIgnoreCase)))
                return null;

            return strVal;
        }

        public static bool HasTextualRepresentation(this PrimaryMeasure primaryMeasure)
        {
            if (primaryMeasure.Codelist != null)
                return true;

            if (primaryMeasure.TextFormat == null)
                return true;

            if (primaryMeasure.TextFormat.TextType.EnumType == TextEnumType.BigInteger ||
                 primaryMeasure.TextFormat.TextType.EnumType == TextEnumType.Integer ||
                 primaryMeasure.TextFormat.TextType.EnumType == TextEnumType.Long ||
                 primaryMeasure.TextFormat.TextType.EnumType == TextEnumType.Short ||
                 primaryMeasure.TextFormat.TextType.EnumType == TextEnumType.Decimal ||
                 primaryMeasure.TextFormat.TextType.EnumType == TextEnumType.Float ||
                 primaryMeasure.TextFormat.TextType.EnumType == TextEnumType.Double ||
                 //To be revised, Boolean looks like it is not considered as numeric https://www.w3.org/TR/xmlschema11-2/#boolean thus doesn't support NaN
                 primaryMeasure.TextFormat.TextType.EnumType == TextEnumType.Boolean ||
                 primaryMeasure.TextFormat.TextType.EnumType == TextEnumType.Count)
                return false;

            return true;
        }

        public static bool HasTextualRepresentation(this Attribute attr)
        {
            //Coded attributes are considered of type Enumeration which is textual
            //At the moment we only support textual attributes
            return true;
        }
       
        #region Datareader

        public static T ColumnValue<T>(this IDataReader rd, string column, T def = default(T))
        {
            return rd[column].ColumnValue(def);
        }

        public static T ColumnValue<T>(this IDataReader rd, int columnIndex, T def = default(T))
        {
            return rd[columnIndex].ColumnValue(def);
        }

        public static T ColumnValue<T>(this DataRow row, string column, T def = default(T))
        {
            return row[column].ColumnValue(def);
        }

        public static T ColumnValue<T>(this DataRow row, int columnIndex, T def = default(T))
        {
            return row[columnIndex].ColumnValue(def);
        }

        private static T ColumnValue<T>(this object o, T def = default(T))
        {
            if (o == DBNull.Value || o == null)
                return def;

            if (o is T)
                return (T)o;

            return (T)Convert.ChangeType(o, typeof(T));
        }

        #endregion

        #region IObservation

        public static bool IsDeleteOperation(this IObservation observation, Dataflow dataflow)
        {
            var observationAttributes = dataflow.Dsd.Base.ObservationAttributes.Select(a => a.Id).ToList();

            return string.IsNullOrEmpty(observation.ObservationValue) && observation.Attributes.All(a =>
                       string.IsNullOrEmpty(a.Code) ||
                       !observationAttributes.Contains(a.Concept, StringComparer.InvariantCultureIgnoreCase));
        }

        public static string GetKey(this IObservation observation, bool withTime = true)
        {
            return string.Join(":", observation.SeriesKey.Key.Select(x => x.Code)) + (withTime ? ":" + observation.ObsTime : string.Empty);
        }

        public static string GetKeyStartEndPeriod(this IObservation observation, bool hasReportingYearStartDayAttr)
        {
            if (string.IsNullOrWhiteSpace(observation.ObsTime))
                return string.Join(":", observation.SeriesKey.Key.Select(x => x.Code));

            try
            {
                var (periodStart, periodEnd) = DateUtils.GetPeriod(
                    observation.ObsTime,
                    hasReportingYearStartDayAttr ? observation.GetAttribute(AttributeObject.Repyearstart)?.Code : null
                );

                var timeKeys = ":" + periodStart + ":" + periodEnd;
                return string.Join(":", observation.SeriesKey.Key.Select(x => x.Code)) + timeKeys;
            }
            catch
            {
                //Exclude time when it is wildCarded
                return string.Join(":", observation.SeriesKey.Key.Select(x => x.Code));
            }
        }
        
        public static string GetFullKey(this IObservation observation, bool withTime = true, string dimTimeConcept = "")
        {
            return string.Join(",", observation.SeriesKey.Key.Where(k => !string.IsNullOrEmpty(k.Code))
                .Select(x => $"{x.Concept}:{x.Code}")) + (withTime ? "," + $"{dimTimeConcept}:{observation.ObsTime}" : string.Empty);
        }
        
        #endregion

        #region IAttributeObject

        public static IList<string> GetDimensionReferences(this IAttributeObject attribute) {

            if (attribute.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
            {
                return attribute.DimensionReferences;
            }

            if (attribute.AttachmentLevel == AttributeAttachmentLevel.DataSet)
            {
                return new List<string>();
            }

            var dsd = (IDataStructureObject) attribute.IdentifiableParent.IdentifiableParent;

            if (dsd == null)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingDsdForAttribute),
                    attribute.GetFullIdPath(true))
                ); 
            }

            if (attribute.AttachmentLevel == AttributeAttachmentLevel.Observation)
            {
                return dsd.DimensionList.Dimensions.Select(d => d.Id).ToList();
            }

            if (attribute.AttachmentLevel != AttributeAttachmentLevel.Group)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnknownAttributeAttachmentLevel),
                    attribute.GetFullIdPath(true),
                    attribute.AttachmentLevel,
                    $"{dsd.AgencyId}:{dsd.Id}({dsd.Version})")
                );
            }

            IGroup group = null;
            foreach(var g in dsd.Groups)
            {
                if(string.Equals(g.Id, attribute.AttachmentGroup, StringComparison.OrdinalIgnoreCase))
                {
                    group = g;
                    break;
                }
            }

            if (group == null)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingGroupForAttribute),
                    attribute.AttachmentGroup,
                    attribute.GetFullIdPath(true))
                );
            }

            return group.DimensionRefs;
        }

        #endregion

        #region IKeyable

        public static string GetKey(this IKeyable keyable)
        {
            return string.Join(":", keyable.Key.Select(x => x.Code));
        }

        public static string GetFullKey(this IKeyable keyable, bool includeConcept = false)
        {
            var dimensionReferences = keyable.DataStructure.GetDimensions();
            var ret = includeConcept ?
                string.Join(",", dimensionReferences.Where(d => !string.IsNullOrEmpty(keyable.GetKeyValue(d.Id)))
                    .Select(d => $"{d.Id}:{keyable.GetKeyValue(d.Id)}"))
                : string.Join(":", dimensionReferences.Select(d => keyable.GetKeyValue(d.Id)));
            return ret;
        }

        public static IList<IKeyValue> GetKeyOfAttribute(this IKeyable keyable, IAttributeObject attribute)
        {
            var ret = new List<IKeyValue>();

            var dimensionReferences = attribute.GetDimensionReferences();

            foreach (var dimRef in dimensionReferences)
            {
                foreach(var key in keyable.Key)
                {
                    if (string.Equals(dimRef, key.Concept, StringComparison.OrdinalIgnoreCase))
                    {
                        ret.Add(new KeyValueImpl(key?.Code ?? "", dimRef));
                        break;
                    }
                }
            }

            return ret;
        }

        #endregion
    }

    public static class Extensions
    {
        public static IEnumerable<IEnumerable<T>> CartesianProduct<T>(this IEnumerable<IEnumerable<T>> sequences)
        {
            IEnumerable<IEnumerable<T>> emptyProduct = new[] { Enumerable.Empty<T>() };
            return sequences.Aggregate(
                emptyProduct,
                (accumulator, sequence) =>
                    from acc in accumulator
                    from item in sequence
                    select acc.Concat(new[] { item }));
        }
    }
}
