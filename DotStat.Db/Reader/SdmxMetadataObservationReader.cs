﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.Exception;
using DotStat.Db.Util;
using DotStat.Db.Validation;
using DotStat.DB;
using DotStat.DB.Reader;
using DotStat.DB.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DotStat.Db.Reader
{
    public sealed class SdmxMetadataObservationReader : BulkCopyDataReader
    {
        private readonly IAsyncEnumerator<ObservationRow> _enumerator;
        private readonly object[] _values;
        private bool Eof { get; set; }
        private ObservationRow CurrentRecord { get; set; }
        public int CurrentIndex { get; private set; }
        private int _errorCount;

        private Dataflow Dataflow { get; }

        private readonly DataflowMetadataBuilder _metadata;
        private readonly ICodeTranslator _codeTranslator;
        private readonly MetadataValidator _metadataValidator;
        private readonly IGeneralConfiguration _configuration;
        private readonly ReportedComponents _reportedComponents;

        public IDictionary<int, DataSetAttributeRow> DatasetMetadataAttributes { get; set; } = new Dictionary<int, DataSetAttributeRow>();

        public SdmxMetadataObservationReader(IAsyncEnumerable<ObservationRow> observations, ReportedComponents reportedComponents, Dataflow dataflow, ICodeTranslator codeTranslator, IGeneralConfiguration configuration, bool fullValidation, bool isTimeAtTimeDimensionSupported)
        {
            _enumerator = observations?.GetAsyncEnumerator();
            _codeTranslator = codeTranslator;

            Dataflow = dataflow;
            _configuration = configuration;
            _metadata = new DataflowMetadataBuilder(dataflow, reportedComponents);
            _metadataValidator = new MetadataValidator(dataflow, reportedComponents, configuration, fullValidation, isTimeAtTimeDimensionSupported);
            _reportedComponents = reportedComponents;
            _values = new object[FieldCount];
            CurrentIndex = 0;
            _errorCount = 0;
        }

        public override int FieldCount => Dataflow.Dimensions.Count + _reportedComponents.MetadataAttributes.Count + // only reported components 
                                          1 + // Requested Action
                                          1 + // Real Action
                                          (_metadata.HasTime ? 2 : 0); // time Period columns 

        public override object GetValue(int i) => _values[i];

        public override bool Read()
        {
            if (Eof)
            {
                return false;
            }

            do
            {
                var isValid = false;

                try
                {
                    if (_enumerator.MoveNextAsync().AsTask().Result)
                    {
                        CurrentRecord = _enumerator.Current;
                        isValid = _metadataValidator.ValidateObservation(CurrentRecord.Observation, ++CurrentIndex);

                        if (CurrentRecord.Action == StagingRowActionEnum.Merge &&
                            CurrentRecord.Observation.SeriesKey.Key.All(x => string.IsNullOrEmpty(x.Code)) &&
                            string.IsNullOrEmpty(CurrentRecord.Observation.ObsTime))
                        {
                            AddDsdLevelMetadata();
                            continue;
                        }

                        if (CurrentRecord.Action == StagingRowActionEnum.Delete)
                        {
                            AddDsdLevelMetadata();
                        }
                    }
                    else
                    {
                        Eof = true;
                        return false;
                    }
                }
                catch (ObservationReadException e)
                {
                    _metadataValidator.AddError(e.Error, CurrentIndex);
                }
                // Unrecoverable error while reading, we stop here
                catch (System.Exception e)
                {
                    _metadataValidator.AddError(ValidationErrorType.Undefined, CurrentIndex, null, e.Message);
                }

                if (isValid && _errorCount == 0)
                {
                    break;
                }

                if (!isValid && _configuration.MaxTransferErrorAmount > 0 && ++_errorCount >= _configuration.MaxTransferErrorAmount)
                {
                    Eof = true;
                    return false;
                }
            }
            while (true);

            Fill(_values);

            return !Eof;
        }

        public override bool IsClosed => Eof;

        private void Fill(object[] values)
        {
            Array.Clear(_values, 0, values.Length);

            if (CurrentRecord.Action == StagingRowActionEnum.DeleteAll)
            {
                var index = Dataflow.Dimensions.Count + _reportedComponents.MetadataAttributes.Count;

                values[index++] = (int)StagingRowActionEnum.Delete;
                values[index] = (int)StagingRowActionEnum.Delete;

                var newDataSetMetadataAttributeRow = new DataSetAttributeRow(CurrentRecord.DataSetNumber, StagingRowActionEnum.DeleteAll);

                if (!DatasetMetadataAttributes.TryGetValue(CurrentRecord.DataSetNumber, out _))
                {
                    DatasetMetadataAttributes.Add(CurrentRecord.DataSetNumber, newDataSetMetadataAttributeRow);
                }
                else
                {
                    DatasetMetadataAttributes[CurrentRecord.DataSetNumber] = newDataSetMetadataAttributeRow;
                }

                return;
            }


            if (CurrentRecord.Action != StagingRowActionEnum.Delete &&
                Dataflow.Dsd.TimeDimension != null &&
                CurrentRecord.Observation.ObsTime == null)
            {
                throw new DotStatException(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingDimensionMetadataImport), Dataflow.Dsd.TimeDimension.Code));
            }

            SetTimeDimensionValues(values);
            SetDimensionValues(values);

            var currentAttributeList = CurrentRecord.Observation.Attributes.Where(a =>
                _reportedComponents.MetadataAttributes.Any(r =>
                    r.HierarchicalId.Equals(a.Concept, StringComparison.InvariantCultureIgnoreCase))).ToList();

            var emptyMetadataAttributeCount = SetMetadataAttributeValues(values, currentAttributeList);

            var currentIndex = Dataflow.Dimensions.Count + _reportedComponents.MetadataAttributes.Count;

            values[currentIndex++] = (int)CurrentRecord.Action;

            StagingRowActionEnum realAction;
            if (CurrentRecord.Action == StagingRowActionEnum.Delete && emptyMetadataAttributeCount == currentAttributeList.Count)
            {
                realAction = StagingRowActionEnum.Delete;
            }
            else
            {
                realAction = StagingRowActionEnum.Merge;
            }

            values[currentIndex] = (int)realAction;
        }

        private int SetMetadataAttributeValues(IList<object> values, List<IKeyValue> currentAttributeList)
        {
            var currentIndex = Dataflow.Dimensions.Count;
            var emptyMetadataAttributeCount = 0;

            // metadata attribute values
            foreach (var attr in currentAttributeList)
            {
                var attrMeta = _metadata.MetaAttr(attr.Concept);

                if (string.IsNullOrEmpty(attr.Code))
                {
                    values[currentIndex + attrMeta.Index] = null;
                    emptyMetadataAttributeCount++;
                }
                else
                {
                    if (attrMeta.MetadataAttribute.Base.HasCodedRepresentation())
                    {
                        values[currentIndex + attrMeta.Index] =
                            attr.Code.Equals("#N/A", StringComparison.OrdinalIgnoreCase)
                                ? 0
                                : _codeTranslator.TranslateCodeToId(attr.Concept, attr.Code);
                    }
                    else
                    {
                        values[currentIndex + attrMeta.Index] = attr.Code;
                    }
                }
            }

            return emptyMetadataAttributeCount;
        }

        private void SetDimensionValues(IList<object> values)
        {
            foreach (var dim in Dataflow.Dimensions.Where(dim => !dim.Base.TimeDimension))
            {
                var key = CurrentRecord.Observation.SeriesKey.Key.FirstOrDefault(k => k.Concept.Equals(dim.Code, StringComparison.InvariantCultureIgnoreCase));

                if (key == null)
                {
                    if (CurrentRecord.Action != StagingRowActionEnum.Delete)
                    {
                        throw new DotStatException(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingDimensionMetadataImport), dim.Code));
                    }

                    // Missing or empty -> anything including NULL
                    values[_metadata.NonReportedDim(dim.Code).Index] = null;
                }
                else if (string.IsNullOrEmpty(key.Code))
                {
                    // Missing or empty -> null (anything including NULL)
                    values[_metadata[dim.Code].Index] = null;
                }
                else if (key.Code.Equals("*", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (CurrentRecord.Action != StagingRowActionEnum.Delete)
                    {
                        throw new DotStatException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingDimensionMetadataImport),
                            dim.Code));
                    }

                    // * -> NOT NULL
                    values[_metadata[dim.Code].Index] = -1;
                }
                else if (key.Code.Equals("-", StringComparison.InvariantCultureIgnoreCase))
                {
                    // - -> only NULL
                    values[_metadata[dim.Code].Index] = 0;
                }
                else
                {
                    // Actual dim value
                    values[_metadata[dim.Code].Index] = _codeTranslator.TranslateCodeToId(key.Concept, key.Code);
                }
            }
        }

        private void SetTimeDimensionValues(IList<object> values)
        {
            if (string.IsNullOrEmpty(CurrentRecord.Observation.ObsTime))
            {
                // Missing or empty -> anything including NULL
                values[0] = null;
                values[FieldCount - 2] = null;
                values[FieldCount - 1] = null;
            }
            else if (CurrentRecord.Observation.ObsTime.Equals("*", StringComparison.InvariantCultureIgnoreCase))
            {
                if (CurrentRecord.Action != StagingRowActionEnum.Delete)
                {
                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingDimensionMetadataImport),
                        Dataflow.Dsd.TimeDimension?.Code));
                }

                // * -> NOT NULL
                values[0] = null;
                values[FieldCount - 2] = DateTime.MaxValue.AddYears(-1);
                values[FieldCount - 1] = DateTime.MinValue.AddYears(1);
            }
            else if (CurrentRecord.Observation.ObsTime.Equals("-", StringComparison.InvariantCultureIgnoreCase))
            {
                // - -> only NULL
                values[0] = null;
                values[FieldCount - 2] = DateTime.MaxValue;
                values[FieldCount - 1] = DateTime.MinValue;
            }
            else
            {
                // Actual dim value
                var (periodStart, periodEnd) = DateUtils.GetPeriod(CurrentRecord.Observation.ObsTime,
                    _metadata.HasReportingYearStartDayAttr
                        ? CurrentRecord.Observation.GetAttribute(AttributeObject.Repyearstart)?.Code
                        : null);

                values[0] = CurrentRecord.Observation.ObsTime;
                values[FieldCount - 2] = periodStart;
                values[FieldCount - 1] = periodEnd;
            }
        }

        private void AddDsdLevelMetadata()
        {
            if (!CurrentRecord.Observation.Attributes.Any())
            {
                foreach (var metadataAttribute in _reportedComponents.MetadataAttributes)
                {
                    AddDatasetMetadataAttribute(new KeyValueImpl("*", metadataAttribute.HierarchicalId));
                }

                return;
            }

            foreach (var attr in CurrentRecord.Observation.Attributes.Where(a => _reportedComponents.MetadataAttributes.Any(r =>
                         r.HierarchicalId.Equals(a.Concept, StringComparison.InvariantCultureIgnoreCase))))
            {
                AddDatasetMetadataAttribute(attr);
            }
        }

        private void AddDatasetMetadataAttribute(IKeyValue attr)
        {
            if (!DatasetMetadataAttributes.TryGetValue(CurrentRecord.DataSetNumber, out var dataSetAttributeRow))
            {
                DatasetMetadataAttributes.Add(CurrentRecord.DataSetNumber, new DataSetAttributeRow(CurrentRecord.DataSetNumber, CurrentRecord.Action));
                DatasetMetadataAttributes[CurrentRecord.DataSetNumber].Attributes.Add(attr);
            }
            else
            {
                var existingAttribute = dataSetAttributeRow.Attributes.FirstOrDefault(a =>
                    a.Concept.Equals(attr.Concept, StringComparison.CurrentCultureIgnoreCase));

                if (existingAttribute == null)
                {
                    DatasetMetadataAttributes[CurrentRecord.DataSetNumber].Attributes.Add(attr);
                }
                else if (existingAttribute.Code != attr.Code)
                {
                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MultipleValuesForDatasetAttribute),
                        existingAttribute.Code, attr.Code, attr.Concept));
                }
            }
        }

        public List<IValidationError> GetErrors() => _metadataValidator.GetErrors();

        public int GetObservationsCount() => CurrentIndex;

        public override async void Close()
        {
            await _enumerator.DisposeAsync();
        }
    }
}