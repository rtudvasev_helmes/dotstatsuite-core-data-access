﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.DB;
using DotStat.Db.Exception;
using DotStat.DB.Reader;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Db.Validation;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Db.Reader
{
    public sealed class SdmxObservationReader : BulkCopyDataReader
    {
        private readonly IAsyncEnumerator<ObservationRow> _enumerator;
        private readonly object[] _values;
        private readonly bool[] _observationLvlComponentsPresent;
        private readonly bool[] _seriesLvlComponentsPresent;
        private readonly int _observationLvlComponentsCount;
        private readonly int _seriesLvlComponentsCount;

        private bool Eof { get; set; }
        private ObservationRow CurrentRecord { get; set; }
        public int CurrentIndex { get; private set; }
        private int _errorCount;
        private int _deletionCount;
        private int _mergesCount;
        private int _skipCount;

        private Dataflow Dataflow { get; }

        private readonly DataflowMetadataBuilder _metadata;
        private readonly CodeTranslator _codeTranslator;
        private readonly ObservationValidator _observationValidator;
        private readonly IGeneralConfiguration _configuration;
        private readonly ReportedComponents _reportedComponents;
        private readonly List<string> _allReportedComponentConcepts;


        public IDictionary<int, DataSetAttributeRow> DataSetAttributes { get; set; }

        public SdmxObservationReader(IAsyncEnumerable<ObservationRow> observationRows, ReportedComponents reportedComponents, Dataflow dataflow, CodeTranslator codeTranslator, IGeneralConfiguration configuration, bool fullValidation, bool isTimeAtTimeDimensionSupported)
        {
            _enumerator = observationRows?.GetAsyncEnumerator();

            _codeTranslator = codeTranslator;
            Dataflow = dataflow;
            _configuration = configuration;

            DataSetAttributes = new Dictionary<int, DataSetAttributeRow>();

            _metadata = new DataflowMetadataBuilder(dataflow, reportedComponents);
            _observationValidator = new ObservationValidator(dataflow, reportedComponents, configuration, fullValidation, isTimeAtTimeDimensionSupported, _metadata);
            _reportedComponents = reportedComponents;
            CurrentIndex = 0;
            CurrentIndex = 0;
            _errorCount = 0;

            _values = new object[FieldCount];

            var observationLvlComponents = dataflow.Dsd.Attributes.Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                                                                               || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();
            _observationLvlComponentsCount = observationLvlComponents.Count;
            _observationLvlComponentsPresent = new bool[_observationLvlComponentsCount + (_reportedComponents.IsPrimaryMeasureReported ? 1 : 0)];//+primary measure

            var seriesLvlComponents = dataflow.Dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.Group or AttributeAttachmentLevel.DimensionGroup)
                    .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();
            _seriesLvlComponentsCount = seriesLvlComponents.Count;
            _seriesLvlComponentsPresent = new bool[_seriesLvlComponentsCount];

            _allReportedComponentConcepts = new List<string>(
                dataflow.Dimensions.Select(d => d.Code)
                    .Concat(observationLvlComponents.Select(c => c.Code)
                        .Concat(seriesLvlComponents.Select(c => c.Code))));
            if (_reportedComponents.IsPrimaryMeasureReported)
                _allReportedComponentConcepts.Add(dataflow.Dsd.PrimaryMeasure.Code);
        }

        #region BulkCopyDataReader

        public override int FieldCount => _metadata.DimensionCount + _metadata.NonDatasetAttributeCount + // only reported components 
                                          (_reportedComponents.IsPrimaryMeasureReported ? 1 : 0) + //value (if reported)
                                          2 + //Requested Action Column & Batch_Number column
                                          1 + // Real Action Fact
                                          1 + // Real Action Attr
                                          (_metadata.HasTime ? 2 : 0); // time Period columns 

        public override object GetValue(int i) => _values[i];

        public override bool Read()
        {
            if (Eof)
                return false;

            do
            {
                var isValid = false;
                try
                {
                    if (_enumerator.MoveNextAsync().AsTask().Result)
                    {
                        CurrentRecord = _enumerator.Current;
                        CurrentIndex++;
                        if (_enumerator.Current.Action is StagingRowActionEnum.Delete or StagingRowActionEnum.DeleteAll)
                            _deletionCount++;
                        else
                            _mergesCount++;

                        isValid = _observationValidator.ValidateObservation(CurrentRecord.Observation, CurrentIndex, _enumerator.Current.Action);
                    }
                    else
                    {
                        Eof = true;
                        return false;
                    }
                }
                catch (ObservationReadException e)
                {
                    _observationValidator.AddError(e.Error, CurrentIndex);
                }
                // Unrecoverable error while reading, we stop here
                catch (System.Exception e)
                {
                    _observationValidator.AddError(ValidationErrorType.Undefined, CurrentIndex, null, e.Message);
                }

                if (isValid && _errorCount == 0)
                {
                    break;
                }

                if (!isValid && _configuration.MaxTransferErrorAmount > 0 && ++_errorCount >= _configuration.MaxTransferErrorAmount)
                {
                    Eof = true;
                    return false;
                }
            }
            while (true);

            Fill(_values);

            return !Eof;
        }

        public override bool IsClosed => Eof;

        #endregion

        private void Fill(object[] values)
        {
            Array.Clear(_values, 0, values.Length);
            Array.Clear(_observationLvlComponentsPresent, 0, _observationLvlComponentsCount);
            Array.Clear(_seriesLvlComponentsPresent, 0, _seriesLvlComponentsCount);

            //Special action Delete all operation
            //When either all components are omitted or all are present and all dimensions are omitted
            if (CurrentRecord.Action is StagingRowActionEnum.DeleteAll)
            {
                var x = _metadata.DimensionCount + _metadata.NonDatasetAttributeCount + (_reportedComponents.IsPrimaryMeasureReported ? 1 : 0);

                // Requested Action
                values[x++] = (int)StagingRowActionEnum.Delete;
                // Real Action observation level
                values[x++] = (int)StagingRowActionEnum.Delete;
                // Real Action series level
                values[x] = (int)StagingRowActionEnum.Delete;

                //Add DataSetAttributeRow to indicate that all Df attributes should also be deleted
                var newDataSetAttributeRow =
                    new DataSetAttributeRow(CurrentRecord.DataSetNumber, StagingRowActionEnum.DeleteAll);

                if (!DataSetAttributes.TryGetValue(CurrentRecord.DataSetNumber, out _))
                    DataSetAttributes.Add(CurrentRecord.DataSetNumber, newDataSetAttributeRow);
                else
                    DataSetAttributes[CurrentRecord.DataSetNumber] = newDataSetAttributeRow;

                return;
            }

            //Dimensions
            var reportedDimensions = new List<Dimension>();
            //time dimension, index - 0
            var timeDimensionPresent = false;
            if (_metadata.HasTime && !string.IsNullOrEmpty(CurrentRecord.Observation.ObsTime))
            {
                timeDimensionPresent = true;
                reportedDimensions.Add(_metadata.TimeDim);
                var (periodStart, periodEnd) = DateUtils.GetPeriod(
                    CurrentRecord.Observation.ObsTime,
                    _metadata.HasReportingYearStartDayAttr ? CurrentRecord.Observation.GetAttribute(AttributeObject.Repyearstart)?.Code : null
                );

                values[0] = CurrentRecord.Observation.ObsTime;
                values[FieldCount - 2] = periodStart;
                values[FieldCount - 1] = periodEnd;
            }

            // dimensions should respect order of DSD, used on table creation
            foreach (var key in CurrentRecord.Observation.SeriesKey.Key)
            {
                if (_metadata.HasTime &&
                    key.Concept.Equals(_metadata.TimeDim.Code, StringComparison.CurrentCultureIgnoreCase))
                    continue;

                if (string.IsNullOrEmpty(key.Code)) continue;

                reportedDimensions.Add(_metadata[key.Concept].Dimension);
                values[_metadata[key.Concept].Index] = _codeTranslator.TranslateCodeToId(key.Concept, key.Code);
            }

            var ii = _metadata.DimensionCount;

            //Measure
            var obsLevelCount = 0;
            if (_reportedComponents.IsPrimaryMeasureReported)
            {
                var obsValue = Dataflow.Dsd.PrimaryMeasure.GetObjectFromString(CurrentRecord.Observation.ObservationValue, CurrentRecord.Action);

                values[ii++] = obsValue;

                if (obsValue is not null)
                {
                    _observationLvlComponentsPresent[obsLevelCount++] = true;
                }
            }

            //Attributes
            var seriesLevelCount = 0;
            var hasSkippedComponentsAtObsLevel = false;
            var hasSkippedComponentsAtSeriesLevel = false;
            var hasDataSetLevelComponentsPresent = false;
            foreach (var attr in CurrentRecord.Observation.Attributes.Where(a => a.Code is not null))
            {
                var attrMeta = _metadata.Attr(attr.Concept);
                var attachmentLevel = attrMeta.Attribute.Base.AttachmentLevel;

                //DataSet Level attributes
                if (attachmentLevel == AttributeAttachmentLevel.DataSet)
                {
                    hasDataSetLevelComponentsPresent = true;
                    if (!DataSetAttributes.TryGetValue(CurrentRecord.DataSetNumber, out var dataSetAttributeRow))
                    {
                        DataSetAttributes.Add(CurrentRecord.DataSetNumber, new DataSetAttributeRow(CurrentRecord.DataSetNumber, CurrentRecord.Action) { Attributes = new List<IKeyValue> { attr } });
                    }
                    else
                    {
                        var existingAttribute = dataSetAttributeRow.Attributes.FirstOrDefault(a =>
                            a.Concept.Equals(attr.Concept, StringComparison.CurrentCultureIgnoreCase));
                        if (existingAttribute == null)
                        {
                            DataSetAttributes[CurrentRecord.DataSetNumber].Attributes.Add(attr);
                        }
                        else if (!string.IsNullOrEmpty(existingAttribute.Code) && 
                                 !string.IsNullOrEmpty(attr.Code)  && 
                                 !existingAttribute.Code.Equals(attr.Code, StringComparison.OrdinalIgnoreCase))
                        {
                            throw new DotStatException(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MultipleValuesForDatasetAttribute),
                                existingAttribute.Code, attr.Code, attr.Concept));
                        }
                        else
                        {
                            DataSetAttributes[CurrentRecord.DataSetNumber].Action = CurrentRecord.Action;
                            DataSetAttributes[CurrentRecord.DataSetNumber].Attributes = new List<IKeyValue>() { attr };
                        }
                    }
                }
                //Non-DataSet Level attributes
                else
                {
                    var attrValue = attrMeta.Attribute.GetObjectFromString(attr.Code, _codeTranslator, CurrentRecord.Action);

                    var storedInObsLevel = _reportedComponents.ObservationAttributes.Any(a =>
                            a.Code.Equals(attr.Concept, StringComparison.CurrentCultureIgnoreCase));

                    //No need to skip obs lvl attributes because they always reference all the reported dimensions 
                    if (CurrentRecord.Action == StagingRowActionEnum.Delete && attachmentLevel != AttributeAttachmentLevel.Observation)
                    {
                        var groupDimRefs = Dataflow.Dsd.Base.Groups.Where(g =>
                                g.Id.Equals(attrMeta.Attribute.Base.AttachmentGroup,
                                    StringComparison.InvariantCultureIgnoreCase)).Select(g => g.DimensionRefs)
                            .FirstOrDefault();

                        if (groupDimRefs is { Count: > 0 })
                        {
                            var dimensionsReferenced = Dataflow.Dsd.Dimensions
                                .Where(dim => groupDimRefs.Contains(dim.Code)).ToList();

                            //Mark attribute as omitted, because it does not reference all dimensions that are reported (present)
                            //The dimensions reported contain at least one which is not part of the referenced dimensions of this attribute
                            if (dimensionsReferenced.Any(dim => !reportedDimensions.Any(d =>
                                    d.Code.Equals(dim.Code, StringComparison.CurrentCultureIgnoreCase))))
                            {
                                if (storedInObsLevel)
                                    hasSkippedComponentsAtObsLevel = true;
                                else
                                    hasSkippedComponentsAtSeriesLevel = true;

                                attrValue = null;

                                Log.Warn(string.Format(
                                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeSkipped),
                                    attr.Concept, CurrentRecord.Observation?.GetFullKey(true, CurrentRecord.Observation.SeriesKey.DataStructure.TimeDimension?.Id), CurrentIndex));
                            }
                        }
                    }

                    //Keep track of the components that are present to calculate the real action
                    if (attrValue is not null)
                    {
                        if (storedInObsLevel)
                        {
                            _observationLvlComponentsPresent[obsLevelCount++] = true;
                        }
                        else
                        {
                            _seriesLvlComponentsPresent[seriesLevelCount++] = true;
                        }
                    }

                    values[ii + attrMeta.Index] = attrValue;
                }
            }

            var allObsLevelComponentsOmitted = _observationLvlComponentsPresent.All(a => !a);
            var allSeriesLevelComponentsOmitted = _seriesLvlComponentsPresent.All(a => !a);

            var allObsLevelComponentsPresentOrAllOmitted = _observationLvlComponentsPresent.Distinct().Count() == 1;
            var allSeriesLevelComponentsPresentOrAllOmitted = _seriesLvlComponentsPresent.Distinct().Count() == 1;

            // --------------------------------------------------------------

            ii = _metadata.DimensionCount + _metadata.NonDatasetAttributeCount + (_reportedComponents.IsPrimaryMeasureReported ? 1 : 0);

            // Requested Action
            values[ii++] = (int)CurrentRecord.Action;

            // Real Action observation level (stored in the fact table)
            var observationLevelRealAction = CurrentRecord.Action switch
            {
                //When all components at this level are omitted & there is at least one component present in the series level
                //Then no action needed at this level
                StagingRowActionEnum.Delete when allObsLevelComponentsOmitted &&
                                                 !allSeriesLevelComponentsOmitted => StagingRowActionEnum.Skip,

                //When all components at this level are omitted & there is at least one component present in the dataSet level
                //Then no action needed at this level
                StagingRowActionEnum.Delete when allObsLevelComponentsOmitted &&
                                                 hasDataSetLevelComponentsPresent => StagingRowActionEnum.Skip,

                //When all components at this level are present, or all omitted, and no components where marked to be skipped,
                //And no components have been reported at other levels
                //Then it is a real deletion at this level
                StagingRowActionEnum.Delete when allObsLevelComponentsPresentOrAllOmitted &&
                                                 !hasSkippedComponentsAtObsLevel => StagingRowActionEnum.Delete,
                //When all components at this level are not present (cannot have skipped values if all was omitted),
                //and at least one component was marked to be skipped, Then this row should be skipped
                StagingRowActionEnum.Delete when allObsLevelComponentsPresentOrAllOmitted &&
                                                 hasSkippedComponentsAtObsLevel => StagingRowActionEnum.Skip,

                //When all components at this level are omitted, then no action needed at this level
                StagingRowActionEnum.Merge when allObsLevelComponentsOmitted => StagingRowActionEnum.Skip,
                _ => StagingRowActionEnum.Merge
            };

            values[ii++] = (int)observationLevelRealAction;

            // Real Action series level (stored in attr table) 
            var seriesLevelRealAction = CurrentRecord.Action switch
            {
                //In a delete operation, the attributes stored at series level (attr table)
                //cannot be modified if the time dimension is present, regardless if some, all or no attributes at this level are present.
                StagingRowActionEnum.Delete when timeDimensionPresent => StagingRowActionEnum.Skip,

                //When all components at this level are omitted & there is at least one component present in the obs level
                //Then no action needed at this level
                StagingRowActionEnum.Delete when allSeriesLevelComponentsOmitted &&
                                                 !allObsLevelComponentsOmitted => StagingRowActionEnum.Skip,

                //When all components at this level are omitted & there is at least one component present in the dataSet level
                //Then no action needed at this level
                StagingRowActionEnum.Delete when allSeriesLevelComponentsOmitted &&
                                                 hasDataSetLevelComponentsPresent => StagingRowActionEnum.Skip,

                //When all components at this level are present, or all omitted, and no components where marked to be skipped,
                //Then it is a real deletion at this level
                StagingRowActionEnum.Delete when allSeriesLevelComponentsPresentOrAllOmitted &&
                                                 !hasSkippedComponentsAtSeriesLevel => StagingRowActionEnum.Delete,
                //When all components at this level are not present (cannot have skipped values if all was omitted),
                //and at least one component was marked to be skipped, Then this row should be skipped
                StagingRowActionEnum.Delete when allSeriesLevelComponentsPresentOrAllOmitted &&
                                                 hasSkippedComponentsAtSeriesLevel => StagingRowActionEnum.Skip,

                //When all components at this level are omitted, then no action needed at this level
                StagingRowActionEnum.Merge when allSeriesLevelComponentsOmitted => StagingRowActionEnum.Skip,
                _ => StagingRowActionEnum.Merge
            };

            values[ii] = (int)seriesLevelRealAction;

            if (seriesLevelRealAction == StagingRowActionEnum.Skip && observationLevelRealAction == StagingRowActionEnum.Skip)
                _skipCount++;
        }

        public List<IValidationError> GetErrors() => _observationValidator.GetErrors();

        public int GetObservationsCount() => CurrentIndex;
        public int GetDeletionCount() => _deletionCount;
        public int GetMergesCount() => _mergesCount;
        public int GetSkipCount() => _skipCount;

        public override async void Close()
        {
            await _enumerator.DisposeAsync();
        }
    }
}