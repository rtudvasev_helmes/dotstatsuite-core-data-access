﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class BigIntegerValidator : NumericPatternMinMaxValueValidator
    {
        private const string ValidationExpression = @"^(\+|-)?[0-9]+$|^(NaN)$";

        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotBigInteger;

        public BigIntegerValidator(decimal? minValue, decimal? maxValue, bool allowNullValue = true) : base(ValidationExpression, minValue, maxValue, allowNullValue)
        {
        }
    }
}
