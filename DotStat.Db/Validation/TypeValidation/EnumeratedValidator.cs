﻿using System;
using DotStat.Domain;

namespace DotStat.DB.Validation.TypeValidation
{
    public class EnumeratedValidator : SimpleSdmxTypeValidator
    {
        private readonly PrimaryMeasure _primaryMeasure;

        public EnumeratedValidator(PrimaryMeasure primaryMeasure, bool allowNullValue = true) : base(allowNullValue)
        {
            _primaryMeasure = primaryMeasure ?? throw new ArgumentNullException(nameof(primaryMeasure));

            if (primaryMeasure.Codelist == null)
            {
                throw new ArgumentException(nameof(primaryMeasure.Codelist));
            }
        }

        public override bool IsValid(string observationValue)
        {
            ValidationError = Db.Validation.ValidationErrorType.Undefined;

            if ((AllowNullValue && string.IsNullOrEmpty(observationValue)) || _primaryMeasure.HasCode(observationValue))
            {
                return true;
            }

            ValidationError = Db.Validation.ValidationErrorType.InvalidValueFormatUnknownCodeMember;

            ExtraErrorMessageParameters = new[] { _primaryMeasure.Codelist.FullId };

            return false;
        }
    }
}
