﻿using System;
using System.Globalization;
using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class DoubleValidator : SimpleSdmxMinMaxValueValidator
    {
        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotDouble;

        public DoubleValidator(decimal? minValue, decimal? maxValue, bool allowNullValue = true) : base(minValue, maxValue, allowNullValue)
        {
        }

        protected override bool TryParseDecimalOutFunction(string observationValue, out decimal? decimalForMinMaxCheck, out bool isNegative)
        {
            decimalForMinMaxCheck = null;

            if (!double.TryParse(observationValue, NumberStyles.Float, NumberFormatInfo.InvariantInfo, out var doubleValue) || !double.IsFinite(doubleValue))
            {
                isNegative = false;
                return false;
            }

            isNegative = doubleValue < 0;
            try
            {
                decimalForMinMaxCheck = (decimal)doubleValue;
            }
            catch (OverflowException)
            {
                decimalForMinMaxCheck = (doubleValue < 1 && doubleValue > -1) ?
                    new decimal(1, 0, 0, isNegative, 28)
                    : (decimal?)null;
            }

            return true;
        }
    }
}
