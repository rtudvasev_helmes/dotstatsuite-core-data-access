﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class DecimalValidator : NumericPatternMinMaxValueValidator
    {
        private const string ValidationExpression = @"^(\+|-)?([0-9]+(\.[0-9]+)?|\.[0-9]+)$|^(NaN)$";

        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotDecimal;

        public DecimalValidator(decimal? minValue, decimal? maxValue, bool allowNullValue = true) : base(ValidationExpression, minValue, maxValue, allowNullValue)
        {
        }
    }
}
