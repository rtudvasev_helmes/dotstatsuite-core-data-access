﻿using System;
using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class FloatValidator : SimpleSdmxMinMaxValueValidator
    {
        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotFloat;

        public FloatValidator(decimal? minValue, decimal? maxValue, bool allowNullValue = true) : base(minValue, maxValue, allowNullValue)
        {
        }

        protected override bool TryParseDecimalOutFunction(string observationValue, out decimal? decimalForMinMaxCheck, out bool isNegative)
        {
            decimalForMinMaxCheck = null;
            isNegative = false;

            if (!float.TryParse(observationValue, out var floatValue) || !float.IsFinite(floatValue))
            {
                return false;
            }

            isNegative = floatValue < 0;
            try
            {
                decimalForMinMaxCheck = (decimal)floatValue;
            }
            catch (OverflowException)
            {
                decimalForMinMaxCheck = (floatValue < 1 && floatValue > -1) ?
                    new decimal(1, 0, 0, isNegative, 28)
                    : (decimal?)null;
            }
            
            return true;
        }
    }
}
