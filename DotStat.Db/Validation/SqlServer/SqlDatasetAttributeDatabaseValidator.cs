﻿using System.Collections.Generic;
using System.Linq;
using Attribute = DotStat.Domain.Attribute;
using System.Data.SqlClient;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Validation.SqlServer
{
    public class SqlDatasetAttributeDatabaseValidator : DatasetAttributeDatabaseValidator, ISqlDatasetAttributeDatabaseValidator
    {
        public SqlDatasetAttributeDatabaseValidator(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        protected override async Task CheckAttributeValuesInDatabase(IDotStatDb dotStatDataDb, List<Attribute> attributeList, DbTableVersion tableVersion, bool fullValidation, CancellationToken cancellationToken)
        {
            if (attributeList.Count == 0)
                return;
            var attrTableName = Dataflow.Dsd.SqlDsdAttrTable((char) tableVersion);
            //TODO: validate proper handling 
            if (!await dotStatDataDb.TableExists(attrTableName, cancellationToken))
            {
                return;
            }

            var sql = $@"
                        SELECT {string.Join(",", attributeList.Select(m => m.SqlColumn()))}
                            FROM [{dotStatDataDb.DataSchema}].[{attrTableName}] 
                            WHERE [DF_ID] = @DfId";

            await using (var reader = await dotStatDataDb.ExecuteReaderSqlWithParamsAsync(
                sql, cancellationToken, CommandBehavior.SingleRow, parameters: new SqlParameter("DfId", SqlDbType.Int) { Value = Dataflow.DbId }))
            {
                if (await reader.ReadAsync(cancellationToken))
                {
                    // Reduce list of mandatory attributes with the ones having value in the database for the current dataflow
                    attributeList.RemoveAll(m => reader.ColumnValue(m.SqlColumn().Trim("[]".ToCharArray()), -1) != -1);
                }
            }
        }
    }
}