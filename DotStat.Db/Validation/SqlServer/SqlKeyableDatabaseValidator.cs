﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Db.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Validation.SqlServer
{
    public class SqlKeyableDatabaseValidator : KeyableDatabaseValidator, ISqlKeyableDatabaseValidator
    {
        public SqlKeyableDatabaseValidator(int maxTransferErrorAmount) : base(maxTransferErrorAmount)
        {
        }

        protected override async Task<bool> CheckForMissingMandatoryDimensionGroupAttributesValuesInStaging(IDotStatDb dotStatDataDb, ICodeTranslator translator, Dataflow dataflow, Dictionary<string, (List<Dimension>, List<Attribute>)> attributesWithSameDimensionReferences, List<Attribute> mandatorySeriesAttributes, CancellationToken cancellationToken)
        {
            var isValid = true;
            var stagingTableName = dataflow.Dsd.SqlStagingTable();

            foreach (var (dimensionsReferenced, attr) in attributesWithSameDimensionReferences.Values)
            {
                if (IsMaxErrorLimitReached())
                    break;

                var attributes = attr.Where(ata =>
                    mandatorySeriesAttributes.Select(a => a.Code)
                        .Any(a => a.Equals(ata.Code, StringComparison.InvariantCultureIgnoreCase))).ToList();

                if (attributes.Count == 0)
                    continue;

                if (dimensionsReferenced.Count == 0)
                    continue;

                var dimensionReferencedColumns = string.Join(",", dimensionsReferenced.Select(dim => $"{dim.SqlColumn()}"));
                var dimensionReferencedColumnsAlias = string.Join(",", dimensionsReferenced.Select(dim => $"{dim.SqlColumn()} AS [{dim.Code}]"));

                var attributeColumnsAlias = string.Join(",", attributes.Select(a => $"MAX(CASE WHEN NULLIF({a.SqlColumn()}, '') IS NULL THEN 1 ELSE 0 END) AS [{a.Code}]"));
                var attributeColumnsIsNull = string.Join(" OR ", attributes.Select(a => $"  NULLIF({a.SqlColumn()}, '') IS NULL"));

                var topResults = _maxErrorCount <= 0 ? "" : $"TOP({ _maxErrorCount - _errors.Count})";

                var sqlCommand = $@"
                    SELECT {topResults} {dimensionReferencedColumnsAlias}, {attributeColumnsAlias}
                    FROM [{dotStatDataDb.DataSchema}].[{stagingTableName}]
                    WHERE {attributeColumnsIsNull}
                    GROUP BY {dimensionReferencedColumns};
                ";

                using (var dr = await dotStatDataDb.ExecuteReaderSqlAsync(sqlCommand, cancellationToken))
                {
                    while (dr.Read() && !IsMaxErrorLimitReached())
                    {
                        var dimValues = await Task.WhenAll(dimensionsReferenced
                            .Select(async dim => 
                                $"{dim.Code}:{(dim.Base.HasCodedRepresentation() ? (await translator[dim, cancellationToken])[dr.ColumnValue<int>(dim.Code)] : dr.ColumnValue<string>(dim.Code))}")
                            .ToList());
                        
                        var attributesWithMissingValues =
                            attributes.Where(a => dr.ColumnValue<int>(a.Code) > 0).Select(a => a.Code);

                        foreach (var missingAttribute in attributesWithMissingValues)
                        {
                            AddError(ValidationErrorType.MandatoryAttributeWithNullValueInStaging,
                                string.Join(",", dimValues),
                                null, null, missingAttribute);
                        }

                        isValid = false;
                    }
                }
            }

            return isValid;
        }
        
        protected override async Task<bool> CheckForMissingMandatoryNonDatasetAttributesValuesInDatabase(IDotStatDb dotStatDataDb, ICodeTranslator translator, Dataflow dataflow, DbTableVersion tableVersion, Dictionary<string, (List<Dimension>, List<Attribute>)> attributesWithSameDimensionReferences, List<Attribute> mandatoryNonDatasetAttributes, CancellationToken cancellationToken)
        {
            //This validation is currently skiped
            //Should be re-introduced with the ticket https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/375
            return true;

            var isValid = true;
            var stagingTableName = dataflow.Dsd.SqlStagingTable(); ;
            var filtTableName = dataflow.Dsd.SqlFilterTable();  
            var factTableName = dataflow.Dsd.SqlFactTable((char)tableVersion);
            var attrTableName = dataflow.Dsd.SqlAttributeTable((char)tableVersion);

            var attrJoin = $"INNER JOIN [{dotStatDataDb.DataSchema}].[{attrTableName}] Attr ON Attr.[SID] = Filt.[SID]";
            var factJoin = $"INNER JOIN [{dotStatDataDb.DataSchema}].[{factTableName}] Fact ON Fact.[SID] = Filt.[SID]";

            var timeDim = dataflow.Dsd.TimeDimension;
            var nonTimeDims = dataflow.Dsd.Dimensions
                .Where(x => !x.Base.TimeDimension)
                .ToArray();

            foreach (var (dimensionsReferenced, attr) in attributesWithSameDimensionReferences.Values)
            {
                if (IsMaxErrorLimitReached())
                    break;

                var attributes = attr.Where(ata =>
                    mandatoryNonDatasetAttributes.Any(a =>
                        a.Code.Equals(ata.Code, StringComparison.InvariantCultureIgnoreCase))).ToList();

                if (attributes.Count == 0)
                    continue;

                if (dimensionsReferenced.Count == 0)
                    continue;

                var dimensionsJoin = string.Join(" AND ", nonTimeDims.Select(a => string.Format("Filt.{0} = Import.{0}", a.SqlColumn())));
                var dimensionReferencedColumns = string.Join(",", dimensionsReferenced.Select(dim => $"{dim.SqlColumn()}"));
                var dimensionReferencedColumnsAlias = string.Join(",", dimensionsReferenced.Select(dim => $"{dim.SqlColumn()} AS [{dim.Code}]"));

                var referencesTimeDim = timeDim != null && dimensionsReferenced.Contains(timeDim);
                var isObsLvl = attributes.Any(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation);

                var attributeColumnsAlias = string.Join(",", attributes.Select(a => $"MAX(CASE WHEN NULLIF({a.SqlColumn()}, '') IS NULL THEN 1 ELSE 0 END) AS [{a.Code}]"));
                var attributeColumnsIsNull = string.Join(" OR ", attributes.Select(a => $"  NULLIF({a.SqlColumn()}, '') IS NULL"));

                var topResults = _maxErrorCount <= 0 ? "" : $"TOP({ _maxErrorCount - _errors.Count})";

                var sqlCommand = $@"
                    SELECT {topResults} {dimensionReferencedColumnsAlias}, {attributeColumnsAlias}
                    FROM [{dotStatDataDb.DataSchema}].[{filtTableName}] Filt  			
                    {(isObsLvl ? factJoin : attrJoin)}
                    WHERE NOT EXISTS (
                        SELECT TOP 1 1 
                        FROM [{dotStatDataDb.DataSchema}].[{stagingTableName}] Import
                        WHERE {dimensionsJoin} {(referencesTimeDim ? "AND Fact." + timeDim.SqlColumn() + "=Import." + timeDim.SqlColumn() : "")}
                    )
                    AND ({attributeColumnsIsNull})
                    GROUP BY {dimensionReferencedColumns};
                ";

                using (var dr = await dotStatDataDb.ExecuteReaderSqlAsync(sqlCommand, cancellationToken))
                {
                    while (dr.Read() && !IsMaxErrorLimitReached())
                    {
                        var dimValues = await Task.WhenAll(dimensionsReferenced
                            .Select(async dim =>
                                $"{dim.Code}:{(dim.Base.HasCodedRepresentation() ? (await translator[dim, cancellationToken])[dr.ColumnValue<int>(dim.Code)] : dr.ColumnValue<string>(dim.Code))}")
                            .ToList());

                        var attributesWithMissingValues =
                            attributes.Where(a => dr.ColumnValue<int>(a.Code) > 0).Select(a => a.Code);

                        foreach (var missingAttribute in attributesWithMissingValues)
                        {
                            AddError(ValidationErrorType.MandatoryAttributeWithNullValueInDatabase,
                                string.Join(",", dimValues),
                                null, null, missingAttribute);
                        }

                        isValid = false;
                    }
                }
            }

            return isValid;
        }

        protected override async Task<bool> CheckForDimensionGroupAttributesWithMultipleValuesInStaging(IDotStatDb dotStatDataDb, ICodeTranslator translator, Dataflow dataflow, Dictionary<string, (List<Dimension>, List<Attribute>)> attributesWithSameDimensionReferences, List<Attribute> seriesAttributes, CancellationToken cancellationToken)
        {
            var isValid = true;
            var stagingTableName = dataflow.Dsd.SqlStagingTable();
            var timeDim = dataflow.Dsd.TimeDimension;

            foreach (var (dimensionsReferenced, attr) in attributesWithSameDimensionReferences.Values)
            {
                if (IsMaxErrorLimitReached())
                    break;

                var attributes = attr.Where(ata =>
                    seriesAttributes.Select(a => a.Code)
                        .Any(a => a.Equals(ata.Code, StringComparison.InvariantCultureIgnoreCase))).ToList();

                if (attributes.Count == 0)
                    continue;

                if (dimensionsReferenced.Count == 0)
                    continue;

                // Skip validation of group attributes that reference the time dimension
                // TODO remove during the implementation of the ticket https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/189
                if (timeDim != null && dimensionsReferenced.Contains(timeDim))
                    continue;

                var dimensionReferencedColumns = string.Join(",", dimensionsReferenced.Select(dim => $"{dim.SqlColumn()}"));
                var dimensionReferencedColumnsAlias = string.Join(",", dimensionsReferenced.Select(dim => $"{dim.SqlColumn()} AS [{dim.Code}]"));
                var attributeColumnsAlias = string.Join(",", attributes.Select(a => $"COUNT(DISTINCT {a.SqlColumn()}) AS [{a.Code}]"));
                var attributeColumns = string.Join(" OR ", attributes.Select(a => $"COUNT(DISTINCT {a.SqlColumn()}) > 1"));

                var topResults = _maxErrorCount <= 0 ? "" : $"TOP({ _maxErrorCount - _errors.Count})";

                var sqlCommand = $@"
                    SELECT {topResults} {dimensionReferencedColumnsAlias}, {attributeColumnsAlias}
                    FROM  [{dotStatDataDb.DataSchema}].[{stagingTableName}] Import 
                    GROUP BY {dimensionReferencedColumns}
                    HAVING {attributeColumns};
                ";

                using (var dr = await dotStatDataDb.ExecuteReaderSqlAsync(sqlCommand, cancellationToken))
                {
                    while (dr.Read() && !IsMaxErrorLimitReached())
                    {
                        var dimValues = await Task.WhenAll(dimensionsReferenced
                            .Select(async dim => 
                                $"{dim.Code}:{(dim.Base.HasCodedRepresentation() ? (await translator[dim, cancellationToken])[dr.ColumnValue<int>(dim.Code)] : dr.ColumnValue<string>(dim.Code))}")
                            .ToList());

                        var attributesWithMultipleValues =
                            attributes.Select(a => (a.Code, dr.ColumnValue<int>(a.Code)));

                        foreach (var (code, count) in attributesWithMultipleValues.Where(a => a.Item2 > 1))
                        {
                            AddError(ValidationErrorType.MultipleValuesForAttributeInStaging,
                                string.Join(",", dimValues),
                                null, null, code, count.ToString());
                        }

                        isValid = false;
                    }
                }
            }
            return isValid;

        }
       
        protected override async Task<bool> CheckForGroupAttributesWithMultipleValuesInDatabase(IDotStatDb dotStatDataDb, ICodeTranslator translator, Dataflow dataflow, DbTableVersion tableVersion, Dictionary<string, (List<Dimension>, List<Attribute>)> attributesWithSameDimensionReferences, List<Attribute> groupAttributes, CancellationToken cancellationToken)
        {
            //This validation is currently skiped
            //Should be re-introduced with the ticket https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/375
            return true;

            var isValid = true;
            var stagingTableName = dataflow.Dsd.SqlStagingTable(); ;
            var filtTableName = dataflow.Dsd.SqlFilterTable();
            var factTableName = dataflow.Dsd.SqlFactTable((char)tableVersion);
            var attrTableName = dataflow.Dsd.SqlAttributeTable((char)tableVersion);

            var timeDim = dataflow.Dsd.TimeDimension;
            var nonTimeDims = dataflow.Dsd.Dimensions
                .Where(x => !x.Base.TimeDimension)
                .ToArray();
            
            foreach (var (dimensionsReferenced, attr) in attributesWithSameDimensionReferences.Values)
            {
                if (IsMaxErrorLimitReached())
                    break;

                var attributes = attr.Where(ata =>
                    groupAttributes.Select(a => a.Code)
                        .Any(a => a.Equals(ata.Code, StringComparison.InvariantCultureIgnoreCase))).ToList();
                
                if (attributes.Count == 0)
                    continue;

                if (dimensionsReferenced.Count == 0)
                    continue;

                // Skip validation of group attributes that reference the time dimension
                // TODO remove during the implementation of the ticket https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/189
                if (timeDim!=null && dimensionsReferenced.Contains(timeDim))
                    continue;
                
                var dimensionReferencedColumns = string.Join(",", dimensionsReferenced.Select(dim => $"{dim.SqlColumn()}"));
                var dimensionReferencedColumnsAlias = string.Join(",", dimensionsReferenced.Select(dim => $"{dim.SqlColumn()} AS [{dim.Code}]"));
                var dimensionReferencedColumnsFilt = string.Join(",", dimensionsReferenced.Where(d=> !d.Base.TimeDimension).Select(dim => $"Filt.{dim.SqlColumn()}"));
                var dimensionsJoin = string.Join(" AND ", nonTimeDims.Select(dim => string.Format("Filt.{0} = Import.{0}", dim.SqlColumn())));

                var referencesTimeDim = timeDim != null && dimensionsReferenced.Contains(timeDim);

                var attributeColumnsAlias = string.Join(",", attributes.Select(a => $"COUNT(DISTINCT {a.SqlColumn()}) AS [{a.Code}]"));
                var attributeColumnsDistinct = string.Join(", ", attributes.Select(a => $" COALESCE(Import.{a.SqlColumn()}, ISNULL({(referencesTimeDim ? "Fact." : "Attr.")}{a.SqlColumn()},'')) AS {a.SqlColumn()}"));
                var attributeColumnsHavingCount = string.Join(" OR ", attributes.Select(a => $"COUNT(DISTINCT {a.SqlColumn()}) > 1"));
                
                var attrJoin = $"INNER JOIN [{dotStatDataDb.DataSchema}].[{attrTableName}] Attr ON Attr.[SID] = Filt.[SID]";
                var factJoin = $"INNER JOIN [{dotStatDataDb.DataSchema}].[{factTableName}] Fact ON Fact.[SID] = Filt.[SID]";

                var topResults = _maxErrorCount <= 0 ? "" : $"TOP({ _maxErrorCount - _errors.Count})";

                var sqlCommand = $@"
                    SELECT {topResults} {dimensionReferencedColumnsAlias}, {attributeColumnsAlias}
                    FROM (
                        SELECT {dimensionReferencedColumnsFilt}
                        {(referencesTimeDim ? ", Fact." + timeDim.SqlColumn() : "")}
                        , {attributeColumnsDistinct}
                        FROM [{dotStatDataDb.DataSchema}].[{filtTableName}] Filt  				
                        {(referencesTimeDim ? factJoin : attrJoin)}
		                LEFT JOIN [{dotStatDataDb.DataSchema}].[{stagingTableName}] Import 
                            ON {dimensionsJoin} {(referencesTimeDim ? "AND Fact." + timeDim.SqlColumn() + "=Import." + timeDim.SqlColumn() : "")}
                    ) AS [all_values]
                    GROUP BY {dimensionReferencedColumns}
                    HAVING {attributeColumnsHavingCount};
                ";

                using (var dr = await dotStatDataDb.ExecuteReaderSqlAsync(sqlCommand, cancellationToken))
                {
                    while (dr.Read() && !IsMaxErrorLimitReached())
                    {
                        var dimValues = await Task.WhenAll(dimensionsReferenced
                            .Select(async dim =>
                                $"{dim.Code}:{(dim.Base.HasCodedRepresentation() ? (await translator[dim, cancellationToken])[dr.ColumnValue<int>(dim.Code)] : dr.ColumnValue<string>(dim.Code))}")
                            .ToList());

                        var attributesWithMultipleValues =
                            attributes.Select(a => (a.Code, dr.ColumnValue<int>(a.Code)));

                        foreach (var (code, count) in attributesWithMultipleValues.Where(a => a.Item2 > 1))
                        {
                            AddError(ValidationErrorType.MultipleValuesForAttributeInDatabase,
                                string.Join(",", dimValues),
                                null, null, code, count.ToString());
                        }

                        isValid = false;
                    }
                }
            }

            return isValid;
        }

    }
}