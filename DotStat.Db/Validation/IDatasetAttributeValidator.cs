﻿using System.Collections.Generic;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Db.Validation
{
    public interface IDatasetAttributeValidator
    {
        bool Validate(List<Attribute> reportedAttributes, IList<DataSetAttributeRow> dataSetAttributeRowsReportedAtDataSetLevel, IList<DataSetAttributeRow> dataSetAttributeRowsReportedAtObservationLevel, Dataflow dataflow, int? maxErrorCount, bool fullValidation);

        List<IValidationError> GetErrors();

        string GetErrorsMessage();
    }
}