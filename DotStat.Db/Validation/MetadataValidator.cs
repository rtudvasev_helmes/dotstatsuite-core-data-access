﻿using System;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System.Collections.Generic;
using System.Linq;
using DotStat.DB.Util;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;

namespace DotStat.Db.Validation
{
    public class MetadataValidator : Validator
    {
        private readonly IReadOnlyList<Dimension> _dimensions;
        private readonly Dictionary<string, MetadataAttribute> _reportedMetadataAttributes;
        private readonly bool _isTimeAtTimeDimensionSupported;

        public MetadataValidator(Dataflow dataflow, ReportedComponents reportedComponents, IGeneralConfiguration configuration, bool fullValidation, bool isTimeAtTimeDimensionSupported) : base(configuration, fullValidation)
        {
            _reportedMetadataAttributes = dataflow.Dsd.Msd.MetadataAttributes
                .Where(a => reportedComponents.MetadataAttributes.Any(r => r.Code.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase)))
                .ToDictionary(a => a.HierarchicalId, a => a);

            _dimensions = dataflow.Dimensions;
            _isTimeAtTimeDimensionSupported = isTimeAtTimeDimensionSupported;
        }

        public override bool ValidateObservation(IObservation observation, int row=0, StagingRowActionEnum action = StagingRowActionEnum.Merge)
        {
            if (observation == null)
            {
                return true;
            }

            // validity of reported time period value to be checked first
            if (!TimeDimensionCheck(observation, row))
                return false;

            if (FullValidation && !Duplicates[action].Add(observation.GetKeyStartEndPeriod(false)))
            {
                return AddError(ValidationErrorType.DuplicatedCoordinate, row, observation);
            }

            foreach (var metaAttr in observation.Attributes.Where(a =>
                _reportedMetadataAttributes.Any(r =>
                    r.Key.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase))))
            {
                if (!MetadataAttributeTypeCheck(metaAttr, observation, row))
                    return false;
            }

            return DimensionConstraintCheck(observation, row);
        }

        private bool TimeDimensionCheck(IObservation observation, int row)
        {
            try
            {
                if (observation.ObsTime.Equals("*", StringComparison.OrdinalIgnoreCase) ||
                    observation.ObsTime.Equals("-", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }

                var timeDimension = observation.ObsAsTimeDate;

                // If full validation then log error when time is not supported by DSD but observation's time period contains a time fragment
                // Note that reporting trimester also contains 'T', e.g. '2021-T2', '2021-T3Z', '2022-T1+02:00' etc.
                if (timeDimension != null && FullValidation && !_isTimeAtTimeDimensionSupported &&
                    (timeDimension.Value.TimeOfDay.Ticks > 0 || observation.ObsTime.Contains("T00")))
                {

                    return AddError(ValidationErrorType.TimeAtTimeDimensionNotSupported, row, observation);
                }
            }
            catch (SdmxSemmanticException)
            {
                return AddError(ValidationErrorType.TimeDimensionFormatNotRecognized, row, observation);
            }

            return true;
        }

        private bool DimensionConstraintCheck(IObservation observation, int row)
        {
            var seriesKey = observation.GetKey(false);

            if (Series.Contains(seriesKey))
            {
                return true;
            }

            foreach (var key in observation.SeriesKey.Key)
            {
                if (string.IsNullOrEmpty(key.Code) || key.Code.Equals("*", StringComparison.OrdinalIgnoreCase) || key.Code.Equals("-", StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }

                var dimension = _dimensions.First(x => x.Code.Equals(key.Concept));

                if (!dimension.HasCode(key.Code))
                {
                    return AddError(ValidationErrorType.UnknownCodeMember, row, observation, key.Concept + ":" + key.Code);
                }
            }

            Series.Add(seriesKey);

            return true;
        }

        
        public bool MetadataAttributeTypeCheck(IKeyValue attribute, IObservation observation, int row)
        {
            _reportedMetadataAttributes.TryGetValue(attribute.Concept, out var attrMeta);
            
            var isAttributeCodeEmpty = string.IsNullOrEmpty(attribute.Code);
            
            var hasCodeRepresentation = attrMeta.Base.HasCodedRepresentation();
            if (hasCodeRepresentation)
            {
                if (!isAttributeCodeEmpty && attrMeta.FindMember(attribute.Code, false) == null)
                    return AddError(ValidationErrorType.UnknownAttributeCodeMember, row, observation, attribute.Concept, attribute.Code);
            }
            return true;
        }
    }
}