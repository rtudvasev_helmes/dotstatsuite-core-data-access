﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.DB.Util;
using DotStat.DB.Validation.TypeValidation;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Validation
{
    public class ObservationValidator : Validator
    {
        private readonly Dictionary<string, Attribute> _reportedDsdAttributes;
        private readonly List<Attribute> _reportedMandatoryAttributes;
        private readonly DataflowMetadataBuilder _metadata;
        private readonly List<ISdmxTypeValidator> _measureValidators;
        private readonly int _maxTextAttributeLength;
        private readonly bool _validateObservationValue;
        private readonly bool _isTimeAtTimeDimensionSupported;

        public ObservationValidator(Dataflow dataflow, ReportedComponents reportedComponents,
            IGeneralConfiguration configuration, bool fullValidation, bool isTimeAtTimeDimensionSupported,
            DataflowMetadataBuilder metadata = null)
            : base(configuration, fullValidation)
        {
            _metadata = metadata ?? new DataflowMetadataBuilder(dataflow, reportedComponents);

            _reportedDsdAttributes = reportedComponents.DatasetAttributes
                .Concat(reportedComponents.SeriesAttributes)
                .Concat(reportedComponents.ObservationAttributes).ToDictionary(a => a.Code, a => a);

            _reportedMandatoryAttributes =
                reportedComponents.ObservationAttributes.Where(a => a.Base.Mandatory).ToList();

            _measureValidators = new SdmxTypeValidatorBuilder(configuration)
                .BuildFromPrimaryMeasure(dataflow.Dsd.PrimaryMeasure).ToList();

            _maxTextAttributeLength = dataflow.Dsd.MaxTextAttributeLength ??
                                      dataflow.Dsd.GetMaxLengthOfTextAttributeFromConfig(configuration
                                          .MaxTextAttributeLength);

            _validateObservationValue = reportedComponents.IsPrimaryMeasureReported;

            _isTimeAtTimeDimensionSupported = isTimeAtTimeDimensionSupported;
        }

        public override bool ValidateObservation(IObservation observation, int row = 0,
            StagingRowActionEnum action = StagingRowActionEnum.Merge)
        {
            if (action is not StagingRowActionEnum.Merge and not StagingRowActionEnum.Delete)
                return true;

            // simple and fast checks 

            // validity of reported time period value to be checked first
            if (!TimeDimensionCheck(observation, row, action))
                return false;

            if (!DimensionConstraintCheck(observation, row, action))
                return false;
            
            var presentComponents = new HashSet<string>();
            if (_validateObservationValue)
            {
                if (!ObservationValueTypeCheck(observation, row))
                    return false;

                if (FullValidation &&
                    !Duplicates[action].Add(observation.GetKeyStartEndPeriod(_metadata.HasReportingYearStartDayAttr)))
                    return AddError(ValidationErrorType.DuplicatedCoordinate, row, observation);

                if (action is StagingRowActionEnum.Merge && !string.IsNullOrEmpty(observation.ObservationValue.HarmonizeSpecialCaseValues()))
                    presentComponents.Add(_metadata.Dataflow.Dsd.Base.PrimaryMeasure.Id);
            }

            if (FullValidation && !MandatoryAttributesCheck(observation, row, action))
                return false;
            
            foreach (var attribute in observation.Attributes)
            {
                if (!AttributeTypeCheck(attribute, observation, row))
                    return false;

                if (action is StagingRowActionEnum.Merge && !string.IsNullOrEmpty(attribute.Code.HarmonizeSpecialCaseValues()))
                    presentComponents.Add(attribute.Concept);
            }
            
            return CheckReferenceDimensionsPresent(presentComponents, observation, row, action);
        }

        public bool DimensionConstraintCheck(IObservation observation, int row, StagingRowActionEnum action)
        {
            var seriesKey = observation.GetKey(false);

            if (Series.Contains(seriesKey))
                return true;
            
            foreach (var key in observation.SeriesKey.Key)
            {
                var codeIsNullOrEmpty = string.IsNullOrEmpty(key.Code);
                ////Referenced dimensions with missing value
                //if (action is StagingRowActionEnum.Merge && codeIsNullOrEmpty)
                //{
                //    if(referencedDimensions.Any(dr => dr.Equals(key.Concept, StringComparison.CurrentCultureIgnoreCase)))
                //        return AddError(ValidationErrorType.MissingCodeMember, row, observation, key.Concept);
                //}

                var meta = _metadata[key.Concept];

                if (!codeIsNullOrEmpty && !meta.Dimension.HasCode(key.Code))
                    return AddError(ValidationErrorType.UnknownCodeMember, row, observation,
                        key.Concept + ":" + key.Code);

                if (!codeIsNullOrEmpty && !meta.Dimension.IsAllowed(key.Code))
                    return AddError(ValidationErrorType.DimensionConstraintViolation, row, observation,
                        key.Concept + ":" + key.Code);
            }

            Series.Add(seriesKey);

            return true;
        }

        public bool TimeDimensionCheck(IObservation observation, int row, StagingRowActionEnum action)
        {
            if (action != StagingRowActionEnum.Merge &&
                string.IsNullOrEmpty(observation.ObsTime))
                return true;

            try
            {
                var timeDimension = observation.ObsAsTimeDate;

                // If full validation then log error when time is not supported by DSD but observation's time period contains a time fragment
                // Note that reporting trimester also contains 'T', e.g. '2021-T2', '2021-T3Z', '2022-T1+02:00' etc.
                if (timeDimension != null && FullValidation && !_isTimeAtTimeDimensionSupported &&
                    (timeDimension.Value.TimeOfDay.Ticks > 0 || observation.ObsTime.Contains("T00")))
                {

                    return AddError(ValidationErrorType.TimeAtTimeDimensionNotSupported, row, observation);
                }
            }
            catch (SdmxSemmanticException)
            {
                return AddError(ValidationErrorType.TimeDimensionFormatNotRecognized, row, observation);
            }

            return true;
        }

        public bool ObservationValueTypeCheck(IObservation observation, int row)
        {
            var obsVal = observation.ObservationValue.HarmonizeSpecialCaseValues();
            foreach (ISdmxTypeValidator validator in _measureValidators)
            {
                if (!validator.IsValid(obsVal))
                {
                    return AddError(validator.ValidationError, row, observation, validator.ExtraErrorMessageParameters);
                }
            }

            return true;
        }
        
        public bool MandatoryAttributesCheck(IObservation observation, int row, StagingRowActionEnum action)
        {
            var isValid = true;
            if (action != StagingRowActionEnum.Merge)
                return true;
            if (_reportedMandatoryAttributes.Count == 0)
                return true;

            //Non reported
            var missingMandatoryAttributes = _reportedMandatoryAttributes.Where(mandatoryAttribute =>
                !observation.Attributes.Any(a => a.Concept.Equals(mandatoryAttribute.Code,
                    StringComparison.InvariantCultureIgnoreCase)));

            foreach (var attribute in missingMandatoryAttributes)
            {
                isValid = false;
                AddError(ValidationErrorType.MandatoryAttributeMissing, row, observation, attribute.Code);
            }

            //Reported with null value
            var mandatoryAttributesNullValue = _reportedMandatoryAttributes
                .Where(mandatoryAttribute => observation.Attributes.Any(a =>
                    a.Concept.Equals(mandatoryAttribute.Code,
                        StringComparison.InvariantCultureIgnoreCase) &&
                    string.IsNullOrWhiteSpace(a.Code.HarmonizeSpecialCaseValues()))).ToList();

            foreach (var attribute in mandatoryAttributesNullValue)
            {
                isValid = false;
                AddError(ValidationErrorType.MandatoryAttributeWithNullValueInStaging, row, observation,
                    attribute.Code);
            }

            return isValid;
        }

        public bool AttributeTypeCheck(IKeyValue attribute, IObservation observation, int row)
        {
            if (!_reportedDsdAttributes.TryGetValue(attribute.Concept, out var attrMeta))
            {
                //Skip Ignore unknown attributes
                observation.Attributes.Remove(attribute);
                return true;
                //return AddError(ValidationErrorType.AttributeNotInDsd, row, observation, attribute.Concept,
                //    _dataflow.Dsd.FullId);
            }

            //skip dsd attributes
            if (attrMeta.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null)
                return true;

            var isAttributeCodeEmpty = string.IsNullOrEmpty(attribute.Code.HarmonizeSpecialCaseValues());

            var hasCodeRepresentation = attrMeta.Base.HasCodedRepresentation();
            if (hasCodeRepresentation)
            {
                if (!isAttributeCodeEmpty && attrMeta.FindMember(attribute.Code, false) == null)
                    return AddError(ValidationErrorType.UnknownAttributeCodeMember, row, observation, attribute.Concept,
                        attribute.Code);

                //Check allowed content constraint
                if (!isAttributeCodeEmpty && !attrMeta.IsAllowed(attribute.Code))
                    return AddError(ValidationErrorType.AttributeConstraintViolation, row, observation,
                        attribute.Concept + ":" + attribute.Code);

            }
            else
            {
                var maxTextAttributeLength = attrMeta.Dsd.MaxTextAttributeLength ?? _maxTextAttributeLength;

                if (!isAttributeCodeEmpty && maxTextAttributeLength != 0 &&
                    attribute.Code.Length > maxTextAttributeLength)
                {
                    return AddError(ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit, row, observation,
                        attribute.Concept, attribute.Code,
                        attribute.Code.Length.ToString(),
                        maxTextAttributeLength.ToString());
                }

                // Currently attribute can be coded or text. Additional type check should be placed here 
                // when other types will be supported. (eg. valid date, integer, double, etc.)
            }

            return true;
        }
        
        public bool CheckReferenceDimensionsPresent(HashSet<string> presentComponents, IObservation observation, int row,
            StagingRowActionEnum action)
        {
            if (action != StagingRowActionEnum.Merge)
                return true;

            var dimensionsPresent = observation.SeriesKey.Key;
            if(_metadata.HasTime && observation.ObsTime is not null)
                dimensionsPresent.Add(new KeyValueImpl(observation.ObsTime, _metadata.Dataflow.Dsd.TimeDimension.Code));

            var allDimensionsValidated = false;
            foreach (var component in presentComponents)
            {
                if (allDimensionsValidated) return true;

                if (!_metadata.DimensionsReferenceGroups.TryGetValue(component, out var referencedDimensions)) continue;
                
                //Referenced dimension omitted
                var referencedDimensionsMissing = referencedDimensions
                    .Where(rd =>
                        !dimensionsPresent.Any(d =>
                            rd.Equals(d.Concept, StringComparison.CurrentCultureIgnoreCase)))
                    .Select(d => d).ToList();

                var isPrimaryMeasure = component == _metadata.Dataflow.Dsd.Base.PrimaryMeasure.Id;
                //Referenced dimensions not present
                if (referencedDimensionsMissing.Any())
                {
                    var validationErrorType = isPrimaryMeasure
                        ? ValidationErrorType.ObservationReferencedDimensionMissing
                        : ValidationErrorType.AttributeReferencedDimensionMissing;
                        
                    return AddError(validationErrorType, row, observation,
                        component,
                        string.Join(",", referencedDimensions),
                        string.Join(",", referencedDimensionsMissing));
                }

                foreach (var dimKeyValue in dimensionsPresent)
                {
                    //Referenced dimension with empty code
                    if (referencedDimensions.Any(d => d.Equals(dimKeyValue.Concept, StringComparison.CurrentCultureIgnoreCase)
                                                      && string.IsNullOrEmpty(dimKeyValue.Code)))
                        return AddError(ValidationErrorType.MissingCodeMember, row, observation, dimKeyValue.Concept);
                }

                if (isPrimaryMeasure)
                    allDimensionsValidated = true;
            }

            return true;
        }

    }
}