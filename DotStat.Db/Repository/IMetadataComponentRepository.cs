﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.Dto;
using DotStat.Domain;

namespace DotStat.Db.Repository
{
    public interface IMetadataComponentRepository : IDatabaseRepository
    {
        Task<int> GetMetadataAttributeDbId(IDotStatIdentifiable component, int msdDbId, CancellationToken cancellationToken, bool errorIfNotFound = false);
        ValueTask<IList<MetadataAttributeItem>> GetMetadataAttributesOfMsd(Msd msd, CancellationToken cancellationToken);
        Task<IList<MetadataAttributeItem>> GetAllMetadataAttributeComponents(CancellationToken cancellationToken);
    }
}
