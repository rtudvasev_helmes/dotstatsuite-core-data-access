﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Enums;
using DotStat.Db.Dto;
using DotStat.Db.Util;
using DotStat.Domain;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace DotStat.Db.Repository
{
    public interface IArtefactRepository
    {
        Task CheckManagementTables(CancellationToken cancellationToken);
        ValueTask<bool> CreateNewDsdAftefact(Dsd dsd, CancellationToken cancellationToken);

        ValueTask<bool> CleanUpDsd(int dsdDbId, IList<ArtefactItem> dataflowArtefacts, List<ComponentItem> allComponents, CancellationToken cancellationToken);
        ValueTask<bool> CleanUpMsdOfDsd(int dsdDbId, IList<ArtefactItem> dataflowArtefacts, List<ComponentItem> allComponents, List<MetadataAttributeItem> allMetadataAttributeComponents, CancellationToken cancellationToken);
        ValueTask<bool> CleanUpCodelist(int clId, CancellationToken cancellationToken);
        Task<bool> UpdatePITInfoOfDsdArtefact(Dsd dsd, CancellationToken cancellationToken);

        Task<int?> GetDsdMsdDbId(int dsdDbId, CancellationToken cancellationToken);
        ValueTask<bool> DeleteArtefact(int dbId, CancellationToken cancellationToken);
        ValueTask<bool> DeleteArtefacts(IEnumerable<int> dbIds, CancellationToken cancellationToken);
        Task<ArtefactItem> GetArtefactByDbId(int dbId, CancellationToken cancellationToken, bool errorIfNotFound = false);
        Task<int> GetArtefactDbId(IDotStatMaintainable artefact, CancellationToken cancellationToken, bool errorIfNotFound = false);
        Task<int> GetArtefactDbId(IMaintainableObject artefact, CancellationToken cancellationToken);
        Task<int> GetArtefactDbId(string agencyId, string artefactId, SdmxVersion version, SDMXArtefactType sdmxDbType, CancellationToken cancellationToken);
        Task<IList<ArtefactItem>> GetListOfArtefacts(CancellationToken cancellationToken, string type = null);
        Task<ArtefactItem> GetArtefact(string agencyId, string artefactId, SdmxVersion version, SDMXArtefactType sdmxDbType, CancellationToken cancellationToken, bool errorIfNotFound = true);
        //Task<bool> GetDataExistsForDataflow(ArtefactItem dfArtefact, IDataStoreRepository dataStoreRepository, DbTableVersion tableVersion, CancellationToken cancellationToken);
        Task<IList<ArtefactItem>> GetListOfDataflowArtefactsForDsd(int dsdDbId, CancellationToken cancellationToken);
        Task<IList<ArtefactItem>> GetListOfCodelistsWithoutDsdAndMsd(CancellationToken cancellationToken);

        Task<JObject> GetDSDPITInfo(Dataflow dataFlow, CancellationToken cancellationToken);
        Task FillMeta(Dsd dsd, CancellationToken cancellationToken, bool errorIfNotFound = true);
        Task FillMeta(Msd msd, CancellationToken cancellationToken);
        Task UpdateMsdOfDsd(int dsdDbId, int? msdDbId, CancellationToken cancellationToken);

        Task VerifyAndCreateMeasures(Dsd dsd, IList<ComponentItem> componentItems,
            bool factTableExists, CancellationToken cancellationToken);
        Task VerifyAndCreateOrUpdateDynamicTables(Dsd dsd, bool factTableExists, CancellationToken cancellationToken);
        Task VerifyAndCreateDataflow(Dataflow dataflow, CancellationToken cancellationToken);
        ValueTask VerifyAndCreateOrUpdateMetadataDynamicTables(Dsd dsd, bool metaDataTableExists,
            CancellationToken cancellationToken);

        ValueTask VerifyAndCreateCodelistOfComponent(Dsd dsd, IDotStatCodeListBasedIdentifiable component,
            ComponentItem componentInDb, bool factTableExists, CancellationToken cancellationToken);

        ValueTask VerifyAndCreateCodelistOfMetadataAttributesComponent(Msd msd, MetadataAttribute metadataAttribute,
            MetadataAttributeItem metadataAttributeInDb, bool metadataTableExists, CancellationToken cancellationToken);

        Task<ComponentItem> TryCreateDimensionComponent(Dimension dim, SDMXArtefactType dimType,
            CancellationToken cancellationToken);
        
        Task GetOrCreateMsd(Msd msd, CancellationToken cancellationToken);
        Task CreateMetadataAttribute(MetadataAttribute attr, CancellationToken cancellationToken);
        Task CreateMetaDataFlow(Dataflow dataFlow, CancellationToken cancellationToken);


        Task<bool> CheckSupportOfTimeAtTimeDimension(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken, string columnName = null);

        Task<AvailabilityHelper> CreateDataAvailabilityOfDataflow(int dataflowId, string dataflowWhereClause,
            string dataflowJoinClause, Dsd dsd,
            DbTableVersion tableVersion, CancellationToken cancellationToken);

    }
}
