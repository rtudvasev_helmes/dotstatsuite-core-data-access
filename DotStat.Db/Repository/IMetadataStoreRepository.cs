﻿using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.Db.Repository
{
    public interface IMetadataStoreRepository : IDatabaseRepository
    {
        Task<BulkImportResult> BulkInsertMetadata(
            IAsyncEnumerable<ObservationRow> observations, 
            ReportedComponents reportedComponents, 
            ICodeTranslator translator, Dataflow dataflow, 
            bool fullValidation, bool isTimeAtTimeDimensionSupported,
            CancellationToken cancellationToken
            );
        
        Task RecreateMetadataStagingTables(Dataflow dataFlow, ReportedComponents reportedComponents, bool isTimeAtTimeDimensionSupported, CancellationToken cancellationToken);
        Task DropMetadataStagingTables(Dsd dsd, CancellationToken cancellationToken);
        Task DropMetadataStagingTables(int dsdDbId, CancellationToken cancellationToken);
        Task AddIndexMetadataStagingTable(Dataflow dataFlow, ReportedComponents reportedComponents, CancellationToken cancellationToken);

        Task DeleteMetadata(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken);

        ValueTask CopyMetadataToNewVersion(Dataflow dataFlow, DbTableVersion sourceTableVersion, DbTableVersion targetTableVersion, CancellationToken cancellationToken);

        Task<MergeResult> MergeMetadataStagingToMetadataTable(Dataflow dataFlow, ReportedComponents reportedComponents, DbTableVersion tableVersion, bool includeSummary, int rowsCopied, CancellationToken cancellationToken);

        Task<MergeResult> MergeMetadataStagingToDatasetMetadataTable(Dataflow dataFlow, ReportedComponents reportedComponents, DataSetAttributeRow dataSetLevelAttributeRow, ICodeTranslator translator, DbTableVersion tableVersion, bool includeSummary, CancellationToken cancellationToken);

        Task UpdateStatisticsOfMetadataTable(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken);

        Task DropMetadataTables(Dsd dsd, CancellationToken cancellationToken);

        Task DropMetadataTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken);
        
        string GetMetadataViewQuery(Dataflow dataFlow, char tableVersion);

        string GetEmptyMetadataViewQuery(Dataflow dataFlow);
        Task<bool> MetadataTablesExist(Dsd dsd, CancellationToken cancellationToken);


    }
}
