﻿using System.Collections.Generic;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

namespace DotStat.Db.Repository
{
    public interface IObservationRepository
    {
        IAsyncEnumerable<ObservationRow> GetDataObservations(
            IDataQuery dataQuery, 
            Dataflow dataflow,
            ICodeTranslator codeTranslator,
            DbTableVersion tableVersion, 
            CancellationToken cancellationToken,
            long? startRow = null, 
            long? endRow = null
        );

        IAsyncEnumerable<ObservationRow> GetMetadataObservations(
            IDataQuery metadataQuery,
            Dataflow dataflow,
            ICodeTranslator codeTranslator,
            DbTableVersion tableVersion,
            CancellationToken cancellationToken
        );

        Task<int> GetObservationCount(
            Dataflow dataflow,
            DbTableVersion tableVersion,
            string dataflowWhereClause,
            CancellationToken cancellationToken);

        Task<string> BuildDataSqlQuery(
            IDataQuery dataQuery,
            ICodeTranslator codeTranslator,
            Dataflow dataflow,
            IList<Dimension> dimensions,
            IList<Attribute> attributes,
            List<DbParameter> @params,
            DbTableVersion tableVersion,
            long? startRow,
            long? endRow,
            CancellationToken cancellationToken
        );

        Task<string> BuildMetadataSqlQuery(
            IDataQuery metadataQuery,
            ICodeTranslator codeTranslator,
            Dataflow dataflow,
            IList<Dimension> dimensions,
            IList<MetadataAttribute> metadataAttributes,
            DbTableVersion tableVersion,
            CancellationToken cancellationToken
        );
    }
}