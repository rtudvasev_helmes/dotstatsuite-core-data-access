﻿using System;
using System.Collections.Generic;
using DotStat.Common.Auth;
using DotStat.Domain;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.Dto;

namespace DotStat.Db.Repository
{
    public interface ITransactionRepository
    {
        Task<bool> TryLockNewTransaction(Transaction transaction, bool isTransactionWithNoDsd,
            DbTableVersion origTransferTargetTableVersion, CancellationToken cancellationToken, bool logErrors = true);
        Task<bool> UpdateTableVersionOfTransaction(int transactionId, DbTableVersion tableVersion, CancellationToken cancellationToken);
        Task<bool> UpdateFinalTargetVersionOfTransaction(int transactionId, TargetVersion targetVersion, CancellationToken cancellationToken);
        Task<bool> UpdateArtefactFullIdOfTransaction(int transactionId, string artefactFullId, CancellationToken cancellationToken);
        Task<int> GetNextTransactionId(CancellationToken cancellationToken);
        Task<Transaction> GetTransactionById(int transactionId, CancellationToken cancellationToken, bool tryUseReadOnlyConnection = false);
        IAsyncEnumerable<Transaction> GetTransactions(string userEmail, string artFullId, DateTime start, DateTime end, TransactionStatus? status, CancellationToken cancellationToken, bool tryUseReadOnlyConnection = false);
        IAsyncEnumerable<Transaction> GetInProgressTransactions(int transactionId, int dsdDbId, CancellationToken cancellationToken);
        Task<Transaction> CreateTransactionItem(int transactionId, ArtefactItem artefact, DotStatPrincipal principal, string sourceDataSpace, string dataSource, TransactionType type, TargetVersion? requestedTargetVersion, CancellationToken cancellationToken, bool blockAllTransactions = false);
        Task<Transaction> CreateTransactionItem(int transactionId, string atrFullId, DotStatPrincipal principal, string sourceDataSpace, string dataSource, TransactionType type, TargetVersion? requestedTargetVersion, CancellationToken cancellationToken, bool blockAllTransactions = false);
        Task<bool> LockTransaction(int transactionId, int dsdDbId, int? dsdChildDbId, DbTableVersion tableVersion, TargetVersion? FinalTargetVersion, CancellationToken cancellationToken);
        Task<bool> MarkTransactionAsTimedOut(int transactionId);
        Task<bool> MarkTransactionAsCanceled(int transactionId);
        Task<bool> MarkTransactionAsCompleted(int transactionId, bool successful);
        Task<bool> MarkTransactionReadyForValidation(int transactionId, CancellationToken cancellationToken);
        IAsyncEnumerable<TransactionLog> GetTransactionLogs(int transactionId, bool includeFatal, CancellationToken cancellationToken, bool tryUseReadOnlyConnection = false);
    }
}
