﻿using DotStat.Db.DB;
using DotStat.Db.Repository;

namespace DotStat.DB.Repository
{
    public delegate IUnitOfWork UnitOfWorkResolver(string dataSpace);

    public class UnitOfWork : IUnitOfWork
    {
        public string DataSpace { get; }

        public int MaxDataImportTimeInMinutes { get; }

        public int DatabaseCommandTimeout { get; }

        public IArtefactRepository ArtefactRepository { get; }

        public IAttributeRepository AttributeRepository { get; }

        public IDataStoreRepository DataStoreRepository { get; }

        public IMetadataComponentRepository MetaMetadataComponentRepository { get; }

        public IMetadataStoreRepository MetadataStoreRepository { get; }

        public IObservationRepository ObservationRepository { get; }

        public ITransactionRepository TransactionRepository { get; }

        public IComponentRepository ComponentRepository { get; }

        public ICodelistRepository CodeListRepository { get; }
        //private IDotStatDb _dotStatDb;
        
        public UnitOfWork(
            IDotStatDb dotStatDb,
            IArtefactRepository artefactRepository,
            IAttributeRepository attributeRepository,
            IDataStoreRepository dataStoreRepository,
            IMetadataStoreRepository metadataStoreRepository,
            IMetadataComponentRepository metaMetadataComponentRepository,
            IObservationRepository observationRepository,
            ITransactionRepository transactionRepository,
            IComponentRepository componentRepository,
            ICodelistRepository codeListRepository)
        {
            DataSpace = dotStatDb.Id;
            MaxDataImportTimeInMinutes = dotStatDb.DataSpace.DataImportTimeOutInMinutes;
            DatabaseCommandTimeout = dotStatDb.DataSpace.DatabaseCommandTimeoutInSec;

            ArtefactRepository = artefactRepository;
            AttributeRepository = attributeRepository;
            DataStoreRepository = dataStoreRepository;
            MetaMetadataComponentRepository = metaMetadataComponentRepository;
            MetadataStoreRepository = metadataStoreRepository;
            ObservationRepository = observationRepository;
            TransactionRepository = transactionRepository;
            ComponentRepository = componentRepository;
            CodeListRepository = codeListRepository;
        }

        #region private methods


        #endregion
    }
}
