﻿using System.Collections.Generic;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Domain;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.Db.Repository
{
    public interface IDataStoreRepository : IDatabaseRepository
    {
        Task<BulkImportResult> BulkInsertData(IAsyncEnumerable<ObservationRow> observations,
            ReportedComponents reportedComponents,
            CodeTranslator translator,
            Dataflow dataflow,
            bool fullValidation,
            bool isTimeAtTimeDimensionSupported,
            CancellationToken cancellationToken);
        
        Task RecreateStagingTables(Dsd dsd, ReportedComponents reportedComponents, bool isTimeAtTimeDimensionSupported, CancellationToken cancellationToken);
        Task DropStagingTables(Dsd dsd, CancellationToken cancellationToken);
        Task DropStagingTables(int dsdDbId, CancellationToken cancellationToken);
        Task AddIndexStagingTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken);
        
        Task DeleteData(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken);

        Task CopyDataToNewVersion(Dsd dsd, DbTableVersion sourceDbTableVersion, DbTableVersion targetDbTableVersion, CancellationToken cancellationToken);

        ValueTask CopyAttributesToNewVersion(Dsd dsd, DbTableVersion sourceDbTableVersion, DbTableVersion targetDbTableVersion, CancellationToken cancellationToken);

        Task<int> SetBatchNumberToStagingTableRows(Dsd dsd, ReportedComponents reportedComponents,
            CancellationToken cancellationToken);

        ValueTask MergeStagingToFilterTable(Dsd dsd, ReportedComponents reportedComponents,
            CancellationToken cancellationToken);

        ValueTask<MergeResult> MergeStagingToFactTable(Dsd dsd, ReportedComponents reportedComponents,
            DbTableVersion tableVersion, bool includeSummary, int? batchNumber, BulkImportResult bulkImportResult, CancellationToken cancellationToken);

        ValueTask<MergeResult> MergeStagingToAttrTable(Dsd dsd, ReportedComponents reportedComponents,
            DbTableVersion tableVersion, bool includeSummary, int? batchNumber, BulkImportResult bulkImportResult, CancellationToken cancellationToken);

        ValueTask<MergeResult> MergeStagingToDatasetAttributes(Dataflow dataflow, IList<DataSetAttributeRow> dataSetAttributeRows, 
            CodeTranslator translator, DbTableVersion tableVersion, bool includeSummary, CancellationToken cancellationToken);
        
        Task UpdateStatisticsOfFactTable(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken);
        Task UpdateStatisticsOfFilterTable(Dsd dsd, CancellationToken cancellationToken);

        Task DropDsdTables(Dsd dsd, CancellationToken cancellationToken);
        
        Task DropAttributeTables(Dsd dsd, CancellationToken cancellationToken);

        string GetDataViewQuery(Dataflow dataflow, char tableVersion);

        string GetEmptyDataViewQuery(Dataflow dataflow);

        string GetOrderByClause(Dataflow dataflow, bool isTimeAscending = true);

        Task<bool> DataTablesExist(Dsd dsd, CancellationToken cancellationToken);
    }
}
