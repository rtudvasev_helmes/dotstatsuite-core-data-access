﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.DB;
using DotStat.Db.Reader;
using DotStat.Db.Util;
using DotStat.Db.Validation;
using DotStat.DB.Util;
using DotStat.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.Db.Repository.SqlServer
{
    public class SqlMetadataStoreRepository : DatabaseRepositoryBase<SqlDotStatDb>, IMetadataStoreRepository
    {
        public SqlMetadataStoreRepository(SqlDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }

        public async Task<BulkImportResult> BulkInsertMetadata(
            IAsyncEnumerable<ObservationRow> observations, 
            ReportedComponents reportedComponents, 
            ICodeTranslator translator, 
            Dataflow dataFlow,
            bool fullValidation, bool isTimeAtTimeDimensionSupported,
            CancellationToken cancellationToken)
        {
            await using var connection = DotStatDb.GetConnection();
            using var bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.UseInternalTransaction, null);

            bulkCopy.DestinationTableName = $"[{DotStatDb.DataSchema}].[{dataFlow.Dsd.SqlMetadataStagingTable()}]";
            bulkCopy.BatchSize = 30000;
            bulkCopy.BulkCopyTimeout = DotStatDb.DatabaseCommandTimeout;
            if (DotStatDb.DataSpace.NotifyImportBatchSize > 0)
            {
                bulkCopy.SqlRowsCopied += new SqlRowsCopiedEventHandler(OnSqlRowsCopied);
                bulkCopy.NotifyAfter = DotStatDb.DataSpace.NotifyImportBatchSize;
            }

            var metadataReader = new SdmxMetadataObservationReader(observations, reportedComponents, dataFlow, translator, GeneralConfiguration, fullValidation, isTimeAtTimeDimensionSupported);

            //Not all dimensions might be reported
            // if staging contains calculated ROW_ID column
            if (reportedComponents.Dimensions.Count > 32)
            {
                for (var i = 0; i < metadataReader.FieldCount; i++)
                {
                    bulkCopy.ColumnMappings.Add(i, i + 1);
                }
            }

            await bulkCopy.WriteToServerAsync(metadataReader, cancellationToken);

            var errors = metadataReader.GetErrors();

            var rowsCopied = metadataReader.GetObservationsCount();

            Log.Notice(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ReadingMetaSourceFinished),
                rowsCopied));

            return new BulkImportResult(metadataReader.DatasetMetadataAttributes.Values.ToList(), errors, rowsCopied, 0, 0, 0);
        }
        
        public async Task DropMetadataStagingTables(Dsd dsd, CancellationToken cancellationToken)
        {
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlMetadataStagingTable(), cancellationToken);
        }

        public async Task DropMetadataStagingTables(int dsdDbId, CancellationToken cancellationToken)
        {
            await DotStatDb.DropTable(DotStatDb.DataSchema, DbExtensions.SqlMetadataStagingTable(dsdDbId), cancellationToken);
        }

        public async Task RecreateMetadataStagingTables(Dataflow dataFlow, ReportedComponents reportedComponents, bool isTimeAtTimeDimensionSupported, CancellationToken cancellationToken)
        {
            await DropMetadataStagingTables(dataFlow.Dsd, cancellationToken);
            await BuildMetadataStagingTables(dataFlow, reportedComponents, isTimeAtTimeDimensionSupported, cancellationToken);
        }

        public async Task AddIndexMetadataStagingTable(Dataflow dataFlow, ReportedComponents reportedComponents, CancellationToken cancellationToken)
        {
            if (dataFlow.Dsd?.Msd == null)
            {
                return;
            }

            var tableName = dataFlow.Dsd.SqlMetadataStagingTable();
            var nonTimeDims = dataFlow.Dsd.Dimensions
                .Where(x => !x.Base.TimeDimension)
                .ToArray();

            var metadataAttributesColumns = dataFlow.Dsd.Msd.MetadataAttributes
                .Where(a =>
                    reportedComponents.MetadataAttributes.Any(r =>
                        r.HierarchicalId.Equals(a.HierarchicalId, StringComparison.InvariantCultureIgnoreCase)))
                .ToColumnList(true);

            var dimensionColumns = nonTimeDims.ToColumnList();
            var timeDimColumns = dataFlow.Dsd.TimeDimension != null
                ? "," + string.Join(",", dataFlow.Dsd.TimeDimension.SqlColumn(), DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd())
                : null;

            var indexColumns = dataFlow.Dsd.Dimensions.Count > 32 ? "[ROW_ID]" : $"{dimensionColumns}{timeDimColumns}";
            var indexName = dataFlow.Dsd.Dimensions.Count > 32 ? $"[idx_ROW_ID_{dataFlow.Dsd.Msd.DbId}]" : $"[idx_dimensions_{dataFlow.Dsd.Msd.DbId}]";

            await DotStatDb.ExecuteNonQuerySqlAsync($@"CREATE NONCLUSTERED INDEX {indexName} 
                ON [{DotStatDb.DataSchema}].[{tableName}]({indexColumns})
                INCLUDE ({metadataAttributesColumns})", 
                cancellationToken);
        }
        
        public async Task DeleteMetadata(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await TruncateMetadataTables(dsd, tableVersion, cancellationToken);
        }

        public async ValueTask CopyMetadataToNewVersion(Dataflow dataFlow, DbTableVersion sourceTableVersion, DbTableVersion targetTableVersion, CancellationToken cancellationToken)
        {
            if (sourceTableVersion == targetTableVersion)
            {
                throw new ArgumentException($"The same value was provided for the parameters 'sourceDbTableVersion' and 'targetDbTableVersion'");
            }

            if (dataFlow.Dsd?.Msd == null)
            {
                return;
            }

            var dimensionColumns = dataFlow.Dsd.Dimensions.Where(x => !x.Base.TimeDimension).ToColumnList();
            var timeDim = dataFlow.Dsd.TimeDimension;
            var timeDimColumns = timeDim!=null
                ? "," + string.Join(",", timeDim.SqlColumn(), DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd())
                : null;

            var metadataAttributes = dataFlow.Dsd.Msd.MetadataAttributes;
            
            var metadataAttributeColumns = metadataAttributes.Any() 
                ? "," + metadataAttributes.ToColumnList() 
                : null;

            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@" INSERT
                      INTO [{DotStatDb.DataSchema}].[{dataFlow.Dsd.SqlMetadataTable((char)targetTableVersion)}] 
                      ({dimensionColumns}{timeDimColumns}{metadataAttributeColumns} )
                    SELECT {dimensionColumns}{timeDimColumns}{metadataAttributeColumns} 
                      FROM [{DotStatDb.DataSchema}].[{dataFlow.Dsd.SqlMetadataTable((char)sourceTableVersion)}]",
                cancellationToken
            );

            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@" INSERT
                      INTO [{DotStatDb.DataSchema}].[{dataFlow.Dsd.SqlMetadataDsdTable((char)targetTableVersion)}] 
                      ([DF_ID]{metadataAttributeColumns} )
                    SELECT [DF_ID]{metadataAttributeColumns} 
                      FROM [{DotStatDb.DataSchema}].[{dataFlow.Dsd.SqlMetadataDsdTable((char)sourceTableVersion)}]",
                cancellationToken
            );
        }

        public async Task<MergeResult> MergeMetadataStagingToMetadataTable(Dataflow dataFlow, ReportedComponents reportedComponents,
            DbTableVersion tableVersion, bool includeSummary, int rowsCopied, CancellationToken cancellationToken)
        {
            if (dataFlow.Dsd?.Msd == null)
            {
                return new MergeResult();
            }

            var metadataAttributes = dataFlow.Dsd.Msd.MetadataAttributes
                .Where(a =>
                    reportedComponents.MetadataAttributes.Any(r =>
                        r.HierarchicalId.Equals(a.HierarchicalId, StringComparison.InvariantCultureIgnoreCase))).ToList();

            (string metadataAttrColumns, string metadataAttrColumnsUpdate) = (null, null);

            if (metadataAttributes.Any())
            {
                metadataAttrColumns = ", " + metadataAttributes.ToColumnList();
                metadataAttrColumnsUpdate = string.Join(",\n", metadataAttributes
                    .Select(a => $@"[ME].{a.SqlColumn()} = CASE 
                        WHEN [REQUESTED_ACTION] = {(int)StagingRowActionEnum.Merge} THEN [ST].{a.SqlColumn()}
                        WHEN [REQUESTED_ACTION] = {(int)StagingRowActionEnum.Delete} AND [ST].{a.SqlColumn()} IS NOT NULL THEN NULL 
                        ELSE [ME].{a.SqlColumn()} END"));
            }

            var nonTimeDims = dataFlow.Dsd.Dimensions
                .Where(x => !x.Base.TimeDimension)
                .ToArray();

            var dimensionColumnsNoTimeDim = nonTimeDims.ToColumnList();
            var dimensionColumnsNoTimeDimJoin = string.Join(" AND ", nonTimeDims.Select(a => string.Format("([ME].{0} = COALESCE([ST].{0}, [ME].{0}) OR ([ST].{0} = -1 AND [ME].{0} != 0))\n", a.SqlColumn())));
            var dimensionColumnsNoTimeDimInsertValues = string.Join(", ", nonTimeDims.Select(a => $"COALESCE({a.SqlColumn()}, 0)"));

            string timeDimColumns = null;
            string timeDimColumnsInsertValues = null;
            string timeDimColumnsJoin = null;
            var sqlParams = new List<DbParameter>();

            if (dataFlow.Dsd.TimeDimension != null)
            {
                timeDimColumns = ", " + string.Join(", ", dataFlow.Dsd.TimeDimension.SqlColumn(), DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd());
                timeDimColumnsInsertValues = ", " + string.Join(", ", dataFlow.Dsd.TimeDimension.SqlColumn(), $"COALESCE({DbExtensions.SqlPeriodStart()}, @maxDT)", $"COALESCE({DbExtensions.SqlPeriodEnd()}, @minDT)");
                timeDimColumnsJoin =
                    string.Format(" AND ([ME].{0} = COALESCE([ST].{0}, [ME].{0}) OR ([ST].{0} = @maxDTmin1Year AND [ME].{0} != @maxDT))\n", DbExtensions.SqlPeriodStart()) +
                    string.Format("AND ([ME].{0} = COALESCE([ST].{0}, [ME].{0}) OR ([ST].{0} = @minDTplus1Year AND [ME].{0} != @minDT))\n", DbExtensions.SqlPeriodEnd());

                sqlParams.Add(new SqlParameter("maxDTmin1Year", SqlDbType.DateTime2){Value = DateTime.MaxValue.AddYears(-1)});
                sqlParams.Add(new SqlParameter("maxDT", SqlDbType.DateTime2){Value = DateTime.MaxValue });
                sqlParams.Add(new SqlParameter("minDTplus1Year", SqlDbType.DateTime2){Value = DateTime.MinValue.AddYears(1) });
                sqlParams.Add(new SqlParameter("minDT", SqlDbType.DateTime2){Value = DateTime.MinValue });
            }

            var mergeResult = new MergeResult();
            var metadataTableName = $"[{DotStatDb.DataSchema}].[{dataFlow.Dsd.SqlMetadataTable((char)tableVersion)}]";
            var stagingTableName = $"[{ DotStatDb.DataSchema}].[{ dataFlow.Dsd.SqlMetadataStagingTable()}]";

            var sqlCommand = $@"{(includeSummary ? @"DECLARE @SummaryOfChanges TABLE(Change VARCHAR(20));" : string.Empty)}
                MERGE INTO {metadataTableName} [ME]
                USING {stagingTableName} [ST]
                ON {dimensionColumnsNoTimeDimJoin}{timeDimColumnsJoin}
                WHEN MATCHED AND [ST].[REAL_ACTION] = 1 THEN
                    UPDATE SET 
                    {metadataAttrColumnsUpdate} 
                WHEN MATCHED AND [ST].[REAL_ACTION] = 2 THEN
                    DELETE
                WHEN NOT MATCHED AND [ST].[REQUESTED_ACTION] = 1 THEN
                    INSERT ({dimensionColumnsNoTimeDim}{timeDimColumns} {metadataAttrColumns})
                    VALUES ({dimensionColumnsNoTimeDimInsertValues}{timeDimColumnsInsertValues} {metadataAttrColumns})
                {(includeSummary ? @"OUTPUT $action INTO @SummaryOfChanges;
                SELECT
                    sum(case when Change = 'INSERT' then 1 else 0 end) inserted,
                    sum(case when Change = 'UPDATE' then 1 else 0 end) updated,
                    sum(case when Change = 'DELETE' then 1 else 0 end) deleted
                FROM @SummaryOfChanges
                GROUP BY Change;" : string.Empty)};";

            try
            {
                if (includeSummary)
                {
                    await using var dbDataReader = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlCommand, cancellationToken, commandBehavior: CommandBehavior.SingleRow, parameters:sqlParams.ToArray());

                    if (await dbDataReader.ReadAsync(cancellationToken))
                    {
                        mergeResult.InsertCount += dbDataReader.GetInt32(0);
                        mergeResult.UpdateCount += dbDataReader.GetInt32(1);
                        mergeResult.DeleteCount += dbDataReader.GetInt32(2);
                        mergeResult.TotalCount = mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                    }
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, sqlParams.ToArray());
                }
            }
            catch (SqlException e)
            {
                switch (e.Number)
                {
                    case 2627://duplicates in the staging table insert
                        mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.DuplicatedRowsInMetadataStagingTable });
                        break;
                    case 8672://Overlapping rows  
                        mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.MultipleRowsModifyingTheSameRegionInStagingTable });
                        break;
                    default:
                        throw;
                }
            }

            return mergeResult;
        }

        public async Task UpdateStatisticsOfMetadataTable(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.ExecuteNonQuerySqlAsync($@"UPDATE STATISTICS [{DotStatDb.DataSchema}].[{dsd.SqlMetadataTable((char)tableVersion)}] WITH FULLSCAN, ALL", cancellationToken);
        }

        public async Task<MergeResult> MergeMetadataStagingToDatasetMetadataTable(Dataflow dataflow, ReportedComponents reportedComponents,
            DataSetAttributeRow dataSetLevelAttributeRow, ICodeTranslator translator, DbTableVersion tableVersion, bool includeSummary, CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();

            if (reportedComponents.MetadataAttributes.Count == 0 || dataSetLevelAttributeRow == null)
            {
                return mergeResult;
            }

            var metadataDsdTableName = $"[{DotStatDb.DataSchema}].[{dataflow.Dsd.SqlMetadataDsdTable((char)tableVersion)}]";

            if (dataSetLevelAttributeRow.Action is StagingRowActionEnum.DeleteAll)
            {
                if (includeSummary)
                {
                    mergeResult.DeleteCount += (int)await DotStatDb.ExecuteScalarSqlAsync(
                    $"DELETE FROM {metadataDsdTableName} WHERE [DF_ID] = {dataflow.DbId}; SELECT @@ROWCOUNT AS [ROW_COUNT];", cancellationToken);
                    mergeResult.TotalCount = mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlAsync($"DELETE FROM {metadataDsdTableName} WHERE [DF_ID] = {dataflow.DbId}", cancellationToken);
                }

                Log.Warn(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DeleteAllMetadataActionPerformed));
                return mergeResult;
            }

            var referencedMetadataAttributes = new Dictionary<MetadataAttribute, object>();

            foreach (var attributeKeyValue in dataSetLevelAttributeRow.Attributes)
            {
                var attribute = dataflow.Dsd.Msd.MetadataAttributes.First(a =>
                    a.HierarchicalId.Equals(attributeKeyValue.Concept, StringComparison.InvariantCultureIgnoreCase));
                
                var attributeValue = string.IsNullOrWhiteSpace(attributeKeyValue.Code)
                    ? (object)DBNull.Value
                    : attribute.GetSqlValue(attribute.Base.HasCodedRepresentation()
                        ? translator.TranslateCodeToId(attribute.Code, attributeKeyValue.Code).ToString()
                        : attributeKeyValue.Code);

                referencedMetadataAttributes[attribute] = attributeValue;
            }

            if (referencedMetadataAttributes.Count == 0 && dataSetLevelAttributeRow.Action != StagingRowActionEnum.Delete)
            {
                return mergeResult;
            }

            var columnNames = string.Join(",", referencedMetadataAttributes.Select(kvp => kvp.Key.SqlColumn()));
            var updateClause = string.Join(",\n", referencedMetadataAttributes
                .Select(a => $@"[ME].{a.Key.SqlColumn()} = CASE 
                        WHEN [REQUESTED_ACTION] = {(int)StagingRowActionEnum.Merge} THEN [Import].{a.Key.SqlColumn()}
                        WHEN [REQUESTED_ACTION] = {(int)StagingRowActionEnum.Delete} AND [Import].{a.Key.SqlColumn()} IS NOT NULL THEN NULL 
                        ELSE [ME].{a.Key.SqlColumn()} END"));

            StagingRowActionEnum realAction;
            if (dataSetLevelAttributeRow.Action == StagingRowActionEnum.Delete && referencedMetadataAttributes.Count == 0)
            {
                realAction = StagingRowActionEnum.Delete;
            }
            else
            {
                realAction = StagingRowActionEnum.Merge;
            }

            var attributeDbParameters = referencedMetadataAttributes
                .Select(kvp =>
                    new SqlParameter($"Comp{kvp.Key.DbId}", kvp.Key.Base.HasCodedRepresentation() ? SqlDbType.Int : SqlDbType.VarChar)
                    {
                        Value = kvp.Value
                    } as DbParameter)
                .Concat(new[]
                    {
                        new SqlParameter("DfId", SqlDbType.Int) { Value = dataflow.DbId },
                        new SqlParameter("RequestedAction", SqlDbType.Int) { Value = (int)dataSetLevelAttributeRow.Action },
                        new SqlParameter("RealAction", SqlDbType.Int) { Value = (int)realAction }
                    })
                .ToArray();

            var sqlCommand = $@"{(includeSummary ? @"declare @updated as int = 0;" : string.Empty)}
                    MERGE INTO {metadataDsdTableName} AS [ME]
                    USING (SELECT @DfId AS [DF_ID], @RequestedAction AS [REQUESTED_ACTION], @RealAction AS [REAL_ACTION], {string.Join(",", referencedMetadataAttributes.Select(kvp => $"@Comp{kvp.Key.DbId} AS {kvp.Key.SqlColumn()}"))}) AS [Import]
                    ON [ME].[DF_ID] = [Import].[DF_ID]
                    WHEN MATCHED AND [Import].[REAL_ACTION] = 1 THEN
                        UPDATE SET {updateClause}
                        {(includeSummary ? ", @updated = @updated + 1" : string.Empty)}
                    WHEN MATCHED AND [Import].[REAL_ACTION] = 2 THEN
                        DELETE
                    WHEN NOT MATCHED AND [Import].[REQUESTED_ACTION] = 1 THEN
                        INSERT ([DF_ID], {columnNames})
                        VALUES (@DfId, {string.Join(",", referencedMetadataAttributes.Select(kvp => $"@Comp{kvp.Key.DbId}"))});
                    {(includeSummary ? "SELECT @updated as [UPDATED], @@ROWCOUNT as [TOTAL];" : string.Empty)}";

            if (includeSummary)
            {
                await using var dbDataReader = await DotStatDb.ExecuteReaderSqlWithParamsAsync(
                    sqlCommand, cancellationToken, commandBehavior: CommandBehavior.SingleRow, parameters: attributeDbParameters);

                if (await dbDataReader.ReadAsync(cancellationToken))
                {
                    mergeResult.UpdateCount += dbDataReader.GetInt32(0);
                    var totalRowsModified = dbDataReader.GetInt32(1);
                    mergeResult.InsertCount += totalRowsModified - mergeResult.UpdateCount;
                    mergeResult.TotalCount = mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                }
            }
            else
            {
                await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, attributeDbParameters);
            }

            return mergeResult;
        }

        public async Task DropMetadataTables(Dsd dsd, CancellationToken cancellationToken)
        {
            await DropMetadataTables(dsd, DbTableVersion.A, cancellationToken);
            await DropMetadataTables(dsd, DbTableVersion.B, cancellationToken);
        }

        public async Task DropMetadataTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlMetadataTable((char)tableVersion), cancellationToken);
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlMetadataDsdTable((char)tableVersion), cancellationToken);
        }
        
        public async Task TruncateMetadataTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.SqlMetadataTable((char)tableVersion), cancellationToken);
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.SqlMetadataDsdTable((char)tableVersion), cancellationToken);
        }
        
        public string GetMetadataViewQuery(Dataflow dataFlow, char tableVersion)
        {
            if (dataFlow.Dsd.Msd == null)
                throw new ArgumentException($"There is no linked MSD for the DSD of the parameter 'dataFlow'");

            var columns = new List<string>();

            foreach (var dim in dataFlow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"[TIME_PERIOD] AS [{dim.Code}]");
                    columns.Add($"[{DbExtensions.SqlPeriodStart()}]");
                    columns.Add($"[{DbExtensions.SqlPeriodEnd()}]");
                }
                else
                {
                    columns.Add($"[{dim.Code}]");
                }
            }

            columns.AddRange(dataFlow.Dsd.Msd.MetadataAttributes.Select(attr => $"[{attr.HierarchicalId}]"));

            return $"SELECT {string.Join(",", columns)} FROM [{DotStatDb.DataSchema}].[{dataFlow.SqlMetadataDataFlowViewName(tableVersion)}]";
        }

        public string GetEmptyMetadataViewQuery(Dataflow dataFlow)
        {
            if (dataFlow.Dsd.Msd == null)
                throw new ArgumentException($"There is no linked MSD for the DSD of the parameter 'dataFlow'");

            var columns = new List<string>();

            foreach (var dim in dataFlow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"NULL AS [{dim.Code}]");
                    columns.Add($"NULL AS [{DbExtensions.SqlPeriodStart()}]");
                    columns.Add($"NULL AS [{DbExtensions.SqlPeriodEnd()}]");
                }
                else
                {
                    columns.Add($"NULL AS [{dim.Code}]");
                }
            }

            columns.AddRange(dataFlow.Dsd.Msd.MetadataAttributes.Select(attr => $"NULL AS [{attr.HierarchicalId}]"));

            return "SELECT TOP 0 " + string.Join(",", columns);
        }

        public async Task<bool> MetadataTablesExist(Dsd dsd, CancellationToken cancellationToken)
        {
            return await DotStatDb.TableExists(dsd.SqlMetadataTable((char)DbTableVersion.A), cancellationToken) || await DotStatDb.TableExists(dsd.SqlMetadataTable((char)DbTableVersion.B), cancellationToken);
        }

        private async ValueTask BuildMetadataStagingTables(Dataflow dataFlow, ReportedComponents reportedComponents, bool isTimeAtTimeDimensionSupported, CancellationToken cancellationToken)
        {
            if (dataFlow.Dsd?.Msd == null)
                return;

            var tableName= dataFlow.Dsd.SqlMetadataStagingTable();
            //Change to allowNull=true when non coded dimensions are introduced. 
            //A value of 0 should be given when the dimension is not being referenced.
            var dimensionColumns= dataFlow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension)
                .ToColumnList(true,null, true);
            var metadataAttributeColumns = dataFlow.Dsd.Msd.MetadataAttributes
                .Where(a =>
                    reportedComponents.MetadataAttributes.Any(r =>
                        r.Code.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase)))
                .ToColumnList(true);

            if (!string.IsNullOrWhiteSpace(metadataAttributeColumns))
            {
                metadataAttributeColumns += ',';
            }

            var rowId= dataFlow.Dsd.Dimensions.Count > 32 ? 
                $"[ROW_ID] AS ({ dataFlow.Dsd.SqlBuildRowIdFormula(true)}) PERSISTED NOT NULL," 
                : null;

            var timeDim = dataFlow.Dsd.TimeDimension;
            // NULL is stored when time is not referenced by the metadata.
            var sdmxDimTimeColumn = timeDim != null
                ? new[] {timeDim}.ToColumnList(true, null, true) + "," : null;

            var supportsDateTime = dataFlow.Dsd.Base.HasSupportDateTimeAnnotation();
            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    //The maximum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodStart(true, supportsDateTime, true),
                    //The minimum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodEnd(true, supportsDateTime, true)
                ) + ","
                : null;

            await DotStatDb.ExecuteNonQuerySqlAsync($@"CREATE TABLE [{DotStatDb.DataSchema}].[{tableName}](
                    {rowId}
                    {sdmxDimTimeColumn}
                    {dimensionColumns},
                    {metadataAttributeColumns}
                    [REQUESTED_ACTION] [TINYINT] NOT NULL,
                    [REAL_ACTION] [TINYINT] NOT NULL,
                    {timeDimensionColumns}
                )", cancellationToken);
        }

        private void OnSqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            var batchNumber = e.RowsCopied / DotStatDb.DataSpace.NotifyImportBatchSize;
            Log.Notice(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ProcessingMetadataAttributes),
                e.RowsCopied, batchNumber));
        }
    }
}