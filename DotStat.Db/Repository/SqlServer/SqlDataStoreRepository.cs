﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.DB;
using DotStat.Db.Reader;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Db.Validation;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using System.Threading;
using System.Threading.Tasks;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Repository.SqlServer
{
    public class SqlDataStoreRepository : DatabaseRepositoryBase<SqlDotStatDb>, IDataStoreRepository
    {
        public SqlDataStoreRepository(SqlDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }

        public async Task<BulkImportResult> BulkInsertData(IAsyncEnumerable<ObservationRow> observations,
            ReportedComponents reportedComponents,
            CodeTranslator translator,
            Dataflow dataflow,
            bool fullValidation,
            bool isTimeAtTimeDimensionSupported,
            CancellationToken cancellationToken)
        {
            using (var connection = DotStatDb.GetConnection())
            {
                using (var bulkCopy = new SqlBulkCopy(connection,
                    SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.UseInternalTransaction, null))
                {
                    bulkCopy.DestinationTableName = $"[{DotStatDb.DataSchema}].[{dataflow.Dsd.SqlStagingTable()}]";
                    bulkCopy.BatchSize = 30000;
                    bulkCopy.BulkCopyTimeout = DotStatDb.DatabaseCommandTimeout;
                    if (DotStatDb.DataSpace.NotifyImportBatchSize > 0)
                    {
                        bulkCopy.SqlRowsCopied += OnSqlRowsCopied;
                        bulkCopy.NotifyAfter = DotStatDb.DataSpace.NotifyImportBatchSize;
                    }

                    var observationReader = new SdmxObservationReader(observations, reportedComponents, dataflow, translator, GeneralConfiguration, fullValidation, isTimeAtTimeDimensionSupported);

                    // if staging contains calculated ROW_ID column
                    if (dataflow.Dimensions.Count > 32)
                        for (var i = 0; i < observationReader.FieldCount; i++)
                        {
                            bulkCopy.ColumnMappings.Add(i, i + 1);
                        }

                    await bulkCopy.WriteToServerAsync(observationReader, cancellationToken);

                    var errors = observationReader.GetErrors();

                    var rowsCopied = observationReader.GetObservationsCount();
                    var rowsToDeleteCount = observationReader.GetDeletionCount();
                    var rowsToMergeCount = observationReader.GetMergesCount();
                    var rowsToSkipCount = observationReader.GetSkipCount();
                    
                    Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ReadingSourceFinished),
                        rowsCopied));

                    var dataSetAttributesRows = observationReader.DataSetAttributes.Values.ToList();

                    return new BulkImportResult(dataSetAttributesRows, errors, rowsCopied, rowsToDeleteCount, rowsToMergeCount, rowsToSkipCount);
                }
            }
        }
        
        public async Task DropStagingTables(Dsd dsd, CancellationToken cancellationToken)
        {
            var tableName = dsd.SqlStagingTable();
            await DotStatDb.DropTable(DotStatDb.DataSchema, tableName, cancellationToken);
        }

        public async Task DropStagingTables(int dsdDbId, CancellationToken cancellationToken)
        {
            var tableName = DbExtensions.SqlStagingTable(dsdDbId);
            await DotStatDb.DropTable(DotStatDb.DataSchema, tableName, cancellationToken);
        }

        public async Task RecreateStagingTables(Dsd dsd, ReportedComponents reportedComponents, bool isTimeAtTimeDimensionSupported, CancellationToken cancellationToken)
        {
            await DropStagingTables(dsd, cancellationToken);
            await BuildStagingTables(dsd, reportedComponents, isTimeAtTimeDimensionSupported, cancellationToken);
        }

        public async Task AddIndexStagingTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken)
        {
            var dimensionColumns = reportedComponents.Dimensions.ToColumnList();
            var timeDimColumns = reportedComponents.TimeDimension is not null 
                ? string.Join(",", DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd()) : null;

            var indexColumns = "[ROW_ID]";
            var indexName = "[idx_ROW_ID]";
            //32 is the SQL limit of columns that can participate in a Unique index
            if (reportedComponents.Dimensions.Count + 1 <= 32)
            {
                indexColumns = $"{dimensionColumns}, [REQUESTED_ACTION]";
                indexName = "[idx_dimensions]";
            }
           
            var obsAttrColumns = reportedComponents.ObservationAttributes.Any() 
                ? "," + reportedComponents.ObservationAttributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength) : null;
            var measure = reportedComponents.IsPrimaryMeasureReported ? ", [VALUE]" : null;
            var actionColumns = ", [REAL_ACTION_FACT], [REAL_ACTION_ATTR], [BATCH_NUMBER]";

            await DotStatDb.ExecuteNonQuerySqlAsync($@"
                CREATE NONCLUSTERED INDEX {indexName} 
                ON [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}]({indexColumns})
                INCLUDE ({timeDimColumns}{measure}{obsAttrColumns}{actionColumns})
            ", cancellationToken);
        }

        public async Task DeleteData(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await TruncateDsdTables(dsd, tableVersion, cancellationToken);
            await TruncateAttributeTables(dsd, tableVersion, cancellationToken);
        }

        public async Task CopyDataToNewVersion(Dsd dsd, DbTableVersion sourceDbTableVersion, DbTableVersion targetDbTableVersion, CancellationToken cancellationToken)
        {
            if (sourceDbTableVersion == targetDbTableVersion)
                throw new ArgumentException($"The same value was provided for the parameters 'sourceDbTableVersion' and 'targetDbTableVersion'");

            var timeDim = dsd.TimeDimension;
            var timeDimColumns = timeDim!=null
                ? "," + string.Join(",", timeDim.SqlColumn(), DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd())
                : null;

            var obsAttributes = dsd.Attributes
                .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                            (a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || a.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                            a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToArray();
            
            var attributeColumns = obsAttributes.Any() 
                ? "," + obsAttributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength) 
                : null;
            
            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@" INSERT
                      INTO [{DotStatDb.DataSchema}].[{dsd.SqlFactTable((char)targetDbTableVersion)}] 
                      ([SID], [VALUE], [{DbExtensions.LAST_UPDATED_COLUMN}], [{DbExtensions.LAST_STATUS_COLUMN}] {timeDimColumns} {attributeColumns} )
                    SELECT [SID], [VALUE], [{DbExtensions.LAST_UPDATED_COLUMN}], [{DbExtensions.LAST_STATUS_COLUMN}] {timeDimColumns} {attributeColumns} 
                      FROM [{DotStatDb.DataSchema}].[{dsd.SqlFactTable((char)sourceDbTableVersion)}]"
                      , cancellationToken
            );
        }


        public async Task UpdateStatisticsOfFactTable(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@"UPDATE STATISTICS [{DotStatDb.DataSchema}].[{dsd.SqlFactTable((char)tableVersion)}] WITH FULLSCAN, ALL",
                cancellationToken
            );
        }

        public async Task UpdateStatisticsOfFilterTable(Dsd dsd, CancellationToken cancellationToken)
        {
            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@"UPDATE STATISTICS [{DotStatDb.DataSchema}].[{dsd.SqlFilterTable()}] WITH FULLSCAN, ALL",
                cancellationToken
            );
        }

        public async ValueTask CopyAttributesToNewVersion(Dsd dsd, DbTableVersion sourceDbTableVersion, DbTableVersion targetDbTableVersion, CancellationToken cancellationToken)
        {
            if (sourceDbTableVersion == targetDbTableVersion)
                throw new ArgumentException($"The same value was provided for the parameters 'sourceDbTableVersion' and 'targetDbTableVersion'");

            if (!dsd.Attributes.Any())
                return;
            
            var dimGroupAttributes = dsd.Attributes.Where(a =>
                (a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup ||
                a.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)
            ).ToArray();

            if (dimGroupAttributes.Any())
            {
                var dimGroupAttributeColumns = dimGroupAttributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength);

                await DotStatDb.ExecuteNonQuerySqlAsync(
                    $@" INSERT
                    INTO [{DotStatDb.DataSchema}].[{dsd.SqlDimGroupAttrTable((char) targetDbTableVersion)}] (SID, {dimGroupAttributeColumns})
                    SELECT SID, {dimGroupAttributeColumns}
                    FROM [{DotStatDb.DataSchema}].[{dsd.SqlDimGroupAttrTable((char)sourceDbTableVersion)}]",
                    cancellationToken
                );
            }

            var datasetAttributes = dsd.Attributes.Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToArray();

            if (datasetAttributes.Any())
            {
                var datasetAttributeColumns = datasetAttributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength);

                await DotStatDb.ExecuteNonQuerySqlAsync(
                    $@" INSERT
                    INTO [{DotStatDb.DataSchema}].[{dsd.SqlDsdAttrTable((char)targetDbTableVersion)}] ([DF_ID], {datasetAttributeColumns})
                    SELECT [DF_ID], {datasetAttributeColumns}
                    FROM [{DotStatDb.DataSchema}].[{dsd.SqlDsdAttrTable((char)sourceDbTableVersion)}]",
                    cancellationToken
                );
            }
        }

        public async ValueTask MergeStagingToFilterTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken)
        {
            var noTimeDimensions = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension).ToList();
            if (!noTimeDimensions.Any())
                return;

            var dimensionColumnsNoTimeDim = noTimeDimensions.ToColumnList();
            var dimensionColumnsNoTimeDimJoin = string.Join(" AND ", noTimeDimensions.Select(a => string.Format("Filt.{0} = Import.{0}", a.SqlColumn())));
            var dimensionColumnsNoTimeNotNull = string.Join(" AND ", noTimeDimensions.Select(a => $"{a.SqlColumn()} IS NOT NULL"));

            var sqlCommand = $@"
MERGE INTO [{DotStatDb.DataSchema}].[{dsd.SqlFilterTable()}] Filt
USING (
    SELECT DISTINCT {dimensionColumnsNoTimeDim} FROM [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}] 
    WHERE {dimensionColumnsNoTimeNotNull}
) 
    Import
ON {dimensionColumnsNoTimeDimJoin}
WHEN NOT MATCHED THEN
    INSERT ({dimensionColumnsNoTimeDim})
    VALUES ({dimensionColumnsNoTimeDim});
";

            await DotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        public async ValueTask<MergeResult> MergeStagingToFactTable(Dsd dsd, ReportedComponents reportedComponents,
            DbTableVersion tableVersion, bool includeSummary, int? batchNumber, BulkImportResult bulkImportResult, CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();
            //No reported components at observation level
            if (!reportedComponents.IsPrimaryMeasureReported && !reportedComponents.ObservationAttributes.Any())
                return mergeResult;

            var isMergeOnly = bulkImportResult.NumberOfRowsToMerge > 0 && bulkImportResult.NumberOfRowsToDelete == 0;
            var includeMerge = bulkImportResult.NumberOfRowsToMerge > 0;
            var includeDelete = bulkImportResult.NumberOfRowsToDelete > 0;
            
            //Dimensions columns
            var noTimeDimensions = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension
                && dsd.Dimensions.Any(rd => rd.Code.Equals(d.Code, StringComparison.CurrentCultureIgnoreCase)));

            var dimensionColumnsNoTimeDimJoinTemplate =
                isMergeOnly ? " Filt.{0} = Import.{0} " : "( Filt.{0} = COALESCE(Import.{0}, Filt.{0}))";
            var dimensionColumnsNoTimeDimJoin =
                string.Join(" AND ", noTimeDimensions.Select(a => string.Format(dimensionColumnsNoTimeDimJoinTemplate, a.SqlColumn())));

            string timeDimColumns = null;
            string timeDimColumnsJoin = null;
            string sdmxTimeDimNotNull = null;
            if (reportedComponents.TimeDimension is not null)
            {
                timeDimColumns = "," + string.Join(",", reportedComponents.TimeDimension.SqlColumn(), DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd());
                var timeDimColumnsJoinTemplate =
                    isMergeOnly ? "Fact.{0} = Import.{0}" : "Import.{0} IS NULL OR Fact.{0} =  Import.{0}";
                timeDimColumnsJoin = @$"AND (
                    { string.Format(timeDimColumnsJoinTemplate, DbExtensions.SqlPeriodStart())} AND 
                    {string.Format(timeDimColumnsJoinTemplate, DbExtensions.SqlPeriodEnd())})";

                sdmxTimeDimNotNull = string.Format("AND Import.{0} IS NOT NULL", reportedComponents.TimeDimension.SqlColumn());
            }

            //Measure columns
            string measureColumn = null;
            string measureColumnInsert = null;
            string measureColumnUpdate = null;
            if (reportedComponents.IsPrimaryMeasureReported)
            {
                measureColumn = ", [VALUE]";
                measureColumnInsert = $", CASE WHEN [VALUE] = {dsd.PrimaryMeasure.GetSqlMinValue(true)} THEN NULL ELSE [VALUE] END";
                
                var updatePart = includeMerge ?
                    $@"WHEN Import.[REQUESTED_ACTION] = {(int)StagingRowActionEnum.Merge} AND Import.[VALUE] = {dsd.PrimaryMeasure.GetSqlMinValue(true)} THEN NULL -- Intentionally missing value
                    WHEN Import.[REQUESTED_ACTION] = {(int)StagingRowActionEnum.Merge} AND Import.[VALUE] IS NOT NULL THEN Import.[VALUE] -- Actions I, A, M"
                    : "";
                var deletePart = includeDelete ?
                    $@"WHEN Import.[REAL_ACTION_FACT] = {(int)StagingRowActionEnum.Delete} THEN NULL
                    WHEN Import.[REQUESTED_ACTION] = {(int)StagingRowActionEnum.Delete} AND Import.[VALUE] IS NOT NULL THEN NULL -- Action D, but not a real delete"
                    : "";

                measureColumnUpdate = @$"Fact.[VALUE] = CASE 
                    {updatePart}
                    {deletePart}
                    ELSE Fact.[VALUE] END, ";
            }

            //Attributes columns
            string obsAttrColumns = null;
            string obsAttrColumnsInsert = null;
            string obsAttrColumnsUpdate = null;
            var attributes = reportedComponents.ObservationAttributes;
            if (attributes.Any())
            {
                obsAttrColumns = ", " + attributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength);
                
                obsAttrColumnsInsert = ", " + string.Join(", ", attributes
                    .Select(a => $@"CASE WHEN {a.SqlColumn()} = {a.GetSqlMinValue(true)} THEN NULL ELSE {a.SqlColumn()} END"));

                obsAttrColumnsUpdate = string.Join(",\n", attributes
                    .Select(a => $@"Fact.{a.SqlColumn()} = CASE 
                    {(includeMerge ?
                        $@"WHEN [REQUESTED_ACTION] = {(int)StagingRowActionEnum.Merge} AND Import.{a.SqlColumn()} = {a.GetSqlMinValue(true)} THEN NULL -- Intentionally missing value
                        WHEN [REQUESTED_ACTION] = {(int)StagingRowActionEnum.Merge} AND Import.{a.SqlColumn()} IS NOT NULL THEN Import.{ a.SqlColumn()} -- Actions I, A, M": null)}
                    {(includeDelete ? 
                        $@"WHEN [REAL_ACTION_FACT] = {(int)StagingRowActionEnum.Delete} THEN NULL
                        WHEN [REQUESTED_ACTION] = {(int)StagingRowActionEnum.Delete} AND Import.{a.SqlColumn()} IS NOT NULL THEN NULL -- Action D, but not a real delete": null)}
                    ELSE Fact.{a.SqlColumn()} END")) + ",\n";
            }

            //Batch group
            var batchGroup = batchNumber is not null ? $"AND Import.[BATCH_NUMBER] = { batchNumber}" : null;

            //Merge staging table to Fact table
            //might be better performance using:
            //{ (includeSummary ? @"declare @updated as int = 0, @deleted as int = 0;" : "")}

            //{ (includeSummary ? @$", @updated = @updated + CASE WHEN Import.[REAL_ACTION_ATTR] = {(int)StagingRowActionEnum.Merge} THEN 1 ELSE 0 END, 
            //@deleted = @deleted + CASE WHEN Import.[REAL_ACTION_ATTR] = {(int)StagingRowActionEnum.Delete} THEN 1 ELSE 0 END " : "")}

            //{ (includeSummary ? "SELECT @updated as [UPDATED], @deleted as [DELETED], @@ROWCOUNT as [TOTAL];" : "")}";

            var sqlCommand = $@"
{ (includeSummary ? @"DECLARE @SummaryOfChanges TABLE(Change VARCHAR(20));" : "")}

MERGE INTO [{DotStatDb.DataSchema}].[{dsd.SqlFactTable((char)tableVersion)}] Fact
USING
(
    SELECT Filt.[SID] {timeDimColumns}{measureColumn}{obsAttrColumns}, [REQUESTED_ACTION], [REAL_ACTION_FACT]
    FROM [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}] Import
    INNER JOIN [{DotStatDb.DataSchema}].[{dsd.SqlFilterTable()}] Filt
    ON {dimensionColumnsNoTimeDimJoin}
    WHERE Import.[REAL_ACTION_FACT] <> {(int)StagingRowActionEnum.Skip} {batchGroup}
) Import
ON Fact.[SID] = Import.[SID] {timeDimColumnsJoin}
WHEN MATCHED THEN
    UPDATE SET 
    {measureColumnUpdate}
    {obsAttrColumnsUpdate}
    [LAST_UPDATED] = @CurrentDateTime,
    [LAST_STATUS] = {(includeDelete ? $"case when Import.[REAL_ACTION_FACT] = {(int)StagingRowActionEnum.Delete} then 'D' else 'A' end" : "'A'")}
{(includeMerge ? $@"WHEN NOT MATCHED {sdmxTimeDimNotNull} AND Import.[REQUESTED_ACTION] = {(int)StagingRowActionEnum.Merge} THEN -- Inserts
INSERT ([SID]{timeDimColumns}{measureColumn}{obsAttrColumns}, [LAST_UPDATED] )
VALUES ([SID]{timeDimColumns}{measureColumnInsert}{obsAttrColumnsInsert}, @CurrentDateTime )" : null)}
{(includeSummary ? @"OUTPUT 
    case 
        when $action='UPDATE' then 
            case inserted.[LAST_STATUS] 
                when 'D' then 'DELETE'
                else 'UPDATE'
            end
        else $action 
    end 
    INTO @SummaryOfChanges" : "")};

{(includeSummary ?
    @"SELECT
        sum(case when Change = 'INSERT' then 1 else 0 end) inserted,
        sum(case when Change = 'UPDATE' then 1 else 0 end) updated,  
        sum(case when Change = 'DELETE' then 1 else 0 end) deleted
    FROM @SummaryOfChanges
    GROUP BY Change;" : "")}";

            try
            {
                var currentDateTime = DateTime.Now;
                if (includeSummary)
                {
                    await using var dbDataReader = await DotStatDb.ExecuteReaderSqlWithParamsAsync(
                        sqlCommand, cancellationToken, commandBehavior: CommandBehavior.SingleRow,
                        parameters: new SqlParameter("CurrentDateTime", SqlDbType.DateTime) { Value = currentDateTime });

                    if (await dbDataReader.ReadAsync(cancellationToken))
                    {
                        mergeResult.InsertCount += dbDataReader.GetInt32(0);
                        mergeResult.UpdateCount += dbDataReader.GetInt32(1);
                        mergeResult.DeleteCount += dbDataReader.GetInt32(2);
                        mergeResult.TotalCount =
                            mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                    }
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, new SqlParameter("CurrentDateTime", SqlDbType.DateTime) { Value = currentDateTime });
                }
            }
            catch (SqlException e)
            {
                switch (e.Number)
                {
                    case 2627://duplicates in the staging table insert
                        mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.DuplicatedRowsInStagingTable });
                        break;
                    case 8672://Overlapping rows or duplicates
                        if (await StagingTableHasDuplicates(dsd, reportedComponents, cancellationToken))
                            mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.DuplicatedRowsInStagingTable });
                        else
                            mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.MultipleRowsModifyingTheSameRegionInStagingTable });
                        break;
                    default:
                        throw;
                }
            }

            return mergeResult;
        }

        public async ValueTask<MergeResult> MergeStagingToAttrTable(Dsd dsd, ReportedComponents reportedComponents,
            DbTableVersion tableVersion, bool includeSummary, int? batchNumber, BulkImportResult bulkImportResult, CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();
            if (!reportedComponents.SeriesAttributes.Any())
                return mergeResult;

            var isMergeOnly = bulkImportResult.NumberOfRowsToMerge > 0 && bulkImportResult.NumberOfRowsToDelete == 0;
            var includeMerge = bulkImportResult.NumberOfRowsToMerge > 0;
            var includeDelete = bulkImportResult.NumberOfRowsToDelete > 0;

            //Dimensions columns
            var noTimeDimensions = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension);
            var dimensionColumnsNoTimeDimJoinTemplate =
                isMergeOnly ? " Filt.{0} = Import.{0} " : "(Filt.{0} = COALESCE(Import.{0}, Filt.{0}))";
            var dimensionColumnsNoTimeDimJoin =
                string.Join(" AND ", noTimeDimensions.Select(a => string.Format(dimensionColumnsNoTimeDimJoinTemplate, a.SqlColumn())));

            //Attributes columns 
            var attributes = reportedComponents.SeriesAttributes;
            var seriesAttrColumns = attributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength);

            var seriesAttrColumnsInsert = string.Join(", ", attributes
                .Select(a => $@"CASE WHEN {a.SqlColumn()} = {a.GetSqlMinValue(true)} THEN NULL ELSE {a.SqlColumn()} END"));

            var seriesAttrColumnsWithMin = string.Join(",", attributes.Select(a => string.Format("MIN({0}) {0}", a.SqlColumn())));
            
            var seriesAttrColumnsUpdate = string.Join(",\n", attributes
                    .Select(a => $@"Attr.{a.SqlColumn()} = CASE 
                        {(includeMerge ?
                            $@"WHEN [REQUESTED_ACTION] = {(int)StagingRowActionEnum.Merge} AND Import.{a.SqlColumn()} = {a.GetSqlMinValue(true)} THEN NULL -- Intentionally missing value
                            WHEN [REQUESTED_ACTION] = {(int)StagingRowActionEnum.Merge} AND Import.{a.SqlColumn()} IS NOT NULL THEN Import.{a.SqlColumn()} -- Actions I, A, M": null)}
                    {(includeDelete ? $@"WHEN [REQUESTED_ACTION] = {(int)StagingRowActionEnum.Delete} AND Import.{a.SqlColumn()} IS NOT NULL THEN NULL -- Action D, but not a real delete": null)}
                    ELSE Attr.{a.SqlColumn()} END"));

            //Batch group
            var batchGroup = batchNumber is not null ? $"AND Import.[BATCH_NUMBER] = { batchNumber}" : null;

            //Merge staging table to Attributes table
            //might be better performance using:
            //{ (includeSummary ? @"declare @updated as int = 0, @deleted as int = 0;" : "")}

            //{ (includeSummary ? @$", @updated = @updated + CASE WHEN Import.[REAL_ACTION_ATTR] = {(int)StagingRowActionEnum.Merge} THEN 1 ELSE 0 END, 
            //@deleted = @deleted + CASE WHEN Import.[REAL_ACTION_ATTR] = {(int)StagingRowActionEnum.Delete} THEN 1 ELSE 0 END " : "")}

            //{ (includeSummary ? "SELECT @updated as [UPDATED], @deleted as [DELETED], @@ROWCOUNT as [TOTAL];" : "")}";

            var sqlCommand = $@"
{ (includeSummary ? @"DECLARE @SummaryOfChanges TABLE(Change VARCHAR(20));" : "")}

MERGE INTO [{DotStatDb.DataSchema}].[{dsd.SqlDimGroupAttrTable((char)tableVersion)}] Attr
USING
(    
    SELECT Filt.[SID], {seriesAttrColumnsWithMin}, MIN([REQUESTED_ACTION]) [REQUESTED_ACTION], MIN([REAL_ACTION_ATTR]) [REAL_ACTION_ATTR]
    FROM [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}] Import
    INNER JOIN [{DotStatDb.DataSchema}].[{dsd.SqlFilterTable()}] Filt 
    ON {dimensionColumnsNoTimeDimJoin}
    WHERE Import.[REAL_ACTION_ATTR] <> {(int)StagingRowActionEnum.Skip} {batchGroup}
    GROUP BY Filt.[SID]
) Import
ON Attr.[SID] = Import.[SID]
WHEN MATCHED AND Import.[REAL_ACTION_ATTR] = {(int)StagingRowActionEnum.Merge} THEN -- Updates
UPDATE SET 
    {seriesAttrColumnsUpdate}     
{(includeDelete ? $@"WHEN MATCHED AND Import.[REAL_ACTION_ATTR] = {(int)StagingRowActionEnum.Delete} THEN DELETE -- Deletions" : "")}
{(includeMerge ? $@"WHEN NOT MATCHED AND Import.[REAL_ACTION_ATTR] = {(int)StagingRowActionEnum.Merge} THEN -- Inserts
INSERT ([SID], {seriesAttrColumns})
VALUES ([SID], {seriesAttrColumnsInsert})" : "")}
{(includeSummary ? @"OUTPUT $action INTO @SummaryOfChanges":"")};

{(includeSummary ? 
    @"SELECT
        sum(case when Change = 'INSERT' then 1 else 0 end) inserted,
        sum(case when Change = 'UPDATE' then 1 else 0 end) updated,  
        sum(case when Change = 'DELETE' then 1 else 0 end) deleted
    FROM @SummaryOfChanges
    GROUP BY Change;" : "")}";

            try
            {
                if (includeSummary)
                {
                    await using var dbDataReader = await DotStatDb.ExecuteReaderSqlAsync(
                        sqlCommand, cancellationToken, commandBehavior: CommandBehavior.SingleRow);

                    if (await dbDataReader.ReadAsync(cancellationToken))
                    {
                        mergeResult.InsertCount += dbDataReader.GetInt32(0);
                        mergeResult.UpdateCount += dbDataReader.GetInt32(1);
                        mergeResult.DeleteCount += dbDataReader.GetInt32(2);
                        mergeResult.TotalCount =
                            mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                    }
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
                }
            }
            catch (SqlException e)
            {
                switch (e.Number)
                {
                    case 2627://duplicates in the staging table insert
                        mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.DuplicatedRowsInStagingTable });
                        break;
                    case 8672://Overlapping rows or duplicates
                        if (await StagingTableHasDuplicates(dsd, reportedComponents, cancellationToken))
                            mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.DuplicatedRowsInStagingTable });
                        else
                            mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.MultipleRowsModifyingTheSameRegionInStagingTable });
                        break;
                    default:
                        throw;
                }
            }
            
            return mergeResult;
        }

        public async ValueTask<MergeResult> MergeStagingToDatasetAttributes(Dataflow dataflow,
            IList<DataSetAttributeRow> dataSetAttributeRows, CodeTranslator translator, 
            DbTableVersion tableVersion, bool includeSummary, CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();

            var datasetAttributesOfDds = dataflow.Dsd.Attributes
                .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList();

            if (datasetAttributesOfDds.Count == 0)
                return mergeResult;

            dataSetAttributeRows ??= new List<DataSetAttributeRow>();
            
            if (!dataSetAttributeRows.Any())
                return mergeResult;

            foreach (var dataSetAttributeRow in dataSetAttributeRows)
            {
                //Special Case Delete all
                if (dataSetAttributeRow.Action is StagingRowActionEnum.DeleteAll)
                {
                    if (includeSummary)
                    {
                        mergeResult.DeleteCount += (int)await DotStatDb.ExecuteScalarSqlAsync(
                            $"DELETE FROM [{DotStatDb.DataSchema}].[{dataflow.Dsd.SqlDsdAttrTable((char)tableVersion)}] WHERE [DF_ID] = {dataflow.DbId}; SELECT @@ROWCOUNT AS [ROW_COUNT];", cancellationToken);
                        mergeResult.TotalCount =
                            mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                    }
                    else
                    {
                        await DotStatDb.ExecuteNonQuerySqlAsync(
                            $"DELETE FROM [{DotStatDb.DataSchema}].[{dataflow.Dsd.SqlDsdAttrTable((char)tableVersion)}] WHERE [DF_ID] = {dataflow.DbId}", cancellationToken);
                    }

                    Log.Warn(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DeleteAllActionPerformed));

                    continue;
                }

                //No dataSet level attributes to process
                if (!dataSetAttributeRow.Attributes.Any())
                    continue;

                var attributesToModify = new Dictionary<Attribute, object>();
                var i = 0;

                var attributesPresent = new bool[datasetAttributesOfDds.Count];
                //Translate to internal codes
                foreach (var attributeKeyValue in dataSetAttributeRow.Attributes.Where(x => !string.IsNullOrEmpty(x.Code)))
                {
                    var attribute = datasetAttributesOfDds.First(a =>
                        a.Base.Id.Equals(attributeKeyValue.Concept, StringComparison.InvariantCultureIgnoreCase));

                    var attrValue = attribute.GetObjectFromString(attributeKeyValue.Code, translator, dataSetAttributeRow.Action);
                    attributesToModify[attribute] = attrValue;
                    attributesPresent[i++] = true;
                }

                var allDfComponentsPresentOrAllOmitted = attributesPresent.Distinct().Count() == 1;

                var realAction = dataSetAttributeRow.Action == StagingRowActionEnum.Delete && allDfComponentsPresentOrAllOmitted
                    ? StagingRowActionEnum.Delete : StagingRowActionEnum.Merge;
                
                //DELETE
                if(realAction == StagingRowActionEnum.Delete)
                {
                    if (includeSummary)
                    {
                        mergeResult.DeleteCount += (int)await DotStatDb.ExecuteScalarSqlAsync(
                            $"DELETE FROM [{DotStatDb.DataSchema}].[{dataflow.Dsd.SqlDsdAttrTable((char)tableVersion)}] WHERE [DF_ID] = {dataflow.DbId}; SELECT @@ROWCOUNT AS [ROW_COUNT];", cancellationToken);
                        mergeResult.TotalCount =
                            mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                    }
                    else
                    {
                        await DotStatDb.ExecuteNonQuerySqlAsync(
                            $"DELETE FROM [{DotStatDb.DataSchema}].[{dataflow.Dsd.SqlDsdAttrTable((char)tableVersion)}] WHERE [DF_ID] = {dataflow.DbId}", cancellationToken);
                    }

                    continue;
                }

                //MERGE
                if (!attributesToModify.Any())
                    continue;

                var columnNames = string.Join(",", attributesToModify.Select(kvp => kvp.Key.SqlColumn()));
                var insertColumns = string.Join(",", attributesToModify
                    .Select(kvp => $@"CASE WHEN @Comp{kvp.Key.DbId} = {kvp.Key.GetSqlMinValue(true)} THEN NULL ELSE {kvp.Key.SqlColumn()} END"));

                //UPDATE COLUMNS WHERE STAGING COLUMN IS NOT NULL
                var updateClause = string.Join(",\n", attributesToModify
                    .Select(kvp => @$"Fact.{kvp.Key.SqlColumn()} = CASE 
                        WHEN {(int)dataSetAttributeRow.Action} = {(int)StagingRowActionEnum.Merge} AND Import.{kvp.Key.SqlColumn()} = {kvp.Key.GetSqlMinValue(true)} THEN NULL -- Intentionally missing value
                        WHEN {(int)dataSetAttributeRow.Action} = {(int)StagingRowActionEnum.Merge} AND Import.{kvp.Key.SqlColumn()} IS NOT NULL THEN Import.{kvp.Key.SqlColumn()} -- Actions I, A, M
                        WHEN {(int)dataSetAttributeRow.Action} = {(int)StagingRowActionEnum.Delete} AND Import.{kvp.Key.SqlColumn()} IS NOT NULL THEN NULL -- Action D, but not a real delete
                        ELSE Fact.{kvp.Key.SqlColumn()} END"));

                var attributeDbParameters = attributesToModify.Select(kvp =>
                    new SqlParameter($"Comp{kvp.Key.DbId}", kvp.Key.Base.HasCodedRepresentation() ? SqlDbType.Int : SqlDbType.NVarChar)
                    {
                        Value = kvp.Value
                    } as DbParameter).Concat(new[] { new SqlParameter("DfId", SqlDbType.Int) { Value = dataflow.DbId } as DbParameter }).ToArray();
                
                var sqlCommand = $@"
            {(includeSummary ? @"declare @updated as int = 0;" : "")}
MERGE
INTO [{DotStatDb.DataSchema}].[{dataflow.Dsd.SqlDsdAttrTable((char)tableVersion)}] AS Fact
USING (SELECT @DfId AS [DF_ID], {
        string.Join(",", attributesToModify.Select(kvp => $"@Comp{kvp.Key.DbId} AS {kvp.Key.SqlColumn()}"))
    }) AS Import
    ON Fact.[DF_ID] = Import.[DF_ID]
WHEN MATCHED THEN
    UPDATE SET
    {updateClause} 
    {(includeSummary ? ", @updated = @updated + 1" : "")} 
WHEN NOT MATCHED AND {(int)realAction} = {(int)StagingRowActionEnum.Merge}  THEN
    INSERT (DF_ID, {columnNames})
    VALUES (@DfId, {insertColumns});

{(includeSummary ? "SELECT @updated as [UPDATED], @@ROWCOUNT as [TOTAL];" : "")}";

                if (includeSummary)
                {
                    await using var dbDataReader = await DotStatDb.ExecuteReaderSqlWithParamsAsync(
                        sqlCommand, cancellationToken, commandBehavior: CommandBehavior.SingleRow, parameters: attributeDbParameters);

                    if (await dbDataReader.ReadAsync(cancellationToken))
                    {
                        mergeResult.UpdateCount += dbDataReader.GetInt32(0);
                        var totalRowsModified = dbDataReader.GetInt32(1);
                        mergeResult.InsertCount += totalRowsModified - mergeResult.UpdateCount;
                        mergeResult.TotalCount =
                            mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                    }
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, attributeDbParameters);
                }
            }
            
            return mergeResult;
        }
        
        public async Task<int> SetBatchNumberToStagingTableRows(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken)
        {
            var partitionColumns = reportedComponents.Dimensions.ToColumnList();
            var sqlCommand = $@";WITH CTE AS(
	                SELECT [BATCH_NUMBER], ROW_NUMBER() OVER (PARTITION BY {partitionColumns} ORDER BY {partitionColumns}) AS [CALCULATED_BATCH_NUMBER]
                    FROM [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}]
                )
                UPDATE CTE SET CTE.[BATCH_NUMBER] = [CALCULATED_BATCH_NUMBER];

                SELECT MAX([BATCH_NUMBER]) FROM [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}];";

            return (int)await DotStatDb.ExecuteScalarSqlAsync(sqlCommand, cancellationToken);
        }

        public async Task DropDsdTables(Dsd dsd, CancellationToken cancellationToken)
        {
            await DropDsdTables(dsd, DbTableVersion.A, cancellationToken);
            await DropDsdTables(dsd, DbTableVersion.B, cancellationToken);
        }

        private async Task DropDsdTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlFactTable((char)tableVersion), cancellationToken);
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlFilterTable(), cancellationToken);
        }
        
        public async Task DropAttributeTables(Dsd dsd, CancellationToken cancellationToken)
        {
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlDimGroupAttrTable((char) DbTableVersion.A), cancellationToken);
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlDimGroupAttrTable((char) DbTableVersion.B), cancellationToken);
        }

        public async Task TruncateDsdTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.SqlFactTable((char)tableVersion), cancellationToken);
        }

        public async Task TruncateAttributeTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.SqlDsdAttrTable((char) tableVersion), cancellationToken);
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.SqlDimGroupAttrTable((char) tableVersion), cancellationToken);
        }

        public string GetDataViewQuery(Dataflow dataflow, char tableVersion)
        {
            var columns = new List<string>();

            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"[TIME_PERIOD] AS [{dim.Code}]");
                    columns.Add($"[{DbExtensions.SqlPeriodStart()}]");
                    columns.Add($"[{DbExtensions.SqlPeriodEnd()}]");
                }
                else
                {
                    columns.Add($"[{dim.Code}]");
                }
            }

            columns.Add($"[{dataflow.Dsd.Base.PrimaryMeasure.Id}]");

            foreach (var attr in dataflow.Dsd.Attributes)
            {
                columns.Add($"[{attr.Code}]");
            }

            columns.Add($"[{DbExtensions.LAST_UPDATED_COLUMN}]");
            columns.Add($"[{DbExtensions.LAST_STATUS_COLUMN}]");

            return $"SELECT [SID], {string.Join(",", columns)} FROM [{DotStatDb.DataSchema}].[{dataflow.SqlDataDataFlowViewName(tableVersion)}]";
        }

        public string GetEmptyDataViewQuery(Dataflow dataflow)
        {
            var columns = new List<string>();

            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"NULL AS [{dim.Code}]");
                    columns.Add($"NULL AS [{DbExtensions.SqlPeriodStart()}]");
                    columns.Add($"NULL AS [{DbExtensions.SqlPeriodEnd()}]");
                }
                else
                {
                    columns.Add($"NULL AS [{dim.Code}]");
                }
            }

            columns.Add($"NULL AS [{dataflow.Dsd.Base.PrimaryMeasure.Id}]");

            foreach (var attr in dataflow.Dsd.Attributes)
            {
                columns.Add($"NULL AS [{attr.Code}]");
            }

            columns.Add($"NULL as [{DbExtensions.LAST_UPDATED_COLUMN}]");
            columns.Add($"NULL as [{DbExtensions.LAST_STATUS_COLUMN}]");

            return "SELECT TOP 0 NULL AS [SID], " + string.Join(",", columns);
        }

        public string GetOrderByClause(Dataflow dataflow, bool isTimeAscending = true)
        {
            var columns = new List<string>{"[SID] ASC"};

            if (dataflow.Dsd.TimeDimension != null)
            {
                columns.Add($"[{DbExtensions.SqlPeriodStart()}] {(isTimeAscending ? "ASC":"DESC")}");
                columns.Add($"[{DbExtensions.SqlPeriodEnd()}] {(isTimeAscending ? "DESC" : "ASC")}");
            }

            return string.Join(",", columns);
        }

        public async Task<bool> DataTablesExist(Dsd dsd, CancellationToken cancellationToken)
        {
            return
                await DotStatDb.TableExists(dsd.SqlFactTable((char)DbTableVersion.A), cancellationToken)
                || await DotStatDb.TableExists(dsd.SqlFactTable((char)DbTableVersion.B), cancellationToken);
        }

        #region Private methods

        private async Task BuildStagingTables(Dsd dsd, ReportedComponents reportedComponents, bool isTimeAtTimeDimensionSupported, CancellationToken cancellationToken)
        {
            //Dimensions
            //32 is the SQL limit of columns that can participate in a Unique index
            var rowId = reportedComponents.Dimensions.Count > 32 ? $"[ROW_ID] AS ({dsd.SqlBuildRowIdFormula(true)}) PERSISTED NOT NULL," : null;

            var dimensionColumns = reportedComponents.Dimensions?.ToColumnList(true, null, true);
            if (!string.IsNullOrWhiteSpace(dimensionColumns))
                dimensionColumns += ',';

            var timePeriodColumns = reportedComponents.TimeDimension is not null
                ? "," + string.Join(",",
                      DbExtensions.SqlPeriodStart(true, isTimeAtTimeDimensionSupported, true),
                      DbExtensions.SqlPeriodEnd(true, isTimeAtTimeDimensionSupported, true)
                ) : null;

            //Measure
            var measure = reportedComponents.IsPrimaryMeasureReported
                ? $"[VALUE] {dsd.PrimaryMeasure.GetSqlType(GeneralConfiguration)} NULL,"
                : null;

            //Attributes
            var nonDsdAttributes = reportedComponents.SeriesAttributes.Concat(reportedComponents.ObservationAttributes).ToList().OrderBy(a => a.DbId);
            var attributeColumns = nonDsdAttributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength, withType: true, applyNotNull: false);

            if (!string.IsNullOrWhiteSpace(attributeColumns))
                attributeColumns += ',';

            //Create table
            var sqlCommand = $@"CREATE TABLE [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}](
                    {rowId}
                    {dimensionColumns}
                    {measure}
                    {attributeColumns}
                    [REQUESTED_ACTION] [TINYINT] NOT NULL,
                    [REAL_ACTION_FACT] [TINYINT] NOT NULL,
                    [REAL_ACTION_ATTR] [TINYINT] NOT NULL,
                    [BATCH_NUMBER] [INT] NULL
                    {timePeriodColumns}
                )";

            await DotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        private void OnSqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            var batchNumber = e.RowsCopied / DotStatDb.DataSpace.NotifyImportBatchSize;
            Log.Notice(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ProcessingObservations),
                e.RowsCopied, batchNumber));
        }

        private async Task<bool> StagingTableHasDuplicates(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken)
        {
            //Dimensions columns
            var noTimeDimensions = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension
                                                                            && dsd.Dimensions.Any(rd => rd.Code.Equals(d.Code, StringComparison.CurrentCultureIgnoreCase)));
            
            var dimensionColumnsNoTimeDimJoin =
                string.Join(", ", noTimeDimensions.Select(a => a.SqlColumn()));

            string timeDimColumns = null;
            if (reportedComponents.TimeDimension is not null)
            {
                timeDimColumns = "," + string.Join(",", DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd());
            }

            var sqlCommand = $@"SELECT TOP 1 1 FROM [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}]
GROUP BY {dimensionColumnsNoTimeDimJoin} {timeDimColumns}, [REQUESTED_ACTION]
HAVING COUNT(*) > 1";

            return ((int?)await DotStatDb.ExecuteScalarSqlAsync(sqlCommand, cancellationToken) ?? 0) > 0;
        }
        #endregion
    }
}