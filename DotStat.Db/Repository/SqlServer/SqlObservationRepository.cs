﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Db.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Repository.SqlServer
{
    public class SqlObservationRepository: DatabaseRepositoryBase<SqlDotStatDb>, IObservationRepository
    {
        private readonly List<AttributeAttachmentLevel> attachmentFilter = new List<AttributeAttachmentLevel>()
        {
            AttributeAttachmentLevel.Observation,
            AttributeAttachmentLevel.Group,
            AttributeAttachmentLevel.DimensionGroup
        };
        

        public SqlObservationRepository(SqlDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration): 
            base(dotStatDb, generalConfiguration)
        {
        }

        public async IAsyncEnumerable<ObservationRow> GetDataObservations(
            IDataQuery dataQuery, 
            Dataflow dataflow,
            ICodeTranslator codeTranslator,
            DbTableVersion tableVersion,
            [EnumeratorCancellation] CancellationToken cancellationToken,
            long? startRow = null, 
            long? endRow = null
        )
        {

            var dimensions = dataflow.Dimensions.OrderTimeFirst().ToArray();
            var attributes = dataflow.Dsd.Attributes
                .Where(x => attachmentFilter.Contains(x.Base.AttachmentLevel))
                .ToArray();

            var sqlParameters = new List<DbParameter>();
            var sqlQuery = await BuildDataSqlQuery(dataQuery, codeTranslator, dataflow, dimensions, attributes, sqlParameters, tableVersion, startRow, endRow, cancellationToken);
            
            await using (var dr = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlQuery, cancellationToken, tryUseReadOnlyConnection: true, parameters: sqlParameters.ToArray()))
            {
                while (await dr.ReadAsync(cancellationToken))
                {
                    yield return await GetDataObservation(
                        dimensions,
                        attributes,
                        codeTranslator,
                        dataQuery,
                        dr,
                        cancellationToken
                        );
                }
            }
        }

        public async IAsyncEnumerable<ObservationRow> GetMetadataObservations(
            IDataQuery metadataQuery,
            Dataflow dataflow,
            ICodeTranslator codeTranslator,
            DbTableVersion tableVersion,
            [EnumeratorCancellation] CancellationToken cancellationToken
        )
        {
            var dimensions = dataflow.Dimensions.OrderTimeFirst().ToArray();
            var metadataAttributes = dataflow.Dsd.Msd.MetadataAttributes;
            
            var sqlQuery = await BuildMetadataSqlQuery(metadataQuery, codeTranslator, dataflow, dimensions, 
                metadataAttributes, tableVersion, cancellationToken);

            using (var dr = await DotStatDb.ExecuteReaderSqlAsync(sqlQuery, cancellationToken, tryUseReadOnlyConnection: true))
            {
                while (await dr.ReadAsync(cancellationToken))
                {
                    yield return await GetMetadataObservation(
                        dimensions,
                        metadataAttributes,
                        codeTranslator,
                        metadataQuery,
                        dr,
                        cancellationToken
                    );
                }
            }
        }

        public async Task<string> BuildDataSqlQuery(
            IDataQuery dataQuery,
            ICodeTranslator codeTranslator,
            Dataflow dataflow,
            IList<Dimension> dimensions,
            IList<Attribute> attributes,
            List<DbParameter> @params,
            DbTableVersion tableVersion,
            long? startRow,
            long? endRow,
            CancellationToken cancellationToken
            )
        {
            var isPaging = endRow != null;

            var sbSelect = new StringBuilder("SELECT ");

            if (isPaging)
            {
                sbSelect.Append("TOP(@top) ");
                @params.Add(new SqlParameter("top", SqlDbType.Int) { Value = endRow });
            }

            sbSelect
                .Append(dimensions.ToColumnList())
                .Append(",[value]");
            
            var sbFrom = new StringBuilder(" FROM ")
                .Append($"[{DotStatDb.DataSchema}].{dataflow.Dsd.SqlFilterTable("FI")}")
                .Append($" INNER JOIN [{DotStatDb.DataSchema}].{dataflow.Dsd.SqlFactTable((char)tableVersion, "FA")} ON FI.SID = FA.SID");

            if (attributes.Any())
            {
                if (attributes.Any(x => x.Base.AttachmentLevel != AttributeAttachmentLevel.Observation))
                {
                    sbFrom.Append($" LEFT JOIN [{DotStatDb.DataSchema}].{dataflow.Dsd.SqlDimGroupAttrTable((char)tableVersion, "ATTR")} ON FI.[SID] = ATTR.[SID]");
                }
                
                sbSelect.Append(",").Append(attributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength));
            }

            sbSelect.Append(sbFrom);

            var where = await Predicate.BuildWhereForDataObservationLevel(codeTranslator, dataQuery, dataflow, cancellationToken);

            if (!string.IsNullOrEmpty(where))
                sbSelect.Append(" WHERE ").Append(where);

            return sbSelect.ToString();
        }

        public async Task<string> BuildMetadataSqlQuery(
            IDataQuery metadataQuery,
            ICodeTranslator codeTranslator,
            Dataflow dataflow,
            IList<Dimension> dimensions,
            IList<MetadataAttribute> metadataAttributes,
            DbTableVersion tableVersion,
            CancellationToken cancellationToken
        )
        {
            var where = await Predicate.BuildWhereForMetadataObservationLevel(codeTranslator, metadataQuery, dataflow, cancellationToken);
            var isFiltered = !string.IsNullOrEmpty(where);
            
            var selectDataFlowLevelMetadata = $@"SELECT 
            {string.Join(", ", dimensions.Select(d => $"NULL AS {d.SqlColumn()}"))},{metadataAttributes.ToColumnList()}
            FROM[{DotStatDb.DataSchema}].{dataflow.Dsd.SqlMetadataDsdTable((char) tableVersion)}
            WHERE DF_ID = {dataflow.DbId}";

            var dimensionColumns = string.Join(", ", dimensions.Where(dim => !dim.Base.TimeDimension).Select(d => $"CASE WHEN {d.SqlColumn()} = 0 THEN NULL ELSE {d.SqlColumn()} END AS {d.SqlColumn()}"));
            var timeDim = dataflow.Dsd.TimeDimension;
            var timeColumn = timeDim != null ? timeDim.SqlColumn() + "," : null;

            var selectLowLevelMetadata = $@"SELECT 
            {timeColumn}{dimensionColumns},{metadataAttributes.ToColumnList()}
            FROM [{DotStatDb.DataSchema}].{dataflow.Dsd.SqlMetadataTable((char)tableVersion)}";

            var whereStatement = isFiltered ? $"WHERE {where}":"";

            var sqlMetadataQuery = isFiltered ? 
                $@"{selectLowLevelMetadata}
                   {whereStatement}":
                $@"SELECT {dimensions.ToColumnList()},{metadataAttributes.ToColumnList()}
                FROM (
                        {selectDataFlowLevelMetadata}
                    UNION ALL 
                        {selectLowLevelMetadata}
                ) All_META";

            return sqlMetadataQuery;
        }

        public async Task<int> GetObservationCount(
            Dataflow dataflow, 
            DbTableVersion tableVersion,
            string dataflowWhereClause,
            CancellationToken cancellationToken)
        {
            var sql = new StringBuilder("SELECT count(*) FROM ")
                .Append($"[{DotStatDb.DataSchema}].{dataflow.Dsd.SqlFilterTable("FI")}")
                .Append($" INNER JOIN [{DotStatDb.DataSchema}].{dataflow.Dsd.SqlFactTable((char)tableVersion, "FA")} ON FI.SID = FA.SID");

            if (!string.IsNullOrWhiteSpace(dataflowWhereClause))
            {
                sql.Append($" AND {dataflowWhereClause}");
            }

            return (int)await DotStatDb.ExecuteScalarSqlAsync(sql.ToString(), cancellationToken);
        }
       
        // expected order from DB: [time dimension], [dimensions], [value], [attributes]
        private async Task<ObservationRow> GetDataObservation(
            IList<Dimension> dimensions,
            IList<Attribute> attributes,
            ICodeTranslator codeTranslator,
            IDataQuery dataQuery,
            IDataReader dr,
            CancellationToken cancellationToken
            )
        {
            var index   = 0;
            string time = null;

            if (dimensions[0].Base.TimeDimension)
            {
                time = dr.GetString(index++);
            }
            
            var seriesKey = new List<IKeyValue>(dimensions.Count - index);

            for (var i = index; i < dimensions.Count; i++)
            {
                var dim = dimensions[i];
                var dmCode = (await codeTranslator[dim,cancellationToken])[dr.GetInt32(index++)];
                seriesKey.Add(new KeyValueImpl(dmCode, dim.Code));
            }

            var obj = dr[index];
            var value = obj == DBNull.Value ? null : obj.ToString();
            index++;

            var attrList = new List<IKeyValue>();

            // attributes ----------------------------

            foreach (var attr in attributes)
            {
                var o = dr[index++];

                if (o == DBNull.Value || o == null)
                    continue;

                var attrValue = attr.Codelist == null
                    ? (string) o
                    : (await codeTranslator[attr, cancellationToken])[(int) o];

                attrList.Add(new KeyValueImpl(attrValue, attr.Code));
            }
            
            // ---------------------------------------

            return new ObservationRow(1,
                StagingRowActionEnum.Merge,
                new ObservationImpl(new KeyableImpl(dataQuery.Dataflow, dataQuery.DataStructure, seriesKey, null),
                time,
                value,
                attrList,
                crossSectionValue:null
            ));
        }

        // expected order from DB: [time dimension], [dimensions], [metadataAttributes]
        private async Task<ObservationRow> GetMetadataObservation(
            IList<Dimension> dimensions,
            IList<MetadataAttribute> metadataAttributes,
            ICodeTranslator codeTranslator,
            IDataQuery dataQuery,
            IDataReader dr,
            CancellationToken cancellationToken
            )
        {
            var index = 0;
            string time = null;

            if (dimensions[0].Base.TimeDimension)
            {
                var o = dr[index++];
                if (o != DBNull.Value && o != null)
                    time = (string)o;
            }

            var seriesKey = new List<IKeyValue>();

            for (var i = index; i < dimensions.Count; i++)
            {
                var o = dr[index++];
                if (o == DBNull.Value || o == null)
                    continue;

                var dim = dimensions[i];
                var dmCode = (await codeTranslator[dim, cancellationToken])[(int)o];
                seriesKey.Add(new KeyValueImpl(dmCode, dim.Code));
            }
            
            var attrList = new List<IKeyValue>();
            
            foreach (var attr in metadataAttributes)
            {
                var o = dr[index++];

                if (o == DBNull.Value || o == null)
                    continue;

                var attrValue = attr.Codelist == null
                    ? (string)o
                    : (await codeTranslator[attr,cancellationToken])[(int)o];

                attrList.Add(new KeyValueImpl(attrValue, attr.HierarchicalId));
            }

            // ---------------------------------------

            return new ObservationRow(1,
                StagingRowActionEnum.Merge,
                new ObservationImpl(new KeyableImpl(dataQuery.Dataflow, dataQuery.DataStructure, seriesKey, null),
                time,
                null,
                attrList,
                crossSectionValue: null
            ));
        }
        
    }
}