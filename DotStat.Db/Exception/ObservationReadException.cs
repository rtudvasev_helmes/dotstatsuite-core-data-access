﻿using System.Diagnostics.CodeAnalysis;
using DotStat.DB.Exception;
using DotStat.Db.Validation;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    public class ObservationReadException : SdmxValidationException
    {
        public ObservationReadException(ValidationErrorType errorType, IObservation observation, params string[] argument)
            : base(new ObservationError()
            {
                Type = errorType,
                Coordinate = observation?.GetFullKey(true, observation?.SeriesKey.DataStructure.TimeDimension?.Id),
                Value = observation?.ObservationValue,
                Argument = argument
            })
        {

        }
    }
}