﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace DotStat.Db.Dto
{
    [ExcludeFromCodeCoverage]
    public class ArtefactItem
    {
        public int DbId { get; set; }
        public string Type { get; set; }
        public string Id { get; set; }
        public string Agency { get; set; }
        public string Version { get; set; }
        public char? LiveVersion { get; set; }
        public char? PITVersion { get; set; }
        public DateTime? PITReleaseDate { get; set; }
        public DateTime? PITRestorationDate { get; set; }
        public int? DfDsdId { get; set; }
        public int? DsdMsdId { get; set; }
        public string DfWhereClause { get; set; }
        public int? MaxTextAttributeLength { get; set; }
        public DateTime? LastModified { get; set; }

        public override string ToString() => $"{Agency}:{Id}({Version})";
    }
}
