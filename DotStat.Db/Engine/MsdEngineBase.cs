﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Engine
{

    public abstract class MsdEngineBase<TDotStatDb> : ArtefactEngineBase<Msd, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected MsdEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public async Task CreateDynamicDbObjects(Dsd dsd, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dsd == null)
            {
                throw new ArgumentNullException(nameof(dsd));
            }

            if (dsd.Msd == null)
            {
                throw new ArgumentNullException(nameof(dsd.Msd));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            await BuildDynamicMetaDataTable(dsd, (char)DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDynamicMetaDataTable(dsd, (char)DbTableVersion.B, dotStatDb, cancellationToken);

            await BuildDynamicDatasetMetaDataTable(dsd, (char)DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDynamicDatasetMetaDataTable(dsd, (char)DbTableVersion.B, dotStatDb, cancellationToken);

            await BuildDynamicMetaDataDsdView(dsd, (char) DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDynamicMetaDataDsdView(dsd, (char) DbTableVersion.B, dotStatDb, cancellationToken);
        }

        public override async Task CleanUp(Msd msd, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Does not delete nor changes structure of related metadata table!

            if (msd == null)
            {
                throw new ArgumentNullException(nameof(msd));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (msd.DbId < 1)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MsdInvalidDbId),
                    msd.FullId, msd.DbId)
                );
            }

            await DeleteFromArtefactTableByDbId(msd.DbId, dotStatDb, cancellationToken);
        }
        

        protected abstract Task BuildDynamicMetaDataTable(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildDynamicDatasetMetaDataTable(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);
        
        protected abstract Task BuildDynamicMetaDataDsdView(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

    }
}