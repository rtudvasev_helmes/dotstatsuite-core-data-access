﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlDsdEngine : DsdEngineBase<SqlDotStatDb>
    {
        public SqlDsdEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var aretefactDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT [ART_ID] 
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ID] = @Id AND [AGENCY] = @Agency AND [TYPE] = @Type
                     AND [VERSION_1] = @Version1 AND [VERSION_2] = @Version2 
                     AND ([VERSION_3] = @Version3 OR ([VERSION_3] IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) { Value = sdmxId },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = sdmxAgency },                
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dsd) },
                new SqlParameter("Version1", SqlDbType.Int) { Value = verMajor },
                new SqlParameter("Version2", SqlDbType.Int) { Value = verMinor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = ((object)verPatch) ?? DBNull.Value }

                );

            return (int)(aretefactDbId ?? -1);
        }

        public override async Task<int> InsertToArtefactTable(Dsd dsd, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var artefactId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"INSERT 
                    INTO [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                       ([SQL_ART_ID],[TYPE],[ID],[AGENCY],[VERSION_1],[VERSION_2],[VERSION_3],[LAST_UPDATED],[DSD_LIVE_VERSION],[DSD_PIT_VERSION],[DSD_PIT_RELEASE_DATE],[DSD_PIT_RESTORATION_DATE],[DSD_MAX_TEXT_ATTR_LENGTH],[MSD_ID]) 
                  OUTPUT INSERTED.ART_ID 
                  VALUES (NULL, @Type, @Id, @Agency, @Version1, @Version2, @Version3, GETDATE(), @LiveVersion, @PITVersion, @PITReleaseDate, @PITRestorationDate, @MaxTextAttrLength, @MsdId)",
                cancellationToken,
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dsd) },
                new SqlParameter("Id", SqlDbType.VarChar) { Value = dsd.Code },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = dsd.AgencyId },
                new SqlParameter("Version1", SqlDbType.Int) { Value = dsd.Version.Major },
                new SqlParameter("Version2", SqlDbType.Int) { Value = dsd.Version.Minor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = (object)dsd.Version.Patch ?? DBNull.Value },
                new SqlParameter("LiveVersion", SqlDbType.Char) { Value = (object)dsd.LiveVersion ?? DBNull.Value },
                new SqlParameter("PITVersion", SqlDbType.Char) { Value = (object)dsd.PITVersion ?? DBNull.Value },
                new SqlParameter("PITReleaseDate", SqlDbType.DateTime) { Value = (object)dsd.PITReleaseDate ?? DBNull.Value },
                new SqlParameter("PITRestorationDate", SqlDbType.DateTime) { Value = (object)dsd.PITRestorationDate ?? DBNull.Value },
                new SqlParameter("MaxTextAttrLength", SqlDbType.Int) { Value = dsd.GetMaxLengthOfTextAttributeFromConfig(GeneralConfiguration.MaxTextAttributeLength) },
                new SqlParameter("MsdId", SqlDbType.Int) { Value = (object)dsd.MsdDbId?? DBNull.Value }
                );

            return (int)artefactId;
        }
        
        protected override async Task DeleteFromArtefactTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM [{dotStatDb.ManagementSchema}].[{DbExtensions.SqlArtefactTableName()}] 
                   WHERE [ART_ID] = @DbId AND [TYPE] = 'DSD'",
                cancellationToken,
                new SqlParameter("DbId", SqlDbType.VarChar) { Value = dbId }
                );
        }
        
        protected override async Task BuildDynamicFactTable(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var attributeColumns = dsd.Attributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength, new[] { AttributeAttachmentLevel.Observation }, withType: true, applyNotNull: false);

            if (!string.IsNullOrWhiteSpace(attributeColumns))
            {
                attributeColumns += ',';
            }

            var groupAttributeColumnsWithTime = dsd.Attributes
                .Where(x => (x.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || x.Base.AttachmentLevel == AttributeAttachmentLevel.Group) 
                            && x.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToColumnList(GeneralConfiguration.MaxTextAttributeLength, null, withType: true, applyNotNull: false);

            if (!string.IsNullOrWhiteSpace(groupAttributeColumnsWithTime))
            {
                groupAttributeColumnsWithTime += ',';
            }

            var timeDim = dsd.TimeDimension;

            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();
            
            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    new[] { timeDim }.ToColumnList(true),
                    DbExtensions.SqlPeriodStart(true, supportsDateTime),
                    DbExtensions.SqlPeriodEnd(true, supportsDateTime)
                ) + ","
                : null;
            var timeConstraint =
                timeDim != null ? $", [{DbExtensions.SqlPeriodStart()}] ASC, [{DbExtensions.SqlPeriodEnd()}] DESC" : null;

            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[FACT_{dsd.DbId}_{targetVersion}](
[SID] [int] NOT NULL,
{timeDimensionColumns}
{groupAttributeColumnsWithTime}
[VALUE] {dsd.PrimaryMeasure.GetSqlType(GeneralConfiguration)},
{attributeColumns}
[LAST_UPDATED] [datetime] NOT NULL DEFAULT (GETDATE()),
[LAST_STATUS] char(1) CONSTRAINT DF_FACT_{dsd.DbId}_{targetVersion}_LAST_STATUS default 'A' not null,
CONSTRAINT [PK_DSD_{dsd.DbId}_{targetVersion}] PRIMARY KEY NONCLUSTERED ([SID]{timeConstraint}) 
)";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            // Performance improvement generate actual content constraint
            if (timeDim != null)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE NONCLUSTERED COLUMNSTORE INDEX [NCCI_TIME_DIM_{dsd.DbId}_{targetVersion}] 
ON [{dotStatDb.DataSchema}].[FACT_{dsd.DbId}_{targetVersion}](
    {DbExtensions.SqlPeriodStart()},
    {DbExtensions.SqlPeriodEnd()}
)", cancellationToken);
            }

            // Create clustered index
            await dotStatDb.ExecuteNonQuerySqlAsync(
                $@"CREATE UNIQUE CLUSTERED INDEX [I_CLUSTERED_DSD_{dsd.DbId}_{targetVersion}] 
ON [{dotStatDb.DataSchema}].[FACT_{dsd.DbId}_{targetVersion}]([LAST_STATUS],[SID]{timeConstraint})", cancellationToken);
        }

        protected override async Task BuildDynamicFilterTable(Dsd dsd, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            var dimensionColumnsWithType = dimensions.ToColumnList(true);
            var dimensionColumns =  dimensions.ToColumnList();
            var table = dsd.SqlFilterTable();

            string rowId = null;
            var uIndex = dimensionColumns;
            
            if (dsd.Dimensions.Count > 32)
            {
                rowId = $"[ROW_ID] AS ({dsd.SqlBuildRowIdFormula(true, false)}) PERSISTED NOT NULL,";
                uIndex = "[ROW_ID]";
            }

            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{table}](
                        [SID] [int] identity(1,1) NOT NULL PRIMARY KEY,
                        {rowId}
	                    {dimensionColumnsWithType},
                        CONSTRAINT [U_{table}] UNIQUE ({uIndex}) 
                )";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            // https://docs.microsoft.com/en-us/sql/sql-server/maximum-capacity-specifications-for-sql-server
            if (dimensions.Length <= 32)
            {
                // Performance improvement generate actual content constraint
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE NONCLUSTERED COLUMNSTORE INDEX [NCCI_{table}] ON [{dotStatDb.DataSchema}].[{table}]({dimensionColumns})",
                        cancellationToken
                    );
            }
        }

        protected override async Task AlterTextAttributeColumnsInFactTable(Dsd dsd, IList<Domain.Attribute> attributes,
            int newMaxAttributeLength, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            foreach (var attribute in attributes)
            {
                var sqlCommand =
                    $@"ALTER TABLE [{dotStatDb.DataSchema}].[FACT_{dsd.DbId}_{targetVersion}] ALTER COLUMN {attribute.SqlColumn()} NVARCHAR({(newMaxAttributeLength == 0 ? "MAX" : newMaxAttributeLength.ToString())})";

                await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            }
        }

        protected override async Task BuildDynamicDataDsdView(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The codelist table doesnt exist, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists(
                $"CL_{dsd.Dimensions.First(x=>!x.Base.TimeDimension).Codelist.DbId}",
                cancellationToken)
            )
                return;

            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            var observationAttributes = dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                               (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                               attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToArray();

            // SELECT ----------------------------------------------------------------

            var selectStatement = new StringBuilder();
            var whereStatement = new StringBuilder();

            //obs value
            selectStatement.Append($"[FA].[VALUE] AS [{dsd.Base.PrimaryMeasure.Id}]");
            whereStatement.Append("WHERE [FA].[SID] IS NOT NULL ");

            var timeDim = dsd.TimeDimension;

            //time dimension
            if (timeDim!=null)
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(",",
                        $"[FA].{timeDim.SqlColumn()} AS [{dsd.Base.TimeDimension.Id}]",
                        DbExtensions.SqlPeriodStart(),
                        DbExtensions.SqlPeriodEnd()
                     ));
            }

            //dimensions
            selectStatement
                .Append(",")
                .Append(string.Join(", ", dimensions.Select(d =>
                d.Base.HasCodedRepresentation() ? $"[CL_{d.Code}].[ID] AS [{d.Code}]" : $"[FI].{d.SqlColumn()} AS [{d.Code}]"
            )));

            //obs lvl attributes
            if (observationAttributes.Any())
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(", ", observationAttributes.Select(a =>
                    a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[FA].{a.SqlColumn()} AS [{a.Code}]"
                )));
            }

            // LAST_UPDATED + LAST_STATUS columns
            selectStatement.Append($",[FA].{DbExtensions.LAST_UPDATED_COLUMN},[FA].{DbExtensions.LAST_STATUS_COLUMN}");

            // JOIN ------------------------------------------------------------------

            var joinStatement = new StringBuilder();

            //dimensions
            joinStatement.AppendLine(string.Join("\n", dimensions.Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON FI.{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                )
            ));

            //obs lvl attributes
            joinStatement.AppendLine(string.Join("\n", observationAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                     $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON FA.{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"
                )
            ));


            // ------------------------------------------------------
            var dimGroupAttributes = dsd.Attributes.Where(attr =>
                    (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                    !attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToArray();

            if (dimGroupAttributes.Any())
            {
                selectStatement.Append(",");
                selectStatement.Append(string.Join(", ", dimGroupAttributes.Select(a =>
                    a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[ATTR].{a.SqlColumn()} AS [{a.Code}]"
                )));

                joinStatement.AppendLine($"LEFT JOIN [{dotStatDb.DataSchema}].{dsd.SqlDimGroupAttrTable(targetVersion, "ATTR")} ON FI.[SID] = ATTR.[SID] ");

                joinStatement.AppendLine(string.Join("\n", dimGroupAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                        $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON ATTR.{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"
                    )
                ));

                whereStatement.Append("OR ATTR.[SID] IS NOT NULL");
            }

            // ------------------------------------------------------

            var sqlCommand =
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dsd.SqlDataDsdViewName(targetVersion)} 
                   AS
                        SELECT FI.[SID], {selectStatement}
                        FROM [{dotStatDb.DataSchema}].{dsd.SqlFilterTable("FI")} 
                        LEFT JOIN [{dotStatDb.DataSchema}].{dsd.SqlFactTable(targetVersion, "FA")} ON FI.[SID] = FA.[SID] 
                        {joinStatement}
                        {whereStatement}
                ";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        

    }
}
