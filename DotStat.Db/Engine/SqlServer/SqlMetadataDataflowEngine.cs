﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlMetadataDataflowEngine : MetadataDataflowEngineBase<SqlDotStatDb>
    {

        public SqlMetadataDataflowEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override Task<int> InsertToArtefactTable(Dataflow artefact, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        protected override async Task BuildDynamicMetadataDataFlowView(Dataflow dataFlow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The dependent view does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.ViewExists($"{dataFlow.Dsd.SqlMetaDataDsdViewName(targetVersion)}", cancellationToken))
                return;

            //The view already exists, therefore no need to recreate it.
            if (await dotStatDb.ViewExists($"{dataFlow.SqlMetadataDataFlowViewName(targetVersion)}", cancellationToken))
                return;

            var dimensions = dataFlow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            // SELECT ----------------------------------------------------------------

            var selectStatement = new StringBuilder();
            var dfSelectStatement = new StringBuilder();

            //dimensions
            selectStatement
                .Append(string.Join(", ", dimensions.Select(d => $"[{d.Code}]")));

            dfSelectStatement
                .Append(string.Join(", ", dimensions.Select(d => $"NULL AS [{d.Code}]")));

            //time dimension
            var timeDim = dataFlow.Dsd.TimeDimension;
            var (sqlMin, sqlMax) = dataFlow.Dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MinValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.ToString("yyyy-MM-dd"));

            if (timeDim != null)
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(", ",
                        $"[{dataFlow.Dsd.Base.TimeDimension.Id}]",
                        $"[{DbExtensions.SqlPeriodStart()}]",
                        $"[{DbExtensions.SqlPeriodEnd()}]"
                    ));

                dfSelectStatement
                    .Append(",")
                    .Append(string.Join(", ",
                        $"NULL AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                        $"'{sqlMax}' AS [{DbExtensions.SqlPeriodStart()}]",
                        $"'{sqlMin}' AS [{DbExtensions.SqlPeriodEnd()}]"
                    ));
            }

            //metadata attributes
            if (dataFlow.Dsd.Msd.MetadataAttributes.Any())
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(", ", dataFlow.Dsd.Msd.MetadataAttributes.Select(a =>$"[{a.HierarchicalId}]")));

                dfSelectStatement
                    .Append(",")
                    .Append(string.Join(", ", dataFlow.Dsd.Msd.MetadataAttributes.Select(a =>
                        a.Base.HasCodedRepresentation()
                            ? $"[CL_{a.HierarchicalId}].[ID] AS [{a.HierarchicalId}]"
                            : $"[ME].{a.SqlColumn()} AS [{a.HierarchicalId}]"
                    )));
            }

            // JOIN ------------------------------------------------------------------
            var joinStatement = new StringBuilder();
            
            //metadata attributes
            joinStatement.AppendLine(string.Join("\n", dataFlow.Dsd.Msd.MetadataAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                     $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.HierarchicalId}] ON [ME].{a.SqlColumn()} = [CL_{a.HierarchicalId}].[ITEM_ID]"
                )
            ));

            var sqlCommand =
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlMetadataDataFlowViewName(targetVersion)} 
                   AS 
                        SELECT {dfSelectStatement}
                        FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlMetadataDsdTable(targetVersion)} ME
                        {joinStatement}
                        WHERE DF_ID = {dataFlow.DbId}
                      UNION ALL 
                        SELECT {selectStatement}
                        FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlMetaDataDsdViewName(targetVersion)} 
                ";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        protected override Task DeleteFromArtefactTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}