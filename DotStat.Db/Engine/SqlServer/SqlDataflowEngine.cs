﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlDataflowEngine : DataflowEngineBase<SqlDotStatDb>
    {

        public SqlDataflowEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var aretefactDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT [ART_ID] 
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ID] = @Id AND [AGENCY] = @Agency AND [TYPE] = @Type
                     AND [VERSION_1] = @Version1 AND [VERSION_2] = @Version2 
                     AND ([VERSION_3] = @Version3 OR ([VERSION_3] IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) {Value = sdmxId},
                new SqlParameter("Agency", SqlDbType.VarChar) {Value = sdmxAgency},                
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dataflow) },
                new SqlParameter("Version1", SqlDbType.Int) {Value = verMajor},
                new SqlParameter("Version2", SqlDbType.Int) {Value = verMinor},
                new SqlParameter("Version3", SqlDbType.Int) {Value = ((object) verPatch) ?? DBNull.Value}
                );

            return (int) (aretefactDbId ?? -1);
        }

        public override async Task<int> InsertToArtefactTable(Dataflow dataflow, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var artefactId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"INSERT 
                    INTO [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                         ([SQL_ART_ID],[TYPE],[ID],[AGENCY],[VERSION_1],[VERSION_2],[VERSION_3],[LAST_UPDATED],[DF_DSD_ID],[DF_WHERE_CLAUSE]) 
                  OUTPUT INSERTED.ART_ID 
                  VALUES (NULL, @Type, @Id, @Agency, @Version1, @Version2, @Version3, GETDATE(), @DsdId, NULL)",
                cancellationToken,
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dataflow) },
                new SqlParameter("Id", SqlDbType.VarChar) {Value = dataflow.Code},
                new SqlParameter("Agency", SqlDbType.VarChar) {Value = dataflow.AgencyId},
                new SqlParameter("Version1", SqlDbType.Int) {Value = dataflow.Version.Major},
                new SqlParameter("Version2", SqlDbType.Int) {Value = dataflow.Version.Minor},
                new SqlParameter("Version3", SqlDbType.Int) {Value = (object) dataflow.Version.Patch ?? DBNull.Value},
                new SqlParameter("DsdId", SqlDbType.Int) {Value = dataflow.Dsd.DbId}
                );

            return (int) artefactId;
        }

        protected override async Task DeleteFromArtefactTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ART_ID] = @DbId AND [TYPE] = 'DF'",
                cancellationToken,
                new SqlParameter("DbId", SqlDbType.VarChar) { Value = dbId }
                );
        }

        protected override async Task BuildDynamicDataDataFlowView(Dataflow dataFlow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The dependent view does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.ViewExists($"{dataFlow.Dsd.SqlDataDsdViewName(targetVersion)}", cancellationToken))
            {
                return;
            }

            //Dimensions
            var dimensionsColumns = ", " + string.Join(", ", dataFlow.Dsd.Dimensions.Select(d => $"[{d.Code}]"));

            //Measure
            //TODO extend to multiple measures
            var measureColumn = $", [{dataFlow.Dsd.Base.PrimaryMeasure.Id}]";

            //Attributes
            var attributeColumns = string.Empty;
            var attributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel != AttributeAttachmentLevel.DataSet).ToList();

            if (attributes.Any())
            {
                attributeColumns = ", " + string.Join(", ", attributes.Select(a => $"[{a.Code}]"));
            }

            //Dataset attributes
            var dataSetAttributesColumns = string.Empty;
            var joinDataSetAttributes = string.Empty;
            var joinCodedDataSetAttributes = string.Empty;
            var dataSetAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList();

            if (dataSetAttributes.Any())
            {
                dataSetAttributesColumns = ", " + string.Join(", ", dataSetAttributes.Select(a =>
                    a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[ADF].{a.SqlColumn()} AS [{a.Code}]"));

                joinDataSetAttributes = $@"FULL JOIN [{dotStatDb.DataSchema}].[{dataFlow.Dsd.SqlDsdAttrTable(targetVersion)}] [ADF] ON [ADF].[DF_ID] = {dataFlow.DbId}";

                joinCodedDataSetAttributes = string.Join("\n",
                    dataSetAttributes.Where(a => a.Base.HasCodedRepresentation())
                        .Select(a => 
                            $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON [ADF].[DF_ID] = {dataFlow.DbId} AND [ADF].{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            //Time dimension columns
            var timeDimensionColumns = string.Empty;

            if (dataFlow.Dsd.TimeDimension != null)
            {
                timeDimensionColumns = ", " + string.Join(", ", DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd());
            }

            // LAST_UPDATED + LAST_STATUS columns
            var updatedAfterColumns = $",{DbExtensions.LAST_UPDATED_COLUMN},{DbExtensions.LAST_STATUS_COLUMN}";

            var sqlCommand = 
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlDataDataFlowViewName(targetVersion)} 
                   AS
                       SELECT [SID]{dimensionsColumns}
                            {measureColumn}
                            {attributeColumns}
                            {dataSetAttributesColumns}
                            {timeDimensionColumns}
                            {updatedAfterColumns}
                       FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDataDsdViewName(targetVersion)} AS V 
                       {joinDataSetAttributes}
                       {joinCodedDataSetAttributes}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }
    }
}