﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlMsdEngine : MsdEngineBase<SqlDotStatDb>
    {
        public SqlMsdEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }
        
        public override async Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var aretefactDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT [ART_ID] 
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ID] = @Id AND [AGENCY] = @Agency AND [TYPE] = @Type
                     AND [VERSION_1] = @Version1 AND [VERSION_2] = @Version2 
                     AND ([VERSION_3] = @Version3 OR ([VERSION_3] IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) { Value = sdmxId },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = sdmxAgency },                
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Msd) },
                new SqlParameter("Version1", SqlDbType.Int) { Value = verMajor },
                new SqlParameter("Version2", SqlDbType.Int) { Value = verMinor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = ((object)verPatch) ?? DBNull.Value }

                );

            return (int)(aretefactDbId ?? -1);
        }

        public override async Task<int> InsertToArtefactTable(Msd msd, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var artefactId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"INSERT 
                    INTO [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                       ([SQL_ART_ID],[TYPE],[ID],[AGENCY],[VERSION_1],[VERSION_2],[VERSION_3],[LAST_UPDATED]) 
                  OUTPUT INSERTED.ART_ID 
                  VALUES (NULL, @Type, @Id, @Agency, @Version1, @Version2, @Version3, GETDATE())",
                cancellationToken,
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Msd) },
                new SqlParameter("Id", SqlDbType.VarChar) { Value = msd.Code },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = msd.AgencyId },
                new SqlParameter("Version1", SqlDbType.Int) { Value = msd.Version.Major },
                new SqlParameter("Version2", SqlDbType.Int) { Value = msd.Version.Minor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = (object)msd.Version.Patch ?? DBNull.Value }
                );

            return (int)artefactId;
        }

        public override Task CreateDynamicDbObjects(Msd artefact, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        protected override async Task DeleteFromArtefactTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ART_ID] = @DbId AND [TYPE] = @Type",
                cancellationToken,
                new SqlParameter("DbId", SqlDbType.VarChar) { Value = dbId },
                        new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Msd) }
                );
        }
        
        protected override async Task BuildDynamicMetaDataTable(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            //Change to allowNull=true when non coded dimensions are introduced. 
            //A value of 0 should be given when the dimension is not being referenced
            var dimensionColumnsWithType = dimensions.ToColumnList(true, null, false);
            var dimensionColumns = dimensions.ToColumnList();
            var table = dsd.SqlMetadataTable(targetVersion);
            
            var timeDim = dsd.TimeDimension;
            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    // NULL is stored when time is not referenced by the metadata.
                    new[] { timeDim }.ToColumnList(true, null, true),
                    //The maximum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodStart(true, supportsDateTime, false),
                    //The minimum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodEnd(true, supportsDateTime, false)
                ) + ","
                : null;

            var timeConstraint =
                timeDim != null ? $", [{DbExtensions.SqlPeriodStart()}], [{DbExtensions.SqlPeriodEnd()}] DESC" : null;
            
            string rowId = null;
            var uIndex = dimensionColumns + timeConstraint;

            //Time dimension requires two columns (period_start and period_end)
            if (dsd.Dimensions.Count > (timeDim != null ? 30: 32))
            {
                rowId = $"[ROW_ID] AS ({dsd.SqlBuildRowIdFormula(true)}) PERSISTED NOT NULL,";
                uIndex = "[ROW_ID]";
            }
            
            var metaAttributeColumns = dsd.Msd.MetadataAttributes
                .ToColumnList(true);

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }
            
            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{table}](
                        {rowId}
	                    {dimensionColumnsWithType},
                        {timeDimensionColumns}
                        {metaAttributeColumns}
                        CONSTRAINT [PK_{table}] PRIMARY KEY CLUSTERED ({uIndex})
                )";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        protected override async Task BuildDynamicDatasetMetaDataTable(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var metaAttributeColumns = dsd.Msd.MetadataAttributes
                .ToColumnList(true);

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }

            var table = dsd.SqlMetadataDsdTable(targetVersion);
            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{table}](
                        DF_ID INT NOT NULL,
                        {metaAttributeColumns}
                        CONSTRAINT [PK_{table}] PRIMARY KEY CLUSTERED (DF_ID)
                )";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        protected override async Task BuildDynamicMetaDataDsdView(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The codelist table doesnt exist, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"CL_{dsd.Dimensions.First(x => !x.Base.TimeDimension).Codelist.DbId}", cancellationToken))
                return;

            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();
            
            // SELECT ----------------------------------------------------------------

            var selectStatement = new StringBuilder();
            
            //dimensions
            selectStatement
                .Append(string.Join(", ", dimensions.Select(d =>
                d.Base.HasCodedRepresentation() ? $"[CL_{d.Code}].[ID] AS [{d.Code}]" : $"[ME].{d.SqlColumn()} AS [{d.Code}]"
            )));

            //time dimension
            var timeDim = dsd.TimeDimension;
            if (timeDim != null)
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(",",
                        $"[ME].{timeDim.SqlColumn()} AS [{dsd.Base.TimeDimension.Id}]",
                        DbExtensions.SqlPeriodStart(),
                        DbExtensions.SqlPeriodEnd()
                    ));
            }

            //metadata attributes
            if (dsd.Msd.MetadataAttributes.Any())
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(", ", dsd.Msd.MetadataAttributes.Select(a =>
                    a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.HierarchicalId}].[ID] AS [{a.HierarchicalId}]"
                        : $"[ME].{a.SqlColumn()} AS [{a.HierarchicalId}]"
                )));
            }

            // JOIN ------------------------------------------------------------------
            var joinStatement = new StringBuilder();

            //dimensions
            joinStatement.AppendLine(string.Join("\n", dimensions.Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [ME].{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                )
            ));

            //metadata attributes
            joinStatement.AppendLine(string.Join("\n", dsd.Msd.MetadataAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                     $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.HierarchicalId}] ON [ME].{a.SqlColumn()} = [CL_{a.HierarchicalId}].[ITEM_ID]"
                )
            ));
            
            var sqlCommand =
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dsd.SqlMetaDataDsdViewName(targetVersion)} 
                   AS
                        SELECT {selectStatement}
                        FROM [{dotStatDb.DataSchema}].[{dsd.SqlMetadataTable(targetVersion)}] AS [ME]
                        {joinStatement}
                ";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }


    }
}
