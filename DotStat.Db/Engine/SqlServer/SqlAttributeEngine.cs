﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlAttributeEngine : AttributeEngineBase<SqlDotStatDb>
    {
        public SqlAttributeEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<int> GetDbId(string sdmxId, int dsdId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var componentDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
            $@"SELECT [COMP_ID] 
                FROM [{dotStatDb.ManagementSchema}].[COMPONENT] 
               WHERE [ID] = @Id AND [DSD_ID] = @DsdId AND [TYPE] = @Type",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) { Value = sdmxId },
                new SqlParameter("DsdId", SqlDbType.Int) { Value = dsdId },                
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.DataAttribute) }
            );

            return (int)(componentDbId ?? -1);
        }

        public override async Task<int> InsertToComponentTable(Attribute attribute, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //TODO: - attribute type if not coded - currently fixed for string (ENUM_ID = 1)
            var componentId = (int) await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"INSERT 
                    INTO [{dotStatDb.ManagementSchema}].[COMPONENT] 
                         ([ID],[TYPE],[DSD_ID],[CL_ID],[ATT_ASS_LEVEL],[ATT_STATUS],[ENUM_ID],[ATT_GROUP_ID])
                  OUTPUT Inserted.COMP_ID
                  VALUES (@Id, 'Attribute', @DsdId, @ClId, @AttachmentLevel, @Status, @EnumId, @GroupId)",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) {Value = attribute.Code},
                new SqlParameter("DsdId", SqlDbType.Int) {Value = attribute.Dsd.DbId},
                new SqlParameter("ClId", SqlDbType.Int)
                {
                    Value = attribute.Base.HasCodedRepresentation() ? (object) attribute.Codelist.DbId : DBNull.Value
                },
                new SqlParameter("AttachmentLevel", SqlDbType.VarChar) {Value = attribute.Base.AttachmentLevel.ToString()},
                new SqlParameter("Status", SqlDbType.VarChar)
                {
                    Value = (attribute.Base.Mandatory ? AttributeAssignmentStatus.Mandatory : AttributeAssignmentStatus.Conditional)
                },
                new SqlParameter("EnumId", SqlDbType.BigInt) {Value = attribute.Base.HasCodedRepresentation() ? DBNull.Value : (object) (1)},
                new SqlParameter("GroupId", SqlDbType.VarChar)
                {
                    Value = (string.IsNullOrWhiteSpace(attribute.Base.AttachmentGroup)
                        ? DBNull.Value
                        : (object) attribute.Base.AttachmentGroup)
                }
            );

            return componentId;
        }

        protected override async Task DeleteFromComponentTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM [{dotStatDb.ManagementSchema}].[COMPONENT] 
                   WHERE [COMP_ID] = @DbId AND [TYPE] = 'Attribute'",
                    cancellationToken,
                    new SqlParameter("DbId", SqlDbType.VarChar) { Value = dbId }
                    );
        }

        protected override async Task BuildDynamicDatasetAttributeTable(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var attributeColumns = dsd.Attributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength,
                new[] {AttributeAttachmentLevel.DataSet}, withType: true, applyNotNull: false);

            if (!string.IsNullOrWhiteSpace(attributeColumns))
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE TABLE [{dotStatDb.DataSchema}].[ATTR_{dsd.DbId}_{targetVersion}_DF]( 
                        [DF_ID] [int] NOT NULL, 
                        {attributeColumns} 
                        )",
                    cancellationToken
                );
            }
        }

        protected override async Task BuildDynamicDimensionGroupAttributeTable(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var attributeColumns = dsd.Attributes
                .Where(x => !x.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToColumnList(
                    GeneralConfiguration.MaxTextAttributeLength, 
                    new[]
                    {
                        AttributeAttachmentLevel.DimensionGroup, 
                        AttributeAttachmentLevel.Group
                    }, 
                    withType: true, 
                    applyNotNull: false
                );

            if (string.IsNullOrWhiteSpace(attributeColumns))
                return;

            var sqlTable = dsd.SqlDimGroupAttrTable(targetVersion);

            await dotStatDb.ExecuteNonQuerySqlAsync($@"CREATE TABLE [{dotStatDb.DataSchema}].[{sqlTable}] (
                SID int not null,
                {attributeColumns},
                CONSTRAINT [PK_{sqlTable}] PRIMARY KEY (SID)
            )", cancellationToken);
        }

        protected override async Task AlterTextAttributeColumnsInDatasetAttributeTable(Dsd dsd, IList<Attribute> attributes,
            int newMaxAttributeLength, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            foreach (var attribute in attributes)
            {
                var sqlCommand =
                    $@"ALTER TABLE [{dotStatDb.DataSchema}].[ATTR_{dsd.DbId}_{targetVersion.ToString()}_DF] ALTER COLUMN {attribute.SqlColumn()} NVARCHAR({(newMaxAttributeLength == 0 ? "MAX" : newMaxAttributeLength.ToString())})";

                await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            }
        }

        protected override async Task AlterTextAttributeColumnsInDimensionGroupAttributeTable(
            Dsd dsd, 
            IList<Attribute> attributes,
            int newMaxAttributeLength, 
            char targetVersion, 
            SqlDotStatDb dotStatDb, 
            CancellationToken cancellationToken
        )
        {
            foreach (var attribute in attributes)
            {
                var sqlCommand =
                    $@"ALTER TABLE [{dotStatDb.DataSchema}].[{dsd.SqlDimGroupAttrTable(targetVersion)}] ALTER COLUMN {attribute.SqlColumn()} NVARCHAR({(newMaxAttributeLength == 0 ? "MAX" : newMaxAttributeLength.ToString())})";

                await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            }
        }
    }
}
