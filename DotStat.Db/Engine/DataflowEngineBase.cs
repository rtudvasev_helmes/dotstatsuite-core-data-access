﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Db.Util;
using DotStat.Domain;

namespace DotStat.Db.Engine
{
    public abstract class DataflowEngineBase<TDotStatDb> : ArtefactEngineBase<Dataflow, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected DataflowEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task CreateDynamicDbObjects(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Set A
            await BuildDynamicDataDataFlowView(dataflow, (char) DbTableVersion.A, dotStatDb, cancellationToken);
            
            //Set B
            await BuildDynamicDataDataFlowView(dataflow, (char) DbTableVersion.B, dotStatDb, cancellationToken);
        }

        public override async Task CleanUp(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (dataflow.DbId > 0)
            {
                await DeleteFromArtefactTableByDbId(dataflow.DbId, dotStatDb, cancellationToken);
            }
        }

        protected abstract Task BuildDynamicDataDataFlowView(Dataflow dataFlow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);
    }
}