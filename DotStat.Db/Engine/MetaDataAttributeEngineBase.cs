﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Engine
{
    public abstract class MetadataAttributeEngineBase<TDotStatDb> : ComponentEngineBase<MetadataAttribute, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {

        protected MetadataAttributeEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }
        
        public override async Task CleanUp(MetadataAttribute component, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Does not delete nor changes structure of related attribute table!

            if (component == null)
            {
                throw new ArgumentNullException(nameof(component));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (component.DbId < 1)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MetadataAttributeInvalidDbId),
                    component.Code, component.DbId)
                );
            }

            await DeleteFromComponentTableByDbId(component.DbId, dotStatDb, cancellationToken);
        }
        
    }
}
