﻿using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Db.Util;
using DotStat.Domain;

namespace DotStat.Db.Engine
{
    public abstract class MetadataDataflowEngineBase<TDotStatDb> : ArtefactEngineBase<Dataflow, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected MetadataDataflowEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task CreateDynamicDbObjects(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Set A
            await BuildDynamicMetadataDataFlowView(dataflow, (char) DbTableVersion.A, dotStatDb, cancellationToken);

            //Set B
            await BuildDynamicMetadataDataFlowView(dataflow, (char) DbTableVersion.B, dotStatDb, cancellationToken);
        }

        public override Task CleanUp(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        protected abstract Task BuildDynamicMetadataDataFlowView(Dataflow dataFlow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);
    }
}