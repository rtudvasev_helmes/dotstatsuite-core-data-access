﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Engine
{

    public abstract class DsdEngineBase<TDotStatDb> : ArtefactEngineBase<Dsd, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected DsdEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }
        
        public override async Task CreateDynamicDbObjects(Dsd dsd, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dsd == null)
            {
                throw new ArgumentNullException(nameof(dsd));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            await BuildDynamicFactTable(dsd, (char)DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDynamicFactTable(dsd, (char)DbTableVersion.B, dotStatDb, cancellationToken);
            await BuildDynamicFilterTable(dsd, dotStatDb, cancellationToken);

            await BuildDynamicDataDsdView(dsd, (char) DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDynamicDataDsdView(dsd, (char) DbTableVersion.B, dotStatDb, cancellationToken);
        }

        public override Task CleanUp(Dsd dsd, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public async Task AlterTextAttributeColumns(Dsd dsd, int newMaxAttributeLength, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var affectedAttributes = dsd.Attributes.Where(a =>
                    !a.Base.HasCodedRepresentation() &&
                    (a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                    (a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || a.Base.AttachmentLevel == AttributeAttachmentLevel.Group) && a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)))
                .ToList();

            // Alter tables only if there are affected attributes
            if (affectedAttributes.Any())
            {
                await AlterTextAttributeColumnsInFactTable(dsd, affectedAttributes, newMaxAttributeLength, (char) DbTableVersion.A, dotStatDb, cancellationToken);
                await AlterTextAttributeColumnsInFactTable(dsd, affectedAttributes, newMaxAttributeLength, (char) DbTableVersion.B, dotStatDb, cancellationToken);
            }
        }

        protected abstract Task BuildDynamicFactTable(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildDynamicFilterTable(Dsd dsd, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task AlterTextAttributeColumnsInFactTable(Dsd dsd, IList<Attribute> attributes, int newMaxAttributeLength, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildDynamicDataDsdView(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

    }
}