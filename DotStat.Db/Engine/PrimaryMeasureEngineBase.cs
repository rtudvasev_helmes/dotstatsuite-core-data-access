﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.DB;
using DotStat.Db.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Engine
{
    public abstract class PrimaryMeasureEngineBase<TDotStatDb> : ComponentEngineBase<PrimaryMeasure, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected PrimaryMeasureEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task CleanUp(PrimaryMeasure component, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Does not delete nor changes structure of related fact table!

            if (component == null)
            {
                throw new ArgumentNullException(nameof(component));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (component.DbId < 1)
            {
                throw new DotStatException(string.Format( 
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MeasureInvalidDbId), 
                        component.Code, component.DbId)
                );
            }

            await DeleteFromComponentTableByDbId(component.DbId, dotStatDb, cancellationToken);
        }
    }
}
