﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Engine
{
    public abstract class CodelistEngineBase<TDotStatDb> : ArtefactEngineBase<Codelist, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected CodelistEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task CreateDynamicDbObjects(Codelist codelist, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (codelist == null)
            {
                throw new ArgumentNullException(nameof(codelist));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (codelist.DbId < 1)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CodelistInvalidDbId),
                    codelist.Code, codelist.DbId)
                ); 
            }

            if (!await dotStatDb.TableExists(GetCodelistTableName(codelist), cancellationToken))
            {
                await BuildDynamicCodelistTable(codelist, dotStatDb, cancellationToken);
           
                await FillDynamicCodelistTable(codelist, dotStatDb, cancellationToken);
            }
        }

        public override async Task CleanUp(Codelist codelist, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (codelist == null)
            {
                throw new ArgumentNullException(nameof(codelist));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (codelist.DbId < 1)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CodelistInvalidDbId),
                    codelist.Code, codelist.DbId)
                );
            }

            await DeleteFromArtefactTableByDbId(codelist.DbId, dotStatDb, cancellationToken);

            await dotStatDb.DropTable(dotStatDb.ManagementSchema, GetCodelistTableName(codelist), cancellationToken);
        }

        public async Task InsertCodes(Codelist codelist, IReadOnlyList<string> codes, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await FillDynamicCodelistTable(codelist, dotStatDb, cancellationToken, codes);
        }

        protected abstract string GetCodelistTableName(Codelist codelist);
        protected abstract Task BuildDynamicCodelistTable(Codelist codelist,  TDotStatDb dotStatDb, CancellationToken cancellationToken);
        protected abstract Task FillDynamicCodelistTable(Codelist codelist, TDotStatDb dotStatDb, CancellationToken cancellationToken, IReadOnlyList<string> codesToInsert = null);
    }
}