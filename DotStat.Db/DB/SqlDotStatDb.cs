﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Db.Helpers;


namespace DotStat.Db.DB
{
    public class SqlDotStatDb : DotStatDbBase<SqlConnection>
    {
        private Version _currentDbVersion;
        private new const string ManagementSchema ="management";
        private new const string DataSchema = "data";
        private readonly string _connectionString;
        private readonly string _readOnlyConnectionString;

        public SqlDotStatDb(DataspaceInternal dataspace, Version supportedDbVersion) : 
            base(ManagementSchema, DataSchema, dataspace, supportedDbVersion )
        {
            _connectionString = dataspace.DotStatSuiteCoreDataDbConnectionString;
            //Do not use two different connection strings if readonly replica is not available
            _readOnlyConnectionString = string.IsNullOrEmpty(DataSpace.DotStatSuiteCoreDataDbConnectionStringReadOnlyReplica) ?
                dataspace.DotStatSuiteCoreDataDbConnectionString : DataSpace.DotStatSuiteCoreDataDbConnectionStringReadOnlyReplica;
        }

        public override SqlConnection GetConnection(bool readOnly = false)
        {
            return GetSqlConnection(readOnly);
        }

        public override async Task<string> GetDatabaseVersion(CancellationToken cancellationToken)
        {
            return (await ExecuteScalarSqlAsync("SELECT VERSION FROM dbo.DB_VERSION", cancellationToken)).ToString();
        }

        public override async Task<bool> ViewExists(string viewName, CancellationToken cancellationToken)
        {
            return await ExecuteScalarSqlWithParamsAsync(
                @"SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE UPPER(TABLE_NAME) = @Name",
                cancellationToken,
                new SqlParameter("Name", SqlDbType.VarChar) { Value = viewName.ToUpper() }
            ) != null;
        }
        
        public override async Task<bool> TableExists(string tableName, CancellationToken cancellationToken)
        {
            return await ExecuteScalarSqlWithParamsAsync(
                @"SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE UPPER(TABLE_NAME) = @Name and TABLE_TYPE = 'BASE TABLE'",
                cancellationToken,
                new SqlParameter("Name", SqlDbType.VarChar) { Value = tableName.ToUpper() }
                ) != null;
        }

        public override async Task<bool> ColumnExists(string tableName, string columnName, CancellationToken cancellationToken)
        {
            return (int) await ExecuteScalarSqlWithParamsAsync(
                       "select count(*) noOfColumns from INFORMATION_SCHEMA.COLUMNS where table_name = @TableName AND COLUMN_NAME = @ColumnName",
                       cancellationToken,
                       new SqlParameter("TableName", SqlDbType.VarChar) { Value=tableName },
                       new SqlParameter("ColumnName", SqlDbType.VarChar) { Value=columnName }
                   ) == 1;
        }

        public override async Task DropView(string schema, string viewName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"IF (EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.VIEWS WHERE UPPER(TABLE_NAME) = @Name))
                BEGIN
                    DROP VIEW [{schema}].[{viewName}];
                END";

            await ExecuteNonQuerySqlWithParamsAsync(
                sqlCommand,
                cancellationToken,
                new SqlParameter("Name", SqlDbType.VarChar) { Value = viewName.ToUpper() }
            );
        }

        public override async Task DropTable(string schema, string tableName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"IF (EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES WHERE UPPER(TABLE_NAME) = @Name and TABLE_TYPE = 'BASE TABLE'))
                BEGIN
                    DROP TABLE [{schema}].[{tableName}];
                END";

            await ExecuteNonQuerySqlWithParamsAsync(
                sqlCommand,
                cancellationToken,
                new SqlParameter("Name", SqlDbType.VarChar) {Value = tableName.ToUpper()}
            );
        }

        public override async Task TruncateTable(string schema, string tableName, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"IF (EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES WHERE UPPER(TABLE_NAME) = @Name and TABLE_TYPE = 'BASE TABLE'))
                BEGIN
                    TRUNCATE TABLE [{schema}].[{tableName}];
                END";

            await ExecuteNonQuerySqlWithParamsAsync(
                sqlCommand,
                cancellationToken,
                new SqlParameter("Name", SqlDbType.VarChar) { Value = tableName.ToUpper() }
            );
        }
        
        public override async ValueTask<Version> GetCurrentDbVersion(CancellationToken cancellationToken)
        {
            if (_currentDbVersion == null)
            {

                var version = await GetDatabaseVersion(cancellationToken);

                if (!Version.TryParse(version, out _currentDbVersion))
                {
                    throw new NotSupportedException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DatabaseVersionIsInvalid),
                        Id, version));
                }
            }

            return _currentDbVersion;
        }

        private SqlConnection GetSqlConnection(bool readOnly = false)
        {
            var connString = new SqlConnectionStringBuilder(readOnly ? _readOnlyConnectionString : _connectionString)
            {
                ApplicationName = DbHelper.DbConnectionAppName
            };

            var conn = new SqlConnection(connString.ConnectionString);            
            conn.Open();

            if (System.Transactions.Transaction.Current == null)
            {
                using var cmd = conn.CreateCommand();
                cmd.CommandText = "SET TRANSACTION ISOLATION LEVEL READ COMMITTED;";
                cmd.ExecuteNonQuery();
            }

            return conn;
        }
    }
}
