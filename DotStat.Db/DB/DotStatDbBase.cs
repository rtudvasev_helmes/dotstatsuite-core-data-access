﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Logger;
using DotStat.DB;

namespace DotStat.Db.DB
{
    public delegate IDotStatDb DotStatDbResolver(string dataSpace);

    public abstract class DotStatDbBase<TConnection> : IDotStatDb
        where TConnection : DbConnection
    {
        public string Id { get; set; }

        public virtual string ManagementSchema { get; protected set; }

        public virtual string DataSchema { get; protected set; }

        public int DatabaseCommandTimeout { get; protected set; }

        public DataspaceInternal DataSpace { get; protected set; }

        private readonly Version _supportedDbVersion;
        protected DotStatDbBase(
            string managementSchema, 
            string dataSchema, 
            DataspaceInternal dataSpaceConfiguration,
            Version supportedDbVersion
        )
        {
            if (string.IsNullOrWhiteSpace(managementSchema))
            {
                throw new ArgumentNullException(nameof(managementSchema));
            }

            if (string.IsNullOrWhiteSpace(dataSchema))
            {
                throw new ArgumentNullException(nameof(dataSchema));
            }

            if (dataSpaceConfiguration == null)
            {
                throw new ArgumentNullException(nameof(dataSpaceConfiguration));
            }

            if (string.IsNullOrEmpty(dataSpaceConfiguration.DotStatSuiteCoreDataDbConnectionString))
            {
                throw new ArgumentNullException(nameof(dataSpaceConfiguration.DotStatSuiteCoreDataDbConnectionString));
            }

            if (string.IsNullOrWhiteSpace(dataSpaceConfiguration.Id))
            {
                throw new ArgumentNullException(nameof(dataSpaceConfiguration.Id));
            }

            ManagementSchema = managementSchema;
            DataSchema = dataSchema;
            DataSpace = dataSpaceConfiguration;
            Id = dataSpaceConfiguration.Id.ToUpper();
            DatabaseCommandTimeout = dataSpaceConfiguration.DatabaseCommandTimeoutInSec;
            _supportedDbVersion = supportedDbVersion ?? SupportedDatabaseVersion.DataDbVersion;
        }

        public abstract TConnection GetConnection(bool readOnly = false);

        public virtual DbConnection GetDbConnection(bool readOnly = false)
        {
            return GetConnection(readOnly);
        }

        public abstract Task<string> GetDatabaseVersion(CancellationToken cancellationToken);

        public virtual async Task<int> ExecuteNonQuerySqlAsync(string command, CancellationToken cancellationToken)
        {
            return await ExecuteNonQuerySqlWithParamsAsync(command, cancellationToken);
        }
        public virtual async Task<int> ExecuteNonQuerySqlWithParamsAsync(string command, CancellationToken cancellationToken, params DbParameter[] parameters)
        {
            if (string.IsNullOrWhiteSpace(command))
            {
                throw new ArgumentNullException(nameof(command));
            }

            await using (var con = GetConnection())
            {
                await using (var cmd = con.CreateCommand()) 
                { 
                    cmd.CommandText = command;

                    if (parameters != null)
                    {
                        cmd.Parameters.AddRange(parameters);
                    }

                    cmd.CommandTimeout = DatabaseCommandTimeout;
                    return await cmd.ExecuteNonQueryAsync(cancellationToken);
                }
            }
        }
        
        public virtual async Task<object> ExecuteScalarSqlAsync(string command, CancellationToken cancellationToken)
        {
            return await ExecuteScalarSqlWithParamsAsync(command, cancellationToken);
        }
        
        public virtual async Task<object> ExecuteScalarSqlWithParamsAsync(string command, CancellationToken cancellationToken, params DbParameter[] parameters)
        {
            if (string.IsNullOrWhiteSpace(command))
            {
                throw new ArgumentNullException(nameof(command));
            }

            LogSqlQuery(command, parameters);

            await using (var con = GetConnection())
            {
                await using (var cmd = con.CreateCommand()) { 
                    cmd.CommandText = command;
                    cmd.Parameters.AddRange(parameters);
                    cmd.CommandTimeout = DatabaseCommandTimeout;

                    return await cmd.ExecuteScalarAsync(cancellationToken);
                }
            }
        }
        
        public virtual async Task<DbDataReader> ExecuteReaderSqlAsync(string command, CancellationToken cancellationToken, CommandBehavior commandBehavior = CommandBehavior.CloseConnection, bool tryUseReadOnlyConnection = false)
        {
            return await ExecuteReaderSqlWithParamsAsync(command, cancellationToken, commandBehavior, tryUseReadOnlyConnection);
        }
        
        public virtual async Task<DbDataReader> ExecuteReaderSqlWithParamsAsync(string command, CancellationToken cancellationToken, CommandBehavior commandBehavior = CommandBehavior.CloseConnection, bool tryUseReadOnlyConnection = false, params DbParameter[] parameters)
        {
            if (string.IsNullOrWhiteSpace(command))
            {
                throw new ArgumentNullException(nameof(command));
            }

            LogSqlQuery(command, parameters);

            await using (var cmd = GetConnection(tryUseReadOnlyConnection).CreateCommand())
            {
                cmd.CommandText = command;
                cmd.Parameters.AddRange(parameters);
                cmd.CommandTimeout = DatabaseCommandTimeout;

                return await cmd.ExecuteReaderAsync(commandBehavior | CommandBehavior.CloseConnection, cancellationToken);
            }
        }

        public abstract Task<bool> ViewExists(string viewName, CancellationToken cancellationToken);

        public abstract Task<bool> TableExists(string tableName, CancellationToken cancellationToken);

        public abstract Task<bool> ColumnExists(string tableName, string columnName, CancellationToken cancellationToken);
        
        public abstract Task DropView(string schema, string viewName, CancellationToken cancellationToken);

        public abstract Task DropTable(string schema, string tableName, CancellationToken cancellationToken);

        public abstract Task TruncateTable(string schema, string tableName, CancellationToken cancellationToken);
        
        private void LogSqlQuery(string sql, params DbParameter[] parameters)
        {
            var sb = new StringBuilder($"SQL: {sql}");

            if (parameters != null && parameters.Any())
            {
                sb.Append(" (");

                foreach (var p in parameters)
                    sb.Append(p.ParameterName + "=" + p.Value).Append(",");

                sb.Length--;
                sb.Append(")");
            }
            
            Log.Debug(sb.ToString());
        }

        public abstract ValueTask<Version> GetCurrentDbVersion(CancellationToken cancellationToken);
        public Version GetSupportedDbVersion()
        {
            return _supportedDbVersion;
        }

    }
}
