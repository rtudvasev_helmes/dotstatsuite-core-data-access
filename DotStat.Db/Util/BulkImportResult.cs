﻿using DotStat.Db.Validation;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System.Collections.Generic;

namespace DotStat.DB.Util
{
    public class BulkImportResult
    {
        public IList<DataSetAttributeRow> DataSetLevelAttributeRows;
        public List<IValidationError> Errors;
        public int RowsCopied;
        public int NumberOfRowsToDelete;
        public int NumberOfRowsToMerge;
        public int NumberOfRowsToSkip;

        public BulkImportResult()
        {
        }

        public BulkImportResult(IList<DataSetAttributeRow> dataSetLevelAttributeRows, List<IValidationError> errors, int rowsCopied, int numberOfRowsToDelete, int numberOfRowsToMerge, int numberOfRowsToSkip)
        {
            DataSetLevelAttributeRows = dataSetLevelAttributeRows;
            Errors = errors;
            RowsCopied = rowsCopied;
            NumberOfRowsToDelete = numberOfRowsToDelete;
            NumberOfRowsToMerge = numberOfRowsToMerge;
            NumberOfRowsToSkip = numberOfRowsToSkip;
        }

    }
}
