﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Db;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.DB.Util
{
    public class DimMeta
    {
        public int Index { get; }
        public Dimension Dimension { get; }

        public DimMeta(int index, Dimension dimension)
        {
            Index = index;
            Dimension = dimension;
        }
    }

    public class AttrMeta
    {
        public int Index { get; }
        public Attribute Attribute { get; }

        public AttrMeta(int index, Attribute attribute)
        {
            Index = index;
            Attribute = attribute;
        }
    }
    public class MetaAttrMeta
    {
        public int Index { get; }
        public MetadataAttribute MetadataAttribute { get; }

        public MetaAttrMeta(int index, MetadataAttribute metadataAttribute)
        {
            Index = index;
            MetadataAttribute = metadataAttribute;
        }
    }

    public class DataflowMetadataBuilder
    {
        public readonly Dataflow Dataflow;
        public readonly int DimensionCount;
        public readonly int NonTimeDimensionCount;
        public readonly int NonDatasetAttributeCount;
        public readonly int MetadataAttributeCount;
        public Dictionary<string, List<string>> DimensionsReferenceGroups { get; }

        public readonly bool HasTime;
        public readonly Dimension TimeDim;
        public readonly bool HasReportingYearStartDayAttr;
        private readonly Dictionary<string, DimMeta> _dimCodeToIndexMap;
        private readonly Dictionary<string, DimMeta> _nonReportedDimCodeToIndexMap;
        private readonly Dictionary<string, AttrMeta> _attrCodeToIndexMap;
        private readonly Dictionary<string, MetaAttrMeta> _metaAttrCodeToIndexMap;

        public DataflowMetadataBuilder(Dataflow dataflow, ReportedComponents reportedComponents)
        {
            _dimCodeToIndexMap = new Dictionary<string, DimMeta>();
            _nonReportedDimCodeToIndexMap = new Dictionary<string, DimMeta>();
            _attrCodeToIndexMap = new Dictionary<string, AttrMeta>();
            _metaAttrCodeToIndexMap = new Dictionary<string, MetaAttrMeta>();

            Dataflow = dataflow;
            HasTime = false;
            DimensionCount = 0;
            NonDatasetAttributeCount = 0;
            MetadataAttributeCount = 0;

            foreach (var dim in dataflow.Dsd.Dimensions.OrderTimeFirst())
            {
                if (reportedComponents.Dimensions.Any(r => r.Code.Equals(dim.Code, StringComparison.InvariantCultureIgnoreCase)))
                {
                    _dimCodeToIndexMap.Add(dim.Code, new DimMeta(DimensionCount++, dim));
                }
                else
                {
                    _nonReportedDimCodeToIndexMap.Add(dim.Code, new DimMeta(DimensionCount++, dim));
                }

                if (dim.Base.TimeDimension)
                {
                    TimeDim = dim;
                    HasTime = true;
                }
            }

            var allReportedAttributes = reportedComponents.DatasetAttributes
                .Concat(reportedComponents.SeriesAttributes)
                .Concat(reportedComponents.ObservationAttributes);

            foreach (var attr in dataflow.Dsd.Attributes.Where(a =>
                         allReportedAttributes.Any(r => r.Code.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase))))
            {
                if (attr.Code == AttributeObject.Repyearstart)
                {
                    HasReportingYearStartDayAttr = true;
                }

                if (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet)
                {
                    _attrCodeToIndexMap.Add(attr.Code, new AttrMeta(-1, attr));
                }
                else
                {
                    _attrCodeToIndexMap.Add(attr.Code, new AttrMeta(
                        NonDatasetAttributeCount++,
                        attr
                    ));
                }
            }

            if (dataflow.Dsd.Msd != null && reportedComponents.MetadataAttributes != null)
            {
                foreach (var metaAttr in dataflow.Dsd.Msd.MetadataAttributes.Where(a =>
                    reportedComponents.MetadataAttributes.Any(r =>
                        r.HierarchicalId.Equals(a.HierarchicalId, StringComparison.InvariantCultureIgnoreCase))))
                {
                    if (metaAttr.HierarchicalId == AttributeObject.Repyearstart)
                    {
                        HasReportingYearStartDayAttr = true;
                    }

                    _metaAttrCodeToIndexMap.Add(metaAttr.HierarchicalId, new MetaAttrMeta(
                        MetadataAttributeCount++,
                        metaAttr
                    ));
                }
            }

            NonTimeDimensionCount = DimensionCount - (HasTime ? 1 : 0);

            DimensionsReferenceGroups = new Dictionary<string, List<string>>();
            if(reportedComponents.IsPrimaryMeasureReported)
                DimensionsReferenceGroups.Add(dataflow.Dsd.Base.PrimaryMeasure.Id, dataflow.Dsd.Dimensions.Select(d => d.Code).ToList());

            foreach (var attribute in allReportedAttributes)
            {
                switch (attribute.Base.AttachmentLevel)
                {
                    //Observation level
                    case AttributeAttachmentLevel.Observation:
                    {
                        var dimensionsReferenced = dataflow.Dsd.Dimensions.Select(d=>d.Code).ToList();
                        DimensionsReferenceGroups.Add(attribute.Code, dimensionsReferenced);
                        break;
                    }
                    //DataSet Level
                    case AttributeAttachmentLevel.DataSet 
                        or AttributeAttachmentLevel.Null:
                        continue;
                    //All other levels
                    default:
                    {
                        var dimensionsCodeReferenced = attribute.Base.DimensionReferences;
                        var dimensionsReferenced = dataflow.Dsd.Dimensions
                            .Where(dim => dimensionsCodeReferenced.Contains(dim.Code))
                            .Select(d => d.Code).ToList();

                        if (dimensionsReferenced.Count <= 0)
                        {
                            var groupDimRefs = dataflow.Dsd.Base.Groups.Where(g =>
                                    g.Id.Equals(attribute.Base.AttachmentGroup,
                                        StringComparison.InvariantCultureIgnoreCase)).Select(g => g.DimensionRefs)
                                .FirstOrDefault();

                            dimensionsReferenced = groupDimRefs is null ? dataflow.Dsd.Dimensions.Select(d => d.Code).ToList()
                                : dataflow.Dsd.Dimensions
                                    .Where(dim => groupDimRefs.Contains(dim.Code)).Select(d => d.Code).ToList();
                        }

                        DimensionsReferenceGroups.Add(attribute.Code, dimensionsReferenced);
                        break;
                    }
                }
            }
        }

        public DimMeta this[string code] => _dimCodeToIndexMap[code];
        public DimMeta Dim(string code) => _dimCodeToIndexMap[code];
        public DimMeta NonReportedDim(string code) => _nonReportedDimCodeToIndexMap[code];
        public AttrMeta Attr(string code) => _attrCodeToIndexMap[code];
        public MetaAttrMeta MetaAttr(string code) => _metaAttrCodeToIndexMap[code];
        public bool IsValidDim(string code) => _dimCodeToIndexMap.ContainsKey(code);
        public bool IsValidAttr(string code) => _attrCodeToIndexMap.ContainsKey(code);
        public bool IsValidMetaAttr(string code) => _metaAttrCodeToIndexMap.ContainsKey(code);
        public IEnumerable<DimMeta> Dims => _dimCodeToIndexMap.Values.OrderBy(x=>x.Index);
        public IEnumerable<AttrMeta> Attrs => _attrCodeToIndexMap.Values.OrderBy(x=>x.Index);
        public IEnumerable<MetaAttrMeta> MetaAttrs => _metaAttrCodeToIndexMap.Values.OrderBy(x => x.Index);
    }
}