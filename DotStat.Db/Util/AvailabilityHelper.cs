﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.Registry;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Base;

namespace DotStat.Db.Util
{
    public class AvailabilityHelper
    {
        private readonly Dictionary<string, int> _dimensionsInOrder;
        private readonly HashSet<int>[] _availabilityInfo;
        private DateTime? _periodStart;
        private DateTime? _peridEnd;

        public AvailabilityHelper(Dsd dsd)
        {
            if(dsd == null)
                throw new ArgumentNullException(nameof(dsd));

            var nonTimeDims   = dsd.Dimensions.Where(x => !x.Base.TimeDimension).ToArray();
            _dimensionsInOrder = new Dictionary<string, int>(nonTimeDims.Length);
            _availabilityInfo = new HashSet<int>[nonTimeDims.Length];

            var ii = 0;

            foreach (var dim in nonTimeDims)
            {
                _availabilityInfo[ii] = new HashSet<int>();
                _dimensionsInOrder[dim.Code] = ii++;
            }
        }

        public void AddDimKey(int dimIndex, int internalId)
        {
            _availabilityInfo[dimIndex].Add(internalId);
        }

        public void AddTime(DateTime? periodStart, DateTime? periodEnd)
        {
            _periodStart = periodStart;
            _peridEnd = periodEnd;
        }

        public KeyValuesMutableImpl GetAvailabilityByDimension(Dimension dimension, ICodeTranslator codeTranslator)
        {
            if(dimension==null)
                throw new ArgumentException(nameof(dimension));

            if (codeTranslator == null)
                throw new ArgumentException(nameof(dimension));

            var keyValue = new KeyValuesMutableImpl()
            {
                Id = dimension.Code
            };

            if (!dimension.Base.TimeDimension)
            {
                var availableDimMembers = _availabilityInfo[_dimensionsInOrder[dimension.Code]];

                foreach (var id in availableDimMembers)
                    keyValue.AddValue(codeTranslator[dimension.Code][id]);
            }
            else if(_periodStart!=null && _peridEnd!=null)
            {
                keyValue.TimeRange = new TimeRangeMutableCore
                {
                    StartDate = new SdmxDateCore(_periodStart.Value, TimeFormatEnumType.DateTime),
                    IsStartInclusive = true,
                    EndDate = new SdmxDateCore(_peridEnd.Value, TimeFormatEnumType.DateTime),
                    IsEndInclusive = true
                };
            }

            return keyValue;
        }
      
    }
}