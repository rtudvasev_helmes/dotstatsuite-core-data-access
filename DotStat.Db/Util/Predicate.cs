﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

namespace DotStat.Db.Util
{
    public enum NullTreatment
    {
        IncludeNulls,
        ExcludeNulls
    }

    public abstract class Predicate
    {
        public static async Task<string> BuildWhereForDataObservationLevel(
            ICodeTranslator codeTranslator, 
            IDataQuery dataQuery, 
            Dataflow dataflow,
            CancellationToken cancellationToken,
            NullTreatment nullTreatment = NullTreatment.ExcludeNulls
        )
        {
            var where = await BuildDimQuery(codeTranslator, dataQuery, dataflow, nullTreatment, true, cancellationToken);

            return where.Any() 
                ? string.Join(" AND ", where) 
                : null;
        }
        public static async Task<string> BuildWhereForMetadataObservationLevel(
            ICodeTranslator codeTranslator,
            IDataQuery dataQuery,
            Dataflow dataflow,
            CancellationToken cancellationToken,
            NullTreatment nullTreatment = NullTreatment.ExcludeNulls
        )
        {
            var where = await BuildDimQuery(codeTranslator, dataQuery, dataflow, nullTreatment, false, cancellationToken);

            return where.Any()
                ? string.Join(" AND ", where)
                : null;
        }

        private static async Task<List<string>> BuildDimQuery(
            ICodeTranslator codeTranslator,
            IDataQuery dataQuery,
            Dataflow dataflow,
            NullTreatment nullTreatment,
            bool applyConstraints,
            CancellationToken cancellationToken
        )
        {
            var where = new List<string>();
            var appliedConstraints = new HashSet<string>();
            var querySelectionGroup = dataQuery?.SelectionGroups?.FirstOrDefault();

            if (querySelectionGroup != null)
            {
                // non time dimensions
                foreach (var selection in querySelectionGroup.Selections)
                {
                    var filteredSelection = new List<string>(selection.Values.Count);
                    var dim = dataflow.FindDimension(selection.ComponentId);

                    // filter out non-existing codes & constrained codes from user selection
                    foreach (var mem in selection.Values)
                    {
                        if (!dim.HasCode(mem))
                            continue; // Non existing code

                        if (applyConstraints && !dim.IsAllowed(mem))
                            continue; // Constrained code

                        filteredSelection.Add(mem);
                    }

                    if (!filteredSelection.Any())
                        throw new SdmxNoResultsException();

                    //var codeListProjection = await codeTranslator[dim, cancellationToken];
                    var values = new List<int>();
                    foreach (var dmCode in filteredSelection)
                    {
                        var x = await codeTranslator[dim, cancellationToken];
                        values.Add(x.Map[dmCode]);
                    }
                    
                    var predicate = new CodelistPredicate(
                        dim.SqlColumn(),
                        values,
                        nullTreatment
                    );

                    where.Add(predicate.ToString());

                    // As selection already narrowed down by user selection no need to apply dim constraint again
                    appliedConstraints.Add(dim.Code);
                }
            }

            if (!applyConstraints)
                return where;

            // Apply dimension constraints ------------------------------
            foreach (var dim in dataflow.Dimensions.Where(d => !appliedConstraints.Contains(d.Code) && d.Constraint?.IsCodeListBased == true))
            {
                var codeListProjection = await codeTranslator[dim, cancellationToken];
                var values = dim.Constraint.Codes
                    .Where(code => dim.Codelist.Codes.Any(cl_code => cl_code.Code.Contains(code)))
                    .Select(code => codeListProjection.Map[code]);

                var predicate = new CodelistPredicate(
                    dim.SqlColumn(),
                    values,
                    nullTreatment
                );

                where.Add(predicate.ToString());
            }

            // Apply time constraint/query range filter
            AddTimePredicate(where, dataflow, codeTranslator, querySelectionGroup, nullTreatment);

            return where;
        }

        #region Time predicate

        public static void AddTimePredicate(
            List<string> where,
            Dataflow dataflow,
            ICodeTranslator codeTranslator,
            IDataQuerySelectionGroup querySelectionGroup,
            NullTreatment nullTreatment
        )
        {
            var timeDim = dataflow.Dsd.TimeDimension;

            if (timeDim == null)
                return;

            var timeRange = timeDim.Constraint?.TimeRange;

            var startDate = Max(querySelectionGroup?.DateFrom?.Date, timeRange?.StartDate?.Date);
            var endDate = Min(querySelectionGroup?.DateTo?.EndDate(), timeRange?.EndDate?.Date);

            // todo refactor predicate logic to use db parameters

            if (startDate != null)
            {
                where.Add($"{DbExtensions.SqlPeriodEnd()}>='{startDate:yyyy-MM-dd}'");
            }

            if (endDate != null)
            {
                where.Add($"{DbExtensions.SqlPeriodStart()}<'{endDate:yyyy-MM-dd}'");
            }
        }

        private static DateTime? Max(DateTime? dt1, DateTime? dt2) => dt2 == null || dt1 > dt2 ? dt1 : dt2;

        private static DateTime? Min(DateTime? dt1, DateTime? dt2) => dt2 == null || dt1 < dt2 ? dt1 : dt2;

        #endregion

        public abstract string ToString(NullTreatment nullTreatment);
    }
}