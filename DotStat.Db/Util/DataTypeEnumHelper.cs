﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using DotStat.Db.DB;
using Estat.Sri.MappingStore.Store.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace DotStat.Db.Util
{
    public class DataTypeEnumHelper
    {
        private Dictionary<long, string> _dataTypeMap;
        private readonly IDotStatDb _dotStatDb;

        public DataTypeEnumHelper(IDotStatDb dotStatDb, bool tryUseReadOnlyConnection = true)
        {
            _dotStatDb = dotStatDb;

            InitDataTypeMap(tryUseReadOnlyConnection);
        }

        public long? GetIdFromValue(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            return _dataTypeMap.FirstOrDefault(kvp => kvp.Value == value).Key;
        }


        public long? GetIdFromTextFormat(ITextFormat textFormat)
        {
            if (textFormat == null)
            {
                return null;
            }

            return GetIdFromValue(textFormat.TextType?.EnumType.ToString());
        }

        public string GetValueFromId(long value)
        {
            // Estat.Sri.MappingStore.Store.Model.EnumerationValue
            if (_dataTypeMap.TryGetValue(value, out var enumText))
            {
                return enumText;
            }

            return null;
        }

        private void InitDataTypeMap(bool tryUseReadOnlyConnection)
        {
            _dataTypeMap = new Dictionary<long, string>();
            using (var connection = _dotStatDb.GetDbConnection(tryUseReadOnlyConnection))
            {
                foreach (var enumValue in
                    connection.Query<EnumerationValue>(
                        $"SELECT ENUM_ID AS id, ENUM_NAME AS name, ENUM_VALUE AS value FROM [{_dotStatDb.ManagementSchema}].[ENUMERATIONS] WHERE ENUM_NAME='DataType'",
                        null, (IDbTransaction)null, false))
                {
                    _dataTypeMap.Add(enumValue.ID, enumValue.Value);
                }
            }
        }
    }
}