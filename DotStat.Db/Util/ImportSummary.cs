﻿
using System.Collections.Generic;
using DotStat.Db.Validation;

namespace DotStat.DB.Util
{
    public class ImportSummary
    {
        public int ObservationsCount;
        public MergeResult ObservationLevelMergeResult = new();
        public MergeResult SeriesLevelMergeResult = new();
        public MergeResult DataFlowLevelMergeResult = new();
        public List<IValidationError> Errors = new();
    }
}
