﻿using System.Collections.Generic;
using System.Linq;

namespace DotStat.Db.Util
{
    public class CodelistPredicate : Predicate
    {
        private string DimColumn
        {
            get;
        }
        private List<int> Values
        {
            get;
        }
        private NullTreatment NullTreatment
        {
            get;
        }

        public CodelistPredicate(string dimColumn, IEnumerable<int> values, NullTreatment nullTreatment = NullTreatment.ExcludeNulls)
        {
            DimColumn = dimColumn;
            Values = new List<int>(values);
            NullTreatment = nullTreatment;
        }

        public override string ToString()
        {
            return ToString(NullTreatment);
        }

        public override string ToString(NullTreatment nullTreatment)
        {
            var dimensionClause = string.Join(" OR ", Values.Select(id => DimColumn + "=" + id));
            var nullClause = nullTreatment == NullTreatment.IncludeNulls
                ? (string.IsNullOrWhiteSpace(dimensionClause) ? "" : " OR ") + DimColumn + " IS NULL"
                : "";
            return $"({dimensionClause}{nullClause})";
        }
    }
}