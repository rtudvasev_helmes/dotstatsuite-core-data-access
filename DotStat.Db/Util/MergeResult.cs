﻿using DotStat.Db.Validation;
using System.Collections.Generic;

namespace DotStat.DB.Util
{
    public class MergeResult
    {
        public int DeleteCount;
        public int InsertCount;
        public int UpdateCount;
        public int TotalCount;

        public List<IValidationError> Errors;

        public MergeResult()
        {
            Errors = new List<IValidationError>();
        }

        public MergeResult(int deleteCount, int insertCount, int updateCount, List<IValidationError> errors)
        {
            DeleteCount = deleteCount;
            InsertCount = insertCount;
            UpdateCount = updateCount;
            TotalCount = deleteCount + insertCount + updateCount;
            Errors = errors;
        }

    }
}
