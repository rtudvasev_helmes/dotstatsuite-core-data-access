﻿using System;

namespace DotStat.DB
{
    public static class SupportedDatabaseVersion
    {
        public static Version CommonDbVersion = new Version(3, 7);

        public static Version DataDbVersion = new Version(7, 1);

        public static bool IsCommonDbVersionSupported(Version databaseVersion, Version supportedVersion = null, bool exactMachRequired = false)
        {
            return IsDbVersionSupported(databaseVersion, supportedVersion ?? CommonDbVersion, exactMachRequired); ;
        }

        public static bool IsCommonDbVersionSupported(string databaseVersion, string supportedVersion = null,
            bool exactMachRequired = false)
        {
            Version currentSupportedVersion = null;

            if (string.IsNullOrEmpty(databaseVersion) ||
                !Version.TryParse(databaseVersion, out var currentDbVersion) ||
                !string.IsNullOrEmpty(supportedVersion) &&
                !Version.TryParse(supportedVersion, out currentSupportedVersion))
            {
                return false;
            }

            return IsCommonDbVersionSupported(currentDbVersion, currentSupportedVersion, exactMachRequired);
        }

        public static bool IsDataDbVersionSupported(Version databaseVersion, Version supportedVersion = null,
            bool exactMachRequired = false)
        {
            return IsDbVersionSupported(databaseVersion, supportedVersion ?? DataDbVersion, exactMachRequired);
        }

        public static bool IsDataDbVersionSupported(string databaseVersion, string supportedVersion = null,
            bool exactMachRequired = false)
        {
            Version currentSupportedVersion = null;

            if (string.IsNullOrEmpty(databaseVersion) ||
                !Version.TryParse(databaseVersion, out var currentDbVersion) ||
                !string.IsNullOrEmpty(supportedVersion) &&
                !Version.TryParse(supportedVersion, out currentSupportedVersion))
            {
                return false;
            }

            return IsDataDbVersionSupported(currentDbVersion, currentSupportedVersion, exactMachRequired);
        }

        private static bool IsDbVersionSupported(Version databaseVersion, Version expectedVersion, bool exactMachRequired)
        {
            return databaseVersion != null && (exactMachRequired
                ? IsDbVersionEqual(databaseVersion, expectedVersion)
                : IsDbVersionCompatible(databaseVersion, expectedVersion));
        }

        private static bool IsDbVersionCompatible(Version databaseVersion, Version dataAccessVersion)
        {
            // Semantic versioning: 
            // - Major versions: must be equal
            // - Minor versions: database version cannot be lower, but can be higher (i.e. backward compatible change made on database)
            // - Patch (build) versions: must not be identical, as any change related to patch version change only means compatible change
            return (databaseVersion.Major == dataAccessVersion.Major) && (databaseVersion.Minor >= dataAccessVersion.Minor);
        }

        private static bool IsDbVersionEqual(Version databaseVersion, Version dataAccessVersion)
        {
            return (databaseVersion.Major == dataAccessVersion.Major) && (databaseVersion.Minor == dataAccessVersion.Minor) && databaseVersion.Build == dataAccessVersion.Build;
        }

    }
}
