using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Dto;
using DotStat.Db.Exception;
using DotStat.Db.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Registry;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using DotStat.DB.Repository;
using DotStat.MappingStore;
using Transaction = DotStat.Domain.Transaction;
using TransactionStatus = DotStat.Domain.TransactionStatus;
using DotStat.Common.Exceptions;
using DotStat.DB.Util;
using DotStat.Db.Validation;

namespace DotStat.Db.Service
{
    public delegate IDotStatDbService DotStatDbServiceResolver(string dataSpace);

    [ExcludeFromCodeCoverage]
    public class DotStatDbService : IDotStatDbService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGeneralConfiguration _generalConfiguration;

        public DotStatDbService(IUnitOfWork unitOfWork, IGeneralConfiguration generalConfiguration)
        {
            _unitOfWork = unitOfWork;
            _generalConfiguration = generalConfiguration; 
        }
        

        public async Task<bool> TryNewTransaction(Transaction transaction, Dataflow dataflow, DotStatPrincipal principal, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken, bool onlyLockTransaction = false)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            if (dataflow.Dsd == null)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowParameterWithoutDsd),
                    dataflow.FullId)
                );
            }

            if (mappingStoreDataAccess.HasUserCreatedMappingSets(_unitOfWork.DataSpace, dataflow))
            {
                throw new DataFlowForDisseminationException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataFlowForDisseminationError),
                    dataflow.FullId));
            }

            Log.Debug($"TryNewTransaction");
            
            var isFirstDataUpload = false;
            var isRollbackOrRestore = transaction.Type == TransactionType.Restore ||
                                      transaction.Type == TransactionType.Rollback;

            if (!isRollbackOrRestore)
            {
                //Get Dsd values stored in the database
                await _unitOfWork.ArtefactRepository.FillMeta(dataflow.Dsd, cancellationToken, false);
            }
            
            var artefact = await _unitOfWork.ArtefactRepository.GetArtefact(dataflow.Dsd.AgencyId, dataflow.Dsd.Base.Id, dataflow.Dsd.Version, SDMXArtefactType.Dsd, cancellationToken, false);
            var liveVersion = DbTableVersions.GetDbTableVersion(artefact.LiveVersion.ToString());
            var PITVersion = DbTableVersions.GetDbTableVersion(artefact.PITVersion.ToString());

            DbTableVersion? origTransferTargetTableVersion = transaction.FinalTargetVersion == TargetVersion.Live ?
                GetDsdLiveVersion(artefact) : GetDsdPITVersion(artefact);
            Log.Debug($"Orig. table version : {origTransferTargetTableVersion}");

            if (artefact.DbId <= 0 ) //non-existing DSD
            {
                isFirstDataUpload = true;
                if (isRollbackOrRestore)
                {
                    throw new TableVersionException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoLiveOrPITVersion),
                        dataflow.Dsd.FullId));
                }

                origTransferTargetTableVersion = DbTableVersion.A;

                if (!await _unitOfWork.ArtefactRepository.CreateNewDsdAftefact(dataflow.Dsd, cancellationToken))
                    return false;

                transaction.ArtefactDbId = dataflow.Dsd.DbId;
                dataflow.Dsd.MaxTextAttributeLength =
                    dataflow.Dsd.GetMaxLengthOfTextAttributeFromConfig(_generalConfiguration.MaxTextAttributeLength);
            }
            else
            {
                Log.Debug("DSD already in ARTEFACT table.");
                transaction.ArtefactDbId = artefact.DbId;
                var isFirstLoadOfTargetTable = false;

                //In case there is already a PIT release defined on the DSD, 
                //all transaction on this DSD have to target PIT release irrespectively to the value of this parameter.
                if (transaction.RequestedTargetVersion == TargetVersion.Live && artefact.PITVersion == null)
                    transaction.FinalTargetVersion = TargetVersion.Live;
                else
                    transaction.FinalTargetVersion = TargetVersion.PointInTime;

                //first upload ever
                if (liveVersion == null && PITVersion == null)
                {
                    isFirstLoadOfTargetTable = true;
                    origTransferTargetTableVersion = DbTableVersion.A;

                    if (transaction.FinalTargetVersion == TargetVersion.Live)
                    {
                        liveVersion = DbTableVersion.A;
                        dataflow.Dsd.LiveVersion = (char)liveVersion;
                    }
                    else
                    {
                        PITVersion = DbTableVersion.A;
                        dataflow.Dsd.PITVersion = (char)PITVersion;
                    }
                }

                //No uploads ever to the live version
                if (liveVersion == null)
                {
                    liveVersion = DbTableVersions.GetNewTableVersion((DbTableVersion)DbTableVersions.GetDefaultLiveVersion());
                    dataflow.Dsd.LiveVersion = (char)liveVersion;
                    isFirstLoadOfTargetTable = true;
                }


                if (origTransferTargetTableVersion == null)
                {
                    //Rollback.- PIT version does not exist
                    //Restore.- Live version  does not exist
                    if (isRollbackOrRestore)
                    {
                        if (transaction.FinalTargetVersion == TargetVersion.PointInTime)
                        {
                            throw new TableVersionException(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NonExistingPITVersion),
                                dataflow.Dsd.FullId));
                        }

                        throw new TableVersionException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NonExistingLiveVersion),
                            dataflow.Dsd.FullId));
                    }

                    origTransferTargetTableVersion = transaction.FinalTargetVersion == TargetVersion.PointInTime ?
                        DbTableVersions.GetNewTableVersion((DbTableVersion)liveVersion) :
                        DbTableVersions.GetNewTableVersion((DbTableVersion)PITVersion);

                    isFirstLoadOfTargetTable = true;
                }

                if (transaction.FinalTargetVersion == TargetVersion.Live)
                {
                    liveVersion = origTransferTargetTableVersion;
                }
                else
                {
                    PITVersion = origTransferTargetTableVersion;
                }

                dataflow.Dsd.DbId = artefact.DbId;

                if (isFirstLoadOfTargetTable)
                {
                    await _unitOfWork.ArtefactRepository.UpdatePITInfoOfDsdArtefact(dataflow.Dsd, cancellationToken);
                }
            }

            Log.Debug($"Dsd : {dataflow.Dsd.DbId} [{dataflow.Dsd.Base.Id}]");
            Log.Debug($"Dsd new data table version : {origTransferTargetTableVersion}");

            //Skip if the transaction lock has been acheived previously
            if(!await IsTransactionLocked(transaction.Id, cancellationToken))
            {
                if (await HasActiveTransaction(transaction, false, logErrors: !onlyLockTransaction, cancellationToken))
                    return false;

                //Rollback transaction lock (set artefact id) If the lock is acheived but there are other blocking transactions
                //IsolationLevel.ReadUncommitted - Volatile data can be read and modified during the transaction. 
                //It allows to see other concurrent requests that are trying to acheive a lock, which have not been commited. 
                using (var transactionScope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted, Timeout = new TimeSpan(0, 0, _unitOfWork.DatabaseCommandTimeout) },
                    TransactionScopeAsyncFlowOption.Enabled))
                {
                    if (!await _unitOfWork.TransactionRepository.TryLockNewTransaction(transaction, false, (DbTableVersion)origTransferTargetTableVersion, cancellationToken, logErrors: !onlyLockTransaction))
                        return false;

                    transactionScope.Complete();
                }
            }

            //Stop here, if the request is only trying to acheive a lock for the transaction
            if (onlyLockTransaction)
                return true;

            //TODO: the rest of the steps could be moved to a separate method

            //check if dsd data version has not changed since the beginning
            var dsdDbValues = await _unitOfWork.ArtefactRepository.GetArtefactByDbId(dataflow.Dsd.DbId, cancellationToken);
            var currentTableVersionInDb = transaction.FinalTargetVersion == TargetVersion.Live ?
                GetDsdLiveVersion(dsdDbValues) : GetDsdPITVersion(dsdDbValues);

            dataflow.Dsd.PITRestorationDate = dsdDbValues.PITRestorationDate;

            //first dsd transaction
            if (artefact.DbId <= 0)
            {
                currentTableVersionInDb = DbTableVersion.A;
            }

            //first transaction for the target version
            if (currentTableVersionInDb == null)
            {
                currentTableVersionInDb = transaction.FinalTargetVersion == TargetVersion.PointInTime
                    ? DbTableVersions.GetNewTableVersion((DbTableVersion)liveVersion)
                    : DbTableVersions.GetNewTableVersion((DbTableVersion)PITVersion);
            }

            if (currentTableVersionInDb != origTransferTargetTableVersion)
            {
                Log.Debug($"Data table set changed before creation of the transaction from [{origTransferTargetTableVersion}] to [{currentTableVersionInDb}].");
                origTransferTargetTableVersion = currentTableVersionInDb;

                await _unitOfWork.TransactionRepository.UpdateTableVersionOfTransaction(transaction.Id, (DbTableVersion)currentTableVersionInDb, cancellationToken);
            }

            if (transaction.FinalTargetVersion != transaction.RequestedTargetVersion)
            {
                await _unitOfWork.TransactionRepository.UpdateFinalTargetVersionOfTransaction(transaction.Id, (TargetVersion)transaction.FinalTargetVersion, cancellationToken);
            }

            //Set the table version of the transaction.
            transaction.TableVersion = currentTableVersionInDb;

            Log.Debug("Creating missing management database objects.");

            try
            {
                if (!isRollbackOrRestore)
                {
                    using (var transactionScope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 0, _unitOfWork.DatabaseCommandTimeout), TransactionScopeAsyncFlowOption.Enabled))
                    {
                        await FillDbIdsAndCreateMissingDbObjects(dataflow, cancellationToken);
                        transactionScope.Complete();
                    }
                }
                else
                {
                    await FillIdsFromDisseminationDb(dataflow, cancellationToken);
                }

            }
            catch (System.Data.DBConcurrencyException ex)
            {
                //Concurrent process creating and filling a code list reference by the current dsd.
                Log.Error(ex);
                return false;
            }
            catch (ChangeInDsdException ex)
            {
                //Change detected in DSD between metastore and management database 
                Log.Error(ex);
                return false;
            }
            catch(System.Exception ex)
            {
                Log.Error(ex);
                return false;
            }
            finally
            {
                if (isFirstDataUpload)
                {
                    DateTime liveAccStartDate = DateTime.Now;

                    //Create empty ACC for the LIVE version, when the first data upload targets the PIT version
                    if (transaction.FinalTargetVersion == TargetVersion.PointInTime)
                        mappingStoreDataAccess.SaveContentConstraint(_unitOfWork.DataSpace, dataflow, null, false, (char)DbTableVersions.GetNewTableVersion((DbTableVersion)origTransferTargetTableVersion),
                            TargetVersion.Live, liveAccStartDate);

                    //Create empty ACC for the target version
                    mappingStoreDataAccess.SaveContentConstraint(_unitOfWork.DataSpace, dataflow, null, false, (char)origTransferTargetTableVersion, (TargetVersion)transaction.RequestedTargetVersion, liveAccStartDate);

                    //Create empty mapping sets so the error "NoRecordsFound" is returned by the NSI-WS even if the first data import fails.
                    InitializeDataflowEmptyMappingsets(dataflow, mappingStoreDataAccess);
                }
            }

            Log.Debug($"Dataflow : {dataflow.DbId} [{dataflow.Base.Id}]");

            return true;
        }

        /// <summary>
        /// Creates a transaction without a DSD reference. Applies -1 as DSD db id in TRANSACTION table.
        /// </summary>
        /// <param name="transaction">Transaction</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<bool> TryNewTransactionWithNoDsd(Transaction transaction, CancellationToken cancellationToken, bool onlyLockTransaction = false)
        {
            return await TryNewSimpleTransaction(transaction, -1, null, onlyLockTransaction, cancellationToken);
        }

        public async Task<bool> TryNewTransactionForCleanup(Transaction transaction, int dsdId, int? dsdChildDbId, string agency, string id, SdmxVersion version, CancellationToken cancellationToken, bool onlyLockTransaction = false)
        {
            if (string.IsNullOrWhiteSpace(agency))
            {
                throw new ArgumentNullException(nameof(agency));
            }
            if (string.IsNullOrWhiteSpace(agency))
            {
                throw new ArgumentNullException(nameof(agency));
            }

            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            if (version == null)
            {
                throw new ArgumentNullException(nameof(version));
            }

            Log.Debug($"TryNewTransaction for cleanup");
            
            //var dsdId = await _unitOfWork.ArtefactRepository.GetArtefactDbId(agency, id, version, SDMXArtefactType.Dsd, cancellationToken);

            if (dsdId == -1)
            {
                Log.Debug("Artefact is not in ARTEFACT table, nothing to do");

                return false;
            }
            var dsd = new ArtefactItem() { Agency = agency, Id = id, Version = version.ToString() };

            Log.Debug("Artefact already in ARTEFACT table.");

            return await TryNewSimpleTransaction(transaction, dsdId, dsdChildDbId, onlyLockTransaction, cancellationToken, dsd);
        }

        public async Task<bool> TryNewSimpleTransaction(Transaction transaction, int dsdDbId, int? dsdChildDbId, bool lockOnly, CancellationToken cancellationToken, ArtefactItem dsd = null)
        {
            Log.Debug("TryNewSimpleTransaction");

            if (dsdDbId > 0 && dsd is null)
            {
                throw new ArgumentNullException(nameof(dsd));
            }

            if (dsd != null && dsdDbId <= 0)
            {
                throw new ArgumentException(nameof(dsd));
            }

            var isTransactionWithNoDsd = dsdDbId <= 0 || dsd is null;

            if (isTransactionWithNoDsd)
            {
                Log.Debug("TryNewSimpleTransaction with no DSD specified");
            }

            transaction.ArtefactDbId = dsdDbId;
            transaction.ArtefactChildDbId = dsdChildDbId;

            //Skip if the transaction lock has been acheived previously
            if (!await IsTransactionLocked(transaction.Id, cancellationToken))
            {
                if (await HasActiveTransaction(transaction, isTransactionWithNoDsd, logErrors: !lockOnly, cancellationToken))
                    return false;

                //Rollback transaction lock (set artefact id) If the lock is achieved but there are other blocking transactions
                //IsolationLevel.ReadUncommitted - Volatile data can be read and modified during the transaction. 
                //It allows to see other concurrent requests that are trying to achieve a lock, which have not been committed. 
                using (var transactionScope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted, Timeout = new TimeSpan(0, 0, _unitOfWork.DatabaseCommandTimeout) },
                    TransactionScopeAsyncFlowOption.Enabled))
                {
                    if (!await _unitOfWork.TransactionRepository.TryLockNewTransaction(transaction, isTransactionWithNoDsd, DbTableVersion.X, cancellationToken, logErrors: !lockOnly))
                        return false;

                    transactionScope.Complete();
                }
            }

            transaction.TableVersion = DbTableVersion.X;
            return true;
        }

        public async Task<bool> CloseTransaction(
            Transaction transaction,
            Dataflow dataflow,
            bool PITRestorationAllowed,
            bool calculateActualContentConstraint, 
            IMappingStoreDataAccess mappingStoreDataAccess,
            ICodeTranslator codeTranslator,
            CancellationToken cancellationToken)
        {
            if (transaction == null)
            {
                return false;
            }

            //Execute PIT Release if there if current transaction targets PIT and should be release immediately
            if (dataflow.Dsd.PITVersion != null //Is there a PIT?
                && (dataflow.Dsd.PITReleaseDate != null && dataflow.Dsd.PITReleaseDate <= DateTime.Now) //PIT release date has passed
            )
            {
                // Set PIT release date to current time as this the time when the PIT release version is made available as LIVE
                dataflow.Dsd.PITReleaseDate = DateTime.Now;

                await ApplyPITRelease(dataflow, PITRestorationAllowed, mappingStoreDataAccess, codeTranslator);
            }
            else
            {
                var liveVersion = GetDsdLiveVersion(dataflow.Dsd);
                var isLiveVersionTargeted = liveVersion == null
                         ? dataflow.Dsd.PITVersion == null
                         : liveVersion.Equals(transaction.TableVersion);
                var liveAccStartDate = DateTime.Now;

                if (calculateActualContentConstraint)
                    await UpdateCodeListAvailabilityInfo(
                        dataflow,
                        (DbTableVersion)transaction.TableVersion,
                        isLiveVersionTargeted ? TargetVersion.Live : TargetVersion.PointInTime,
                        mappingStoreDataAccess,
                        codeTranslator,
                        cancellationToken,
                        liveAccStartDate
                    );

                //create empty ACC for live version when the first upload targets the PIT version
                if (!isLiveVersionTargeted && liveVersion == null)
                {
                    await UpdateCodeListAvailabilityInfo(
                        dataflow,
                        DbTableVersions.GetNewTableVersion((DbTableVersion)transaction.TableVersion),
                        TargetVersion.Live,
                        mappingStoreDataAccess,
                        codeTranslator,
                        cancellationToken,
                        liveAccStartDate
                    );
                }

                //Manage all mappingSets of the dataFlows belonging to the Dsd
                await ManageMappingSets(dataflow.Dsd, mappingStoreDataAccess, cancellationToken);
            }

            await _unitOfWork.ArtefactRepository.UpdatePITInfoOfDsdArtefact(dataflow.Dsd, cancellationToken);
            await _unitOfWork.MetadataStoreRepository.DropMetadataStagingTables(dataflow.Dsd.DbId, cancellationToken);
            await _unitOfWork.DataStoreRepository.DropStagingTables(dataflow.Dsd, cancellationToken);

            if (!await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, true))
            {
                //there is no transaction with this id
                Log.Warn(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoActiveTransactionWithId),
                    transaction.Id));
                return false;
            }

            Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TransactionSuccessful));

            return true;
        }
        
        public async Task CleanUpFailedTransaction(Transaction transaction)
        {
            if (transaction == null)
            {
                //throw new ArgumentNullException(nameof(transaction));
                Log.Debug("Transaction parameter is null.");
                return;
            }

            //Non-Cancellable method
            var cancellationToken = CancellationToken.None;
            if (transaction.ArtefactDbId == null)
            {
                Log.Debug("Transaction marked as completed.");
                await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, false);
                return;
            }

            Log.Debug("Cleanup of failed transaction started.");
            
            Log.Debug("Dropping staging tables.");

            await _unitOfWork.MetadataStoreRepository.DropMetadataStagingTables((int)transaction.ArtefactDbId, cancellationToken);
            await _unitOfWork.DataStoreRepository.DropStagingTables((int)transaction.ArtefactDbId, cancellationToken);


            Log.Debug("Closing unsuccessful transaction.");

            await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, false);
            Log.Debug("Cleanup completed.");
        }

        public async Task CloseCanceledOrTimedOutTransaction(int transactionId)
        {
            var cancellationToken = CancellationToken.None;
            try
            {
                var transaction = await _unitOfWork.TransactionRepository.GetTransactionById(transactionId, cancellationToken);
                if (transaction == null)
                    return;

                if (_unitOfWork.MaxDataImportTimeInMinutes > 0 && transaction.ExecutionStart != null &&
                (DateTime.Now - (DateTime)transaction.ExecutionStart).TotalMinutes >= _unitOfWork.MaxDataImportTimeInMinutes)
                {
                    await _unitOfWork.TransactionRepository.MarkTransactionAsTimedOut(transaction.Id);
                    Log.Error(new TransactionTimeOutException(
                        string.Format(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.TerminatingTimedOutTransaction), transactionId)));
                }
                else
                {
                    await _unitOfWork.TransactionRepository.MarkTransactionAsCanceled(transaction.Id);
                    Log.Error(new TransactionKilledException(
                        string.Format(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.TerminatingCanceledTransaction), transactionId)));
                }
            }
            catch (System.Exception e)
            {
                Log.Fatal(e);
            }
        }

        public async Task<TransactionStatus> GetFinalTransactionStatus(int transactionId, CancellationToken cancellationToken)
        {
            try
            {
                var transaction = await _unitOfWork.TransactionRepository.GetTransactionById(transactionId, cancellationToken);
                if (transaction != null && !transaction.Closed)
                {
                    await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, false);
                    transaction.Status = TransactionStatus.Completed;
                }
                return transaction == null ? TransactionStatus.Unknown : transaction.Status;
            }
            catch (System.Exception e)
            {
                Log.Fatal(e);
            }
            return TransactionStatus.Unknown;
        }

        public async Task<bool> Rollback(Dataflow dataflow, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            if (dataflow.Dsd == null)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowParameterWithoutDsd),
                    dataflow.FullId)
                );
            }

            await _unitOfWork.ArtefactRepository.FillMeta(dataflow.Dsd, cancellationToken);
            var PITVersion = dataflow.Dsd.PITVersion;
            var PITReleaseDate = dataflow.Dsd.PITReleaseDate;

            //is there a PIT
            if (PITVersion != null)
            {
                //PIT still not released
                if (PITReleaseDate == null || PITReleaseDate > DateTime.Now)
                {
                    //Delete PIT and restore information from artefact table
                    dataflow.Dsd.PITVersion = null;
                    dataflow.Dsd.PITReleaseDate = null;
                    dataflow.Dsd.PITRestorationDate = null;
                    //Update dsd
                    await _unitOfWork.ArtefactRepository.UpdatePITInfoOfDsdArtefact(dataflow.Dsd, cancellationToken);

                    //Truncate tables containing PIT info
                    await _unitOfWork.DataStoreRepository.DeleteData(dataflow.Dsd, (DbTableVersion)PITVersion, cancellationToken);
                    if (dataflow.Dsd.MsdDbId > 0)
                        await _unitOfWork.MetadataStoreRepository.DeleteMetadata(dataflow.Dsd, (DbTableVersion)PITVersion, cancellationToken);

                    //Manage all mappingSets of the dataFlows belonging to the Dsd
                    await ManageMappingSets(dataflow.Dsd, mappingStoreDataAccess, cancellationToken);

                    var dataflowsOfDsd = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dataflow.Dsd.DbId, cancellationToken);
                    foreach (var df in dataflowsOfDsd)
                    {
                        //Delete actual content constraints of PIT release (all dataflows of DSD)
                        mappingStoreDataAccess.DeleteActualContentConstraint(_unitOfWork.DataSpace, df.Agency, df.Id, df.Version, (char)PITVersion, TargetVersion.PointInTime);
                    }

                    return true;
                }
                //throw PIT already released
                throw new PointInTimeReleaseException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.PITReleased),
                    dataflow.Dsd.FullId));
            }
            //throw non existing PIT
            throw new TableVersionException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NonExistingPITVersion),
                dataflow.Dsd.FullId));
        }
        
        public async Task<bool> Restore(Dataflow dataflow, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            if (dataflow.Dsd == null)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowParameterWithoutDsd),
                    dataflow.FullId)
                );
            }

            await _unitOfWork.ArtefactRepository.FillMeta(dataflow.Dsd, cancellationToken);

            var liveVersion = dataflow.Dsd.LiveVersion; 
            var PITVersion = dataflow.Dsd.PITVersion; 
            var PITReleaseDate = dataflow.Dsd.PITReleaseDate; 
            var PITRestorationDate = dataflow.Dsd.PITRestorationDate;

            //is there a PIT
            if (PITVersion != null)
            {
                //PIT already released
                if (PITReleaseDate != null && PITReleaseDate <= DateTime.Now)
                {
                    var oldLiveVersion = PITVersion;
                    var newLiveVersion = (char?)liveVersion;
                    //Delete PIT and restore information from artefact table
                    dataflow.Dsd.LiveVersion = newLiveVersion;
                    dataflow.Dsd.PITVersion = null;
                    dataflow.Dsd.PITReleaseDate = null;
                    dataflow.Dsd.PITRestorationDate = null;
                    //Update dsd
                    await _unitOfWork.ArtefactRepository.UpdatePITInfoOfDsdArtefact(dataflow.Dsd, cancellationToken);

                    //Truncate tables containing previous LIVE version (in this case PIT version)
                    await _unitOfWork.DataStoreRepository.DeleteData(dataflow.Dsd, (DbTableVersion)oldLiveVersion, cancellationToken);
                    if (dataflow.Dsd.MsdDbId > 0)
                        await _unitOfWork.MetadataStoreRepository.DeleteMetadata(dataflow.Dsd, (DbTableVersion)oldLiveVersion, cancellationToken);

                    //Manage all mappingSets of the dataFlows belonging to the Dsd
                    await ManageMappingSets(dataflow.Dsd, mappingStoreDataAccess, cancellationToken);

                    var dataflowsOfDsd = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dataflow.Dsd.DbId, cancellationToken);
                    foreach (var df in dataflowsOfDsd)
                    {
                        //Delete actual content constraints of PIT release (all dataflows of DSD)
                        mappingStoreDataAccess.DeleteActualContentConstraint(_unitOfWork.DataSpace, df.Agency, df.Id, df.Version, (char)PITVersion, TargetVersion.PointInTime);
                    }

                    return true;
                }
                //throw PIT not yet released
                throw new PointInTimeReleaseException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.PITNotReleased),
                    dataflow.Dsd.FullId));
            }

            //When there is no PIT version then check for Restoration version and make it live if exists
            if (PITRestorationDate != null)
            {
                var newLiveVersion = (liveVersion != null) ? DbTableVersions.GetNewTableVersion((DbTableVersion)liveVersion) : DbTableVersion.A;
                var oldLiveVersion = DbTableVersions.GetNewTableVersion(newLiveVersion);

                var liveAccStartDate = PITRestorationDate.Value;

                //Change live version 
                dataflow.Dsd.LiveVersion = (char)newLiveVersion;
                //Clear restore info
                dataflow.Dsd.PITVersion = null;
                dataflow.Dsd.PITReleaseDate = null;
                dataflow.Dsd.PITRestorationDate = null;
                //Update dsd
                await _unitOfWork.ArtefactRepository.UpdatePITInfoOfDsdArtefact(dataflow.Dsd, cancellationToken);
                //Truncate tables containing previous live data
                await _unitOfWork.DataStoreRepository.DeleteData(dataflow.Dsd, oldLiveVersion, cancellationToken);
                await _unitOfWork.MetadataStoreRepository.DeleteMetadata(dataflow.Dsd, oldLiveVersion, cancellationToken);

                var codeTranslator = new CodeTranslator(_unitOfWork.CodeListRepository);
                await codeTranslator.FillDict(dataflow, cancellationToken);
                //Re-create actual content constraints for restored version (all dataflows of DSD)
                await UpdateCodeListAvailabilityInfo(
                    dataflow,
                    newLiveVersion,
                    TargetVersion.Live,
                    mappingStoreDataAccess,
                    codeTranslator,
                    cancellationToken,
                    liveAccStartDate
                );

                //Manage all mappingSets of the dataFlows belonging to the Dsd
                await ManageMappingSets(dataflow.Dsd, mappingStoreDataAccess, cancellationToken);

                return true;
            }

            //throw no version to restore
            throw new TableVersionException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoRestoringVersion),
                dataflow.Dsd.FullId));
        }

        public async Task ApplyPITRelease(Dataflow dataflow, bool isRestorationAllowed, IMappingStoreDataAccess mappingStoreDataAccess, ICodeTranslator codeTranslator)
        {
            //Non-Cancellable method
            var cancellationToken = CancellationToken.None;
            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            if (dataflow.Dsd == null)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowParameterWithoutDsd),
                    dataflow.FullId)
                );
            }

            Log.Debug($"ApplyPITRelease for dataflow {dataflow.FullId}");

            var oldDsdLiveVersion = dataflow.Dsd.LiveVersion;
            var liveAccStartDate = dataflow.Dsd.PITReleaseDate ?? DateTime.Now;
            dataflow.Dsd.LiveVersion = (char)dataflow.Dsd.PITVersion;//New version
            //Clear PIT info
            dataflow.Dsd.PITVersion = null;
            dataflow.Dsd.PITReleaseDate = null;

            //Keep backup for restoration?
            if (!isRestorationAllowed && oldDsdLiveVersion != null)
            {
                dataflow.Dsd.PITRestorationDate = null;
                await _unitOfWork.DataStoreRepository.DeleteData(dataflow.Dsd, (DbTableVersion)DbTableVersions.GetDbTableVersion((char)oldDsdLiveVersion), cancellationToken);
                if (dataflow.Dsd.MsdDbId > 0)
                    await _unitOfWork.MetadataStoreRepository.DeleteMetadata(dataflow.Dsd, (DbTableVersion)DbTableVersions.GetDbTableVersion((char)oldDsdLiveVersion), cancellationToken);
            }

            if (oldDsdLiveVersion.HasValue)
            {
                var dataflowsOfDsd = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dataflow.Dsd.DbId, cancellationToken);
                foreach (var df in dataflowsOfDsd)
                {
                    //Delete actual content constraints of PIT release (all dataflows of DSD)
                    mappingStoreDataAccess.DeleteActualContentConstraint(_unitOfWork.DataSpace, df.Agency, df.Id, df.Version,
                        oldDsdLiveVersion.Value, TargetVersion.Live);
                }
            }

            //Recalculate Actual content constraints for the activated PIT release (now LIVE)
            await UpdateCodeListAvailabilityInfo(
                dataflow,
                (DbTableVersion)dataflow.Dsd.LiveVersion,
                TargetVersion.Live,
                mappingStoreDataAccess,
                codeTranslator,
                cancellationToken,
                liveAccStartDate
            );

            //Manage all mappingSets of the dataFlows belonging to the Dsd
            await ManageMappingSets(dataflow.Dsd, mappingStoreDataAccess, cancellationToken);

            //Apply PIT Release
            await _unitOfWork.ArtefactRepository.UpdatePITInfoOfDsdArtefact(dataflow.Dsd, cancellationToken);

        }

        public async Task ManageMappingSets(Dsd dsd, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken)
        {
            //Update all mappingSets both PIT and Live 
            var dataFlowsOfDsd = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dsd.DbId, cancellationToken);
            foreach (var df in dataFlowsOfDsd)
            {
                var dataFlow =
                    mappingStoreDataAccess.GetDataflow(_unitOfWork.DataSpace, df.Agency, df.Id, df.Version, false);

                if (dataFlow == null)
                {
                    Log.Error(new ArtefactNotFoundException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                            .ArtefactNotFoundInManagementDb),
                        df.Agency, df.Id, df.Version, DbTypes.GetDbType(SDMXArtefactType.Dataflow))));
                    continue;
                }

                if (mappingStoreDataAccess.HasUserCreatedMappingSets(_unitOfWork.DataSpace,
                    dataFlow))
                {
                    Log.Error(new DataFlowForDisseminationException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                            .DataFlowForDisseminationError), dataFlow.FullId)));
                    continue;
                }
                dataFlow.DbId = df.DbId;

                dataFlow.Dsd.CopyDataManagementPropertiesFrom(dsd);

                ManageDataflowMappingsets(dataFlow, mappingStoreDataAccess);
            }
        }

        public async Task<bool> InitializeAllMappingSets(Transaction transaction, DotStatPrincipal principal, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken)
        {
            var strucDbDsds = mappingStoreDataAccess.GetDataStructures(_unitOfWork.DataSpace);
            if (!strucDbDsds.Any())
            {
                Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoDsdsInMappingStoreDb));

                return true;
            }

            var strucDbDataFlows = mappingStoreDataAccess.GetDataflows(_unitOfWork.DataSpace);
            if (!strucDbDataFlows.Any())
            {
                Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoDataflowsInMappingStoreDb));

                return true;
            }

            try
            {
                if (!await TryNewTransactionWithNoDsd(transaction, cancellationToken))
                {
                    await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, false);
                    // Transaction creation attempt failed due to an ongoing 'InitializeAllMappingSets' transaction
                    return false;
                }

                var dataDbDataFlows = await _unitOfWork.ArtefactRepository.GetListOfArtefacts(cancellationToken, DbTypes.GetDbType(SDMXArtefactType.Dataflow));
                var dataDbDsds = await _unitOfWork.ArtefactRepository.GetListOfArtefacts(cancellationToken, DbTypes.GetDbType(SDMXArtefactType.Dsd));

                var failedDataflows = new List<string>();
                var completedDataflows = new List<string>();
                var updatedDataflows = new List<string>();

                var notProcessedDataflows = strucDbDataFlows.Select(df => $"{df.AgencyId}:{df.Id}({df.Version})").ToList();

                foreach (var df in strucDbDataFlows)
                {
                    try
                    {
                        // Do nothing when dataflow is external reference
                        if (df.IsExternalReference?.IsTrue ?? false)
                        {
                            Log.Warn(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.InitExternalDataflow),
                                $"{df.AgencyId}:{df.Id}({df.Version})"));

                            continue;
                        }

                        var strucDbDsd = strucDbDsds
                            .FirstOrDefault(ob =>
                                string.Equals(ob.Id, df.DataStructureRef?.MaintainableId) &&
                                string.Equals(ob.AgencyId, df.DataStructureRef?.AgencyId) &&
                                string.Equals(ob.Version, df.DataStructureRef?.Version));

                        if (strucDbDsd == null)
                        {
                            failedDataflows.Add($"{df.AgencyId}:{df.Id}({df.Version})");
                            notProcessedDataflows.Remove($"{df.AgencyId}:{df.Id}({df.Version})");

                            Log.Error(new ArtefactNotFoundException(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdNotSet),
                                df.AgencyId,
                                df.Id,
                                df.Version)));

                            continue;
                        }

                        var dsd = new Dsd(strucDbDsd);

                        var dataFlow = mappingStoreDataAccess.GetDataflow(_unitOfWork.DataSpace, df.AgencyId, df.Id, df.Version, true);

                        var hasMappingSet = mappingStoreDataAccess.HasMappingSet(_unitOfWork.DataSpace, dataFlow);
                        var hasNonMigratedDataSets = mappingStoreDataAccess.HasNonMigratedDataSets(_unitOfWork.DataSpace, dataFlow);
                        //Do not process dataflows with existing mapping sets, and DataSets that have been already migrated. 
                        if (hasMappingSet && !hasNonMigratedDataSets)
                        {
                            continue;
                        }

                        var dataDbDataFlow = dataDbDataFlows
                            .FirstOrDefault(ob =>
                                string.Equals(ob.Id, df.Id) &&
                                string.Equals(ob.Agency, df.AgencyId) &&
                                string.Equals(ob.Version, df.Version));

                        //dataflow already initialized in the data DB
                        if (dataDbDataFlow != null)
                        {
                            dataFlow.DbId = dataDbDataFlow.DbId;

                            var dataDbDsd = dataDbDsds
                                .FirstOrDefault(ob =>
                                    string.Equals(ob.Id, dsd.Base?.Id) &&
                                    string.Equals(ob.Agency, dsd.Base?.AgencyId) &&
                                    string.Equals(ob.Version, dsd.Base?.Version));

                            if (dataDbDsd == null)
                            {
                                failedDataflows.Add(dataFlow.FullId);
                                notProcessedDataflows.Remove(dataFlow.FullId);

                                Log.Error(new ArtefactNotFoundException(string.Format(
                                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdNotFound),
                                    dsd.AgencyId, dsd.Code, dsd.Version)));

                                continue;
                            }

                            dataFlow.Dsd.DbId = dataDbDsd.DbId;
                            dataFlow.Dsd.LiveVersion = dataDbDsd.LiveVersion;
                            dataFlow.Dsd.PITVersion = dataDbDsd.PITVersion;
                            dataFlow.Dsd.PITReleaseDate = dataDbDsd.PITReleaseDate;
                        }

                        ManageDataflowMappingsets(dataFlow, mappingStoreDataAccess);

                        if (hasNonMigratedDataSets)
                            updatedDataflows.Add(dataFlow.FullId);
                        else
                            completedDataflows.Add(dataFlow.FullId);
                        notProcessedDataflows.Remove(dataFlow.FullId);
                    }
                    catch (System.Exception ex)
                    {
                        failedDataflows.Add($"{df.AgencyId}:{df.Id}({df.Version})");
                        notProcessedDataflows.Remove($"{df.AgencyId}:{df.Id}({df.Version})");

                        Log.Error(new DotStatException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnexpectedErrorWithDataflow),
                            $"{df.AgencyId}:{df.Id}({df.Version})", ex.Message), ex));
                    }
                }

                if (completedDataflows.Any())
                    Log.Notice(string.Format(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.CreationOfMappingsetsSuccessForDataflows),
                        string.Join(", ", completedDataflows)));

                if (updatedDataflows.Any())
                    Log.Notice(string.Format(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.UpdatedMappingsetsOfDataflows),
                        string.Join(", ", updatedDataflows)));

                if (notProcessedDataflows.Any())
                    Log.Warn(string.Format(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.MappingsetsPreviouslyCreatedForDataflows),
                        string.Join(", ", notProcessedDataflows)));

                var hasFailedDataflows = failedDataflows.Any();

                if (hasFailedDataflows)
                {
                    Log.Warn(string.Format(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.CreationOfMappingsetsFailedForDataflows),
                        string.Join(", ", failedDataflows)));
                }

                await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, !hasFailedDataflows);
            }
            catch (System.Exception)
            {
                await _unitOfWork.TransactionRepository.MarkTransactionAsCompleted(transaction.Id, false);
                throw;
            }

            return true;
        }

        public async Task<bool> CleanUpDsd(int dsdDbId, bool deleteMsdObjectsOnly, IMappingStoreDataAccess mappingStoreDataAccess, List<ComponentItem> allComponents, List<MetadataAttributeItem> allMetadataAttributeComponents, CancellationToken cancellationToken)
        {
            if (dsdDbId == -1)
            {
                return false;
            }

            var dataflowArtefacts = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dsdDbId, cancellationToken);

            //Delete mappingsets and actual content constraints
            foreach (var df in dataflowArtefacts)
            {
                var dataFlow = mappingStoreDataAccess.GetDataflow(_unitOfWork.DataSpace, df.Agency, df.Id, df.Version, false);

                //Dataflow not in mappingstore
                if (dataFlow == null)
                    continue;

                mappingStoreDataAccess.DeleteDataSetsAndMappingSets(_unitOfWork.DataSpace, dataFlow, deleteMsdObjectsOnly);

                if (!deleteMsdObjectsOnly)
                    mappingStoreDataAccess.DeleteAllActualContentConstraints(_unitOfWork.DataSpace, dataFlow);
            }

            //Delete MSD objects in dataDb
            await _unitOfWork.ArtefactRepository.CleanUpMsdOfDsd(dsdDbId, dataflowArtefacts, allComponents, allMetadataAttributeComponents, cancellationToken);

            if (!deleteMsdObjectsOnly)
            {
                //Delete DSD objects in dataDb
                await _unitOfWork.ArtefactRepository.CleanUpDsd(dsdDbId, dataflowArtefacts, allComponents.ToList(), cancellationToken);
            }

            return true;
        }

        public async Task FillIdsFromDisseminationDb(Dataflow dataflow, CancellationToken cancellationToken)
        {
            dataflow.DbId = await _unitOfWork.ArtefactRepository.GetArtefactDbId(dataflow, cancellationToken, true);

            await _unitOfWork.ArtefactRepository.FillMeta(dataflow.Dsd, cancellationToken);

            //Fill MSD info, only when the Dsd has a linked MSD
            if (dataflow.Dsd.MsdDbId != null && dataflow.Dsd.MsdDbId > 0)
                await _unitOfWork.ArtefactRepository.FillMeta(dataflow.Dsd.Msd, cancellationToken);

            // Dimensions
            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                    continue;

                dim.Codelist.DbId = await _unitOfWork.ArtefactRepository.GetArtefactDbId(dim.Codelist, cancellationToken, true);
                dim.DbId = await _unitOfWork.ComponentRepository.GetComponentDbId(dim, dataflow.Dsd.DbId, cancellationToken, true);
            }

            // Primary measure
            var measure = dataflow.Dsd.PrimaryMeasure;

            if (measure.Base.HasCodedRepresentation())
            {
                measure.Codelist.DbId = await _unitOfWork.ArtefactRepository.GetArtefactDbId(measure.Codelist, cancellationToken, true);
            }

            measure.DbId = await _unitOfWork.ComponentRepository.GetComponentDbId(measure, dataflow.Dsd.DbId, cancellationToken, true);

            // Attributes
            foreach (var attr in dataflow.Dsd.Attributes)
            {
                if (attr.Base.HasCodedRepresentation())
                {
                    attr.Codelist.DbId = await _unitOfWork.ArtefactRepository.GetArtefactDbId(attr.Codelist, cancellationToken, true);
                }

                attr.DbId = await _unitOfWork.ComponentRepository.GetComponentDbId(attr, dataflow.Dsd.DbId, cancellationToken, true);
            }

            if (dataflow.Dsd.Msd == null) return;

            //MetadataAttributes
            foreach (var attr in dataflow.Dsd.Msd.MetadataAttributes)
            {
                attr.Dsd = dataflow.Dsd;
                if (attr.Base.HasCodedRepresentation())
                {
                    attr.Codelist.DbId = await _unitOfWork.ArtefactRepository.GetArtefactDbId(attr.Codelist, cancellationToken, true);
                }

                attr.DbId = await _unitOfWork.MetaMetadataComponentRepository.GetMetadataAttributeDbId(attr, dataflow.Dsd.Msd.DbId, cancellationToken, true);
            }

        }

        public async Task FillDbIdsAndCreateMissingDbObjects(Dataflow dataflow, CancellationToken cancellationToken, bool skipDsd = true)
        {
            if (!skipDsd)
            {
                //TODO: check and create DSD
            }
            else
            {
                if (dataflow.Dsd.DbId <= 0)
                {
                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdNotInitialized),
                        dataflow.Dsd.FullId, dataflow.FullId)
                    );
                }
            }

            var factTableExists = await _unitOfWork.DataStoreRepository.DataTablesExist(dataflow.Dsd, cancellationToken);
            var componentItems = await _unitOfWork.ComponentRepository.GetComponentsOfDsd(dataflow.Dsd, cancellationToken);

            //Create missing Db components And fill management information

            await VerifyAndCreateDimensions(dataflow.Dsd, componentItems, factTableExists, cancellationToken);

            await _unitOfWork.ArtefactRepository.VerifyAndCreateMeasures(dataflow.Dsd, componentItems, factTableExists, cancellationToken);

            await VerifyAndCreateAttributes(dataflow.Dsd, componentItems, factTableExists, cancellationToken);

            await _unitOfWork.ArtefactRepository.VerifyAndCreateOrUpdateDynamicTables(dataflow.Dsd, factTableExists, cancellationToken);

            await _unitOfWork.ArtefactRepository.VerifyAndCreateDataflow(dataflow, cancellationToken);

            await VerifyAndUpdateDsdMsd(dataflow.Dsd, cancellationToken);

            //No linked MSD
            if (dataflow.Dsd.Msd == null || !dataflow.Dsd.Msd.MetadataAttributes.Any())
                return;

            var metadataAttributeComponentItems = await _unitOfWork.MetaMetadataComponentRepository.GetMetadataAttributesOfMsd(dataflow.Dsd.Msd, cancellationToken);
            var metaTableExists = await _unitOfWork.MetadataStoreRepository.MetadataTablesExist(dataflow.Dsd, cancellationToken);
            await VerifyAndCreateMetadataAttributes(dataflow.Dsd, metadataAttributeComponentItems, metaTableExists, cancellationToken);

            await _unitOfWork.ArtefactRepository.VerifyAndCreateOrUpdateMetadataDynamicTables(dataflow.Dsd, metaTableExists, cancellationToken);

            await _unitOfWork.ArtefactRepository.CreateMetaDataFlow(dataflow, cancellationToken);

        }

        public DbTableVersion? GetDsdLiveVersion(Dsd dsd)
        {
            var art = new ArtefactItem { 
                LiveVersion = dsd.LiveVersion,
                PITVersion = dsd.PITVersion,
                PITReleaseDate = dsd.PITReleaseDate,
                PITRestorationDate = dsd.PITRestorationDate,
            };

            return GetDsdLiveVersion(art);
        }

        public DbTableVersion? GetDsdPITVersion(Dsd dsd)
        {
            var art = new ArtefactItem
            {
                LiveVersion = dsd.LiveVersion,
                PITVersion = dsd.PITVersion,
                PITReleaseDate = dsd.PITReleaseDate,
                PITRestorationDate = dsd.PITRestorationDate,
            };

            return GetDsdPITVersion(art);
        }

        public async Task<ImportSummary> MergeStagingTable(Dataflow dataFlow, ReportedComponents reportedComponents, IList<DataSetAttributeRow> dataSetAttributeRows, CodeTranslator translator,
            DbTableVersion tableVersion, bool includeSummary, BulkImportResult bulkImportResult, CancellationToken cancellationToken)
        {
            var importSummary = new ImportSummary
            {
                ObservationsCount = bulkImportResult.RowsCopied,
                Errors = new List<IValidationError>()
            };
            
            using var transactionScope = new TransactionScope(TransactionScopeOption.Required,
                new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted, Timeout = new TimeSpan(0, 0, _unitOfWork.DatabaseCommandTimeout) },
                TransactionScopeAsyncFlowOption.Enabled);

            //Process non dataFlow level components
            if (bulkImportResult.RowsCopied > 0)
            {
                //Insert new series found in the import file
                await _unitOfWork.DataStoreRepository.MergeStagingToFilterTable(dataFlow.Dsd, reportedComponents, cancellationToken);

                //When there are multiple actions, modifying the same coordinates, in the same file, the update/merge process is done in max 2 batches
                int? totalBatches = bulkImportResult is { NumberOfRowsToDelete: > 0 } and { NumberOfRowsToMerge: > 0 }
                    ? await _unitOfWork.DataStoreRepository.SetBatchNumberToStagingTableRows(dataFlow.Dsd, reportedComponents, cancellationToken) : null;

                if ((totalBatches ?? 1) > 2)
                {
                    importSummary.Errors.Add(new ObservationError
                        { Type = ValidationErrorType.DuplicatedRowsInStagingTable });
                    return importSummary;
                }
                
                for (var i = 1; i <= (totalBatches ?? 1); i++)
                {
                    int? batchNumber = totalBatches is null ? null : i;

                    //MERGE observation level components (primary measure and attributes which reference all dimensions, including time)
                    var mergeResult = await _unitOfWork.DataStoreRepository.MergeStagingToFactTable(dataFlow.Dsd, reportedComponents, tableVersion, includeSummary,
                        batchNumber, bulkImportResult, cancellationToken);

                    if (mergeResult.Errors.Any())
                    {
                        importSummary.Errors.AddRange(mergeResult.Errors);
                        return importSummary;
                    }

                    if (includeSummary)
                    {
                        importSummary.ObservationLevelMergeResult.InsertCount += mergeResult.InsertCount;
                        importSummary.ObservationLevelMergeResult.UpdateCount += mergeResult.UpdateCount;
                        importSummary.ObservationLevelMergeResult.DeleteCount += mergeResult.DeleteCount;
                    }

                    //MERGE attributes that reference all dimensions, except time dimension
                    mergeResult = await _unitOfWork.DataStoreRepository.MergeStagingToAttrTable(dataFlow.Dsd, reportedComponents, 
                        tableVersion, includeSummary, batchNumber, bulkImportResult, cancellationToken);

                    if (mergeResult.Errors.Any())
                    {
                        importSummary.Errors.AddRange(mergeResult.Errors);
                        return importSummary;
                    }

                    if (includeSummary)
                    {
                        importSummary.SeriesLevelMergeResult.InsertCount += mergeResult.InsertCount;
                        importSummary.SeriesLevelMergeResult.UpdateCount += mergeResult.UpdateCount;
                        importSummary.SeriesLevelMergeResult.DeleteCount += mergeResult.DeleteCount;
                    }
                }
            }

            //Process dataSet level components
            var dsdMergeResult = await _unitOfWork.DataStoreRepository.MergeStagingToDatasetAttributes(dataFlow, dataSetAttributeRows,
                translator, tableVersion, includeSummary, cancellationToken);

            if (dsdMergeResult.Errors.Any())
            {
                importSummary.Errors.AddRange(dsdMergeResult.Errors);
                return importSummary;
            }

            if (includeSummary)
            {
                importSummary.DataFlowLevelMergeResult.InsertCount += dsdMergeResult.InsertCount;
                importSummary.DataFlowLevelMergeResult.UpdateCount += dsdMergeResult.UpdateCount;
                importSummary.DataFlowLevelMergeResult.DeleteCount += dsdMergeResult.DeleteCount;
            }

            transactionScope.Complete();

            return importSummary;
        }

        #region Private methods

        private async Task<bool> HasActiveTransaction(Transaction transaction, bool isTransactionWithNoDsd, bool logErrors, CancellationToken cancellationToken)
        {
            //Check if there are running transactions for the same artefact
            var transactionsInProgress = _unitOfWork.TransactionRepository.GetInProgressTransactions(transaction.Id, (int)transaction.ArtefactDbId, cancellationToken);
            
            var hasActiveTransactions = false;
            await foreach (var transactionInProgress in transactionsInProgress)
            {
                //there is already an ongoing transaction for the same DSD
                Log.Debug($"Concurrent transaction detected : {transactionInProgress.Id}, timeout threshold: {_unitOfWork.MaxDataImportTimeInMinutes}");

                if ((DateTime.Now - (DateTime)transactionInProgress.ExecutionStart).TotalMinutes < _unitOfWork.MaxDataImportTimeInMinutes)
                {
                    if (transaction.ArtefactChildDbId != null && transactionInProgress.ArtefactChildDbId != null)
                    {
                        continue;
                    }

                    if (logErrors)
                    {
                        //the existing transaction is considered to be active
                        var errorMessage = isTransactionWithNoDsd
                            ? string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                    .TransactionAbortedDueToConcurrentTransactionWithNoDsd),
                                transaction.Id,
                                transactionInProgress.Id)
                            : string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                    .TransactionAbortedDueToConcurrentTransaction),
                                transaction.Id,
                                transaction.ArtefactFullId,
                                transactionInProgress.Id);

                        Log.Error(new DotStatException(errorMessage));
                    }

                    hasActiveTransactions = true;
                }
                else
                {
                    //Cleanup Timed out transaction

                    // Set log4net LogicalThreadContext to previous transaction
                    Log.SetTransactionId(transactionInProgress.Id);
                    Log.Error(new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.TerminatingTimedOutTransaction),
                        transactionInProgress.Id)));

                    await CleanUpFailedTransaction(transactionInProgress);

                    //the existing transaction has timed out
                    await _unitOfWork.TransactionRepository.MarkTransactionAsTimedOut(transactionInProgress.Id);

                    // Reset log4net LogicalThreadContext to current transaction
                    Log.SetTransactionId(transaction.Id);
                    var message = isTransactionWithNoDsd
                        ? string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .PreviousTransactionWithNoDsdTimedOut),
                            transactionInProgress.Id)
                        : string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .PreviousTransactionTimedOut),
                            transactionInProgress.Id,
                            transaction.ArtefactFullId);

                    Log.Warn(message);
                }
            }

            return hasActiveTransactions;
        }

        private async Task<bool> IsTransactionLocked(int transactionId, CancellationToken cancellationToken)
        {
            var transaction = await _unitOfWork.TransactionRepository.GetTransactionById(transactionId, cancellationToken);

            if (transaction?.ArtefactDbId != null)
                return true;

            return false;
        }

        private async Task UpdateCodeListAvailabilityInfo(
            Dataflow dataflow,
            DbTableVersion dbTableVersion,
            TargetVersion targetVersion, 
            IMappingStoreDataAccess mappingStoreDataAccess,
            ICodeTranslator codeTranslator,
            CancellationToken cancellationToken,
            DateTime liveAccStartDate
        )
        {
            if (dataflow == null || dataflow.DbId == 0 || dataflow.Dsd.DbId == 0)
                throw new ArgumentException(nameof(dataflow));

            var dsdDataflows = await _unitOfWork.ArtefactRepository.GetListOfDataflowArtefactsForDsd(dataflow.Dsd.DbId, cancellationToken);

            foreach (var df in dsdDataflows)
            {
                //TODO: Optimize it, can reuse artefacts from current dataflow for other dataflows of the same DSD.
                var currentDataflow = df.DbId == dataflow.DbId
                    ? dataflow
                    : mappingStoreDataAccess.GetDataflow(_unitOfWork.DataSpace, df.Agency, df.Id, df.Version, false);

                if (currentDataflow == null)
                {
                    // If a dataflow is not be present in structure database, it means it has been deleted but this deletion hasn't been yet synchronized to ARTEFACT table in data database 
                    Log.Debug($"Dataflow artefact {df}) not found in structure database and will be deleted from ARTEFACT table.");

                    // Delete dataflow from ARTEFACT table if does not exist in structure database
                    await _unitOfWork.ArtefactRepository.DeleteArtefact(df.DbId, cancellationToken);

                    continue;
                }

                if (currentDataflow.DbId == 0)
                {
                    await FillIdsFromDisseminationDb(currentDataflow, cancellationToken);
                    //Copy PIT related info from the target dataflow of the current transfer transaction (these are saved just at the end of the transaction into database)
                    currentDataflow.Dsd.LiveVersion = dataflow.Dsd.LiveVersion;
                    currentDataflow.Dsd.PITVersion = dataflow.Dsd.PITVersion;
                    currentDataflow.Dsd.PITReleaseDate = dataflow.Dsd.PITReleaseDate;
                }

                // ----------------------------------------------

                //create sql WHERE clause that defines the content of the dataflow
                var dataflowWhereClause = await Predicate.BuildWhereForDataObservationLevel(codeTranslator, null, currentDataflow, cancellationToken);
                var availability = await _unitOfWork.ArtefactRepository.CreateDataAvailabilityOfDataflow(currentDataflow.DbId, dataflowWhereClause, null, currentDataflow.Dsd, dbTableVersion, cancellationToken);
                var obsCount = await _unitOfWork.ObservationRepository.GetObservationCount(currentDataflow, dbTableVersion, dataflowWhereClause, cancellationToken);
                var cubeRegion = new List<IKeyValuesMutable>();

                foreach (var dim in currentDataflow.Dsd.Dimensions)
                {
                    cubeRegion.Add(availability.GetAvailabilityByDimension(dim, codeTranslator));
                }

                // Save new content constraint
                mappingStoreDataAccess.SaveContentConstraint(_unitOfWork.DataSpace, currentDataflow, cubeRegion, true, (char)dbTableVersion, targetVersion, liveAccStartDate, obsCount);
                // save new dataset/mapping set
            }
        }

        // TODO should the mappingstore methods be aynchronous?
        private void ManageDataflowMappingsets(Dataflow dataFlow, IMappingStoreDataAccess mappingStoreDataAccess)
        {
            var liveVersion = dataFlow.Dsd.LiveVersion;
            var pitVersion = dataFlow.Dsd.PITVersion;
            var isPitEmpty = dataFlow.Dsd.PITVersion == null;
            var isLiveEmpty = dataFlow.Dsd.LiveVersion == null;
            
            var releaseDate = dataFlow.Dsd.PITReleaseDate ?? ((DateTime)SqlDateTime.MaxValue).AddSeconds(-1);
            //FIX for the NSI WS 8.1.0 when trying to find the PIT mapping set with max datetime
            if (releaseDate > ((DateTime)SqlDateTime.MaxValue).AddSeconds(-1))
                releaseDate = releaseDate.AddSeconds(-1);
            
            //Initialize versions for mappingSets
            switch (liveVersion)
            {
                //No data has been loaded
                case null when isPitEmpty:
                    liveVersion = (char)DbTableVersion.A;
                    pitVersion = (char)DbTableVersion.B;
                    break;
                //Only data has been loaded to the PIT version
                case null:
                    liveVersion = (char)DbTableVersions.GetNewTableVersion(
                        (DbTableVersion)DbTableVersions.GetDbTableVersion((char)pitVersion));
                    break;
                //Only data has been loaded to the Live version
                default:
                    pitVersion = (char)DbTableVersions.GetNewTableVersion(
                        (DbTableVersion)DbTableVersions.GetDbTableVersion((char)liveVersion));
                    break;
            }

            var emptyDataQuery = _unitOfWork.DataStoreRepository.GetEmptyDataViewQuery(dataFlow);
            var orderByAsc = _unitOfWork.DataStoreRepository.GetOrderByClause(dataFlow);
            var orderByDesc = _unitOfWork.DataStoreRepository.GetOrderByClause(dataFlow, false);

            //Manage mappingSet of Live version
            var liveDataQuery = isLiveEmpty ? emptyDataQuery : _unitOfWork.DataStoreRepository.GetDataViewQuery(dataFlow, (char)liveVersion);

            mappingStoreDataAccess.CreateOrUpdateMappingSet(_unitOfWork.DataSpace, dataFlow, (char)liveVersion,
                null, releaseDate, liveDataQuery, orderByAsc, orderByDesc);

            //Manage mappingSet of PIT version
            var pitDataQuery = isPitEmpty ? emptyDataQuery : _unitOfWork.DataStoreRepository.GetDataViewQuery(dataFlow, (char)pitVersion);

            mappingStoreDataAccess.CreateOrUpdateMappingSet(_unitOfWork.DataSpace, dataFlow, (char)pitVersion,
                releaseDate, null, pitDataQuery, orderByAsc, orderByDesc);

            // Metadata mapping sets
            if (dataFlow.Dsd.Msd != null)
            {
                var emptyMetaQuery = _unitOfWork.MetadataStoreRepository.GetEmptyMetadataViewQuery(dataFlow);
                var liveMetaQuery = isLiveEmpty ? emptyMetaQuery : _unitOfWork.MetadataStoreRepository.GetMetadataViewQuery(dataFlow, (char)liveVersion);

                mappingStoreDataAccess.CreateOrUpdateMappingSet(
                        _unitOfWork.DataSpace,
                        dataFlow,
                        (char)liveVersion,
                        validFrom: null,
                        validTo: releaseDate,
                        liveMetaQuery,
                        orderByTimeAsc: null,
                        orderByTimeDesc: null,
                        isMetadata: true
                    );

                // PIT version
                var pitMetaQuery = isPitEmpty ? emptyMetaQuery : _unitOfWork.MetadataStoreRepository.GetMetadataViewQuery(dataFlow, (char)pitVersion);

                mappingStoreDataAccess.CreateOrUpdateMappingSet(
                    _unitOfWork.DataSpace,
                    dataFlow,
                    (char)pitVersion,
                    validFrom: releaseDate,
                    validTo: null,
                    pitMetaQuery,
                    orderByTimeAsc: null,
                    orderByTimeDesc: null,
                    isMetadata: true
                );
            }
        }
        
        private void InitializeDataflowEmptyMappingsets(Dataflow dataFlow, IMappingStoreDataAccess mappingStoreDataAccess)
        {
            //Update all mappingSets both PIT and Live 
            if (mappingStoreDataAccess.HasUserCreatedMappingSets(_unitOfWork.DataSpace,
                dataFlow))
            {
                Log.Error(new DataFlowForDisseminationException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .DataFlowForDisseminationError), dataFlow.FullId)));
                return;
            }

            ManageDataflowMappingsets(dataFlow, mappingStoreDataAccess);
        }

        private async Task VerifyAndCreateDimensions(Dsd dsd, IList<ComponentItem> componentItems, bool factTableExists, CancellationToken cancellationToken)
        {
            var dimensionsInDb = componentItems.Where(ci => ci.IsDimension)
                .ToDictionary(ci => ci.Id, StringComparer.InvariantCultureIgnoreCase);

            var isTimeDimensionInDb = await _unitOfWork.ComponentRepository.CheckTimeDimensionOfDsdInDb(dsd, DbTableVersion.A, cancellationToken);

            if (factTableExists)
            {
                if (dsd.TimeDimension != null && !isTimeDimensionInDb)
                {
                    throw new ChangeInDsdException(dsd.FullId,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdTimeDimensionAdded)));
                }

                if (dsd.TimeDimension == null && isTimeDimensionInDb)
                {
                    throw new ChangeInDsdException(dsd.FullId,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdTimeDimensionRemoved)));
                }
            }

            foreach (var dim in dsd.Dimensions)
            {
                ComponentItem dimensionInManagementDb = null;

                dimensionsInDb.TryGetValue(dim.Code, out dimensionInManagementDb);

                dimensionsInDb.Remove(dim.Code);

                if (dim.Base.TimeDimension && dimensionInManagementDb != null && !dimensionInManagementDb.IsTimeDimension)
                {
                    throw new ChangeInDsdException(dsd.FullId,
                        string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdDimensionToTimeDimension), dim.FullId));
                }

                if (factTableExists && dimensionInManagementDb == null)
                {
                    throw new ChangeInDsdException(dsd.FullId,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdDimensionAdded), dim.FullId));
                }

                var dimType = SDMXArtefactType.TimeDimension;
                if (!dim.Base.TimeDimension)
                {
                    await _unitOfWork.ArtefactRepository.VerifyAndCreateCodelistOfComponent(dsd, dim, dimensionInManagementDb, factTableExists, cancellationToken);
                    dimType = SDMXArtefactType.Dimension;
                }

                
                if(dimensionInManagementDb?.DbId != null)
                    dim.DbId = dimensionInManagementDb.DbId;
                else
                {
                    componentItems.Add(
                        await _unitOfWork.ArtefactRepository.TryCreateDimensionComponent(dim, dimType,
                            cancellationToken));
                }
            }

            if (dimensionsInDb.Count > 0)
            {
                throw new ChangeInDsdException(dsd.FullId,
                    string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdDimensionRemoved),
                        string.Join(", ", dimensionsInDb.Select(d => d.Value.Id))));
            }
        }
        
        private async Task VerifyAndCreateAttributes(Dsd dsd, IList<ComponentItem> componentItems, bool factTableExists, CancellationToken cancellationToken)
        {
            var attributesInDb = componentItems.Where(ci => ci.IsAttribute)
                .ToDictionary(ci => ci.Id, StringComparer.InvariantCultureIgnoreCase);

            foreach (var attr in dsd.Attributes)
            {
                attributesInDb.TryGetValue(attr.Code, out var attributeInManagementDb);

                attributesInDb.Remove(attr.Code);

                if (factTableExists && attributeInManagementDb == null)
                {
                    throw new ChangeInDsdException(dsd.FullId,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdAttributeAdded), attr.FullId));
                }

                if (attributeInManagementDb != null)
                {
                    if (attributeInManagementDb.IsCoded ^ attr.Base.HasCodedRepresentation())
                    {
                        throw new ChangeInDsdException(dsd.FullId,
                            string.Format(
                                LocalizationRepository.GetLocalisedResource(
                                    Localization.ResourceId.ChangeInDsdAttributeCodedRepresentationChanged), attr.FullId,
                                attributeInManagementDb.IsCoded ? "" : "not", attr.Base.HasCodedRepresentation() ? "" : "not"));
                    }

                    if (attributeInManagementDb.IsMandatory ^ attr.Base.Mandatory)
                    {
                        throw new ChangeInDsdException(dsd.FullId, string.Format(
                            LocalizationRepository.GetLocalisedResource(
                                Localization.ResourceId.ChangeInDsdAttributeMandatoryStateChanged), attr.FullId,
                                attributeInManagementDb.AttributeStatus,
                            attr.Base.AssignmentStatus));
                    }

                    if (attributeInManagementDb.AttributeAssignmentlevel != attr.Base.AttachmentLevel)
                    {
                        throw new ChangeInDsdException(dsd.FullId, string.Format(
                            LocalizationRepository.GetLocalisedResource(
                                Localization.ResourceId.ChangeInDsdAttributeAttachmentLevelChanged), attr.FullId,
                                attributeInManagementDb.AttributeAssignmentlevel,
                            attr.Base.AttachmentLevel
                        ));
                    }

                    if (attributeInManagementDb.AttributeGroup != attr.Base.AttachmentGroup)
                    {
                        throw new ChangeInDsdException(dsd.FullId, string.Format(
                            LocalizationRepository.GetLocalisedResource(
                                Localization.ResourceId.ChangeInDsdAttributeAttachmentGroupChanged), attr.FullId,
                            attributeInManagementDb.AttributeGroup,
                            attr.Base.AttachmentGroup));
                    }
                }

                if (attr.Base.HasCodedRepresentation())
                {
                    await _unitOfWork.ArtefactRepository.VerifyAndCreateCodelistOfComponent(dsd, attr, attributeInManagementDb, factTableExists, cancellationToken);
                }

                if (attributeInManagementDb?.DbId != null)
                    attr.DbId = attributeInManagementDb.DbId;
                else
                    await _unitOfWork.AttributeRepository.CreateAttribute(attr, dsd, componentItems, cancellationToken);
            }

            if (attributesInDb.Count > 0)
            {
                throw new ChangeInDsdException(dsd.FullId, string.Format(LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.ChangeInDsdAttributeRemoved), string.Join(", ", attributesInDb.Select(a => a.Value.Id))));
            }
        }
        
        private async Task VerifyAndCreateMetadataAttributes(Dsd dsd, IList<MetadataAttributeItem> metadataAttributeItems, bool metaDataTableExists, CancellationToken cancellationToken)
        {
            var attributesInDb = metadataAttributeItems
                .ToDictionary(ci => ci.Id, StringComparer.InvariantCultureIgnoreCase);

            foreach (var attr in dsd.Msd.MetadataAttributes)
            {
                attr.Dsd = dsd;
                attributesInDb.TryGetValue(attr.HierarchicalId, out var attributeInManagementDb);

                attributesInDb.Remove(attr.HierarchicalId);

                if (metaDataTableExists && attributeInManagementDb == null)
                {
                    throw new ChangeInMsdException(dsd.Msd.FullId,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInMsdAttributeAdded), attr));
                }

                if (attributeInManagementDb != null)
                {
                    if (attributeInManagementDb.IsCoded ^ attr.Base.HasCodedRepresentation())
                    {
                        throw new ChangeInMsdException(dsd.Msd.FullId,
                            string.Format(
                                LocalizationRepository.GetLocalisedResource(
                                    Localization.ResourceId.ChangeInMsdAttributeCodedRepresentationChanged), attr.ToString(),
                                attributeInManagementDb.IsCoded ? "" : "not", attr.Base.HasCodedRepresentation() ? "" : "not"));
                    }
                }

                if (attr.Base.HasCodedRepresentation())
                {
                    await _unitOfWork.ArtefactRepository.VerifyAndCreateCodelistOfMetadataAttributesComponent(dsd.Msd, attr, attributeInManagementDb, metaDataTableExists, cancellationToken);
                }

                if (attributeInManagementDb?.DbId != null)
                    attr.DbId = attributeInManagementDb.DbId;
                else
                    await _unitOfWork.ArtefactRepository.CreateMetadataAttribute(attr, cancellationToken);
                
            }

            if (attributesInDb.Count > 0)
            {
                throw new ChangeInMsdException(dsd.Msd.FullId, string.Format(LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.ChangeInMsdAttributeRemoved), string.Join(", ", attributesInDb.Select(a => a.Value.Id))));
            }
        }

        private async Task VerifyAndUpdateDsdMsd(Dsd dsd, CancellationToken cancellationToken)
        {
            //current msd
            var msdDbId = await _unitOfWork.ArtefactRepository.GetDsdMsdDbId(dsd.DbId, cancellationToken);

            //No MSD linked to DSD
            if ((msdDbId == null || msdDbId <= 0) && dsd.Msd == null)
                return;

            //MSD not in DB or not linked to DSD in DB
            if (msdDbId == null || msdDbId <= 0)
            {
                //Get MSD dbId if exists otherwise create the MSD
                await _unitOfWork.ArtefactRepository.GetOrCreateMsd(dsd.Msd, cancellationToken);

                Log.Notice(string.Format(
                    LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.ChangeInDsdMsdAdded), dsd.Msd.FullId, dsd.FullId));

                //Link MSD to DSD in DB
                dsd.MsdDbId = dsd.Msd.DbId;
                await _unitOfWork.ArtefactRepository.UpdateMsdOfDsd(dsd.DbId, dsd.MsdDbId, cancellationToken);
            }
            else
            {
                var currentDsdMsdInDb = await _unitOfWork.ArtefactRepository.GetArtefactByDbId((int)msdDbId, cancellationToken);
                //MSD removed
                if (dsd.Msd == null)
                {
                    throw new ChangeInDsdException(dsd.FullId, string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdMsdRemoved),
                        currentDsdMsdInDb, dsd.FullId));
                }

                //MSD changed
                if (currentDsdMsdInDb.ToString() != dsd.Msd.FullId)
                {
                    throw new ChangeInDsdException(dsd.FullId, string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.ChangeInDsdMsdChanged), dsd.FullId, currentDsdMsdInDb,
                        dsd.Msd.FullId));
                }
                //Fill msd db internal ids
                dsd.Msd.DbId = currentDsdMsdInDb.DbId;
                dsd.MsdDbId = currentDsdMsdInDb.DbId;
            }
        }

        private DbTableVersion? GetDsdLiveVersion(ArtefactItem art)
        {
            if (art.PITVersion == null) //non existing PITVersion
            {
                return art.LiveVersion != null ? DbTableVersions.GetDbTableVersion((char)art.LiveVersion) : null;
            }

            if (art.PITReleaseDate == null) //existing PIT with no release date set
            {
                return art.LiveVersion != null ? DbTableVersions.GetDbTableVersion((char)art.LiveVersion) : null;
            }

            if (art.PITReleaseDate <= DateTime.Now) //release date passed
            {
                return art.PITVersion != null ? DbTableVersions.GetDbTableVersion((char)art.PITVersion) : null;
            }

            // release date still not passed
            return art.LiveVersion != null ? DbTableVersions.GetDbTableVersion((char)art.LiveVersion) : null;
        }

        private DbTableVersion? GetDsdPITVersion(ArtefactItem art)
        {
            if (art.PITVersion == null) //non existing PITVersion
            {
                return null;
            }

            if (art.PITReleaseDate == null) //existing PIT with no release date set
            {
                return DbTableVersions.GetDbTableVersion((char)art.PITVersion?.ToString()?[0]);
            }

            if (art.PITReleaseDate <= DateTime.Now) // release date passed
            {
                return null;
            }

            // release date still not passed
            return DbTableVersions.GetDbTableVersion((char)art.PITVersion?.ToString()?[0]);
        }

        #endregion
    }
}
