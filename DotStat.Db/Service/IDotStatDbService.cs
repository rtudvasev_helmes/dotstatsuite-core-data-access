﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Db.Dto;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Domain;
using DotStat.MappingStore;
using Transaction = DotStat.Domain.Transaction;

namespace DotStat.Db.Service
{
    public interface IDotStatDbService
    {
        Task<bool> TryNewTransaction(Transaction transaction, Dataflow dataflow, DotStatPrincipal principal, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken, bool onlyLockTransaction = false);

        Task<bool> TryNewTransactionWithNoDsd(Transaction transaction, CancellationToken cancellationToken, bool onlyLockTransaction = false);

        Task<bool> TryNewTransactionForCleanup(Transaction transaction, int dsdId, int? dsdChildDbId, string agency, string id, SdmxVersion version, CancellationToken cancellationToken, bool onlyLockTransaction = false);

        Task<bool> CloseTransaction(
            Transaction transaction,
            Dataflow dataflow,
            bool PITRestorationAllowed,
            bool calculateActualContentConstraint,
            IMappingStoreDataAccess mappingStoreDataAccess,
            ICodeTranslator codeTranslator,
            CancellationToken cancellationToken);

        Task CleanUpFailedTransaction(Transaction transaction);

        Task CloseCanceledOrTimedOutTransaction(int transactionId);

        Task<TransactionStatus> GetFinalTransactionStatus(int transactionId, CancellationToken cancellationToken);

        Task FillIdsFromDisseminationDb(Dataflow dataflow, CancellationToken cancellationToken);
        Task FillDbIdsAndCreateMissingDbObjects(Dataflow dataflow, CancellationToken cancellationToken, bool skipDsd = true);

        Task<bool> Rollback(Dataflow dataflow, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken);

        Task<bool> Restore(Dataflow dataflow, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken);

        Task ApplyPITRelease(Dataflow dataflow, bool isRestorationAllowed, IMappingStoreDataAccess mappingStoreDataAccess, ICodeTranslator codeTranslator);

        Task ManageMappingSets(Dsd dsd, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken);

        Task<bool> InitializeAllMappingSets(Transaction transaction, DotStatPrincipal principal, IMappingStoreDataAccess mappingStoreDataAccess, CancellationToken cancellationToken);

        Task<bool> CleanUpDsd(int dsdDbId, bool deleteMsdObjectsOnly, IMappingStoreDataAccess mappingStoreDataAccess, List<ComponentItem> allComponents, List<MetadataAttributeItem> allMetadataAttributeComponents, CancellationToken cancellationToken);

        DbTableVersion? GetDsdLiveVersion(Dsd dsd);
        DbTableVersion? GetDsdPITVersion(Dsd dsd);

        Task<ImportSummary> MergeStagingTable(Dataflow dataFlow, ReportedComponents reportedComponents,
            IList<DataSetAttributeRow> dataSetAttributeRows, CodeTranslator translator,
            DbTableVersion tableVersion, bool includeSummary, BulkImportResult bulkImportResult,
            CancellationToken cancellationToken);
    }
}
