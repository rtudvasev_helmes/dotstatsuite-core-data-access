﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using DbUp.Engine;
using DbUp.Engine.Output;
using DbUp.Engine.Transactions;
using DbUp.Support;

namespace DotStat.DbUp
{
    class DotstatConnectionManager : IConnectionManager
    {
        private readonly Regex noTransactionRegex = new Regex("ALTER DATABASE|BEGIN TRAN", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        readonly IConnectionFactory connectionFactory;
        DotstatTransactionStrategy transactionStrategy;
        IDbConnection upgradeConnection;
        private bool noTransaction;

        public DotstatConnectionManager(string connectionString) : this(new DelegateConnectionFactory((log) =>
        {
            var conn = new SqlConnection(connectionString);

            //if (IsScriptOutputLogged)
            //    conn.InfoMessage += (sender, e) => log.WriteInformation($"{{0}}", e.Message);

            return conn;
        }))
        {

        }

        /// <summary>
        /// Manages Database Connections
        /// </summary>
        DotstatConnectionManager(IConnectionFactory connectionFactory)
        {
            this.connectionFactory = connectionFactory;
        }

        /// <summary>
        /// Tells the connection manager is starting
        /// </summary>
        public IDisposable OperationStarting(IUpgradeLog upgradeLog, List<SqlScript> executedScripts)
        {
            upgradeConnection = CreateConnection(upgradeLog);
            
            if (upgradeConnection.State == ConnectionState.Closed)
                upgradeConnection.Open();

            transactionStrategy = new DotstatTransactionStrategy();
            transactionStrategy.Initialise(upgradeConnection, upgradeLog, executedScripts);

            return new DelegateDisposable(() =>
            {
                transactionStrategy.Dispose();
                upgradeConnection.Dispose();
                transactionStrategy = null;
                upgradeConnection = null;
            });
        }

        /// <summary>
        /// Executes an action using the specified transaction mode 
        /// </summary>
        /// <param name="action">The action to execute</param>
        public void ExecuteCommandsWithManagedConnection(Action<Func<IDbCommand>> action)
        {
            transactionStrategy.Execute(action, noTransaction);
        }

        /// <summary>
        /// Executes an action which has a result using the specified transaction mode 
        /// </summary>
        /// <param name="actionWithResult">The action to execute</param>
        /// <typeparam name="T">The result type</typeparam>
        /// <returns>The result of the command</returns>
        public T ExecuteCommandsWithManagedConnection<T>(Func<Func<IDbCommand>, T> actionWithResult)
        {
            return transactionStrategy.Execute(actionWithResult, noTransaction);
        }

        public bool TryConnect(IUpgradeLog upgradeLog, out string errorMessage)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The transaction strategy that DbUp should use
        /// </summary>
        public TransactionMode TransactionMode { get; set; }

        /// <summary>
        /// Specifies whether the db script output should be logged
        /// </summary>
        public bool IsScriptOutputLogged { get; set; }

        /// <summary>
        /// Splits a script into commands, for example SQL Server separates command by the GO statement
        /// </summary>
        /// <param name="scriptContents">The script</param>
        /// <returns>A list of SQL Commands</returns>
        public IEnumerable<string> SplitScriptIntoCommands(string scriptContents)
        {
            noTransaction = noTransactionRegex.IsMatch(scriptContents);

            var commandSplitter = new SqlCommandSplitter();
            var scriptStatements = commandSplitter.SplitScriptIntoCommands(scriptContents);
            return scriptStatements;
        }

        IDbConnection CreateConnection(IUpgradeLog upgradeLog)
        {
            return connectionFactory.CreateConnection(upgradeLog, null);
        }
    }

    class DotstatTransactionStrategy
    {
        IDbConnection connection;

        public void Execute(Action<Func<IDbCommand>> action, bool noTransaction)
        {
            if (noTransaction)
            {
                action(() => connection.CreateCommand());
            }
            else
            {
                using (var transaction = connection.BeginTransaction())
                {
                    action(() =>
                    {
                        var command = connection.CreateCommand();
                        command.Transaction = transaction;
                        return command;
                    });
                    transaction.Commit();
                }
            }
        }

        public T Execute<T>(Func<Func<IDbCommand>, T> actionWithResult, bool noTransaction)
        {
            if (noTransaction)
            {
                return actionWithResult(() => connection.CreateCommand());
            }
            else
            {
                using (var transaction = connection.BeginTransaction())
                {
                    var result = actionWithResult(() =>
                    {
                        var command = connection.CreateCommand();
                        command.Transaction = transaction;
                        return command;
                    });
                    transaction.Commit();
                    return result;
                }
            }
        }

        public void Initialise(IDbConnection dbConnection, IUpgradeLog upgradeLog, List<SqlScript> executedScripts)
        {
            connection = dbConnection ?? throw new ArgumentNullException(nameof(dbConnection));
        }

        public void Dispose() { }
    }

    class DelegateDisposable : IDisposable
    {
        readonly Action dispose;

        public DelegateDisposable(Action dispose)
        {
            this.dispose = dispose;
        }

        public void Dispose() => dispose();
    }
}
